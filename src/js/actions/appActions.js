var appDispatcher = require('../dispatchers/appDispatcher');


//
// if(process.env.NODE_ENV == "production")
// {
//   console.log("[appActions.js] process.env.NODE_ENV: "+process.env.NODE_ENV+ " Deshabilitando logs...")
// 	console.log = function(){};
// }


module.exports = {
	updateDestacados: function(data){
		console.warn('[appActions.js] ciclo vida 3 (updateDestacados) destacados: ' + data);

//var myJSON = JSON.stringify(data);
//console.log('DESTACADOS STRINGIFICACION:::::::::' + myJSON);

		var action = {
			actionType: "UPDATE_DESTACADOS",
			destacados: data
		};
		//console.warn('[appActions.js] updateDestacados ciclo vida 3 y medio, url: ' + action.destacados.data.items[0].component_type);
		appDispatcher.dispatch(action);
	},

	updateSubsections: function(data){

		var myJSON = JSON.stringify(data);
		// console.log('[appActions.js]  UPDATE_SUBSECTIONS:\n' + myJSON);
		// console.warn('[appActions.js] ciclo vida 3 (updateSubsections) subsections: ' + data);
		var action = {
			actionType: "UPDATE_SUBSECTIONS",
			subsections: data
		};
		//console.warn('[appActions.js] updateDestacados ciclo vida 3 y medio, url: ' + action.destacados.data.items[0].component_type);
		appDispatcher.dispatch(action);
	},

	updateSearcher: function(data){

		var myJSON = JSON.stringify(data);
		// console.log('[appActions.js]  UPDATE_SEARCHER:\n' + myJSON);
		 console.log('[appActions.js] ciclo updateSearcher: data.totalItems: ' + data.totalItems+' data.isLoading: ' + data.isLoading);
		var action = {
			actionType: "UPDATE_SEARCHER",
			isLoading:data.isLoading,
			searcher: data.searcher,
			totalItems: data.totalItems,
			pageOfItems: data.pageOfItems,
			currentPage: data.currentPage
			// currentPage: data.currentPage
		};
		//console.warn('[appActions.js] updateDestacados ciclo vida 3 y medio, url: ' + action.destacados.data.items[0].component_type);
		appDispatcher.dispatch(action);
	},

	updateSearchPagination: function(data){
		var myJSON = JSON.stringify(data);
		 console.log('[appActions.js]  UPDATE_SEARCHPAGINATION:\n' + myJSON);
		// console.warn('[appActions.js] ciclo vida 3 (updateSubsections) subsections: ' + data);
		var action = {
			actionType: "UPDATE_SEARCHPAGINATION",
			pager: data
		};
		//console.warn('[appActions.js] updateDestacados ciclo vida 3 y medio, url: ' + action.destacados.data.items[0].component_type);
		appDispatcher.dispatch(action);
	},

	updateAgenda: function(data){
		//console.warn('[appActions.js] ciclo vida 3 (updateDestacados) destacados: ' + data);
		var action = {
			actionType: "UPDATE_AGENDA",
			eventos: data
		};
		//console.warn('[appActions.js] updateDestacados ciclo vida 3 y medio, url: ' + action.destacados.data[0].link_urls.url);
		appDispatcher.dispatch(action);
	},
	updateRecomendaciones: function(data){
		console.log('[appActions.js] ciclo vida 3 (updateRecomendaciones) recomendados: ' + JSON.stringify(data));

//var myJSON = JSON.stringify(data);
//console.log('RECOMENDADOS STRINGIFICACION:::::::::' + myJSON);


		var action = {
			actionType: "UPDATE_RECOMENDACIONES",
			recomendaciones: data.recomendaciones,
			sDiccComponentes: data.sDiccComponentes
		};
		appDispatcher.dispatch(action);
	},
	updateSlides: function(data){
		var action = {
			actionType: "UPDATE_SLIDER",
			slides: data
		}
		//console.warn('[appActions.js] updateSlides url: ' + action.slides.data[0].multimedia.url);
		appDispatcher.dispatch(action);
	},
	updateItinerario: function(data){
		var action  ={
			actionType: "UPDATE_ITINERARIO",
			itinerario: data
		}
		// console.warn('[appActions.js] updateItinerario longitud: ' + action.itinerario.data[0].t_resources.rrtt_longitude);
		// console.warn('[appActions.js] updateItinerario latitud: ' + action.itinerario.data[0].t_resources.rrtt_latitude);
		appDispatcher.dispatch(action);
	},
	updateMapa: function(data){
		// console.log('[appActions.js] updateMapa: data' + JSON.stringify(data));
		// console.log('[appActions.js] updateMapa: data.latlon' + JSON.stringify(data.latlon));


		var action  ={
			actionType: "UPDATE_MAPA",
			map: data
		}
		// console.log('[appActions.js] action' + JSON.stringify(action));

		// console.warn('[appActions.js] updateItinerario longitud: ' + action.itinerario.data[0].t_resources.rrtt_longitude);
		// console.warn('[appActions.js] updateItinerario latitud: ' + action.itinerario.data[0].t_resources.rrtt_latitude);
		appDispatcher.dispatch(action);
	},
	updateArticulo: function(data){
		var action = {
			actionType: "UPDATE_ARTICULO",
			articulo: data
		}
//		console.warn('[appActions.js] updateArticulo url: ' + action.articulo.data.multimedia[0].url);
		appDispatcher.dispatch(action);
	},
	updateRrtt: function(data){
		var action = {
			actionType: "UPDATE_RRTT",
			recurso: data
		}
		console.warn('[appActions.js] updateRecurso url: ' + action.recurso.data.multimedia[0].url);
		appDispatcher.dispatch(action);
	},
	updateExperiencia: function(data){
		var action = {
			actionType: "UPDATE_EXPERIENCIA",
			experiencia: data
		}
		//console.warn('[appActions.js] updateExperiencia url: ' + action.experiencia.data.multimedia[0].url);
		appDispatcher.dispatch(action);
	},
	updateIsla: function(data){
		var action = {
			actionType: "UPDATE_ISLA",
			isla: data
		}
		console.warn('[appActions.js] updateIsla url: ' + action.isla.data.multimedia[0].url);
		appDispatcher.dispatch(action);
	},
	updatePlayas: function(data){
		var action = {
			actionType: "UPDATE_PLAYAS",
			playas: data
		}
		appDispatcher.dispatch(action);
	}
};
