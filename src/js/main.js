var React = require('react');
var ReactDOM = require('react-dom');

var idioma = '';
idioma = location.pathname.split('/')[1];
var isla = location.pathname.split('/')[2];
var rastroMigas = location.pathname;

var oDatosHeader = [];
oDatosHeader.sIdioma = idioma;
oDatosHeader.sIsla = isla;
oDatosHeader.sUrl = location.pathname;
oDatosHeader.aUrlsFriendly = [];

var MenuHeader = require('./components/MenuCabecera');
ReactDOM.render(<MenuHeader oDatosHeader={oDatosHeader}/>,document.getElementById('cabecera'));


try{
	if(oDestacados){
		console.log('[compIsla.js] oDestacados  existe')
		var Destacados = require('./components/Destacados');
		ReactDOM.render(<Destacados sUrl={oDestacados} sIdioma={idioma} sFiltro='false' sIsla={isla}/>,document.getElementById('destacados'));
	} else {
		console.log('[compIsla.js] oDestacados no existe')
	}
} catch(e){
	console.log('[compIsla.js] oDestacados ReactDOM.render. Error: '+e.message);
}

try{
	if(oRecomendados){
		console.log('[compIsla.js] oRecomendados  existe')
		var Recomendaciones = require('./components/Recomendaciones');
		var dicc_tipoComponentes_trans='';
		var componente = '';
		ReactDOM.render(<Recomendaciones  render='client' sUrl={oRecomendados} sIdioma={idioma} sPageType='Landing'/>,document.getElementById('recomendaciones'));
	} else {
		console.log('[compIsla.js] oRecomendados no existe')
	}
} catch(e){
	console.log('[compIsla.js] oRecomendados ReactDOM.render. Error: '+e.message);
}

try{
	if(oAgenda){
		console.log('[compIsla.js] oAgenda  existe')
		var Agenda = require('./components/Agenda');
		ReactDOM.render(<Agenda  render='client' sUrl={oAgenda} sIdioma={idioma}/>,document.getElementById('agenda'));
	} else {
		console.log('[compIsla.js] oAgenda no existe')
	}
} catch(e){
	console.log('[compIsla.js] oAgenda ReactDOM.render. Error: '+e.message);
}


//
// try{
// 	if(oLandingSlider){
// 		console.log('[compIsla.js] oLandingSlider  existe')
// 		var Slider = require('./components/Slider');
// 		var sId = 'rev_slider_104_1';
// 		ReactDOM.render(<Slider sUrl={oLandingSlider} sRastro={rastroMigas} sIdioma={idioma} sId={sId} />,document.getElementById('rev_slider_104_1_wrapper'));
// 	} else {
// 		console.log('[compIsla.js] oLandingSlider no existe')
// 	}
// } catch(e){
// 	console.log('[compIsla.js] oLandingSlider ReactDOM.render. Error: '+e.message);
// }


try{
	if(oSearcher){
		console.log('[compLanding.js] oSearcher  existe')

			var Searcher = require('./components/Searcher');
			// var SearchPagination = require('./components/SearchPagination');


			console.log('[compLanding.js] oSearcher  ReactDOM.render()');

			ReactDOM.render(<Searcher sUrl={oSearcher} sIdioma={idioma} searcherType="1"/>,document.getElementById('searcher'));
			// ReactDOM.render(<SearchPagination/>,document.getElementById('searchpagination'));
		} else {
			console.log('[compLanding.js] oSearcher no existe')
		}
} catch(e){
		console.log('[compLanding.js] oSearcher ReactDOM.render. Error: '+e.message);
}


try{
	if(oSearcherCompanies){
		console.log('[compLanding.js] oSearcherCompanies  existe')

			var Searcher = require('./components/Searcher');
			// var SearchPagination = require('./components/SearchPagination');


			console.log('[compLanding.js] oSearcherCompanies  ReactDOM.render()');

			ReactDOM.render(<Searcher sUrl={oSearcherCompanies} sIdioma={idioma} searcherType="2"/>,document.getElementById('searcher'));
			// ReactDOM.render(<SearchPagination/>,document.getElementById('searchpagination'));
		} else {
			console.log('[compLanding.js] oSearcherCompanies no existe')
		}
} catch(e){
		console.log('[compLanding.js] oSearcherCompanies ReactDOM.render. Error: '+e.message);
}


var Meteomini = require('./components/Meteo');

var latlon =[];
var meteourl = [];

latlon['mallorca']={lat:'39.551530',lon:'2.735623'};
latlon['menorca']={lat:'39.864176',lon:'4.221855'};
latlon['ibiza']={lat:'38.874843',lon:'1.371127'};
latlon['formentera']={lat:'38.703676',lon:'1.452522'};

function _meteourl(island){
	return "/components/meteoclim/latitude/"+latlon[island].lat+"/longitude/"+latlon[island].lon+"/touristic_subjects/naturaleza";
}

ReactDOM.render(<Meteomini render='client' meteotype='meteomini' url={_meteourl('mallorca')} sIdioma={idioma} sIsla='mallorca'  />,document.getElementById('mallorca-weather'));

ReactDOM.render(<Meteomini render='client' meteotype='meteomini' url={_meteourl('menorca')} sIdioma={idioma} sIsla='menorca'  />,document.getElementById('menorca-weather'));

ReactDOM.render(<Meteomini render='client' meteotype='meteomini' url={_meteourl('ibiza')} sIdioma={idioma} sIsla='ibiza'  />,document.getElementById('ibiza-weather'));

ReactDOM.render(<Meteomini render='client' meteotype='meteomini' url={_meteourl('formentera')} sIdioma={idioma} sIsla='formentera'  />,document.getElementById('formentera-weather'));
