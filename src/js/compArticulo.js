var React = require('react');
var ReactDOM = require('react-dom');

var idioma = '';
idioma = location.pathname.split('/')[2];
var isla = location.pathname.split('/')[3];
var rastroMigas = location.pathname;

var oDatosHeader = [];
oDatosHeader.sIdioma = idioma;
oDatosHeader.sIsla = isla;
oDatosHeader.sUrl = location.pathname;
oDatosHeader.aUrlsFriendly = JSON.parse((document.getElementById("aUrlsFriendlyClient").innerHTML).replace(/&#x27;/g,'"'));


var MenuHeader = require('./components/MenuCabecera');
ReactDOM.render(<MenuHeader oDatosHeader={oDatosHeader}/>,document.getElementById('cabecera'));

console.log('[compArticulo.js] ');



try{
	if(oRecomendados){
		console.log('[compArticulo.js] oRecomendados  existe')
		var Recomendaciones = require('./components/Recomendaciones');
		var dicc_tipoComponentes_trans='';
		var componente = '';
		ReactDOM.render(<Recomendaciones render='client' sUrl={oRecomendados} sIdioma={idioma} sDiccComponentes={dicc_tipoComponentes_trans} sPageType='Ficha'/>,document.getElementById('recomendaciones'));
	} else {
		console.log('[compArticulo.js] oRecomendados no existe')
	}
} catch(e){
	console.log('[compArticulo.js] oRecomendados ReactDOM.render. Error: '+e.message);
}



var Meteomini = require('./components/Meteo');

var latlon =[];
var meteourl = [];

latlon['mallorca']={lat:'39.551530',lon:'2.735623'};
latlon['menorca']={lat:'39.864176',lon:'4.221855'};
latlon['ibiza']={lat:'38.874843',lon:'1.371127'};
latlon['formentera']={lat:'38.703676',lon:'1.452522'};

function _meteourl(island){
	return "/components/meteoclim/latitude/"+latlon[island].lat+"/longitude/"+latlon[island].lon+"/touristic_subjects/naturaleza";
}

ReactDOM.render(<Meteomini render='client' meteotype='meteomini' url={_meteourl('mallorca')} sIdioma={idioma} sIsla='mallorca'  />,document.getElementById('mallorca-weather'));

ReactDOM.render(<Meteomini render='client' meteotype='meteomini' url={_meteourl('menorca')} sIdioma={idioma} sIsla='menorca'  />,document.getElementById('menorca-weather'));

ReactDOM.render(<Meteomini render='client' meteotype='meteomini' url={_meteourl('ibiza')} sIdioma={idioma} sIsla='ibiza'  />,document.getElementById('ibiza-weather'));

ReactDOM.render(<Meteomini render='client' meteotype='meteomini' url={_meteourl('formentera')} sIdioma={idioma} sIsla='formentera'  />,document.getElementById('formentera-weather'));
