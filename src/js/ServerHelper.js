var fs = require('fs'),
    http = require('http'),
    url = require('url');

const https = require('https');
const querystring = require('querystring');


var express = require('express');
var app = express();
var async = require('async');

var ReactDOMServer = require('react-dom/server');
var React = require('react');
var _ = require('lodash');


var dameTextoi18 = require('./webutils/utilidades.js').dameTextoi18;

/**********************************************************/
/***********    Fichero de propiedades    ****************/
/**********************************************************/
var PropertiesReader = require('properties-reader');
var properties = PropertiesReader('./src/resources/config.properties');
var _host = properties.get('Host');
var _port = properties.get('Port');
var proxyHost = properties.get('proxyHost');
var proxyPort = properties.get('proxyPort');
var proxyUser = properties.get('proxyUser');
var proxyPass = properties.get('proxyPass');
var urlComponenTypes = properties.get('componentTypes');
var urlComponenTypesTrans = properties.get('componentTypesTrans');
var urlIdTerms = properties.get('ids_terms');
var urlDictionaryWeb = properties.get('dictionaryWeb');

/***************************************************************/
/***********    Proxy-Authorization       **********************/
/***************************************************************/
var auth = 'Basic '+new Buffer(proxyUser+':'+proxyPass).toString('base64');
var options = {
  host: proxyHost,
  port: proxyPort,
  path: "http://"+_host+":"+_port+"/dictionary_of_webpages/",
  headers: {
    Host: _host,
    "Proxy-Authorization" : auth
  }
};
var oHost = {
  host: _host,
  port: _port
};

//
// if(process.env.NODE_ENV == "production")
// {
//   console.log("[ServerHelper.js] process.env.NODE_ENV: "+process.env.NODE_ENV+ " Deshabilitando logs...")
// 	console.log = function(){};
// }


var  dicc_tipoComponentes;
// Diccionario de datos: tipos de componentes
recuperarDiccionario(options, urlComponenTypes, function(componentes){
  dicc_tipoComponentes = componentes;
});

var  dicc_tipoComponentes_trans;

// Diccionario de datos: tipos de componentes traducidos
recuperarDiccionario(options, urlComponenTypesTrans, function(componentes_trans){
  dicc_tipoComponentes_trans = componentes_trans;
});


var dicc_paginas;

recuperarDiccionario(options, urlDictionaryWeb, function(dicc){
  dicc_paginas = dicc;
});



// Make sure to include the JSX transpiler
require('node-jsx').install();

//log4js.configure('./src/resources/log4js_configuracion.json', {reloadSecs: 900});

//var logger = log4js.getLogger('server-logger');

function esHome(sUrl){
  var rt = 'false';
  var sPath = '';


  if(sUrl.charAt(0)=='/'){
    sPath = sUrl.substring(1);
  }
  var oPath = (sPath.split('/'));
  if(oPath[1].toLowerCase() == 'baleares'){
    rt = 'true';
  }
  return rt;
}
/**
 * Obtiene los datos de los componentes que forman la plantila landig(tematica), para renderizarlo en servidor.
 * @param options
 * @param urlRequest Objeto request de la petición http.
 * @param htmlTemplate HTML de la plantilla correspondiente a la página de Landing.
 * @param oUrls Objeto que contiene las peticiones al REST para montar los componentes HTML de la plantilla.
 * @param callback función de vuelta.
*/
function componentesLanding(options,urlRequest,htmlTemplate,oUrls, callback){

console.log('[ServlerHelper.js] componentesLanding() ');
//logger.debug('componentesLanding...');

  var oPath = urlRequest.substring(1).split('/');
  var idioma = oPath[0];
  var isla = oPath[1];

  console.log('[ServlerHelper.js] componentesLanding(): isla '+isla);
  var sFiltro = esHome(String(urlRequest));

  var rt = [];
  var oDatosBreadCrumbs = [];
  var oDatosHeader = [];
  var oDatosSearchTitle = [];
  var oDatosLandingTitle = [];
  var oDatosSEO = []



  var LandingLevel = 1;

  var section = ''; //@TODO incluir id section diccionario

  console.log('[ServlerHelper.js] componentesLanding(): createFactory SliderLanding');

  var Slider = require('./components/SliderLanding');
  var slider = React.createFactory(Slider);

  console.log('[ServlerHelper.js]componentesLanding(): createFactory  Recomendaciones');


  var Recomendado = require('./components/Recomendaciones');
  var recomendado = React.createFactory(Recomendado);


  console.log('[ServlerHelper.js]componentesLanding(): createFactory  Agenda');

  var Agenda = require('./components/Agenda');
  var agenda = React.createFactory(Agenda);

  console.log('[ServlerHelper.js] componentesLanding(): createFactory MenuCabecera');

  var MenuHeader = require('./components/MenuCabecera');
  var menuheader = React.createFactory(MenuHeader);

  console.log('[ServlerHelper.js] componentesLanding(): createFactory BreadCrumbs');


  var BreadCrumbs = require('./components/BreadCrumbs');
  var breadcrumbs = React.createFactory(BreadCrumbs);


  var Destacado = require('./components/Destacados');
  var destacado = React.createFactory(Destacado);

  var Subsections = require('./components/Subsections');
  var subsections = React.createFactory(Subsections);


  var LandingTitle = require('./components/LandingTitle');
  var landingtitle = React.createFactory(LandingTitle);


  var SearchTitle = require('./components/SearchTitle');
  var searchtitle = React.createFactory(SearchTitle);


   console.log('[ServlerHelper.js] empezamos async.parallel(). oUrls ... idioma: '+idioma);

   console.log('[ServlerHelper.js] empezamos async.parallel() oUrls.slider:'+oUrls.slider);
   console.log('[ServlerHelper.js] empezamos async.parallel() oUrls.destacado:'+oUrls.destacado);
   console.log('[ServlerHelper.js] empezamos async.parallel() oUrls.agenda:'+oUrls.agenda);
   console.log('[ServlerHelper.js] empezamos async.parallel() oUrls.recomendado:'+oUrls.recomendado);
   console.log('[ServlerHelper.js] empezamos async.parallel() oUrls.subsections:'+oUrls.subsections);
   console.log('[ServlerHelper.js] empezamos async.parallel() oUrls.searcher:'+oUrls.searcher);
   console.log('[ServlerHelper.js] empezamos async.parallel() oUrls.searcher_companies:'+oUrls.searcher_companies);
   console.log('[ServlerHelper.js] empezamos async.parallel() oUrls.searcher_agenda :'+oUrls.searcher_agenda);
   console.log('[ServlerHelper.js] empezamos async.parallel() oUrls.section :'+oUrls.section);



   console.log('[ServlerHelper.js] empezamos async.parallel() oUrls.breadcrumb:'+oUrls.breadcrumb);

  /* CARGA SINCRONA DE LOS COMPONENTES DE LAS LANDING*/
  async.parallel([

    function(callback){
      if (oUrls.slider==undefined) {rt.slider = ''; callback(null,rt.slider);}
      cargarComponente(oUrls.slider,function(comp){
        try{

          console.log('[ServlerHelper.js] componentesLanding() cargarComponente: slider');

          oDatosSearchTitle.sTitle = dameTextoi18(comp.items[0].landing_title,idioma);;
          oDatosLandingTitle.sTitle = dameTextoi18(comp.items[0].landing_title,idioma);;
          oDatosLandingTitle.sSubTitle = dameTextoi18(comp.items[0].subtitle,idioma);;
          oDatosLandingTitle.sText = dameTextoi18(comp.items[0].landing_text,idioma);;

          oDatosSEO.seotitle = comp.items[0].landing_title[idioma];




            var sid = 'rev_slider_104_1';
            rt.slider = ReactDOMServer.renderToString(slider({componente: comp, sId:sid,  filtro: sFiltro, sIdioma: idioma, sRastro: urlRequest}));
        } catch(e){
          console.log('[ServlerHelper.js] componentesLanding() async.parallel slider (Error) : ' + e.message);
        }
        callback(null,rt.slider);
      });
    },
    function(callback){
      if (oUrls.destacado==undefined) {rt.destacado = ''; callback(null,rt.destacado);}
      try{
        oUrls.destacado = oUrls.destacado.replace(':island', isla=='baleares'?'--all--':isla);
        oUrls.destacado = oUrls.destacado.replace(':limit?', 'all');
      } catch(e){
        console.log('[ServlerHelper.js] componentesLanding() async.parallel destacado (Error) : ' + e.message);
      }

      cargarComponente(oUrls.destacado,function(comp){
        console.log('[ServlerHelper.js] componentesLanding() cargarComponente: destacado');
        try{


          rt.destacado = ReactDOMServer.renderToString(destacado({componente: comp, filtro: sFiltro, sIdioma: idioma,sLandingLevel: LandingLevel, section:oUrls.section, sDiccComponentes: dicc_tipoComponentes_trans}));
        } catch(e){
          console.log('[ServlerHelper.js] componentesLanding() async.parallel destacado (Error) : ' + e.message);
        }
        callback(null,rt.destacado);
      });
    },
    function(callback){
      if (oUrls.agenda==undefined) {rt.agenda = ''; callback(null,rt.agenda);}
      cargarComponente(oUrls.agenda,function(comp){
        console.log('[ServlerHelper.js] componentesLanding() cargarComponente: agenda');

        try{
          rt.agenda = ReactDOMServer.renderToString(agenda({render: 'server', componente: comp, sIdioma: idioma}));
        } catch(e){
          console.log('[ServlerHelper.js] componentesLanding() async.parallel agenda (Error) : ' + e.message);
        }
        callback(null,rt.agenda);
      });
    },
    function(callback){
      if (oUrls.recomendado==undefined) {rt.recomendado = ''; callback(null,rt.recomendado);}

      cargarComponente(oUrls.recomendado,function(comp){

        console.log('[ServlerHelper.js] componentesLanding() cargarComponente: recomendado');

        // Server Side.
        try{
          var pagetype = 'Landing';
          rt.recomendado = ReactDOMServer.renderToString(recomendado({render:'server', componente: comp, sIdioma: idioma, sPageType: pagetype, sDiccComponentes: dicc_tipoComponentes_trans}));
        } catch(e){
          console.log('[ServlerHelper.js] componentesLanding() async.parallel recomendado (Error) : ' + e.message);
        }
        callback(null,rt.recomendado);
      });
    },
    function(callback){
        try{
            oDatosHeader.sIdioma = idioma;
            oDatosHeader.sIsla = isla;
            oDatosHeader.sUrl = urlRequest;
            oDatosHeader.sPageType ="Landing";
            oDatosHeader.aIdFriendly ="";
            oDatosHeader.aIdFriendly = oUrls.section; //
            oDatosHeader.aUrlsFriendly = setUrlsFriendly(oDatosHeader.aIdFriendly,isla,oDatosHeader.sPageType);

            console.log('[ServlerHelper.js] componentesLanding() preparando el menuheader. 3  '+JSON.stringify(oDatosHeader.aUrlsFriendly));
            rt.menuheader =  ReactDOMServer.renderToString(menuheader({oDatosHeader: oDatosHeader}));

        } catch(e){
          console.log('[ServlerHelper.js] componentesLanding() async.parallel menuheader (Error) : ' + e.message);
        }
        callback(null,rt.menuheader);
    },
    function(callback){
        try{

            oDatosBreadCrumbs.sIdioma = idioma;
            oDatosBreadCrumbs.sIsla = isla;
            oDatosBreadCrumbs.sUrl = urlRequest;
            oDatosBreadCrumbs.breadcrumb = oUrls.breadcrumb;

            rt.breadcrumbs =  ReactDOMServer.renderToString(breadcrumbs({oDatosBreadCrumbs: oDatosBreadCrumbs}));
        } catch(e){
          console.log('[ServlerHelper.js] componentesLanding() async.parallel breadcrumbs (Error) : ' + e.message);
        }
        callback(null,rt.breadcrumbs);
    },
    function(callback){
      if (oUrls.subsections==undefined) {rt.subsections = ''; callback(null,rt.subsections);}
      cargarComponente(oUrls.subsections,function(comp){

        console.log('[ServlerHelper.js] componentesLanding() cargarComponente: subsections');

        try{
           rt.subsections = ReactDOMServer.renderToString(subsections({componente: comp, filtro: sFiltro, sIdioma: idioma,sSection: section, sLandingLevel: LandingLevel}));
        } catch(e){
          console.log('[ServlerHelper.js] componentesLanding() async.parallel subsections (Error) : ' + e.message);
        }
        callback(null,rt.subsections);
      });
    }
  ],
  function(err,results){
      try{
            var rt = [];
            rt.slider = results[0];
            rt.destacado = results[1];
            rt.agenda = results[2];
            rt.recomendado = results[3];
            rt.menuheader = results[4];
            rt.breadcrumbs = results[5];
            rt.subsections = results[6];

              // Landing Title
              try{
                  if (oDatosLandingTitle.sTitle!=undefined) {

                      oDatosLandingTitle.sIdioma = idioma;
                      oDatosLandingTitle.sIsla = isla;
                      oDatosLandingTitle.sUrl = urlRequest;
                      rt.landingtitle = ReactDOMServer.renderToString(landingtitle({oDatosLandingTitle: oDatosLandingTitle}));

                  }
                  else{
                      rt.landingtitle = '';
                  }

                } catch(e){
                  console.log('[ServlerHelper.js] componentesLanding() async.parallel landingtitle (Error) : ' + e.message);
                }


                // Search Title
                try{
                    console.log('[ServerHelper.js] oUrls.searcher: '+oUrls.searcher+' oUrls.searcher_companies: '+oUrls.searcher_companies+' oUrls.searcher_agenda: '+oUrls.searcher_agenda);
                    if (oUrls.searcher!=undefined||oUrls.searcher_companies!=undefined||oUrls.searcher_agenda!=undefined ) {
                      oDatosSearchTitle.sIdioma = idioma;
                      oDatosSearchTitle.sIsla = isla;
                      oDatosSearchTitle.sUrl = urlRequest;
                      oDatosSearchTitle.section = oUrls.section;

                      // oDatosSearchTitle.sColor =
                      console.log('[ServlerHelper.js] componentesLanding() rt.searchtitle oDatosSearchTitle.sTitle : ' + oDatosSearchTitle.sTitle);
                      rt.searchtitle = ReactDOMServer.renderToString(searchtitle({oDatosSearchTitle: oDatosSearchTitle}));
                    }
                    else{
                        console.log('[ServerHelper.js] oUrls.searcher is undefined');
                        rt.searchtitle = '';
                    }

                  } catch(e){
                    console.log('[ServlerHelper.js] componentesLanding() async.parallel searchtitle (Error) : ' + e.message);
                  }


                  try{

                    rt.seotitle = oDatosSEO.seotitle
                    rt.seodescription =  oDatosSEO.seodescription;

                  } catch(e){
                    console.log('[ServlerHelper.js] componentesLanding() async.parallel oDatosSEO (Error) : ' + e.message);
                  }




      } catch(e){
        console.log('[ServerHelper.js] ERR RENDER SERVER  ' + e.message);
      }
      callback(rt);
  });
}
exports.componentesLanding = componentesLanding;


/**
 * Obtiene los datos de los componentes que forman la plantila landig(tematica), para renderizarlo en servidor.
 * @param options
 * @param urlRequest Objeto request de la petición http.
 * @param htmlTemplate HTML de la plantilla correspondiente a la página de Landing.
 * @param oUrls Objeto que contiene las peticiones al REST para montar los componentes HTML de la plantilla.
 * @param callback función de vuelta.
*/
function componentesFicha(options,urlRequest,oTipo,oUrls, callback){

  var oPath = urlRequest.substring(1).split('/');
  var idioma = oPath[1];
  var isla = oPath[2];
  var sFiltro = esHome(String(urlRequest));


  console.log('[ServlerHelper.js] componentesFicha() urlRequest: '+urlRequest);
  console.log('[ServlerHelper.js] componentesFicha() idioma: '+idioma);


  var rt = [];


  // Header
  var MenuHeader = require('./components/MenuCabecera');
  var menuheader = React.createFactory(MenuHeader);

  // Slider
  var Slider = require('./components/SliderFicha');
  var slider = React.createFactory(Slider);

  // BreadCrumbs
  var BreadCrumbs = require('./components/BreadCrumbs');
  var breadcrumbs = React.createFactory(BreadCrumbs);

  // Body Content
  var RRTT = require('./components/RRTT');
  var rrtt = React.createFactory(RRTT);

  var Articulo = require('./components/Articulo');
  var articulo = React.createFactory(Articulo);

  // Recomended
  var Recomendado = require('./components/Recomendaciones');
  var recomendado = React.createFactory(Recomendado);

  // Recomended
  var SEO = require('./components/SEO');
  var seo = React.createFactory(SEO);


  // console.log('[ServlerHelper.js] componentesFicha() CargarComponente Mapa...');
  //
  //
  //   // Map
  //   var Mapa = require('./components/Mapa');
  //   var mapa = React.createFactory(Mapa);
  // console.log('[ServlerHelper.js] componentesFicha() CargarComponente Mapa...');
  //


  // Datos obtenidos del Json de la ficha.
  var oDatosSlider = []
  var oDatosBreadCrumbs = [];
  var oDatosHeader = [];
  var oDatosMap = [];
  var oDatosMeteo = []
  var oDatosSEO = []



  /* CARGA SINCRONA DE LOS COMPONENTES DE LAS LANDING*/
  async.series([
    function(callback){
      cargarComponente(oUrls.ficha,function(comp){
        try{
            console.log('[ServlerHelper.js] componentesFicha() CargarComponente de Tipo '+oTipo.tipo);

            if(oTipo.tipo == 'rrtt'){
              console.log('[ServlerHelper.js] componentesFicha() idioma: '+idioma);

              rt.ficha = ReactDOMServer.renderToString(rrtt({componente: comp, sIdioma: idioma, pagetype: oTipo.tipo}));
              // Las imágenes del Slider vienen en el JSON de la ficha.
              oDatosSlider.multimedia = comp.multimedia;
              oDatosSlider.sTerm = dameTextoi18(comp.term.lang,idioma);
              oDatosSlider.sTitle = comp.title;
              oDatosBreadCrumbs.sTitle = comp.title;
              oDatosBreadCrumbs.sTerm =  dameTextoi18(comp.term.lang,idioma);
              oDatosMap.latlon = {'latitude': comp.latitude,'longitude': comp.longitude};
              oDatosMap.kml = _.filter(comp.multimedia,function (o) {return _.endsWith(o.url,'.kml')});

              oDatosSEO.seotitle = comp.term.lang[idioma]+' '+comp.title;
              oDatosSEO.seodescription = comp.opening_paragraphs[idioma];


              try{
                console.log("[ServlerHelper.js] oDatosMeteo...");
                oDatosMeteo.sIdioma = idioma;
                oDatosMeteo.sIsla = isla;
                var lon = comp.longitude;
                var lat = comp.latitude;
                oDatosMeteo.lat = lat;
                oDatosMeteo.lon = lon;
                oDatosMeteo.touristic_subject = comp.touristic_subjects.id[0];
                oDatosMeteo.municipalities = comp.municipalities;
                oDatosMeteo.oLifeStyles = comp.lifestyles;
              }catch(e){
                console.log("[ServlerHelper.js] Render oDatosMeteo (Error)..."+e.message);
              }
            }
            else if (oTipo.tipo == 'art'){
              rt.ficha = ReactDOMServer.renderToString(articulo({componente: comp, sIdioma: idioma, pagetype: oTipo.tipo}));
              oDatosSlider.multimedia = comp.multimedia;
              oDatosSlider.sTitle = dameTextoi18(comp.title,idioma);
              oDatosSlider.sSubtitle = comp.subtitle;
              oDatosBreadCrumbs.sTitle = dameTextoi18(comp.title,idioma);
              oDatosBreadCrumbs.sTerm = '';
              oDatosSEO.seotitle = comp.title[idioma];
              oDatosSEO.seodescription = comp.opening_paragraphs[idioma];


            }
            else if (oTipo.tipo == 'iti'){
              rt.ficha = ReactDOMServer.renderToString(articulo({componente: comp, sIdioma: idioma, pagetype: oTipo.tipo}));
              // console.log('[ServlerHelper.js] oDatosMap(), comp.t_resources: ' + JSON.stringify(comp.t_resources));
              console.log('[ServlerHelper.js] componentesFicha(), comp.title: ' + JSON.stringify(comp.title));

              oDatosMap.itinerario = comp.t_resources;
              oDatosMap.sTitle = comp.title[idioma];
              oDatosBreadCrumbs.sTitle = comp.title[idioma];
              oDatosBreadCrumbs.sTerm = '';
              oDatosSEO.seotitle = comp.title[idioma];
              oDatosSEO.seodescription = comp.opening_paragraphs[idioma];


              console.log('[ServlerHelper.js] componentesFicha(), oDatosBreadCrumbs.sTitle: ' + oDatosBreadCrumbs.sTitle);

            }
            else if (oTipo.tipo == 'exp'){
              rt.ficha = ReactDOMServer.renderToString(articulo({componente: comp, sIdioma: idioma, pagetype: oTipo.tipo}));
              oDatosSlider.multimedia = comp.multimedia;
              oDatosSlider.sTitle = dameTextoi18(comp.title,idioma);
              oDatosSlider.sSubtitle = comp.subtitle;
              oDatosBreadCrumbs.sTitle = dameTextoi18(comp.title,idioma);
              oDatosBreadCrumbs.sTerm = '';
              oDatosSEO.seotitle = comp.title[idioma];
              oDatosSEO.seodescription = _.truncate(comp.description[idioma],{length:157});


            }
            else if (oTipo.tipo == 'pla'){
              rt.ficha = ReactDOMServer.renderToString(articulo({componente: comp, sIdioma: idioma, pagetype: oTipo.tipo}));
              oDatosSlider.multimedia = comp.multimedia;
              oDatosSlider.sTitle = dameTextoi18(comp.title,idioma);
              oDatosSlider.sSubtitle = comp.subtitle;
              oDatosBreadCrumbs.sTitle = dameTextoi18(comp.title,idioma);
              oDatosBreadCrumbs.sTerm = '';
              oDatosSEO.seotitle = comp.title[idioma];
              oDatosSEO.seodescription = comp.title[idioma] + " " +comp.subtitle[idioma];

            }




              // Datos Para Cabecera.
              oDatosHeader.sPageType = comp.component_type;
              oDatosHeader.aIdFriendly = comp.id_friendly; //Array

        } catch(e){
          console.log('[ServlerHelper.js] componentesFicha(), async() Error (rt.ficha): ' + e.message);
        }
        callback(null,rt.ficha);
      });
    },
   function(callback){

      cargarComponente(oUrls.recomendado,function(comp){
        try{
          var pagetype = 'Ficha';
          rt.recomendado =  ReactDOMServer.renderToString(recomendado({render: 'server', componente: comp, sIdioma: idioma, sPageType: pagetype,sDiccComponentes: dicc_tipoComponentes_trans}));
        } catch(e){
          console.log('[ServlerHelper.js] componentesFicha(), async() Error (rt.recomendado): ' + e.message);
        }
        callback(null,rt.recomendado);
      });
    },
    function(callback){
        try{
          if (oTipo.tipo == 'iti'){rt.slider = '';}
          else{
            rt.slider = ReactDOMServer.renderToString(slider({oSlides: oDatosSlider, sIdioma: idioma}));
          }
        } catch(e){
          console.log('[ServlerHelper.js] componentesFicha(), async() Error (rt.slider): ' + e.message);
        }
        callback(null,rt.slider);
     },
    function(callback){
        try{
            oDatosHeader.sIdioma = idioma;
            oDatosHeader.sIsla = isla;
            oDatosHeader.sUrl = urlRequest;

            oDatosHeader.aUrlsFriendly = setUrlsFriendly(oDatosHeader.aIdFriendly,isla,oDatosHeader.sPageType);

            console.log('[ServlerHelper.js] componentesFicha() preparando el menuheader.  '+JSON.stringify(oDatosHeader.aUrlsFriendly));
            rt.menuheader =  ReactDOMServer.renderToString(menuheader({oDatosHeader: oDatosHeader}));

        } catch(e){
          console.log('[ServlerHelper.js] componentesFicha(), async() Error (rt.menuheader): ' + e.message);
        }
        callback(null,rt.menuheader);
    },
    function(callback){
        try{
            oDatosBreadCrumbs.sIdioma = idioma;
            oDatosBreadCrumbs.sIsla = isla;
            oDatosBreadCrumbs.sUrl = urlRequest;

            console.log('[ServlerHelper.js] componentesFicha() preparando el breadcrumbs.  '+oDatosBreadCrumbs.sTitle);

            rt.breadcrumbs =  ReactDOMServer.renderToStaticMarkup(breadcrumbs({oDatosBreadCrumbs: oDatosBreadCrumbs, pagetype: 'Ficha'}));
        } catch(e){
          console.log('[ServlerHelper.js]  componentesFicha(), async() Error (rt.breadcrumbs): ' + e.message);
        }
        callback(null,rt.breadcrumbs);
    },
    function(callback){
        try{
              if (oTipo.tipo != 'iti'&&oTipo.tipo != 'rrtt'){rt.mapa = '';  callback(null,rt.mapa);}
              rt.mapa = oDatosMap;
        } catch(e){
          console.log('[ServlerHelper.js] componentesFicha(), async() Error (rt.mapa): ' + e.message);
        }
        callback(null,rt.mapa);
     },
     function(callback){
         try{
               if (oTipo.tipo != 'rrtt'){rt.meteo = '';  callback(null,rt.meteo);}
               rt.meteo = oDatosMeteo;
         } catch(e){
           console.log('[ServlerHelper.js] componentesFicha(), async() Error (rt.meteo): ' + e.message);
         }
         callback(null,rt.meteo);
      },
     function(callback){
          try{
                // if (oTipo.tipo != 'rrtt'){rt.SEO = '';  callback(null,rt.SEO);}
                oDatosSEO.sIdioma = idioma;
                oDatosSEO.sIsla = isla;
                oDatosSEO.seotype = 'title';

                //rt.seotitle =  ReactDOMServer.renderToStaticMarkup(seo({oDatosSEO: oDatosSEO}));

                rt.seotitle = oDatosSEO.seotitle


          } catch(e){
            console.log('[ServlerHelper.js] componentesFicha(), async() Error (rt.SEOTitle): ' + e.message);
          }
          callback(null,rt.seotitle);
       },
       function(callback){
            try{
                  // if (oTipo.tipo != 'rrtt'){rt.SEO = '';  callback(null,rt.SEO);}
                  rt.seodescription =  oDatosSEO.seodescription;
            } catch(e){
              console.log('[ServlerHelper.js] componentesFicha(), async() Error (rt.seodescription): ' + e.message);
            }
            callback(null,rt.seodescription);
         }

  ],
  function(err,results){
    var rt = [];
    rt.ficha = results[0];
    rt.recomendado = results[1];
    rt.slider = results[2];
    rt.menuheader = results[3];
    rt.breadcrumbs = results[4];
    rt.mapa = results[5];
    rt.meteo = results[6];
    rt.seotitle = results[7];
    rt.seodescription = results[8];





    callback(rt);
  });
}
exports.componentesFicha = componentesFicha;
/**
* Recupera el contenido de un componente invocando al servicio REST y deserializa el resultado.
* @param options Cabecera de la request.
* @param urlComponente petición REST para obtner el contenido del componente.
* @return invoca a la función de callback con el json deserializado.
*/
/***************************************************************************************************/
var cargarComponente = function(urlComponente,callback){
  if(urlComponente != null){

          options.path = urlComponente;


          http.get(options, (res) => {
            const statusCode = res.statusCode;
            const contentType = res.headers['content-type'];
            let error;
            if (statusCode !== 200) {
              error = new Error('Request Failed.\n' +
                                'Status Code: {statusCode}');
            } else if (!/^application\/json/.test(contentType)) {
              error = new Error('Invalid content-type.\n' +
                                'Expected application/json but received ${contentType}');
            }
            if (error) {
              console.error(error.message +' , '+ options.path);
              res.resume();
              return;
            }
            res.setEncoding('utf8');
            let rawData = '';
            res.on('data', (chunk) => rawData += chunk);
            res.on('end', () => {
              try {
        //logger.debug("cargarComponente urlComponente " + urlComponente);
                let parsedData = JSON.parse(rawData);
                console.log('[ServerHelper.js] cargarComponente() JSON raw de:  '+urlComponente+' \n'+rawData);
        //logger.debug("JSON RAW : " + rawData);
                callback(parsedData);
              } catch (e) {
                //logger.debug(e.message);
              }
            });
          }).on('error', (e) => {
            //logger.debug('Got error: ' + e.message);
          });
  }
}

exports.cargarComponente = cargarComponente;
//
// var do_https_post = function(JSONData,options,callback){
//
//     var request = require('request');
//
//     var options = {
//       uri: 'https://www.googleapis.com/urlshortener/v1/url',
//       method: 'POST',
//       json: {
//         "longUrl": "http://www.google.com/"
//       }
//     };
//
// }
// exports.do_https_post = do_https_post;



var do_http_post = function(JSONData,options,callback){

      var req = http.request(options, function(res) {
        console.log('[ServlerHelper.js] do_http_post() Status: ' + res.statusCode);
        console.log('[ServlerHelper.js] do_http_post() Headers: ' + JSON.stringify(res.headers));


        let rawData = '';
        res.setEncoding('utf8');
        res.on('data', (chunk) => rawData += chunk);

        res.on('end', () => {

          try {
            console.log('[ServerHelper.js] do_http_post() response (rawData) from:  '+options.path+' \n'+rawData);
            let parsedData = JSON.parse(rawData);
            callback(parsedData);
          } catch (e) {
            //logger.debug(e.message);
          }

        });


      });

      req.on('error', function(e) {
        console.log('[ServlerHelper.js] do_http_post() problem with request: ' + e.message);
      });

      // write data to request body
      req.write(JSON.stringify(JSONData));

      req.end();

}

exports.do_http_post = do_https_post;

var do_https_post = function(JSONData,options,callback){

      var req = https.request(options, function(res) {
        console.log('[ServlerHelper.js] do_https_post() Status: ' + res.statusCode);
        console.log('[ServlerHelper.js] do_https_post() Headers: ' + JSON.stringify(res.headers));


        let rawData = '';
        res.setEncoding('utf8');
        res.on('data', (chunk) => rawData += chunk);

        res.on('end', () => {

          try {
            console.log('[ServerHelper.js] do_https_post() response (rawData) from:  '+options.path+' \n'+rawData);
            let parsedData = JSON.parse(rawData);
            callback(parsedData);
          } catch (e) {
            //logger.debug(e.message);
          }

        });


      });

      req.on('error', function(e) {
        console.log('[ServlerHelper.js] do_https_post() problem with request: ' + e.message);
      });

      // write data to request body
      req.write(JSON.stringify(JSONData));

      req.end();

}

exports.do_https_post = do_https_post;
//
// var do_https_post = function(JSONData,options,callback){
//   if(options.path != null){
//
//
//           const postData = JSON.stringify(JSONData);
//
//           // const postData = JSON.stringify(JSONData);
//
//           options.method = "POST";
//           options.headers['Content-Type']=  'application/json';
//           options.headers['Content-Length']= postData.length;
//           // options.form=JSONData;
//           // options.body='{body:'+postData+'}';
//
//
//
//           console.log('[ServlerHelper.js] do_https_post()  options: '+JSON.stringify(options));
//           console.log('[ServlerHelper.js] do_https_post()  postData: '+postData);
//
//           const req = https.request(options, (res) => {
//           // https.request(options, (res) => {
//             const statusCode = res.statusCode;
//             const contentType = res.headers['content-type'];
//             let error;
//
//             console.log('[ServerHelper.js] do_https_post Status: ' + res.statusCode);
//             console.log('[ServerHelper.js] do_https_post Headers: ' + JSON.stringify(res.headers));
//
//
//             if (statusCode !== 200) {
//               error = new Error('Request Failed.\n' +
//                                 'Status Code: {statusCode}');
//             }
//             else if (!/^application\/json/.test(contentType)) {
//               error = new Error('Invalid content-type.\n' +
//                                 'Expected application/json but received ${contentType}');
//             }
//
//
//
//             if (error) {
//               console.error("[ServerHelper.js] do_https_post (Error):"+error.message +' , '+ options.path);
//               res.resume();
//               return;
//             }
//
//             let rawData = '';
//             res.setEncoding('utf8');
//             res.on('data', (chunk) => rawData += chunk);
//             res.on('end', () => {
//               try {
//         //logger.debug("cargarComponente urlComponente " + urlComponente);
//                 let parsedData = JSON.parse(rawData);
//                 console.log('[ServerHelper.js] do_https_post() JSON raw de:  '+options.path+' \n'+rawData);
//         //logger.debug("JSON RAW : " + rawData);
//                 callback(parsedData);
//               } catch (e) {
//                 //logger.debug(e.message);
//               }
//             });
//           }).on('error', (e) => {
//             console.log('[ServerHelper.js] do_https_post() (Error):  '+options.path+' \n'+e.message);
//             //logger.debug('Got error: ' + e.message);
//           });
//
//           // const postData = querystring.stringify({
//           //   'msg': 'Hello World!'
//           // });
//
//           req.write(postData);
//           req.end();
//
//   }
// }
//
// exports.do_https_post = do_https_post;

//
//
// var do_http_post = function(postData,options,callback){
//   if(options.path != null){
//
//           options.method = "POST";
//
//           console.log('[ServlerHelper.js] do_http_post()  options: '+JSON.stringify(options));
//
//           const req = https.request(options, (res) => {
//           // https.request(options, (res) => {
//             const statusCode = res.statusCode;
//             const contentType = res.headers['content-type'];
//             let error;
//
//             console.log('[ServerHelper.js] do_http_post Status: ' + res.statusCode);
//             console.log('[ServerHelper.js] do_http_post Headers: ' + JSON.stringify(res.headers));
//
//
//             if (statusCode !== 200) {
//               error = new Error('Request Failed.\n' +
//                                 'Status Code: {statusCode}');
//             } else if (!/^application\/json/.test(contentType)) {
//               error = new Error('Invalid content-type.\n' +
//                                 'Expected application/json but received ${contentType}');
//             }
//
//
//
//             if (error) {
//               console.error("[ServerHelper.js] do_http_post (Error):"+error.message +' , '+ options.path);
//               res.resume();
//               return;
//             }
//
//             let rawData = '';
//             res.setEncoding('utf8');
//             res.on('data', (chunk) => rawData += chunk);
//             res.on('end', () => {
//               try {
//         //logger.debug("cargarComponente urlComponente " + urlComponente);
//                 let parsedData = JSON.parse(rawData);
//                 console.log('[ServerHelper.js] do_http_post() JSON raw de:  '+options.path+' \n'+rawData);
//         //logger.debug("JSON RAW : " + rawData);
//                 callback(parsedData);
//               } catch (e) {
//                 //logger.debug(e.message);
//               }
//             });
//           }).on('error', (e) => {
//             console.log('[ServerHelper.js] do_http_post() (Error):  '+options.path+' \n'+e.message);
//             //logger.debug('Got error: ' + e.message);
//           });
//
//
//           req.write(postData);
//           req.end();
//
//   }
// }
//
// exports.do_http_post = do_http_post;




//=================
// Métodos auxiliares
//=================
var idiomas = ["ca","es","en","de","fr"].join('|');
var islas = ["mallorca","menorca","ibiza","formentera"].join('|');
var baleares = "baleares";

// Comprueba si la petición interceptada es de tipo 'Isla'
var esIsla = function (urlRequest){
  var regIslas = '.*(\/){1}('
               + idiomas +
              ')(\/)(' + islas + ')(\/){0,1}([a-zA-Z])*$';

  var _esIsla = new RegExp(regIslas);

//logger.debug('expresion reg de isla '+_esIsla);
//logger.debug('valor a machear '+ urlRequest);

  return(_esIsla.test(urlRequest.toLowerCase()));
};

exports.esIsla = esIsla

// Comprueba si la petición interceptada es Landing para Baleares
var esBaleares = function(url){
  var regExpLanding = '.*(\/)(' + idiomas +
              ')(\/)(' + baleares + ')\/([a-zA-Z])*$';

  var _esBaleares = new RegExp(regExpLanding);
  return(_esBaleares.test(url.toLowerCase()));
};

exports.esBaleares = esBaleares;

// Comprueba si la petición interceptada es de tipo 'buscador'
var esBuscador = function(urlRequest){
  //logger.debug("Comprobando si es buscador");

  var regExpBuscador = '.*(\/){1}(' + idiomas + ')(\/)(' + islas + ')\/{1}(buscador-playas)\/{0,1}$';

  var _esBuscador = new RegExp(regExpBuscador);
  return(_esBuscador.test(urlRequest.toLowerCase()));
}

exports.esBuscador = esBuscador;



// Comprueba si la petición interceptada es de tipo 'buscador'
var esREST = function(urlRequest){
  console.log('[ServerHelper.js] esSearch(): '+urlRequest);

  var patt = /components/g;

  return(new RegExp(patt).test(urlRequest));

}

exports.esREST = esREST;


// Comprueba si la petición interceptada es de tipo 'buscador'
var esNewsletter = function(urlRequest){
  console.log('[ServerHelper.js] esNewsletter(): '+urlRequest);

  var patt = /newsletter_users/g;

  return(new RegExp(patt).test(urlRequest));

}

exports.esNewsletter = esNewsletter;


// Comprueba si la petición interceptada es de tipo 'buscador'
var esNewsletterMailChimp = function(urlRequest){
  console.log('[ServerHelper.js] esNewsletterMailChimp(): '+urlRequest);

  var patt = /newsletter_mchip/g;

  return(new RegExp(patt).test(urlRequest));

}

exports.esNewsletterMailChimp = esNewsletterMailChimp;





// Comprueba si la petición interceptada es de tipo 'buscador'
var esDiccionario = function(urlRequest){
  console.log('[ServerHelper.js] esDiccionario(): '+urlRequest);

  var patt = /diccionario/g;

  return(new RegExp(patt).test(urlRequest));

}

exports.esDiccionario = esDiccionario;



// Comprueba si la petición interceptada es de tipo 'buscador'
var esSearch = function(urlRequest){
  console.log('[ServerHelper.js] esSearch(): '+urlRequest);

  var patt = /searcher/g;

  return(new RegExp(patt).test(urlRequest));

}

exports.esSearch = esSearch;


// Comprueba si la petición interceptada es de tipo 'buscador'
var esSearchCompanies = function(urlRequest){
  //logger.debug("Comprobando si es buscador");

  // var regExpBuscador = '.*(\/){1}(' + idiomas + ')(\/)(' + islas + ')\/{1}(buscador-playas)\/{0,1}$';
  // var regExpBuscador = '\/components\/searcher\/{1}*';
  console.log('[ServerHelper.js] esSearchCompanies(): '+urlRequest);

  var patt = /companies/g;
  // var result =
// /components/searcher/section/patrimonio-cultural/island/:island/area/:area/municipality/:municipality/title/:title
// http://54.77.91.226:5000/components/searcher/section/patrimonio-cultural/island/:island/area/:area/municipality/:municipality/title/:title'

  // var _es = new RegExp(regExpBuscador);
  // return(_esBuscador.test(urlRequest.toLowerCase()));

  return(new RegExp(patt).test(urlRequest));



}

exports.esSearchCompanies = esSearchCompanies;


// Set Friendly URLs for Page in different Languages
var setUrlsFriendly =  function(objurl,isla,pageType){

  var oUrls =[];
  var pageTypeUrl ='';

  if (pageType=='Landing'){

     _.filter(dicc_paginas,["section",objurl]).map(function(webpage){
       // console.log('[Server.js]  webpage.url_friendly '+ webpage.url_friendly);
       var language = (webpage.url_friendly.split('/'))[1];
       oUrls[language] = webpage.url_friendly+'/';
     });

      // console.log(jsonUrls.url_friendly)
      // oUrls['ca'] = '/ca/'+isla+'/';
      // oUrls['es'] = '/es/'+isla+'/';
      // oUrls['en'] = '/en/'+isla+'/';
      // oUrls['fr'] = '/fr/'+isla+'/';
      // oUrls['de'] = '/de/'+isla+'/';

      // oDatosHeader.aUrlsFriendly = setUrlsFriendly(oDatosHeader.aIdFriendly,isla,oDatosHeader.sPageType);
      // var oComponentes = obtenerNodoUrlFriendly(dicc_paginas,urlRequest);



  }
  else{
      // console.log("[Server.js]  setUrlsFriendly():  objurl: "+ JSON.stringify(objurl));
      // console.log("[Server.js]  setUrlsFriendly():  isla: "+ isla);
      // console.log("[Server.js]  setUrlsFriendly():  pageType: "+ JSON.stringify(pageType));
      // console.log("[Server.js]  setUrlsFriendly(): dicc_tipoComponentes_trans\n"+JSON.stringify(dicc_tipoComponentes_trans));
      pageTypeUrl = (pageType!=undefined ) ? obtenerNodo(dicc_tipoComponentes_trans,pageType): '';

      // console.log("[Server.js]  setUrlsFriendly():  pageTypeUrl: "+ JSON.stringify(pageTypeUrl));
      oUrls['ca'] = '/'+ (pageType!=undefined ? dameTextoi18(pageTypeUrl,'ca') : '') + '/ca/' + isla + '/' + dameTextoi18(objurl,"ca");
      oUrls['es'] = '/'+ (pageType!=undefined ? dameTextoi18(pageTypeUrl,'es') : '') + '/es/' + isla + '/' + dameTextoi18(objurl,"es");
      oUrls['en'] = '/'+ (pageType!=undefined ? dameTextoi18(pageTypeUrl,'en') : '') + '/en/' + isla + '/' + dameTextoi18(objurl,"en");
      oUrls['fr'] = '/'+ (pageType!=undefined ? dameTextoi18(pageTypeUrl,'fr') : '') + '/fr/' + isla + '/' + dameTextoi18(objurl,"fr");
      oUrls['de'] = '/'+ (pageType!=undefined ? dameTextoi18(pageTypeUrl,'de') : '') + '/de/' + isla + '/' + dameTextoi18(objurl,"de");
    }

  console.log("[Server.js]  setUrlsFriendly(): oUrls.ca "+ oUrls.ca);
  console.log("[Server.js]  setUrlsFriendly(): oUrls.es "+ oUrls.es);
  console.log("[Server.js]  setUrlsFriendly(): oUrls.en "+ oUrls.en);
  console.log("[Server.js]  setUrlsFriendly(): oUrls.fr "+ oUrls.fr);
  console.log("[Server.js]  setUrlsFriendly(): oUrls.de "+ oUrls.de);

  return {"ca": oUrls.ca, "es": oUrls.es, "en": oUrls.en, "fr": oUrls.fr,  "de": oUrls.de };

}

exports.setUrlsFriendly = setUrlsFriendly;




/**
* Obtiene el nodo que contiene las urls de los componentes que construyen la plantilla
* @param item. JSON del diccionario de datos.
* @param mask url de la que queremos obtener los componentes. Hay que tener en cuenta que la url puede venir con una barra al final, O EN MAYUSCULAS
* @return nodo con los componentes ecesarios para montar la plantilla.
*/
var obtenerNodo = function(json,mask){
  var oNodo = {};
  var _mask = mask.toLowerCase();

try{
  console.log("[ServerHelper.js] obtenerNodo...json: "+JSON.stringify(json));
  console.log("[ServerHelper.js] obtenerNodo..._mask: "+_mask);
  console.log("[ServerHelper.js] obtenerNodo . json[_mask]"+JSON.stringify(json[_mask]));
  oNodo = json[_mask];
  }
  catch(e){
    console.log("[ServerHelper.js] obtenerNodo (Error): "+e.message);
  }
  return oNodo;
};



/**
* Recupera cualquier diccionario de datos.
*/
function recuperarDiccionario(options,urlDiccionario,callback){
  // get request to obtain the dictionary_of_webpages

  if(urlDiccionario != null){
    options.path = urlDiccionario;
  }

  console.log('recuperarDiccionario ' + options.path);

  http.get(options, (res) => {
    const statusCode = res.statusCode;
    const contentType = res.headers['content-type'];
    let error;
    if (statusCode !== 200) {
      error = new Error('Request Failed.\n' +
                        'Status Code: ' + statusCode);
    } else if (!/^application\/json/.test(contentType)) {
      error = new Error('Invalid content-type.\n' +
                        'Expected application/json but received ${contentType}');
    }
    if (error) {
      console.error(error.message +' , '+ options.path);
      res.resume();
      return;
    }
    res.setEncoding('utf8');
    let rawData = '';
    res.on('data', (chunk) => rawData += chunk);
    res.on('end', () => {
      try {

        console.log('diccionario recuperado');

        let parsedData = JSON.parse(rawData);
        // console.log('JSON rawData\n'+rawData);


        callback(parsedData);
      } catch (e) {
        //logger.debug(e.message);
      }
    });
  }).on('error', (e) => {
    //logger.debug('Got error: ' + e.message);
  });
}

exports.recuperarDiccionario = recuperarDiccionario;




// Obtiene JSON.
function apiSearch(urlRequest,res,callback){

      var theUrl = urlRequest;
  // function(callback){
      //  callback(null,rt.searcher);
      console.log('[ServlerHelper.js] apiSearch() theUrl... : '+theUrl);

       theUrl = theUrl.replace(':featured', '--all--');
       theUrl = theUrl.replace(':island', '--all--');
       theUrl = theUrl.replace(':area', '--all--');
       theUrl = theUrl.replace(':municipality', '--all--');
       theUrl = theUrl.replace(':tipology', '--all--');
       theUrl = theUrl.replace(':term', '--all--');


       theUrl = theUrl.replace(':title', '--all--');
       theUrl = theUrl.replace(':activity_type', '--all--');
       theUrl = theUrl.replace(':distinctive', '--all--');

       theUrl = theUrl.replace(':lang', '--all--');
       theUrl = theUrl.replace(':touristic_subject', '--all--');
       theUrl = theUrl.replace(':season', '--all--');
       theUrl = theUrl.replace(':lifestyle', '--all--');
       theUrl = theUrl.replace(':limit?', 'all');
       theUrl = theUrl.replace(':text', '--all--');
       theUrl = theUrl.replace(':component', '--all--');





       console.log('[ServlerHelper.js] apiSearch() theUrl... : '+theUrl);

    //  /components/searcher/component/:component/featured/:featured/island/:island/area/:area/municipality/:municipality/tipology/:tipology/term/:term/lifestyle/:lifestyle/touristic_subject/:touristic_subject/season/:season/text/:text
    // http://54.77.91.226:5000/components/searcher/publications/lang/:lang/island/--all--/municipality/--all--/lifestyle/:lifestyle/touristic_subject/:touristic_subject/season/:season/title/--all--?_=1502051477324
    // http://54.77.91.226:5000/components/searcher/section/playas/island/:island/area/:area/municipality/:municipality/title/:title
    // http://54.77.91.226:5000/components/searcher/section/playas/island/baleares/area/--all--/municipality/--all--/title/--all--

      console.log('[ServlerHelper.js] apiSearch() cargarCo... theUrl: '+theUrl);

    cargarComponente(theUrl,function(comp){

      // console.log('[ServlerHelper.js] apiSearch() cargarComponente... comp: '+JSON.stringify(comp));

      // console.log('[ServlerHelper.js] apiSearch() cargarComponente... comp: '+JSON.stringify(comp));

      // try{
      //    rt.searcher = ReactDOMServer.renderToString(searcher({componente: comp, filtro: sFiltro, sIdioma: idioma,sSection: section, sLandingLevel: LandingLevel}));
      // } catch(e){
      //   console.log('[ServlerHelper.js] apiSearch() async.parallel searcher (Error) : ' + e.message);
      // }
      callback(comp);
    });

    // callback(comp);

  // }


}

exports.apiSearch = apiSearch;



// Obtiene JSON.
function apiREST(urlRequest,res,callback){
      var theUrl = urlRequest;
      console.log('[ServlerHelper.js] apiREST() theUrl... : '+theUrl);
      console.log('[ServlerHelper.js] apiREST() cargarComponente... theUrl: '+theUrl);

      if (urlRequest.indexOf("newsletter_user")>0) { //@TODO CSC: controlar con un contexto de api.

        urlRequest = urlRequest.replace("/components/","/")
      }


    cargarComponente(theUrl,function(comp){
      callback(comp);
    });

}

exports.apiREST = apiREST;


// Obtiene JSON.
function apiREST_POST(urlEndpoint,JSONPost,res,callback){

    console.log('[ServlerHelper.js] apiREST_POST() urlEndpoint... : '+urlEndpoint);
    var apiRESTHost= properties.get('apiRESTHost');
    var apiRESTPort= properties.get('apiRESTPort');

    var options_newsletter= {
        host: apiRESTHost,
        port: apiRESTPort,
        path: urlEndpoint,
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Content-Length': JSON.stringify(JSONPost).length
        }
      };


      console.log('[ServlerHelper.js] apiREST_POST()  options_newsletter: '+JSON.stringify(options_newsletter));
      console.log('[ServlerHelper.js] apiREST_POST()  JSONPost: '+JSON.stringify(JSONPost));

    do_http_post(JSONPost,options_newsletter,function(res){
      callback(res);
    });

}

exports.apiREST_POST = apiREST_POST;



// Obtiene JSON.
function apiMCHIMP_POST(urlEndpoint,JSONPost,res,callback){


      var mchimpUser = properties.get('mchimpUser');
      var mchimpAPIKey = properties.get('mchimpAPIKey');
      var mchimpRESTHost= properties.get('mchimpRESTHost');


      var options_mchimp= {
          host: mchimpRESTHost,
          path: urlEndpoint,
          method: 'POST',
          headers: {
            'Authorization': 'Basic ' + new Buffer(mchimpUser + ':' + mchimpAPIKey).toString('base64'),
            'Content-Type': 'application/json',
            'Content-Length': JSON.stringify(JSONPost).length
          }
        };

      console.log('[ServlerHelper.js] apiMCHIMP_POST()  options_mchimp: '+JSON.stringify(options_mchimp));
      console.log('[ServlerHelper.js] apiMCHIMP_POST()  JSONPost: '+JSON.stringify(JSONPost));

    do_https_post(JSONPost,options_mchimp,function(res){
      callback(res);
    });

}

exports.apiMCHIMP_POST = apiMCHIMP_POST;




// Elimina el dominio.
function restURL(url){
  var pattern = "http://54.77.91.226:5000";
  var reg =  new RegExp(pattern, "g");
  return (url.replace(reg,''));
}
exports.restURL = restURL;
