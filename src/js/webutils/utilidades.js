var islas = require('../../resources/identificadores/islands_final_list.json');
var estaciones = require('../../resources/identificadores/seasons_final_list.json');
var estilos = require('../../resources/identificadores/lifestyles_final_list.json');
var _ = require('lodash');


/**
*	Método auxiliar para la internacinalización de mensajes
*   @param: obj. Array de literales en diferentes idiomas
*	@param: idioma. El idioma del que se quiere recuperar el idioma
*	return: el literal correspondiente al idioma seleccionado
*/
var dameTextoi18 = function(obj,idioma){

	// console.log('utilidades dameTextoi18...');

	var rt = "";

		try{

			switch(idioma){
				case "es":
					rt = obj.es;
					break;
				case "en":
					rt = obj.en;
					break;
				case "ca":
					rt = obj.ca;
					break;
				case "de":
					rt = obj.de;
					break;
				case "fr":
					rt = obj.fr;
					break;
				default:
					rt = "";
			}

		} catch(e){
			console.warn('Error - utilidades.js dameTextoi18 : ' + e.message);
		}

return rt;

}
exports.dameTextoi18 = dameTextoi18;

/**
* Método auxiliar para la la creación de las urls de cabecera y footer.
*/
var dameUrlFriendly = function(obj,idioma,isla,type){

	var rt = (type!=undefined ) ? dameTextoi18(type,idioma) : '';

	rt += '/' + idioma + '/' + isla + '/' + dameTextoi18(obj,idioma);
	return rt;
}

exports.dameUrlFriendly = dameUrlFriendly;

/**
* Máximo de 9 items, presentados de forma aleatoria.
* Si no hay suficientes, se muestra un total que sea múltiplo de 3.
*/
var sortDestacados = function(cubos,columns,maxitems){
	// Primero barajo, y luego selecciono.
	cubos.sort(function(a, b){return 0.5 - Math.random()});

	cubos = _.slice(cubos,0,maxitems);

	cubos.splice(columns*3, cubos.length);
	if(cubos.length % columns !== 0){
		cubos.splice(columns*2, cubos.length);
		if(cubos.length % columns !== 0){
			cubos.splice(columns, cubos.length);
		}
	}
	return cubos;
}
exports.sortDestacados = sortDestacados;


var sortSubsections = function(cubos){
	// Primero barajo, y luego selecciono.
	cubos.sort(function(a, b){return 0.5 - Math.random()});
	cubos.splice(9, cubos.length);
	if(cubos.length % 3 !== 0){
		cubos.splice(6, cubos.length);
		if(cubos.length % 3 !== 0){
			cubos.splice(3, cubos.length);
		}
	}
	return cubos;
}
exports.sortSubsections = sortSubsections;



var sortSearcher = function(cubos){
	// Primero barajo, y luego selecciono.
	cubos.sort(function(a, b){return 0.5 - Math.random()});
	cubos.splice(9, cubos.length);
	if(cubos.length % 3 !== 0){
		cubos.splice(6, cubos.length);
		if(cubos.length % 3 !== 0){
			cubos.splice(3, cubos.length);
		}
	}
	return cubos;
}
exports.sortSearcher = sortSearcher;

/**
* Máximo de 8 items, mínimo de 4, presentados de forma aleatoria.
* Si no hay suficientes, se oculta.
*/
var sortRecomendados = function(cubos){
	// Primero barajo, y luego selecciono.
	cubos.sort(function(a, b){return 0.5 - Math.random()});
	cubos.splice(8, cubos.length);
	if(cubos.length < 4){
		cubos = null;
	}
	return cubos;
}
exports.sortRecomendados = sortRecomendados;

/**
 * Devuelve un objeto compuesto por el 'destacado' y el resto (de momento solo hay dos items).
 */
var sortEventos = function(cubos){
	var oEventos = {"destacado":[],"eventos":[]};
	for(var i=0;i<cubos.length;i++){
		if(cubos[i].events[0].featured === true){
			oEventos.destacado = cubos[i].events[0];
		} else {
			oEventos.eventos.push(cubos[i].events[0]);
		}
	}
	return oEventos;
}
exports.sortEventos = sortEventos;

/**
 * Convierte la primera letra a mayúsculas
 */
var firstUpper = function(literal){
	return literal.charAt(0).toUpperCase() + literal.slice(1);
}

exports.firstUpper = firstUpper;

/**
 * Compone el rastro de migas para las landings
 * Home / Islas / Mallorca
 * Home / Cuando viajas / Verano
 * Home / Como viajas / Familia
 */
var rastroLanding = function(_rastro){
	var rt = [];

	rt = _rastro.split('/');
	var idioma = rt[1];

	rt.splice(1,1);
	rt[0] = 'Home';

	for(i=0;i<rt.length;i++){
		rt[i] = firstUpper(rt[i]);
	}

	for(j=0;j<estaciones.length;j++){
		if(rt[2].toLowerCase() == estaciones[j].id){
			rt[2] = dameTextoi18(estaciones[j].lang,idioma);
		}
	}
	for(k=0;k<estilos.length;k++){
		if(rt[2].toLowerCase() == estilos[k].id){
			rt[2] = dameTextoi18(estilos[k].lang,idioma);
		}
	}

	return rt;
}
exports.rastroLanding = rastroLanding;

var capitalizedString = function (text){

	return text == "" ? "" :  text.split(/\s+/).map(w => w[0].toUpperCase() + w.slice(1)).join(' ');

}

exports.capitalizedString = capitalizedString;
