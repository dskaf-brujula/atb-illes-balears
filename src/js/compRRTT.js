var React = require('react');
var ReactDOM = require('react-dom');

var idioma = '';
idioma = location.pathname.split('/')[2];
var isla = location.pathname.split('/')[3];
var rastroMigas = location.pathname;

var oDatosHeader = [];
oDatosHeader.sIdioma = idioma;
oDatosHeader.sIsla = isla;
oDatosHeader.sUrl = location.pathname;
oDatosHeader.aUrlsFriendly = JSON.parse((document.getElementById("aUrlsFriendlyClient").innerHTML).replace(/&#x27;/g,'"'));


var MenuHeader = require('./components/MenuCabecera');
ReactDOM.render(<MenuHeader oDatosHeader={oDatosHeader}/>,document.getElementById('cabecera'));

console.log('[compRRTT.js] ');



try{
	if(oRecomendados){
		console.log('[compRRTT.js] oRecomendados  existe')
		var Recomendaciones = require('./components/Recomendaciones');
		var dicc_tipoComponentes_trans='';
		var componente = '';
		ReactDOM.render(<Recomendaciones render='client' sUrl={oRecomendados} sIdioma={idioma} sPageType='Ficha'/>,document.getElementById('recomendaciones'));
	} else {
		console.log('[compRRTT.js] oRecomendados no existe')
	}
} catch(e){
	console.log('[compRRTT.js] oRecomendados ReactDOM.render. Error: '+e.message);
}



try{
	if(oDatosMapLatLon || oDatosMapkml || oDatosMap){

		console.log('[compRRTT.js] oDatosMap existe')
		var Mapa = require('./components/Mapa');
		var dicc_tipoComponentes_trans='';
		var componente = '';

		if (oDatosMapkml.length>0){ //@TODO: controlar mejor el null
			ReactDOM.render(<Mapa render='client' maptype='kml' sIdioma={idioma} kml ={oDatosMapkml} latlon = {oDatosMapLatLon} sIsla={oDatosMapIsla} sTitle={oDatosMapTitle}/>,document.getElementById('mapa'));
		}else if (oDatosMapLatLon.latitude!=0||oDatosMapLatLon.longitude!=0){ //@TODO: controlar mejor el null
			ReactDOM.render(<Mapa render='client' maptype='latlon' sIdioma={idioma} latlon = {oDatosMapLatLon} sIsla={oDatosMapIsla} sTitle={oDatosMapTitle}/>,document.getElementById('mapa'));
		}else{
			console.warn('[compRRTT.js] oDatosMap itinerario en RRTT controlar');
			// ReactDOM.render(<Mapa render='client' maptype='itinerario' sIdioma={idioma} sIsla={oDatosMapIsla} sTitle={oDatosMapTitle}/>,document.getElementById('mapa'));
		}
	} else {
		console.log('[compRRTT.js] oDatosMap no existe')
	}

} catch(e){
	console.log('[compRRTT.js] oDatosMap ReactDOM.render. Error: '+e.message);
}






try{
		if(oDatosMeteoUrl){

			console.log('[compRRTT.js] oDatosMeteoUrl existe')
			var Meteo = require('./components/Meteo');


			var wait = setInterval(canrender, 1000);

			function canrender() {

				console.log('[compRRTT.js] canrender... window.oDatosMeteoUrl: '+window.oDatosMeteoUrl);

				if (!(window.oDatosMeteoUrl.indexOf("latitude/0/")>0)||(window.oDatosMeteoUrl.indexOf("longitude/0/")>0)){
					console.log('[compRRTT.js] oDatosMeteoUrl correcto canrender true... ');

					clearInterval(wait);
					ReactDOM.render(<Meteo meteotype='meteofull' render='client' url ={oDatosMeteoUrl} sIdioma={idioma} sIsla={isla}  />,document.getElementById('meteo'));
				}

			}




		} else {
			console.log('[compRRTT.js] oDatosMeteoUrl no existe')
		}
} catch(e){
	console.log('[compRRTT.js] oDatosMeteoUrl ReactDOM.render. Error: '+e.message);
}




var Meteomini = require('./components/Meteo');

var latlon =[];
var meteourl = [];

latlon['mallorca']={lat:'39.551530',lon:'2.735623'};
latlon['menorca']={lat:'39.864176',lon:'4.221855'};
latlon['ibiza']={lat:'38.874843',lon:'1.371127'};
latlon['formentera']={lat:'38.703676',lon:'1.452522'};

function _meteourl(island){
	return "/components/meteoclim/latitude/"+latlon[island].lat+"/longitude/"+latlon[island].lon+"/touristic_subjects/naturaleza";
}

ReactDOM.render(<Meteomini render='client' meteotype='meteomini' url={_meteourl('mallorca')} sIdioma={idioma} sIsla='mallorca'/>,document.getElementById('mallorca-weather'));
//
ReactDOM.render(<Meteomini render='client' meteotype='meteomini' url={_meteourl('menorca')} sIdioma={idioma} sIsla='menorca'  />,document.getElementById('menorca-weather'));

ReactDOM.render(<Meteomini render='client' meteotype='meteomini' url={_meteourl('ibiza')} sIdioma={idioma} sIsla='ibiza'  />,document.getElementById('ibiza-weather'));

ReactDOM.render(<Meteomini render='client' meteotype='meteomini' url={_meteourl('formentera')} sIdioma={idioma} sIsla='formentera'  />,document.getElementById('formentera-weather'));
