var React = require('react');
var tagsColors = require('../../resources/tagsColors.json');

var _host = '';
// var estaticos = require('../../resources/estaticosCabecera.json');
//
// var dameTextoi18 = require('../webutils/utilidades.js').dameTextoi18;
// var dameUrlFriendly = require('../webutils/utilidades.js').dameUrlFriendly;
//
// // Get TagsButtons
 // var rastroLanding = require('../webutils/utilidades.js').rastroLanding;



 var  _getTagClass  = function(tag,type,reset){
 	try{

 		if (tag==undefined) return '';


		console.log("[TagsButtons.js] render() _getTagClass(tag,type): "+tag+","+type);



 		switch (type){

 			case "seasons":
 			return "tags-temporada tags-temporada-activo";
			break;

 			case "lifestyles":
 			return "tags-grey tags-grey-activo";
			break;

 			default:
 			return tagsColors[0][tag]['class']+" "+tagsColors[0][tag]['class']+"-activo";

 		}

 	} catch(e){
 		console.log("[TagsButtons.js] render() _getTagClass(tag,type): "+tag+","+type+" Error: "+e.message);
 		return '';
 	}
 }


var TagsButtons = React.createClass({

	handleClick: function(sUrl) {
		console.log("[TagsButtons.js] onClick..."+sUrl);
		location.href=sUrl;
	},
	render: function(){


		console.log("[TagsButtons.js] render...");

    console.log("[TagsButtons.js] oDatosTagsButtons.oTouristicSubjects ..."+JSON.stringify(this.props.oDatosTagsButtons.oTouristicSubjects));
		console.log("[TagsButtons.js] oDatosTagsButtons.oLifeStyles ..."+JSON.stringify(this.props.oDatosTagsButtons.oLifeStyles));
		console.log("[TagsButtons.js] oDatosTagsButtons.oSeasons ..."+JSON.stringify(this.props.oDatosTagsButtons.oSeasons));
    console.log("[TagsButtons.js] oDatosTagsButtons.componentType ..."+JSON.stringify(this.props.oDatosTagsButtons.componentType));


    var oDatosTagsButtons = this.props.oDatosTagsButtons;
    var idioma = oDatosTagsButtons.sIdioma;
    var isla = oDatosTagsButtons.sIsla;
    // console.log("[TagsButtons.js] idioma "+idioma);
    // console.log("[TagsButtons.js] isla "+isla);

		var TagsTouristicSubjects='';
		var TagsLifeStyles='';
		var TagsSeasons='';

		var reset = false;

    try{

			if (oDatosTagsButtons.oTouristicSubjects!=undefined){
				var aTagId = oDatosTagsButtons.oTouristicSubjects.id;
				var aTagName = oDatosTagsButtons.oTouristicSubjects.lang[idioma];

				var TagsTouristicSubjects = aTagId.map(function(tagId,index){
		      return(
		          <button key={index} className={_getTagClass(tagId,"touristic_subject",reset)} type="button" data-tagid={aTagId[index]} data-tagtype="touristic_subject" key={index}>{aTagName[index]}</button>
		      )
		    });




			}

			if (oDatosTagsButtons.oLifeStyles!=undefined){
				var aTagId2 = oDatosTagsButtons.oLifeStyles.id;
	      var aTagName2 = oDatosTagsButtons.oLifeStyles.lang[idioma];


				var TagsLifeStyles = aTagId2.map(function(tagId2,index){
		      return(
		          <button key={index} className={_getTagClass(tagId2,"lifestyles",reset)} type="button" data-tagid={aTagId2[index]} data-tagtype="lifestyles">{aTagName2[index]}</button>
		      )
				});



			}

			if (oDatosTagsButtons.oSeasons!=undefined){
				var aTagId3 = oDatosTagsButtons.oSeasons.id;
				var aTagName3 = oDatosTagsButtons.oSeasons.lang[idioma];

				var TagsSeasons = aTagId3.map(function(tagId3,index){
		      return(
		          <button key={index} className={_getTagClass(tagId3,"seasons",reset)}  type="button" data-tagid={aTagId3[index]} data-tagtype="seasons">{aTagName3[index]}</button>
		      )
		    });

			}


    }catch(e){
      console.log("[TagsButtons.js]  (Error)..."+e.message);

    }


		console.log("[TagsButtons.js]  this.props.className..."+this.props.className);


		return(

            <p className={this.props.className}>
                {TagsTouristicSubjects}{TagsLifeStyles}{TagsSeasons}
            </p>

		);
	}
});

module.exports = TagsButtons;
