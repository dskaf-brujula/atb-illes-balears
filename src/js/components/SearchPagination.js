var React = require('react');
var PropTypes = require('prop-types'); // ES5 with npm
var _ = require('lodash');
var ReactDOM = require('react-dom');


// var SearchPaginationStore = require('../stores/SearchPaginationStore');
// var ActionsCreator = require('../actions/appActions');

//
const propTypes = {
    items: PropTypes.array.isRequired,
    onChangePage: PropTypes.func.isRequired,
    initialPage: PropTypes.number
};

const defaultProps = {
    initialPage: 1
};


//
// function getStateFromStore(){
//
// 	console.log('[SearchPagination.js] getStateFromStore() ... SearchPaginationStore.getAll()'+SearchPaginationStore.getAll());
//
// 	return {pager: SearchPaginationStore.getAll()}
// }
//


var SearchPagination = React.createClass({


    getInitialState: function() {
       return { pager: {} };
     },



    componentWillMount: function(){

      console.log("[SearchPagination.js] componentWillMount()...");

        // this.onLoad();

        // set page if items array isn't empty
        if (this.props.items && this.props.items.length) {
            this.setPage(this.props.initialPage);
        }
    },

    componentDidUpdate: function(prevProps, prevState) {

      console.log("[SearchPagination.js] componentDidUpdate()...");

        // reset page if items array has changed
        if (this.props.items !== prevProps.items) {
            this.setPage(this.props.initialPage);
        }
    },


    handleClick: function(page) {
      console.log("[SearchPagination.js] handleClick()... page: "+page);
      //  this.setPage(page);
   },

    setPage: function(page) {

      console.log("[SearchPagination.js] setPage()...");

        var items = this.props.items;
        var pager = this.state.pager;

        console.log("[SearchPagination.js] setPage() items: "+items);
        console.log("[SearchPagination.js] setPage() pager: "+pager);


        // pager = this.getStateFromStore();

        console.log("[SearchPagination.js] setPage() pager.totalPages: "+pager.totalPages);


        if (page < 1 || page > pager.totalPages) {
            return;
        }

        // get new pager object for specified page
        pager = this.getPager(items.length, page);

        // get new page of items from items array
        var pageOfItems = items.slice(pager.startIndex, pager.endIndex + 1);

        // update state
        this.setState({ pager: pager });

        // call change page function in parent component
        this.props.onChangePage(pageOfItems,pager.currentPage);
    },

    getPager: function(totalItems, currentPage, pageSize) {

      console.log("[SearchPagination.js] getPager()...");

        // default to first page
        currentPage = currentPage || 1;

        // default page size is 10
        pageSize = pageSize || 10;

        // calculate total pages
        var totalPages = Math.ceil(totalItems / pageSize);

        var startPage, endPage;
        if (totalPages <= 10) {
            // less than 10 total pages so show all
            startPage = 1;
            endPage = totalPages;
        } else {
            // more than 10 total pages so calculate start and end pages
            if (currentPage <= 6) {
                startPage = 1;
                endPage = 10;
            } else if (currentPage + 4 >= totalPages) {
                startPage = totalPages - 9;
                endPage = totalPages;
            } else {
                startPage = currentPage - 5;
                endPage = currentPage + 4;
            }
        }

        // calculate start and end item indexes
        var startIndex = (currentPage - 1) * pageSize;
        var endIndex = Math.min(startIndex + pageSize - 1, totalItems - 1);

        // create an array of pages to ng-repeat in the pager control
        var pages = _.range(startPage, endPage + 1);

        // return object with all pager properties required by the view
        return {
            totalItems: totalItems,
            currentPage: currentPage,
            pageSize: pageSize,
            totalPages: totalPages,
            startPage: startPage,
            endPage: endPage,
            startIndex: startIndex,
            endIndex: endIndex,
            pages: pages
        };
    },

    render: function(){

      console.log("[SearchPagination.js] render()...");




        var pager = this.state.pager;

        console.log("[SearchPagination.js] pager.pages.length()..."+pager.pages.length);


        if (!pager.pages || pager.pages.length <= 1) {
            // don't display pager if there is only 1 page
            return null;
        }


        console.log("[SearchPagination.js] Render()...");

        console.log("[SearchPagination.js] Render() pager: "+pager.pages.length);

        return (
          <ul className="pagination">

          <li className={pager.currentPage === 1 ? 'disabled' : ''}>
              <a onClick={() => this.setPage(pager.currentPage - 1)}>&laquo;</a>
          </li>
          {pager.pages.map((page, index) =>
                <li key={index} className={pager.currentPage === page ? 'active' : ''}>
                    <a onClick={() => this.setPage(page)}>{page}</a>
                </li>
            )}

          <li className={pager.currentPage === pager.totalPages ? 'disabled' : ''}>
              <a onClick={() => this.setPage(pager.currentPage + 1)}>&raquo;</a>
          </li>
          </ul>

        )



    }
});



var Page = React.createClass({
  render: function () {

    console.log('[SearchPagination.js] Page render this.props.currentPage: '+this.props.currentPage);
    console.log('[SearchPagination.js] Page render this.props.page: '+this.props.page);

    return(
    <li  className={this.props.currentPage === this.props.page ? 'active' : ''}>
         <a  onClick={function () {this.props.handleClick(this.props.page)}}>{this.props.page}</a>
    </li>
    )
  }
});


SearchPagination.propTypes = propTypes;
SearchPagination.defaultProps = defaultProps;

module.exports = SearchPagination;
