var React = require('react');
var estaticos_pie = require('../../resources/estaticosFooter.json');

var urlRegister = "/newsletter_users/"
var urlMailChimp = "/newsletter_mchip/"



var NewsLetterModal = React.createClass({

	getInitialState: function() {
		console.log('[NewsLetterModal.js] getInitialState');
		return ({registrorealizado: false});
	},
	onButtonClick: function(event){
		console.log('[NewsLetterModal.js] onButtonClick() ');

// $( "in//put[value='Hot Fuzz']" )
		//var url = urlRegister+"/"+$(this.refs['newsletterform']).val();
		event.preventDefault();

		var email = $("input[name='newsletterform']").val();

		if (!email) {
			return;
		}

		// registrorealizado = true;

		// var url = urlRegister+"/";

		this.doRegister(urlRegister,{email: email.trim().toLowerCase(),lang:this.props.idioma});
		this.doRegister(urlMailChimp,{email: email.trim().toLowerCase(),lang: this.props.idioma,list: this.text('mchimp_list')});

		this.setState({registrorealizado: true});
	},
	doRegister: function(theUrl,JSONPost){

		try{
			// var theUrl = this.props.url;
			console.log('[NewsLetterModal.js] doRegister (theUrl) ' + theUrl);
				if(typeof $ != 'undefined'){
						$.ajax({
							url: theUrl,
							dataType: 'json',
							type: 'GET',
							data: JSONPost,
							cache: false,
							async: false,
							success: function(data){
								console.log('[NewsLetterModal.js] doRegister '+data);
								// meteoData = data;
								// norender = false;
								// this.setState({norender:false, meteo:data});
							}.bind(this),
							error: function(xhr, status, err) {
								console.error('[NewsLetterModal.js]  doRegister (error)'+this.props.url, status, err.toString());
							}.bind(this),
							complete: function(xhr, status){
								console.log('[NewsLetterModal.js]  doRegister status'+status+'');
							}.bind(this)
						});
				}

		} catch(e){
			console.log('[NewsLetterModal.js] doRegister (error) ' + e.message);
		}
	},

	componentDidMount: function (){

		console.log('[NewsLetterModal.js] componentDidMount()');

		// $('#myModal').modal({
		//   keyboard: false
		// })

		// $(this.refs['form-newsletter-input']).modal();

				$(this.refs['form-newsletter-modal']).on('show.bs.modal', function (e) {
					console.log('[NewsLetterModal.js] show.bs.modal ...');
					if(!e.relatedTarget.form.checkValidity()){
						e.preventDefault();
					}
				  // do something...
				})


				$(this.refs['form-newsletter-modal']).on('hide.bs.modal', function (e) {
					console.log('[NewsLetterModal.js] hide.bs.modal ...');

				  // do something...
				})


	},
	text: function(txt){

		try {
			return estaticos_pie[0][txt][this.props.idioma];
		} catch (e){
			console.log("_get(txt) (error): "+e.message);
			return txt;
		}
	},
	render: function(){

				console.log('[NewsLetterModal.js] render() version 1.1');


			  var idioma = String(this.props.idioma);
				console.log('[NewsLetterModal.js] render() idioma: '+idioma);

				console.log('[NewsLetterModal.js] render() registrorealizado: '+registrorealizado);


				var registrorealizado = this.state.registrorealizado;

				function _get(txt){
					try {
				  	return estaticos_pie[0][txt][idioma];
					} catch (e){
						console.log("_get(txt) (error): "+e.message);
						return txt;
					}
				}


				return(



					            <div className="modal fade" ref="form-newsletter-modal"  id="form-newsletter-modal" tabindex="-1" role="dialog" >

					              <div className="modal-dialog">

												{!registrorealizado&&
					                <div className="modal-content">

					                      <div className="modal-header">
					                        <button type="button" className="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					                        <h4 className="modal-title" id="myModalLabel4">{_get('confirmarsuscripcion')}</h4>
					                      </div>

					                      <div className="modal-body" style={{padding:"20px", paddingTop: "10px"}}>
					                          <div className="row">
					                            <div className="col-md-12">
					                              <p style={{textAlign:"center"}}><i className="icon-custom icon-md rounded-x icon-bg-u icon-envelope"></i></p>
					                              <p>
																				{_get('aceptoprivacidad_1')}&nbsp; <a className="color-light" href={_get('politicaprivacidad_url')} target="_blank">{_get('politicaprivacidad')}</a>
																				</p>
					                            </div>
					                          </div>
					                      </div>

					                      <div className="modal-footer">
					                        <span>
					                        <button type="button" className="btn-u btn-u-default" onClick={this.onButtonClick}>{_get('aceptosuscripcion')}</button>
					                        </span>
					                      </div>

					                </div>
												}

												{registrorealizado&&
													<div className="modal-content">

																<div className="modal-header">
																	<button type="button" className="close" data-dismiss="modal" aria-hidden="true">&times;</button>
																	<h4 className="modal-title" id="myModalLabel4">{_get('confirmarsubscripcion')}</h4>
																</div>

																<div className="modal-body" style={{padding:"20px", paddingTop: "10px"}}>
																		<div className="row">
																			<div className="col-md-12">
																				<p style={{textAlign:"center"}}><i className="icon-custom icon-md rounded-x icon-bg-u icon-envelope"></i></p>
																				<p>{_get('confirmacionsuscripcion')}</p>
																			</div>
																		</div>
																</div>

																<div className="modal-footer">
																	<span>
																	<button type="button" className="btn-u btn-u-default" data-dismiss="modal">{_get('cerrar')}</button>
																	</span>
																</div>

													</div>
												}


					              </div>

					            </div>


				);


		}

});


module.exports = NewsLetterModal;
