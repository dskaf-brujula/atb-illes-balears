var React = require('react');
var ItinerarioStore = require('../stores/ItinerarioStore');
var ActionsCreator = require('../actions/appActions');
var dameTextoi18 = require('../webutils/utilidades.js').dameTextoi18;
var jsonFile = require('../../resources/literalesItinerario.json');
var renderHtml = require('react-render-html');

var itinerario = [];

// Obtiene los recursos para los cubos 'itinerario'.
function cargarItinerario(theUrl){
  try{
	  	if(typeof $ != 'undefined'){
		  $.ajax({
		    url: theUrl,
		    dataType: 'json',
		    cache: false,
		    async: false,
		    success: function(data){
				itinerario = data;
				console.log('[ITINERARIO] ciclo vida 1 itinerario. success '+itinerario);
		    }.bind(this),
		    error: function(xhr, status, err) {
		      console.error(this.props.url, status, err.toString());
		    }.bind(this),
		    complete: function(xhr, status){
				console.log('[ITINERARIO] ciclo vida 2 '+status+' '+itinerario);
		    }.bind(this)
		  });
	   	}

	} catch(e){
		console.log('[ITINERARIO] Cargar itinerario ' + e.message);
	}
}

function getStateFromStore(){
	return {itinerario: ItinerarioStore.getAll()}
}

var Itinerario = React.createClass ({

  getInitialState: function() {

		// Client side
		if(this.props.sUrl){
			var theUrl = String(this.props.sUrl);
			console.log('[ITINERARIO] URL '+ theUrl);
			cargarItinerario(theUrl);
		} else {
			// Server side
			itinerario = this.props.componente;
		}

		ActionsCreator.updateArticulo({data:itinerario});
		console.warn('[itinerario] getInitialState');
		return getStateFromStore();
	},
	componentDidMount: function(){
		console.warn('[itinerario] componentDidMount');
		ItinerarioStore.addLoadListener(this.onLoad);
	},
	ComponentWillUnMount: function(){
		ItinerarioStore.removeLoadListener(this.onLoad);
	},
	onLoad: function() {
		this.setState(getStateFromStore());
	},
	onClick: function(event) {
		console.log('ITINERARIO MANEJANDO CLICK EVENT ..');
	},
	render: function(){

      return ('Hola Itinerario');
  }

})

module.exports = Itinerario;
