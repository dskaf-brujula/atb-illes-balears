var React = require('react');
var RRTTStore = require('../stores/RRTTStore');
var ActionsCreator = require('../actions/appActions');
var dameTextoi18 = require('../webutils/utilidades.js').dameTextoi18;
//var generaPOIs = require('../webutils/mapaHelper.js').generaPOIs;
var estaticos = require('../../resources/literalesRRTT.json');
var renderHtml = require('react-render-html');
var _ = require('lodash');



var TagsButtons = require('./TagsButtons');
var Meteo = require('./Meteo');

// var lon = 0;
// var lat = 0;
var map = null;
var itinerario = [];
var rrtt = [];




// Obtiene los recursos para los cubos 'rrtt'.
function cargarRRTT(theUrl){
  try{
	  	if(typeof $ != "undefined"){
		  $.ajax({
		    url: theUrl,
		    dataType: 'json',
		    cache: false,
		    async: false,
		    success: function(data){
				rrtt = data;
				console.log('ciclo vida 1 rrtt. success '+rrtt);
		    }.bind(this),
		    error: function(xhr, status, err) {
		      console.error(this.props.url, status, err.toString());
		    }.bind(this),
		    complete: function(xhr, status){
				console.log('ciclo vida 2 '+status+' '+rrtt);
		    }.bind(this)
		  });
	   	}

	} catch(e){
		console.log('Cargar rrtt ' + e.message);
	}
}

function getStateFromStore(){
	return {rrtt: RRTTStore.getAll()}
}


var RRTT = React.createClass({

	getInitialState: function() {

		/**
		* Hay que distinguir entre servidor y navegador para hacer el renderizado.
		*/
		// Client side
		if(this.props.sUrl){
			var theUrl = String(this.props.sUrl);
			console.log('URL '+ theUrl);
			cargarRRTT(theUrl);
		} else {
			// Server side
			rrtt = this.props.componente;
		}

		ActionsCreator.updateRrtt({data:rrtt});
		console.warn('[rrtt] getInitialState');
		return getStateFromStore();
	},
	componentDidMount: function(){
		console.warn('[rrtt] componentDidMount');
		RRTTStore.addLoadListener(this.onLoad);
	},
	ComponentWillUnMount: function(){
		RRTTStore.removeLoadListener(this.onLoad);
	},
	onLoad: function() {
		this.setState(getStateFromStore());
	},
	onClick: function(event) {
		console.log("[RRTT.js] onClick Event ..");
	},
	render: function(){

		var idioma = String(this.props.sIdioma);

    console.log('[RRTT.js] componentesFicha() idioma: '+idioma);

    function _get(txt){
      return dameTextoi18(estaticos[0][txt],idioma);
    }


    var  pagetype =  "rrtt";

		var cultura		= dameTextoi18(estaticos[0].cultura,idioma)
		var gastronomia	= dameTextoi18(estaticos[0].gastronomia,idioma)
		var deporte		= dameTextoi18(estaticos[0].deporte,idioma)
		var naturaleza	= dameTextoi18(estaticos[0].naturaleza,idioma)
		var nautica		= dameTextoi18(estaticos[0].nautica,idioma)
		var ocio		= dameTextoi18(estaticos[0].ocio,idioma)
		var familia		= dameTextoi18(estaticos[0].familia,idioma)
		var amics		= dameTextoi18(estaticos[0].amics,idioma)


		var islands = this.state.rrtt.data.islands;


		var _title = dameTextoi18(this.state.rrtt.data.term.lang,idioma) + " " + this.state.rrtt.data.title;
		var _subtitle = dameTextoi18(this.state.rrtt.data.subtitle,idioma);
		var _opening_paragraph = dameTextoi18(this.state.rrtt.data.opening_paragraphs,idioma);
		var _descripcion = dameTextoi18(this.state.rrtt.data.description,idioma);

		var lon = this.state.rrtt.data.longitude;
		var lat = this.state.rrtt.data.latitude;

		var calle = this.state.rrtt.data.address;
		var cp = this.state.rrtt.data.postal_code;
		var municipio = this.state.rrtt.data.municipalities.names[0];
		var area = this.state.rrtt.data.areas.names[0];
		var email = this.state.rrtt.data.email;
    var web = this.state.rrtt.data.web;
		var telefono = this.state.rrtt.data.phone1;

		var _hide = {display:'none'};

    let show = [];

    show.direccion = (calle!=undefined && calle!='')?true:false;
    show.web = (web!=undefined && web!='')?true:false;
    show.email = (email!=undefined && email!='')?true:false;
    show.telefono = (telefono!=undefined && telefono!='')?true:false;



    // Tags
    var oDatosTagsButtons = []
    try{

      console.log("[RRTT.js] oDatosTagsButtons...");
      oDatosTagsButtons.sIdioma = idioma;
      oDatosTagsButtons.sIsla = islands[0];
      oDatosTagsButtons.oTouristicSubjects = this.state.rrtt.data.touristic_subjects;
      oDatosTagsButtons.oLifeStyles = this.state.rrtt.data.lifestyles;
      oDatosTagsButtons.oSeasons = this.state.rrtt.data.seasons;
      oDatosTagsButtons.componentType = pagetype;

    }catch(e){
      console.log("[RRTT.js] Render oDatosTagsButtons (Error)..."+e.message);
    }


    // var oDatosMeteo = []
    // try{
    //
    //   console.log("[RRTT.js] oDatosMeteo...");
    //   oDatosMeteo.sIdioma = idioma;
    //   oDatosMeteo.sIsla = _isla;
    //   oDatosMeteo.lat = lat;
    //   oDatosMeteo.lon = lon;
    //   oDatosMeteo.oTouristicSubjects = this.state.rrtt.data.touristic_subjects;
    //
    //   // console.log("[RRTT.js] oDatosMeteo...");
    //
    //
    //   oDatosMeteo.municipalities = this.state.rrtt.data.municipalities;
    //   oDatosMeteo.oLifeStyles = this.state.rrtt.data.lifestyles;
    //
    // }catch(e){
    //   console.log("[RRTT.js] Render oDatosMeteo (Error)..."+e.message);
    // }


    // Downloads
		var hayDescargas = false;
		var descargas = this.state.rrtt.data.multimedia.map(function(descarga,index){

      console.log('[RRTT.js] hayDescargas '+index);
			console.log('[RRTT.js] descarga.url ' + descarga.url);
			console.log('[RRTT.js] descarga.type ' + descarga.type);



      if(descarga.type == 'document'){
				hayDescargas = true;

        var descarga_title = descarga.title[idioma]!='' ? descarga.title[idioma] : dameTextoi18(estaticos[0].descarga_texto,idioma);

				return(
					<h5 className='heading-sm' key={index}>
						<a href={descarga.url} target="_blank">
							<i className='icon-custom rounded-x icon-sm icon-bg-u fa fa-file-pdf-o'></i>
							<span> {_.truncate(descarga_title, {'length': 30})}</span>
						</a>
					</h5>
				)
				}
		});

    // <dt >E-mail:</dt>
    // <dd onClick="javascript:mailto:{email}">{email}</dd>


    // <p className='margin-bottom-30'>
    // 	<button className='btn-u btn-u-xs rounded-4x btn-u-yellow' onClick={this.onClick}>cultura</button>
    // 	<button className='btn-u btn-u-xs rounded-4x btn-u-red' type='button'>gastronomía</button>
    // 	<button className='btn-u btn-u-xs rounded-4x btn-u-orange' type='button'>deporte</button>
    // 	<button className='btn-u btn-u-xs rounded-4x btn-u-green' type='button'>naturaleza</button>
    // 	<button className='btn-u btn-u-xs rounded-4x btn-u-blue' type='button'>naútica</button>
    // 	<button className='btn-u btn-u-xs rounded-4x btn-u-purple' type='button'>{ocio}</button>
    // 	<button className='btn-u btn-u-xs rounded-4x btn-u-default' type='button'>familia</button>
    // 	<button className='btn-u btn-u-xs rounded-4x btn-u-default' type='button'>amics</button>
    // </p>

    // {show.direccion&& <dd>{area}</dd>}

		return(
		  <section role="main" className='container content-sm txtalign'>
	 	    <div className='margin-bottom-30'>
          <h1 className="bl title">{_title}
            {
              islands.map((isla, index) => {
                  return (<span className={"b"+isla.substring(0,2)}></span>);
              })
            }
          </h1>


          <TagsButtons oDatosTagsButtons={oDatosTagsButtons} className="margin-bottom-30 margin-top-10"/>

          <h2 className="title-v2 font-italic">{_subtitle}</h2>

  				<div className='col-md-9 padding-right-40 padding-left-0'>
  					<p>{_opening_paragraph}</p>
  					{renderHtml(_descripcion)}
            <div id='mapa' className='map margin-top-40' tabindex='0'></div>
  				</div>
    			<div className='col-md-3'>
            <div id='meteo'></div>
            {(show.direccion||show.email||show.web||show.telefono) &&
            <div className='bg-color-grey address'>
    					<dl>
    						{show.direccion&& <dt>{_get('direccion')}</dt>}
    						{show.direccion&& <dd>{calle}</dd>}
    						{show.direccion&& <dd>{cp} {municipio}</dd>}

                {show.email&& <dd><br/></dd>}
    						{show.email&& <dt>{_get('email')}</dt>}
    						{show.email&& <dd><a href={"mailto:"+email}>{email}</a></dd>}

                {show.web&& <dd><br/></dd>}
                {show.web&& <dt>{_get('web')}</dt>}
    						{show.web&& <dd><a href={web} target="_blank">{_.truncate(web, {'length': 40})}</a></dd>}

                {show.telefono&& <dd><br/></dd>}
    						{show.telefono&& <dt>{_get('telefono')}</dt>}
    						{show.telefono&& <dd>{telefono}</dd>}

    					</dl>
    				</div>
            }
    				<div className='margin-top-40'></div>
            {hayDescargas && <h2 className='title-v5'>{dameTextoi18(estaticos[0].descarga_titulo,idioma)}</h2>}
    				{descargas}
    			</div>

      </div>
		</section>

		);
	}
});

module.exports = RRTT;
