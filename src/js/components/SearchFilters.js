var React = require('react');
var PropTypes = require('prop-types'); // ES5 with npm
var ReactDOM = require('react-dom');
var _ = require('lodash');
var $ = require('jquery');
var moment = require('moment');
var datepicker = require('jquery-ui/ui/widgets/datepicker');



var capitalizedString = require('../webutils/utilidades.js').capitalizedString;
var literales = require('../../resources/literalesSearch.json');
var master_areas = require('../../resources/identificadores/areas.json');
var master_municipality = require('../../resources/identificadores/municipalities.json');

var tagsColors = require('../../resources/tagsColors.json');




var  _getTagClass  = function(tag,type,reset){
	try{

		if (tag==undefined) return '';


		switch (type){

			case "seasons":
			return "tags-temporada";

			case "lifestyles":
			return "tags-grey";

			default:
			return tagsColors[0][tag]['class'];

		}

	} catch(e){
		console.log("[SearchFilters.js] render() _getTagClass(tag): "+tag+" Error: "+e.message);
		return '';
	}
}


function _toggleClass(index, item) {

		var buttonClass = _getTagClass(item.value,item.name);

		if (item.getAttribute("data-active")=="true"){
			item.setAttribute("class",buttonClass);
			item.setAttribute("data-active", "false");
		}else{
			item.setAttribute("class",buttonClass+"-activo");
			item.setAttribute("data-active", "true");
		}

}


//
    // function searchByText(collection, text, language) {
    //     text = _.toLower(text);
    //       return _.filter(collection, function(object) {
    //         var string = object.term.lang[language]+'  '+object.title+'  '+( (subtitle in object)? object.subtitle[language]:'' );
    //         var string = getSearchString(object,language)
    //         console.log('[SearchFilters.js] searchByText() '+string);
    //         return _(string).toLower().includes(text);
    //       });
    // }






const nofilter = "*";
const nofilterDate = "";

var searcherType1;
var searcherType2;
var searcherType3;
var searcherType4;
var searcherType5;


var SearchFilters = React.createClass({


      getInitialState: function() {

        console.log ('[SearchFilters.js] getInitialState()...')
        console.log("[SearchFilters.js] this.props.items.length()..."+this.props.items.length);

         return {
           items : this.props.items,
           results: this.props.items,
           filters: {}
        }

        // this.baseState = this.state;

      },
      componentWillMount: function(){

        console.log("[SearchFilters.js] componentWillMount()...");

      },

      componentWillUpdate: function(nextProps, nextState){
        console.log("[SearchFilters.js] componentWillUpdate()...");
        console.log("[SearchFilters.js] this.props.items.length()..."+this.props.items.length);
        console.log("[SearchFilters.js] this.state.items.length()..."+this.state.items.length);
        console.log("[SearchFilters.js] this.state.results.length()..."+this.state.results.length);

      },

      componentWillReceiveProps: function(nextProps) {
        console.log("[SearchFilters.js] componentWillReceiveProps()...");
        console.log("[SearchFilters.js] componentWillReceiveProps() this.props.items.length()..."+this.props.items.length);

        console.log("[SearchFilters.js] componentWillReceiveProps() this.props.reset: "+this.props.reset+" nextProps.reset: "+nextProps.reset);

				if (nextProps.reset){
						this.setState({results:this.state.items,filters:[]});
				}
      },

      componentDidMount: function(){
        console.log("[SearchFilters.js] componentDidMount()...");
				this.initAccordion();
				this.initDatepicker();

      },
			initAccordion: function(element){
        console.log("[SearchFilters.js] initAccordion()...");


				// $(document).ready(function() {
				// 	$('#only-one [data-accordion]').accordion();
				//
				// 	$('#multiple [data-accordion]').accordion({
				// 		singleOpen: false
				// 	});
				//
				// 	$('#single[data-accordion]').accordion({
				// 		transitionEasing: 'cubic-bezier(0.455, 0.030, 0.515, 0.955)',
				// 		transitionSpeed: 200
				// 	});
				// });

					try{
						$.fn[pluginName] = function ( options ) {

								return this.each(function () {
										if (!$.data(this, 'plugin_' + pluginName)) {
												$.data(this, 'plugin_' + pluginName,
												new Accordion( this, options ));
										}
								});
						}

						$(this.refs['accordion-only-one']).children(['data-accordion']).accordion({singleOpen: false });        // console.log("[SearchFilters.js] initDatepicker()...   $(element).val "+$(element).val());

// $( ".selector" ).accordion( "option", { disabled: true } );

					}
					catch(e){
						console.log("[SearchFilters.js] initAccordion()...   error"+e.message);

					}



      },
			makeDatePicker: function(element) {
				console.log("[SearchFilters.js] makeDatePicker()...");
				// try{
        //
				//  var date = new Date();
			  //  date.setDate(date.getDate() - 1);
        //
				// 	   $(element).datepicker({
				// 		     autoclose: true,
				// 		     minDate: new Date(),
				// 		     startDate: date
				// 		   });
        //
        //
				// 			 $.datepicker.setDefaults($.datepicker.regional['es']);
        //
        //
				//  }
 				// catch(e){
 				// 	console.log("[SearchFilters.js] makeDatePicker()...   error"+e.message);
        //
 				// }
        //
        //
				// console.log("[SearchFilters.js] makeDatePicker()...");
				// try{
				// 	$.fn[pluginName] = function ( options ) {
        //
				// 			return this.each(function () {
				// 					if (!$.data(this, 'plugin_' + pluginName)) {
				// 							$.data(this, 'plugin_' + pluginName,
				// 							new Datepicker( this, options ));
				// 					}
				// 			});
				// 	}
        //
				//  var date = new Date();
				//  date.setDate(date.getDate() - 1);
        //
				// 		 $(element).datepicker({
				// 				 autoclose: true,
				// 				 minDate: new Date(),
				// 				 startDate: date
				// 			 });
        //
        //
				//  }
				// catch(e){
				// 	console.log("[SearchFilters.js] makeDatePicker()...   error"+e.message);
        //
				// }






		  },
      initDatepicker: function(){
        console.log("[SearchFilters.js] initDatepicker()...");

				//@TODO initializate DatePicker correctly

				// $.datepicker.setDefaults( $.datepicker.regional[this.props.language]);

        // console.log("[SearchFilters.js] initDatepicker()...   $(element).length "+$(element).length);

        // var date = new Date();
        // date.setDate(date.getDate() - 1);
        //
				// $(this.refs['date-filter-from']).datepicker({
				// 		dateFormat: 'dd.mm.yy',
				// 		prevText: '<i class="fa fa-angle-left"></i>',
				// 		nextText: '<i class="fa fa-angle-right"></i>',
				// 		onSelect: function( selectedDate )
				// 		{
        //
				// 			try{
				// 				//  $.datepicker.parseDate( 'dd.mm.yyyy', selectedDate );
				// 				 $(this.refs['date-filter-to']).datepicker('option', 'minDate', selectedDate);
				// 					var event = new Event('input', { bubbles: true });
				// 					this.dispatchEvent(event);
				// 			 } catch(e){
				// 				 console.log("[datepicker.js] parseDate invalid: "+e.message);
				// 			 }
        //
				// 		}
				// });
        //
        //
				// $(this.refs['date-filter-to']).datepicker({
				// 		dateFormat: 'dd.mm.yy',
				// 		prevText: '<i class="fa fa-angle-left"></i>',
				// 		nextText: '<i class="fa fa-angle-right"></i>',
				// 		onSelect: function( selectedDate )
				// 		{
        //
				// 			try{
				// 				//  $.datepicker.parseDate( 'dd.mm.yyyy', selectedDate );
				// 				$(this.refs['date-filter-from']).datepicker('option', 'maxDate', selectedDate);
				// 					var event = new Event('input', { bubbles: true });
				// 					this.dispatchEvent(event);
				// 			 } catch(e){
				// 				 console.log("[datepicker.js] parseDate invalid: "+e.message);
				// 			 }
				// 		}
				// });


        // console.log("[SearchFilters.js] initDatepicker()...   $(element).val "+$(element).val());

      },
			onButtonClick : function (event){
				console.log("[SearchFilters.js] onButtonClick()...");
				console.log("[SearchFilters.js] onButtonClick()... event.target.type: "+event.target.type);
				console.log("[SearchFilters.js] onButtonClick()... event.target.name: "+event.target.name);
				console.log("[SearchFilters.js] onButtonClick()... event.target.value: "+event.target.value);
				console.log("[SearchFilters.js] onButtonClick()... event.target.id: "+event.target.id);
				console.log("[SearchFilters.js] onButtonClick()... event.target: "+event.target.id);

				var buttonClass = _getTagClass(event.target.value,event.target.name);

			 if ($(event.target).attr("data-active")=="true"){
					$(event.target).toggleClass(buttonClass,true);
					$(event.target).toggleClass(buttonClass+"-activo",false);
					$(event.target).attr("data-active", "false");
				}else{
					$(event.target).toggleClass(buttonClass,true);
					$(event.target).toggleClass(buttonClass+"-activo",true);
					$(event.target).attr("data-active", "true");
				}

				this.onChange(event);

			},
			applyFilter: function (results,name){

				//    results =  ("touristic_subjects" in _filters && _filters["touristic_subjects"].value != nofilter) ?


					var _filters = this.state.filters;

					var output =

					  	(name in _filters && _filters[name].value != nofilter) ?

						 _.filter(results,

										 function(o) { 
											 				try {

																	// var filter_obj = o[_filters[name].id]["id"];
																	var filter_obj = o[_filters[name].id]["id"] == undefined ? o[_filters[name].id] : o[_filters[name].id]["id"];

																	var filter_sel = _filters[name].value;

																		if (typeof filter_obj=="string") {
																			return  _.intersection(filter_obj.split(),filter_sel).length;
																		} else{
																			return  _.intersection(filter_obj,filter_sel).length;
																		}

															} catch(e){return false}})

							: results;

					return output;

			},


      onChange: function(event){
        console.log("[SearchFilters.js] onChange()...");
        console.log("[SearchFilters.js] onChange()... event.target.type: "+event.target.type);

        console.log("[SearchFilters.js] onChange()... event.target.name: "+event.target.name);
        console.log("[SearchFilters.js] onChange()... event.target.id: "+event.target.id);
        console.log("[SearchFilters.js] onChange()... event.target.value: "+event.target.value);

        var _filters = this.state.filters;
        var target_value ='';




						if (event.target.name=='startdate'||event.target.name=='enddate'){
							if (event.target.value.length<10){
							 		event.target.value='';
							}
						}


            if (event.target.type=='checkbox'){
              target_value =
              $("input[name='"+event.target.name+"']:checkbox:checked").map(function () {
              return this.value;
               }).get();

               target_value= target_value.length==0? nofilter : target_value;

            }
						else if (event.target.type=='button'){
							target_value =

							$("button#"+event.target.name+"[data-active=true]").map(function () {
							return this.value;
							 }).get();

							 target_value= target_value.length==0? nofilter : target_value;

						}else{
              target_value = (event.target.value).split();
            }

            _filters[event.target.name] = {"id": event.target.id,  "value": target_value,"type":event.target.type};



					console.log("[SearchFilters.js] onChange()... event.target.value: "+event.target.value);


          var results = this.state.items; // results is total list.


          if (event.target.name == "islands" ||  ("islands" in _filters && _filters["islands"].value != nofilter) ){

            // results =  ("islands" in _filters && _filters["islands"].value != nofilter) ?  _.filter(results, function(o) { return  _.intersection(o[_filters["islands"].id],_filters["islands"].value).length})  : results;

						results = this.applyFilter(results,"islands");


            if (event.target.name == "islands"){
               if ("areas" in _filters) { _filters["areas"].value = nofilter};
               if ("municipalities" in _filters) {  _filters["municipalities"].value = nofilter};
               if ("distinctives" in _filters) {  _filters["distinctives"].value = nofilter};
               if ("activities" in _filters) {  _filters["activities"].value = nofilter};
               if ("touristic_subjects" in _filters) {  _filters["touristic_subjects"].value = nofilter};
               if ("lifestyles" in _filters) {  _filters["lifestyles"].value = nofilter};
               if ("seasons" in _filters) {  _filters["seasons"].value = nofilter};

            }

          }

          if (event.target.name == "areas" || ("areas" in _filters &&  _filters["areas"].value != nofilter)){
            // results =  ("areas" in _filters &&  _filters["areas"].value != nofilter) ? _.filter(results, [_filters["areas"].id, _filters["areas"].value]) : results;
            // results =  ("areas" in _filters && _filters["areas"].value != nofilter) ?    _.filter(results, function(o) { if (_filters["areas"].id in o)  { return  ~o[_filters["areas"].id]["id"].indexOf(_filters["areas"].value)} })  : results;


					  // results =  ("areas" in _filters && _filters["areas"].value != nofilter) ?    _.filter(results, function(o) { if (_filters["areas"].id in o)  { return  _.intersection(o[_filters["areas"].id]["id"],_filters["areas"].value).length} })  : results;


						results = this.applyFilter(results,"areas");



            if (event.target.name == "areas"){
               if ("municipalities" in _filters) {  _filters["municipalities"].value = nofilter};
							 if ("distinctives" in _filters) {  _filters["distinctives"].value = nofilter};

            }

          }

          if (event.target.name == "municipalities" ||  ("municipalities" in _filters && _filters["municipalities"].value != nofilter)){


						results = this.applyFilter(results,"municipalities");

						// results =  ("municipalities" in _filters && _filters["municipalities"].value != nofilter) ?    _.filter(results, function(o) { 
            //
						// 	try {
            //
						// 			var filter_obj = o[_filters["municipalities"].id]["id"];
						// 			var filter_sel = _filters["municipalities"].value;
            //
						// 				if (typeof objFilter=="string") {
						// 					return  _.intersection(filter_obj.split(),filter_sel).length;
						// 				} else{
						// 					return  _.intersection(filter_obj,filter_sel).length;
						// 				}
            //
						// 	} catch(e){return false}})  : results;



						if (event.target.name == "municipalities"){
							if ("distinctives" in _filters) {  _filters["distinctives"].value = nofilter};
							if ("term" in _filters) {  _filters["term"].value = nofilter};
							if ("touristic_subjects" in _filters) {  _filters["touristic_subjects"].value = nofilter};

						}

          }

          if (event.target.name == "companytypes" ||  ("companytypes" in _filters && _filters["companytypes"].value != nofilter) ){
            // results =  ("companytypes" in _filters && _filters["companytypes"].value != nofilter) ?  _.filter(results, [_filters["companytypes"].id, _filters["companytypes"].value])  : results;

            results =  ("companytypes" in _filters && _filters["companytypes"].value != nofilter) ?  _.filter(results, [_filters["companytypes"].id, _filters["companytypes"].value])  : results;

          }

          if (event.target.name == "activities" || ("activities" in _filters && _filters["activities"].value != nofilter)  ){
            // results =  ("activities" in _filters && _filters["activities"].value != nofilter) ?    _.filter(results, function(o) { return  ~o[_filters["activities"].id]["id"].indexOf(_filters["activities"].value)})  : results;

            //results =  ("activities" in _filters && _filters["activities"].value != nofilter) ?    _.filter(results, function(o) { return  _.intersection(o[_filters["activities"].id]["id"].split(),_filters["activities"].value).length})  : results;


						results = this.applyFilter(results,"activities");


          }



          if (event.target.name == "touristic_subjects" || ("touristic_subjects" in _filters && _filters["touristic_subjects"].value != nofilter)){
            // results =  ("touristic_subjects" in _filters && _filters["touristic_subjects"].value != nofilter) ?    _.filter(results, function(o) { return  ~o[_filters["touristic_subjects"].id]["id"].indexOf(_filters["touristic_subjects"].value)})  : results;

        //    results =  ("touristic_subjects" in _filters && _filters["touristic_subjects"].value != nofilter) ?    _.filter(results, function(o) {if (_filters["touristic_subjects"].id in o) { return  _.intersection(o[_filters["touristic_subjects"].id]["id"].split(),_filters["touristic_subjects"].value).length} })  : results;

						results = this.applyFilter(results,"touristic_subjects");




          }

          if (event.target.name == "languages" ||  ("languages" in _filters && _filters["languages"].value != nofilter)){
            // results =  ("languages" in _filters && _filters["languages"].value != nofilter) ? _.filter(results, function(o) { return  ~o[_filters["languages"].id].indexOf(_filters["languages"].value)})  : results;

            results =  ("languages" in _filters && _filters["languages"].value != nofilter) ? _.filter(results, function(o) { return  _.intersection(o[_filters["languages"].id],_filters["languages"].value).length})  : results;

          }

          if (event.target.name == "seasons" || ("seasons" in _filters && _filters["seasons"].value != nofilter) ){
            // results =  ("seasons" in _filters && _filters["seasons"].value != nofilter) ?    _.filter(results, function(o) { return  ~o[_filters["seasons"].id]["id"].indexOf(_filters["seasons"].value)})  : results;


            results =  ("seasons" in _filters && _filters["seasons"].value != nofilter) ?    _.filter(results, function(o) { if (_filters["seasons"].id in o)  { return  _.intersection(o[_filters["seasons"].id]["id"],_filters["seasons"].value).length}})  : results;


          }

          if (event.target.name == "lifestyles" || ("lifestyles" in _filters && _filters["lifestyles"].value != nofilter)){
            // results =  ("lifestyles" in _filters && _filters["lifestyles"].value != nofilter) ?    _.filter(results, function(o) { return  ~o[_filters["lifestyles"].id]["id"].indexOf(_filters["lifestyles"].value)})  : results;

            results =  ("lifestyles" in _filters && _filters["lifestyles"].value != nofilter) ?    _.filter(results, function(o) { if (_filters["lifestyles"].id in o) { return  _.intersection(o[_filters["lifestyles"].id]["id"],_filters["lifestyles"].value).length}})  : results;


          }


          if (event.target.name == "component_type" || ("component_type" in _filters && _filters["component_type"].value != nofilter)){

          //  results =  ("component_type" in _filters && _filters["component_type"].value != nofilter) ?    _.filter(results, function(o) { if (_filters["component_type"].id in o) { return  _.intersection(o[_filters["component_type"].id].split(),_filters["component_type"].value).length}})  : results;

						results = this.applyFilter(results,"component_type");


          }

          if (event.target.name == "term" || ("term" in _filters && _filters["term"].value != nofilter)){

            // results =  ("term" in _filters && _filters["term"].value != nofilter) ?    _.filter(results, function(o) { if (_filters["term"].id in o) { return  _.intersection(o[_filters["term"].id]["id"].split(),_filters["term"].value).length}})  : results;


						results = this.applyFilter(results,"term");


          }


          if (event.target.name == "distinctives" || ("distinctives" in _filters && _filters["distinctives"].value != nofilter)  ){

						// results =  ("distinctives" in _filters && _filters["distinctives"].value != nofilter) ?    _.filter(results, function(o) { if (_filters["distinctives"].id in o) { return  _.intersection(o[_filters["distinctives"].id]["id"].split(),_filters["distinctives"].value).length}})  : results;


						results = this.applyFilter(results,"distinctives");

						// results =  ("distinctives" in _filters && _filters["distinctives"].value != nofilter) ?    _.filter(results, function(o) {  try { return  _.intersection(o[_filters["distinctives"].id]["id"].(),_filters["distinctives"].value).length} catch(e){return false}})  : results;

          }




          if (event.target.name == "eventtypes" || ("eventtypes" in _filters && _filters["term"].value != nofilter)){

            // results =  ("eventtypes" in _filters && _filters["eventtypes"].value != nofilter) ?    _.filter(results, function(o) { if (_filters["eventtypes"].id in o) { return  _.intersection(o[_filters["eventtypes"].id]["id"].(),_filters["eventtypes"].value).length}})  : results;


						results = this.applyFilter(results,"eventtypes");

        	}

          if ("startdate" in _filters && _filters["startdate"].value != nofilterDate){


            if ("startdate" in _filters && _filters["startdate"].value != nofilterDate){
              _filters["startdate"].id = "start_date";
              var arr =  _.split(_filters["startdate"].value, '.', 3);
              _filters["startdate"].value =    _.trimStart(_.join([arr[2],arr[1], arr[0]], '-'), '-');
            }



            results =  _.filter(results, function(n) { return moment(n["end_date"]).isSameOrAfter(moment(_filters["startdate"].value))});


          }

          if ("enddate" in _filters && _filters["enddate"].value != nofilterDate){


            if ("enddate" in _filters && _filters["enddate"].value != nofilterDate){
              _filters["enddate"].id = "end_date";
              var arr =  _.split(_filters["enddate"].value, '.', 3);
              _filters["enddate"].value =   _.trimStart(_.join([arr[2],arr[1], arr[0]], '-'), '-');
            }


            results =  _.filter(results, function(n) { return moment(n["sorting_date"]).isSameOrBefore(moment(_filters["enddate"].value))});

          }



        // Apply filters
        if (results.length>0) {

          let _activefilters = _.filter(_.map(_filters),function (o) {return o.value!=nofilter});

          this.setState({
            lastfilter: event.target.name,
            firstfilter: (_activefilters.length == 1 && event.target.name in _filters) ? event.target.name : this.state.firstfilter,
            filters:  _filters,
            results:  results
          });


          console.log("[SearchFilters.js] onChange()... this.state.filters: "+this.state.filters);
          console.log("[SearchFilters.js] onChange()... this.state.items.length: "+this.state.items.length);
          console.log("[SearchFilters.js] onChange()... this.state.results.length: "+this.state.results.length);
          console.log("[SearchFilters.js] onChange()... this.state.lastfilter: "+this.state.lastfilter);
          console.log("[SearchFilters.js] onChange()... this.state.firstfilter: "+this.state.firstfilter);

          console.log("[SearchFilters.js] onChange()... _filters: "+JSON.stringify(_filters));
          console.log("[SearchFilters.js] onChange()... _activefilters: "+JSON.stringify(_activefilters));

          this.props.onSearchFilter(results);

        }
        else{

          console.log("[SearchFilters.js] onChange()... filtro no valido");


          this.props.onSearchFilter(results);

        }




      },
      componentDidUpdate: function(prevProps, prevState){

        console.log("[SearchFilters.js] componentDidUpdate()  prevState.filters: "+ JSON.stringify(prevState.filters)+" this.state.filters: "+ JSON.stringify(this.state.filters));
        console.log("[SearchFilters.js] componentDidUpdate()  prevState.lastfilter: "+ prevState.lastfilter+" this.state.lastfilter: "+  this.state.lastfilter+" this.state.firstfilter: "+  this.state.firstfilter);

				this.initAccordion();

      },
			onResetFilters: function(){

					console.log("[SearchFilters.js] onResetFilters() reset state...");
					// this.setState(this.baseState);

					var _filters = this.state.filters;


					_.forEach(_filters,function(o){ o.value="*";

								if (o.type=='checkbox'){
									$("input[name='"+o.id+"']:checkbox:checked").prop("checked",false);
								}
								else if (o.type=='button'){
									$("button#"+o.id+"[data-active=true]").map(_toggleClass);
									$("button#"+o.id+"[data-active=true]").attr("data-active","false");
								}
					});


					this.props.onResetFilters()

			},
			onDateKeyPress: function(event){
				console.log("[SearchFilters.js] onKeyPress()... event.charCode: "+event.charCode);

				if (event.charCode==8){
					event.preventDefault();
					event.target.value="";
				}
				else{
					console.log("[SearchFilters.js] onKeyPress()... nothing to do...: ");
				}


			},
			render() {

          console.log ('[SearchFilters.js] render this.state.filters: '+JSON.stringify(this.state.filters));
          // console.log ('[SearchFilters.js] render this.state.results : '+this.state.results);






          var items = [];
           searcherType1 = this.props.searcherType=="1"? true: false;
           searcherType2 = this.props.searcherType=="2"? true: false;
           searcherType3 = this.props.searcherType=="3"? true: false;
           searcherType4 = this.props.searcherType=="4"? true: false;
           searcherType5 = this.props.searcherType=="5"? true: false;

          var language = this.props.language;

          var section = this.props.section;

          var  _get  = function(txt){
            try{
            return literales[0][txt][language];
            } catch(e){
              console.log("[SearchFilters.js] render() _get(txt): "+txt+" Error: "+e.message);
              return txt;
            }
          }

            let _filters = this.state.filters;
            let lastfilter = this.state.lastfilter;
            let firstfilter = this.state.firstfilter;

            let selected = [];
            let show = [];

						var reset = this.props.reset;
						var clear = [];

            console.log ('[SearchFilters.js] render this.props.searcherType : '+this.props.searcherType);

            //
            // if (reset){
            //   console.log("[SearchFilters.js] render() reset state...");
            //   // this.setState(this.baseState);
            //   _.forEach(_filters,function(o){ o.value="*";
            //
						// 				if (o.type=='checkbox'){
						// 					$("input[name='"+o.id+"']:checkbox:checked").prop("checked",false);
						// 				}
						// 				else if (o.type=='button'){
						// 					$("button#"+o.id+"[data-active=true]").toggleClass();
            //
						// 					// $("button#"+o.id+"[data-active=true]").map((item,index) => {item.removeClass()});
						// 					$("button#"+o.id+"[data-active=true]").attr("data-active","false");
            //
						// 				}
            //
						// 	});
            // }



            selected.islands = (lastfilter == "islands") ? true: false;
            selected.areas = (lastfilter == "areas") ? true: false;
            selected.municipalities = (lastfilter == "municipalities") ? true: false;
            selected.seasons = (lastfilter == "seasons") ? true: false;
            selected.lifestyles = (lastfilter == "lifestyles") ? true: false;
            selected.touristic_subjects = (lastfilter == "touristic_subjects") ? true: false;
            selected.companytypes = (lastfilter == "companytypes") ? true: false;
            selected.activities = (lastfilter == "activities") ? true: false;
            selected.distinctives = (lastfilter == "distinctives") ? true: false;
            selected.startdate = (lastfilter == "startdate") ? true: false;
            selected.enddate = (lastfilter == "enddate") ? true: false;
            selected.term = (lastfilter == "term") ? true: false;
						selected.eventtypes = (lastfilter == "eventtypes") ? true: false;
						selected.component_type = (lastfilter == "component_type") ? true: false;


            items.complete = this.state.items;
            items.results = this.state.results;

          //@TODO: include all show filters in diferent searchers or unify

          if (searcherType1){ // RRTT

            items.islands = this.state.items ; // All Items (initial list)

            items.areas = firstfilter=='areas' && selected.areas  ? this.state.items : this.state.results;
            items.municipalities = firstfilter=='municipalities'  && selected.municipalities ? this.state.items : this.state.results;
            items.seasons = firstfilter=='seasons'  && selected.seasons ? this.state.items : this.state.results;
            items.lifestyles = firstfilter=='lifestyles'  && selected.lifestyles ? this.state.items : this.state.results;
            items.touristic_subjects = firstfilter=='touristic_subjects'  && selected.touristic_subjects ? this.state.items : this.state.results;
            items.component_type = firstfilter=='component_type'  && selected.component_type ? this.state.items : this.state.results;
            items.term = firstfilter=='term'  && selected.term ? this.state.items : this.state.results;

            show.municipalities = (_.find(items.municipalities, 'municipalities')!=undefined);
            show.seasons = (_.find(items.seasons, 'seasons')!=undefined);
            show.lifestyles = (_.find(items.lifestyles, 'lifestyles')!=undefined);
            show.touristic_subjects = (_.find(items.touristic_subjects, 'touristic_subjects')!=undefined);
            show.areas = (_.find(items.areas, 'areas')!=undefined);

						if (show.municipalities) {

							show.seasons = false;
							show.lifestyles = false;
							show.touristic_subjects = false;
						}
            // Functional Rules

            if (('eventos-deportivos,patrimonio-cultural,artesania,patrimonio-natural,birdwatching,senderismo,cicloturismo,instalaciones-nauticas,relax,enoturismo-y-oleoturismo,donde-dormir,fiestas-y-tradiciones,turismo-sanitario,playas').indexOf(section)>=0){

							  show.term = (_.find(items.term, 'term')!=undefined);
                show.areas = false;
                show.seasons = false;
                show.lifestyles = false;
                show.touristic_subjects = false;
                if (('eventos-deportivos,enoturismo-y-oleoturismo').indexOf(section)>=0) {show.touristic_subjects = true; show.term = false};

								// if (section=='enoturismo-y-oleoturismo') {show.touristic_subjects = true};

            }else{
                show.term = ("component_type" in _filters && _filters["component_type"].value=="rrtt")?true:false;
            }

          }
          else if (searcherType2) { // COMPANIES

            // items.islands = firstfilter=='islands'  && selected.islands ? this.state.items : this.state.results;
            items.islands = this.state.items ;

            items.municipalities = firstfilter=='municipalities'  && selected.municipalities ? this.state.items : this.state.results;
            items.companytypes = firstfilter=='companytypes'  && selected.companytypes ? this.state.items : this.state.results;
            items.activities = firstfilter=='activities'  && selected.activities ? this.state.items : this.state.results;
            items.distinctives = firstfilter=='distinctives'  && selected.distinctives ? this.state.items : this.state.results;

            show.activities = ("activity_types" in items.activities[0]);
            show.distinctives = ("distinctive" in items.distinctives[0]);

          }
          else if (searcherType3) { // PUBLICATIONS


            // items.islands = firstfilter=='islands'  && selected.islands ? this.state.items : this.state.results;
            items.islands = this.state.items ;

            items.municipalities = firstfilter=='municipalities'  && selected.municipalities ? this.state.items : this.state.results;
            items.seasons = firstfilter=='seasons'  && selected.seasons ? this.state.items : this.state.results;
            items.lifestyles = firstfilter=='lifestyles'  && selected.lifestyles ? this.state.items : this.state.results;
            items.touristic_subjects = firstfilter=='touristic_subjects'  && selected.touristic_subjects ? this.state.items : this.state.results;
            items.languages = firstfilter=='languages'  && selected.languages ? this.state.items : this.state.results;

          }

          else if (searcherType4){  // AGENDA
            // items.islands = firstfilter=='areas'  && selected.islands ? this.state.items : this.state.results;

            items.islands = this.state.items ;
            items.areas = firstfilter=='areas'  && selected.areas ? this.state.items : this.state.results;
            items.municipalities = firstfilter=='areas'  && selected.municipalities ? this.state.items : this.state.results;
						items.eventtypes = firstfilter=='eventtypes'  && selected.eventtypes ? this.state.items	 : this.state.results;


          }
          else if (searcherType5){ // RRTT

            items.islands = this.state.items ; // All Items (initial list)

            // items.areas = firstfilter=='areas' && selected.areas  ? this.state.items : this.state.results;
            // items.municipalities = firstfilter=='municipalities'  && selected.municipalities ? this.state.items : this.state.results;
            // items.seasons = firstfilter=='seasons'  && selected.seasons ? this.state.items : this.state.results;
            // items.lifestyles = firstfilter=='lifestyles'  && selected.lifestyles ? this.state.items : this.state.results;
            // items.touristic_subjects = firstfilter=='touristic_subjects'  && selected.touristic_subjects ? this.state.items : this.state.results;
            // items.component_type = firstfilter=='component_type'  && selected.component_type ? this.state.items : this.state.results;
            // items.term = firstfilter=='term'  && selected.term ? this.state.items : this.state.results;


            items.areas = firstfilter=='areas' && selected.areas  ? this.state.items : this.state.results;
            items.municipalities = firstfilter=='municipalities'  && selected.municipalities ? this.state.items : this.state.results;
            items.seasons = firstfilter=='seasons'  && selected.seasons ? this.state.items : this.state.results;
            // items.lifestyles = items.results;
            items.touristic_subjects = firstfilter=='touristic_subjects'  && selected.touristic_subjects ? this.state.items : this.state.results;
            items.component_type = firstfilter=='component_type'  && selected.component_type ? this.state.items : this.state.results;
            items.term = firstfilter=='term'  && selected.term ? this.state.items : this.state.results;

            show.municipalities = (_.find(items.municipalities, 'municipalities')!=undefined);
            show.seasons = (_.find(items.seasons, 'seasons')!=undefined);
            show.lifestyles = (_.find(items.lifestyles, 'lifestyles')!=undefined);
            show.touristic_subjects = (_.find(items.touristic_subjects, 'touristic_subjects')!=undefined);
            show.areas = (_.find(items.areas, 'areas')!=undefined);

            // Functional Rules
            show.term = ("component_type" in _filters && _filters["component_type"].value=="rrtt")?true:false;

          }


					if (show.term) {clear.term = selected.component_type?true:false;}


          console.log ('[SearchFilters.js] render return...');



          // RENDER  searcherType5

          if (searcherType5){


            var aIslands = _.uniq(_.flattenDeep(_.sortBy(_.map(items.islands, 'islands'))));

            var aSeasons = [];
            aSeasons.complete = _.sortBy(_.zip(_.uniq(_.flattenDeep(_.map(items.complete,'seasons.id'))),_.uniq(_.flattenDeep(_.map(items.complete,'seasons.lang.'+language)))));
            aSeasons.filtered = _.join(_.uniq(_.flattenDeep(_.map(items.results,'seasons.id'))));
            //aSeasons.filtered = _.sortBy(_.zip(_.uniq(_.flattenDeep(_.map(items.seasons,'seasons.id'))),_.uniq(_.flattenDeep(_.map(items.seasons,'seasons.lang.'+language)))));

            var aLifestyles = [];
            aLifestyles.complete = _.sortBy(_.zip(_.uniq(_.flattenDeep(_.map(items.complete,'lifestyles.id'))),_.uniq(_.flattenDeep(_.map(items.complete,'lifestyles.lang.'+language)))));
            aLifestyles.filtered = _.join(_.uniq(_.flattenDeep(_.map(items.results,'lifestyles.id'))));

            var aTouristic_subjects = [];
            aTouristic_subjects.complete = _.sortBy(_.zip(_.uniq(_.flattenDeep(_.map(items.complete,'touristic_subjects.id'))),_.uniq(_.flattenDeep(_.map(items.complete,'touristic_subjects.lang.'+language)))));
            aTouristic_subjects.filtered = _.join(_.uniq(_.flattenDeep(_.map(items.results,'touristic_subjects.id'))));


            var aComponent_type = [];
            aComponent_type.complete = _.sortBy(_.uniq(_.flattenDeep(_.map(items.complete,'component_type'))));
            aComponent_type.filtered = _.join(_.uniq(_.flattenDeep(_.map(items.results,'component_type'))));


            var aTerm = [];
            aTerm.complete = _.sortBy(_.zip(_.uniq(_.flattenDeep(_.map(items.complete,'term.id'))),_.uniq(_.flattenDeep(_.map(items.complete,'term.lang.'+language)))));
            aTerm.filtered = _.join(_.uniq(_.flattenDeep(_.map(items.results,'term.id'))));



            //  {_.zip(_.uniq(_.flattenDeep(_.sortBy(_.map(items.term,'term.id')))),_.uniq(_.flattenDeep(_.sortBy(_.map(items.term,'term.lang.'+language))))).map((item,index) =>

            // {_.uniq(_.flattenDeep(_.sortBy(_.map(items.component_type, 'component_type')))).map((item, index) =>

            // {_.zip(_.uniq(_.flattenDeep(_.sortBy(_.map(items.touristic_subjects,'touristic_subjects.id')))),_.uniq(_.flattenDeep(_.sortBy(_.map(items.touristic_subjects,'touristic_subjects.lang.'+language))))).map((item,index) =>

            // {_.zip(_.uniq(_.flattenDeep(_.sortBy(_.map(items.lifestyles,'lifestyles.id')))),_.uniq(_.flattenDeep(_.sortBy(_.map(items.lifestyles,'lifestyles.lang.'+language))))).map((item,index) =>


        //    aSeasons.disabled = _.difference(aSeasons.complete,aSeasons.filtered);

				// function buttons(aFiltered,item,index){
				//
				// 	if (item[0]!=undefined){
				// 		if (aFiltered.indexOf(item[0])>=0)
				// 				return (
				// 				<button  key={index} onChange={this.onChange} type="button" id="seasons" name="seasons" value={item[0]} className="btn-u btn-u-sm rounded-4x btn-u-darks">{item[1]}</button>
				// 				);
				// 		else{
				// 				return (
				// 				<button  key={index} onChange={this.onChange} type="button" id="seasons" name="seasons" value={item[0]} className="btn-u btn-u-sm rounded-4x btn-u-darks" style={{textDecoration: 'line-through'}} disabled="true">{item[1]}</button>
				// 			);
				// 		}
				// 	}
				//
				// }


				function itemDisabled(aFiltered,item,reset){
					return (aFiltered.indexOf(item)>=0 || reset==true ? false: true);
				}

				function classDisabled(aFiltered,item,reset){
					return (aFiltered.indexOf(item)>=0 || reset==true ? '': 'disabled');
				}

				function classHidden(item){
					return (item == undefined ? 'hidden': '');
				}

				// var styleDisabled = {textDecoration: 'line-through'};

        if (true==true){
        return(


                          	<section id="only-one" data-accordion-group ref='accordion-only-one'>

                                 <section data-accordion >
                                   <button data-control className="acordeon"><span className="titulo">{_get('queislas')}</span></button>
                                   <div data-content>
                                         {aIslands.map((item, index) =>
                                         <article key={index} onChange={this.onChange}>
                                         <input type="checkbox" data-reset={reset} className="checka" id="islands" name="islands" value={item} /><i></i>{capitalizedString(item)}
                                         <span className={"is"+item.substring(0, 2)}></span>
                                         </article>
                                         )}
                                   </div>
                                 </section>


                                 <section data-accordion>
                                       <button data-control className="acordeon"><span className="titulo">{_get('cuandoviajas')}</span></button>
                                       <div data-content>
                                       <div  className="crt">
                                        {aSeasons.complete.map((item,index) =>
																					<button  key={index} data-reset={reset} onChange={this.onChange} onClick={this.onButtonClick} type="button" id="seasons" name="seasons" value={item[0]} data-active="" className={_getTagClass(item[0],"seasons",reset)+" "+classDisabled(aSeasons.filtered,item[0],reset)+" "+classHidden(item[0])} disabled={itemDisabled(aSeasons.filtered,item[0],reset)}>{item[1]}</button>
                                        )}
                                        </div>
                                       </div>
                                 </section>


                                  <section data-accordion>
                                       <button data-control className="acordeon"> <span className="titulo">{_get('conquienviajas')}</span></button>
                                       <div data-content>
                                        <div  className="crt">
                                          {aLifestyles.complete.map((item,index) =>
                                            <button  key={index} data-reset={reset} onChange={this.onChange} onClick={this.onButtonClick} type="button" id="lifestyles" name="lifestyles" value={item[0]} data-active="" className={_getTagClass(item[0],"lifestyles",reset)+" "+classDisabled(aLifestyles.filtered,item[0],reset)+" "+classHidden(item[0])}   disabled={itemDisabled(aLifestyles.filtered,item[0],reset)}>{item[1]}</button>
                                          )}
                                        </div>
                                       </div>
                                 </section>


                                 <section data-accordion>
                                       <button data-control className="acordeon"><span className="titulo">{_get('queteinteresa')}</span></button>
                                       <div data-content>
                                           <div  className="crt">
                                             {aTouristic_subjects.complete.map((item,index) =>
																							 <button  key={index} data-reset={reset} onChange={this.onChange} onClick={this.onButtonClick} type="button" id="touristic_subjects" name="touristic_subjects" value={item[0]} data-active="" className={_getTagClass(item[0],"touristic_subjects",reset)+" "+classDisabled(aTouristic_subjects.filtered,item[0],reset)+" "+classHidden(item[0])}   disabled={itemDisabled(aTouristic_subjects.filtered,item[0]),reset}>{item[1]}</button>
                                             )}
                                            </div>
                                        </div>
                                  </section>


                                  <section data-accordion>
                                    <button data-control className="acordeon"><span className="titulo">{_get('queinfobuscas')}</span></button>
                                    <div data-content>
                                    {aComponent_type.complete.map((item,index) =>
                                                <article key={index} onChange={this.onChange} className={classHidden(item)+" "+classDisabled(aComponent_type.filtered,item,reset)}>
                                                   <input type="checkbox" data-reset={reset} value={item} id="component_type" name="component_type" className="checka" disabled={itemDisabled(aComponent_type.filtered,item,reset)}/>
                                                   <strong>{_get(item)}</strong>
                                                </article>
                                     )}
                                     </div>
                                  </section>

																	{show.term &&
																	<section data-accordion>
																		<button data-control className="acordeon"><span className="titulo">{_get('masespecifico')}</span></button>
																		<div data-content>
																		{aTerm.complete.map((item,index) =>
																							<article key={index} onChange={this.onChange}  className={classHidden(item[0])+" "+classDisabled(aTerm.filtered,item[0],reset||clear.term)}>
																								<input type="checkbox" data-reset={reset||clear.term} value={item[0]} id="term" name="term" className="checka" disabled={itemDisabled(aTerm.filtered,item[0],reset||clear.term)}/>
																								<strong>{_get(item[1])}</strong>
																							</article>
																		 )}
																		</div>
																	</section>
																	}


																	  <div className="clearfix margin-bottom-20"></div>
																		<button className="btn-u btn-u-green  btn-u-green col-sm-5" onClick={this.onResetFilters}>{_get('limpiar')}</button>


                            </section>






                  );



          }else{
            return(

                  <div>

                                  <div className="panel panel-default colfilter">
                        							<div className="panel-heading">
                        								<p className="panel-title">
                        									<a className="accordion-toggle bloq" data-toggle="collapse" data-parent="#accordion-1" href="#collapse-g1">
                                          {_get('queislas')}
                                          </a>
                        								</p>
                        							</div>

                                  		<div id="collapse-g1" className="panel-collapse collapse">
                        								<div className="panel-body sinma">
                                                <ul className="list-op"  onChange={this.onChange}>
                                                {aIslands.map((item, index) =>

                                                <li  key={index}><label className="checkbox"><input type="checkbox" id="islands" name="islands" value={item}/><i></i>{capitalizedString(item)}</label></li>
                                                )}
                                      				 </ul>
                        								</div>
                        							</div>

                                 </div>




                                <div className="panel panel-default colfilter">
                        							<div className="panel-heading">
                        								<p className="panel-title">
                        									<a className="accordion-toggle bloq" data-toggle="collapse" data-parent="#accordion-1" href="#collapse-g2">
                                          {_get('cuandoviajas')}
                        									</a>
                        								</p>
                        							</div>
                        							<div id="collapse-g2" className="panel-collapse collapse">
                        								<div className="panel-body sinma">
                                                 <ul className="list-op" >
                                                 {aSeasons.complete.map((item,index) =>
                                                   <li  key={index}>
                                                       {aSeasons.filtered.indexOf(item[0])>=0 ?
                                                       <label className="checkbox">
                                                       <input type="checkbox" value={item[0]} id="seasons" name="seasons" onChange={this.onChange} />
                                                       <i></i>{item[1]}</label>
                                                       :
                                                       <label className="checkbox" style={{textDecoration: 'line-through'}}>
                                                       <input type="checkbox" value={item[0]} id="seasons" name="seasons" onChange={this.onChange} disabled="true"/>
                                                       <i></i>{item[1]}</label>
                                                       }
                                                   </li>
                                                  )}
                                				         </ul>
                        								</div>
                        							</div>
                  						 </div>


                                <div className="panel panel-default colfilter">
                        							<div className="panel-heading">
                        								<p className="panel-title">
                        									<a className="accordion-toggle bloq" data-toggle="collapse" data-parent="#accordion-1" href="#collapse-g3">
                                          {_get('conquienviajas')}
                        									</a>
                        								</p>
                        							</div>
                        							<div id="collapse-g3" className="panel-collapse collapse">
                        								<div className="panel-body sinma">
                                               <ul className="list-op">
                                               {aLifestyles.complete.map((item,index) =>
                                                 <li  key={index}>
                                                       {aLifestyles.filtered.indexOf(item[0])>=0 ?
                                                       <label className="checkbox">
                                                       <input type="checkbox" value={item[0]} id="lifestyles" name="lifestyles" onChange={this.onChange} />
                                                       <i></i>{item[1]}</label>
                                                       :
                                                       <label className="checkbox" style={{textDecoration: 'line-through'}}>
                                                       <input type="checkbox" value={item[0]} id="lifestyles" name="lifestyles" onChange={this.onChange} disabled="true"/>
                                                       <i></i>{item[1]}</label>
                                                       }
                                                 </li>
                                                )}
                                      				 </ul>
                        								</div>
                        							</div>
                  						</div>



                                <div className="panel panel-default colfilter">
                        							<div className="panel-heading">
                        								<p className="panel-title">
                        									<a className="accordion-toggle bloq" data-toggle="collapse" data-parent="#accordion-1" href="#collapse-g4">
                                          {_get('queteinteresa')}
                        									</a>
                        								</p>
                        							</div>
                        							<div id="collapse-g4" className="panel-collapse collapse">
                        								<div className="panel-body sinma">
                                              <ul className="list-op">
                                              {aTouristic_subjects.complete.map((item,index) =>
                                                   <li  key={index}>
                                                       {aTouristic_subjects.filtered.indexOf(item[0])>=0 ?
                                                       <label className="checkbox">
                                                       <input type="checkbox" value={item[0]} id="touristic_subjects" name="touristic_subjects" onChange={this.onChange} />
                                                       <i></i>{item[1]}</label>
                                                       :
                                                       <label className="checkbox" style={{textDecoration: 'line-through'}}>
                                                       <input type="checkbox" value={item[0]} id="touristic_subjects" name="touristic_subjects" onChange={this.onChange} disabled="true"/>
                                                       <i></i>{item[1]}</label>
                                                       }
                                                   </li>
                                               )}
                                              </ul>
                        								</div>
                        							</div>
                  						</div>




                              <div className="panel panel-default colfilter">
                          							<div className="panel-heading">
                          								<p className="panel-title">
                          									<a className="accordion-toggle bloq" data-toggle="collapse" data-parent="#accordion-1" href="#collapse-g5">
                                            {_get('queinfobuscas')}
        							                      </a>
                          								</p>
                          							</div>
                          							<div id="collapse-g5" className="panel-collapse collapse">
                          								<div className="panel-body sinma">
                                              <ul className="list-op">
                                              {aComponent_type.complete.map((item,index) =>
                                                   <li  key={index}>
                                                       {aComponent_type.filtered.indexOf(item)>=0 ?
                                                       <label className="checkbox">
                                                       <input type="checkbox" value={item} id="component_type" name="component_type" onChange={this.onChange} />
                                                       <i></i>{_get(item)}</label>
                                                       :
                                                       <label className="checkbox" style={{textDecoration: 'line-through'}}>
                                                       <input type="checkbox" value={item} id="component_type" name="component_type" onChange={this.onChange} disabled="true"/>
                                                       <i></i>{_get(item)}</label>
                                                       }
                                                   </li>
                                               )}
                                              </ul>
                            							</div>
                          							</div>
                          			</div>

                                {show.term &&
                                <div className="panel panel-default colfilter">
                            							<div className="panel-heading">
                            								<p className="panel-title">
                            									<a className="accordion-toggle bloq" data-toggle="collapse" data-parent="#accordion-1" href="#collapse-g6">
                                              {_get('masespecifico')}
          							                      </a>
                            								</p>
                            							</div>
                            							<div id="collapse-g6" className="panel-collapse collapse">
                            								<div className="panel-body sinma">
                                                 <ul className="list-op">
                                                 {aTerm.complete.map((item,index) =>
                                                      <li  key={index}>
                                                          {aTerm.filtered.indexOf(item[0])>=0 ?
                                                          <label className="checkbox">
                                                          <input type="checkbox" value={item[0]} id="term" name="term" onChange={this.onChange} />
                                                          <i></i>{item[1]}</label>
                                                          :
                                                          <label className="checkbox" style={{textDecoration: 'line-through'}}>
                                                          <input type="checkbox" value={item[0]} id="term" name="term" onChange={this.onChange} disabled="true"/>
                                                          <i></i>{item[1]}</label>
                                                          }
                                                      </li>
                                                  )}
                                                 </ul>
                              							</div>
                            							</div>
                            	 </div>
                               }

                  </div>

                      );

              }


} else{

  //  {_.zip(_.uniq(_.flattenDeep(_.sortBy(_.map(items.term,'term.id')))),_.uniq(_.flattenDeep(_.sortBy(_.map(items.term,'term.lang.'+language))))).map((item,index) =>

					var colsnumber = 2;
					var coloffset =0;

					if (show.areas) {coloffset = 12-(colsnumber); colsnumber=colsnumber+4};
					if (show.municipalities) {coloffset = 12-(colsnumber); colsnumber=colsnumber+6};
					if (show.term) {coloffset = 12-(colsnumber); colsnumber=colsnumber+4};
					if (show.seasons) {coloffset = 12-(colsnumber); colsnumber=colsnumber+2};
					if (show.lifestyles) {coloffset = 12-(colsnumber); colsnumber=colsnumber+2};
					if (show.touristic_subjects) {coloffset = 12-(colsnumber); colsnumber=colsnumber+2};

					console.log ('[SearchFilters.js] render colsnumber: '+colsnumber+' coloffset: '+coloffset);

          return (


                  <div>
                  {searcherType1 &&
                  <div className="cg">
                   <section className="col-md-2 no-padding fsml">
                     <select className="form-control" id="islands" name="islands"  onChange={this.onChange} value={("islands" in _filters) ? _filters["islands"].value: nofilter}>
                           <option className="cglabel" value={nofilter}  >{_get("islands")}</option>
                           {_.uniq(_.flattenDeep(_.sortBy(_.map(items.islands, 'islands')))).map((item, index) =>
                           <option key={index} value={item} >{capitalizedString(_get(item))}</option>
                           )}
                     </select>
                   </section>
                   {show.areas &&
                    <section className="col-md-4">
                      <select className="form-control" id="areas" name="areas" onChange={this.onChange} value={("areas" in _filters) ? _filters["areas"].value: nofilter}>
                          <option value={nofilter}>{_get("areas")}</option>
                          {_.sortBy(_.zip(_.uniq(_.flattenDeep(_.map(items.areas,'areas.id'))),_.uniq(_.flattenDeep(_.map(items.areas,'areas.names'))))).map((item,index) =>
                              <option key={index} value={item[0]}>{item[1]}</option>
                           )}
                      </select>
                    </section>
                   }
                    {show.municipalities &&
                    <section className="col-md-6 fsmr" >
                      <select className="form-control" id="municipalities"  name="municipalities" onChange={this.onChange} value={("municipalities" in _filters) ? _filters["municipalities"].value: nofilter}>
                          <option value={nofilter} >{_get("municipalities")}</option>
                          {_.sortBy(_.zip(_.uniq(_.flattenDeep(_.map(items.municipalities,'municipalities.id'))),_.uniq(_.flattenDeep(_.map(items.municipalities,'municipalities.names'))))).map((item,index) =>
                              {  if (item[0]!=undefined){
                                  return (<option key={index} value={item[0]}>{item[1]}</option>);
                                }
                              }
                           )}
                      </select>
                    </section>
                    }
                    {show.term &&
                     <section className="col-md-4 fsmr">
                       <select className="form-control" id="term" name="term" onChange={this.onChange} value={("term" in _filters) ? _filters["term"].value: nofilter}>
                           <option value={nofilter}>{_get("term")}</option>
                           {_.sortBy(_.zip(_.uniq(_.flattenDeep(_.map(items.term,'term.id'))),_.uniq(_.flattenDeep(_.map(items.term,'term.lang.'+language))))).map((item,index) =>
                               {  if (item[0]!=undefined){
                                   return (<option key={index} value={item[0]}>{item[1]}</option>);
                                  }
                               }
                            )}
                       </select>
                     </section>
                    }
                  {show.seasons &&
                    <section className="col-sm-2">
                      <select className="form-control" id="seasons" name="seasons" onChange={this.onChange} value={("seasons" in _filters) ? _filters["seasons"].value: nofilter}>
                          <option value={nofilter}>{_get("seasons")}</option>
                         {_.sortBy(_.zip(_.uniq(_.flattenDeep(_.map(items.seasons,'seasons.id'))),_.uniq(_.flattenDeep(_.map(items.seasons,'seasons.lang.'+language))))).map((item,index) =>
                           <option key={index} value={item[0]}>{item[1]}</option>
                          )}
                      </select>
                    </section>
                   }

                   {show.lifestyles &&
                    <section className="col-sm-2">
                      <select className="form-control" id="lifestyles" name="lifestyles" onChange={this.onChange} value={("lifestyles" in _filters) ? _filters["lifestyles"].value: nofilter}>
                          <option value={nofilter}>{_get("lifestyles")}</option>
                         {_.sortBy(_.zip(_.uniq(_.flattenDeep(_.map(items.lifestyles,'lifestyles.id'))),_.uniq(_.flattenDeep(_.map(items.lifestyles,'lifestyles.lang.'+language))))).map((item,index) =>
                           <option key={index} value={item[0]}>{item[1]}</option>
                          )}
                      </select>
                    </section>
                  }

                  {show.touristic_subjects  &&
                     <section className={"col-sm-"+coloffset+" fsmr"}>
                       <select className="form-control" id="touristic_subjects" name="touristic_subjects" onChange={this.onChange} value={("touristic_subjects" in _filters) ? _filters["touristic_subjects"].value: nofilter}>
                           <option value={nofilter}>{_get("touristic_subjects")}</option>
                          {_.sortBy(_.zip(_.uniq(_.flattenDeep(_.map(items.touristic_subjects,'touristic_subjects.id'))),_.uniq(_.flattenDeep(_.map(items.touristic_subjects,'touristic_subjects.lang.'+language))))).map((item,index) =>
                            <option key={index} value={item[0]}>{item[1]}</option>
                           )}
                       </select>
                     </section>
                  }


                  </div>
                }

                {searcherType2 && //Isla / Municipio / Tipo de compañía / Tipo de actividad / Distintivo.

                <div className="cg">

                <section className="col-sm-2 fsml no-padding fsml">
                  <select className="form-control" id="island" name="islands"  onChange={this.onChange} value={("islands" in _filters) ? _filters["islands"].value: nofilter}>
                       <option className="cglabel" value={nofilter} >{_get("islands")}</option>
                       {_.sortedUniqBy(_.sortBy(items.islands,'island'),'island').map((item, index) =>
                       <option key={index} value={item.island} >{capitalizedString(_get(item.island))}</option>
                       )}
                  </select>
                </section>

                 <section className="col-sm-4" >
                   <select className="form-control" id="municipality"  name="municipalities" onChange={this.onChange} value={("municipalities" in _filters) ? _filters["municipalities"].value: nofilter}>
                       <option value={nofilter} >{_get("municipalities")}</option>
                       {_.sortedUniqBy(_.sortBy(items.municipalities,'municipality.id'),'municipality.id').map((item, index) =>
                          {  if ("municipality" in item){
                              return (<option key={index} value={item.municipality.id}>{item.municipality.name}</option>);
                            }
                          }
                       )}
                   </select>
                 </section>

                 {show.activities &&
                 <section className="col-sm-6 fsmr">
                   <select className="form-control" id="activity_types" name="activities" onChange={this.onChange} value={("activities" in _filters) ? _filters["activities"].value: nofilter}>
                       <option value={nofilter}>{_get("activities")}</option>
                       {_.sortBy(_.zip(_.uniq(_.flattenDeep(_.map(items.activities,'activity_types.id'))),_.uniq(_.flattenDeep(_.map(items.activities,'activity_types.lang.'+language))))).map((item,index) =>
                       <option key={index} value={item[0]}>{item[1]}</option>
                       )}
                   </select>
                 </section>
                }

                 {show.distinctives &&
                 <section className="col-sm-6 fsmr">
                   <select className="form-control" id="distinctive" name="distinctives" onChange={this.onChange} value={("distinctives" in _filters) ? _filters["distinctives"].value: nofilter}>
                       <option value={nofilter}>{_get("distinctives")}</option>
											 {_.sortBy(_.zip(_.uniq(_.flattenDeep(_.map(items.distinctives,'distinctive.id'))),_.uniq(_.flattenDeep(_.map(items.distinctives,'distinctive.lang.'+language))))).map((item,index) =>
													 {  if (item[0]!=undefined){
															 return (<option key={index} value={item[0]}>{item[1]}</option>);
															}
													 }
                       )}
                   </select>
                 </section>
                 }

                </div>

                }

                {searcherType3 && //Isla / Municipio / Tipo de compañía / Tipo de actividad / Distintivo.

                <div className="cg">

                <section className="col-sm-2 fsml no-padding">
                  <select className="form-control" id="islands" name="islands"  onChange={this.onChange} value={("islands" in _filters) ? _filters["islands"].value: nofilter}>
                       <option className="cglabel" value={nofilter} >{_get("islands")}</option>
                       {_.uniq(_.flattenDeep(_.sortBy(_.map(items.islands, 'islands')))).map((item, index) =>
                       <option key={index} value={item} >{capitalizedString(_get(item))}</option>
                       )}
                  </select>
                </section>


                <section className="col-sm-2">
                  <select className="form-control" id="languages" name="languages"  onChange={this.onChange} value={("languages" in _filters) ? _filters["languages"].value: nofilter}>
                       <option className="cglabel" value={nofilter} >{_get("languages")}</option>
                       {_.uniq(_.flattenDeep(_.sortBy(_.map(items.languages, 'languages')))).map((item, index) =>
                       <option key={index} value={item} >{capitalizedString(_get(item))}</option>
                       )}
                  </select>
                </section>


                <section className="col-sm-2 ">
                  <select className="form-control" id="seasons" name="seasons" onChange={this.onChange} value={("seasons" in _filters) ? _filters["seasons"].value: nofilter}>
                      <option value={nofilter}>{_get("seasons")}</option>
                      {_.sortBy(_.zip(_.uniq(_.flattenDeep(_.map(items.seasons,'seasons.id'))),_.uniq(_.flattenDeep(_.map(items.seasons,'seasons.lang.'+language))))).map((item,index) =>
                       <option key={index} value={item[0]}>{item[1]}</option>
                      )}
                  </select>
                </section>


                <section className="col-sm-2 ">
                  <select className="form-control" id="lifestyles" name="lifestyles" onChange={this.onChange} value={("lifestyles" in _filters) ? _filters["lifestyles"].value: nofilter}>
                      <option value={nofilter}>{_get("lifestyles")}</option>
                      {_.sortBy(_.zip(_.uniq(_.flattenDeep(_.map(items.lifestyles,'lifestyles.id'))),_.uniq(_.flattenDeep(_.map(items.lifestyles,'lifestyles.lang.'+language))))).map((item,index) =>
                       <option key={index} value={item[0]}>{item[1]}</option>
                      )}
                  </select>
                </section>


                 <section className="col-sm-4 fsmr">
                   <select className="form-control" id="touristic_subjects" name="touristic_subjects" onChange={this.onChange} value={("touristic_subjects" in _filters) ? _filters["touristic_subjects"].value: nofilter}>
                       <option value={nofilter}>{_get("touristic_subjects")}</option>
                       {_.sortBy(_.zip(_.uniq(_.flattenDeep(_.map(items.touristic_subjects,'touristic_subjects.id'))),_.uniq(_.flattenDeep(_.map(items.touristic_subjects,'touristic_subjects.lang.'+language))))).map((item,index) =>
                        <option key={index} value={item[0]}>{item[1]}</option>
                       )}
                   </select>
                 </section>



                </div>

                }


                {searcherType4 &&
                <div className="cg">


                  <section className="col-sm-2 fsml">
                      <label className="input">
                        <i className="icon-append fa fa-calendar"></i>
                        <input type="text" name="startdate" id="date-filter-from" ref={this.makeDatePicker}  data-datevalue="" placeholder="Desde" onChange={this.onChange} onKeyPress={this.onDateKeyPress} />
                      </label>
                  </section>

                  <section className="col-sm-2">
                      <label className="input">
                        <i className="icon-append fa fa-calendar"></i>
                        <input type="text" name="enddate" id="date-filter-to" ref="date-filter-to" data-datevalue="" placeholder="Hasta" onChange={this.onChange} onKeyPress={this.onDateKeyPress}  />
                      </label>
                  </section>

                 <section className="col-md-2 no-padding">
                     <select className="form-control" id="islands" name="islands"  onChange={this.onChange}>
                          <option className="cglabel" value={nofilter} >{_get("islands")}</option>
                          {_.uniq(_.flattenDeep(_.sortBy(_.map(items.islands, 'islands')))).map((item, index) =>
                          <option key={index} value={item} >{capitalizedString(_get(item))}</option>
                          )}
                     </select>
                 </section>
								 {false &&
                  <section className="col-md-2">
                    <select className="form-control" id="areas" name="areas" onChange={this.onChange} value={("areas" in _filters) ? _filters["areas"].value: nofilter}>
                        <option value={nofilter}>{_get("areas")}</option>
                        {_.zip(_.uniq(_.flattenDeep(_.sortBy(_.map(items.areas,'areas.id')))),_.uniq(_.flattenDeep(_.sortBy(_.map(items.areas,'areas.names'))))).map((item,index) =>
                          <option key={index} value={item[0]}>{item[1]}</option>
                         )}
                    </select>
                  </section>
								 }


									<section className="col-md-2">
										<select className="form-control" id="event_type" name="eventtypes" onChange={this.onChange} value={("eventtypes" in _filters) ? _filters["eventtypes"].value: nofilter}>
												<option value={nofilter}>{_get("eventtypes")}</option>
												{_.sortBy(_.zip(_.uniq(_.flattenDeep(_.map(items.eventtypes,'event_type.id'))),_.uniq(_.flattenDeep(_.map(items.eventtypes,'event_type.lang.'+language))))).map((item,index) =>
	 												 {  if (item[0]!=undefined){
	 														 return (<option key={index} value={item[0]}>{item[1]}</option>);
	 														}
	 												 }
	 											)}
										</select>
									</section>

                  <section className="col-md-4 no-padding fsmr" >
                    <select className="form-control" id="municipalities"  name="municipalities" onChange={this.onChange} value={("municipalities" in _filters) ? _filters["municipalities"].value: nofilter}>
                        <option value={nofilter} >{_get("municipalities")}</option>
                        {_.zip(_.uniq(_.flattenDeep(_.sortBy(_.map(items.municipalities,'municipalities.id')))),_.uniq(_.flattenDeep(_.sortBy(_.map(items.municipalities,'municipalities.names'))))).map((item,index) =>
                          <option key={index} value={item[0]}>{item[1]}</option>
                         )}
                    </select>
                  </section>
                </div>
              }


              {searcherType5 &&
              <div className="cg">
               <section className="col-md-2 no-padding fsml">
                 <select className="form-control" id="islands" name="islands"  onChange={this.onChange}>
                      <option className="cglabel" value={nofilter} >{_get("islands")}</option>
                      {_.uniq(_.flattenDeep(_.sortBy(_.map(items.islands, 'islands')))).map((item, index) =>
                      <option key={index} value={item} >{capitalizedString(_get(item))}</option>
                      )}
                 </select>
               </section>

                <section className="col-md-4">
                  <select className="form-control" id="areas" name="areas" onChange={this.onChange} value={("areas" in _filters) ? _filters["areas"].value: nofilter}>
                      <option value={nofilter}>{_get("areas")}</option>
                      {_.zip(_.uniq(_.flattenDeep(_.sortBy(_.map(items.areas,'areas.id')))),_.uniq(_.flattenDeep(_.sortBy(_.map(items.areas,'areas.names'))))).map((item,index) =>
                        <option key={index} value={item[0]}>{item[1]}</option>
                       )}
                  </select>
                </section>

                <section className="col-md-6 no-padding fsmr" >
                  <select className="form-control" id="municipalities"  name="municipalities" onChange={this.onChange} value={("municipalities" in _filters) ? _filters["municipalities"].value: nofilter}>
                      <option value={nofilter} >{_get("municipalities")}</option>
                      {_.zip(_.uniq(_.flattenDeep(_.sortBy(_.map(items.municipalities,'municipalities.id')))),_.uniq(_.flattenDeep(_.sortBy(_.map(items.municipalities,'municipalities.names'))))).map((item,index) =>
                        <option key={index} value={item[0]}>{item[1]}</option>
                       )}
                  </select>
                </section>

              {show.seasons && // !show.municipalities && //col-sm-4 fsml margin-top-10
                <section className="col-sm-4 fsml margin-top-10">
                  <select className="form-control" id="seasons" name="seasons" onChange={this.onChange} value={("seasons" in _filters) ? _filters["seasons"].value: nofilter}>
                      <option value={nofilter}>{_get("seasons")}</option>
                      {_.sortBy(_.zip(_.uniq(_.flattenDeep(_.map(items.seasons,'seasons.id'))),_.uniq(_.flattenDeep(_.map(items.seasons,'seasons.lang.'+language))))).map((item,index) =>
                       <option key={index} value={item[0]}>{item[1]}</option>
                      )}
                  </select>
                </section>
               }

               {show.lifestyles && //!show.municipalities &&
                <section className="col-sm-4 margin-top-10">
                  <select className="form-control" id="lifestyles" name="lifestyles" onChange={this.onChange} value={("lifestyles" in _filters) ? _filters["lifestyles"].value: nofilter}>
                      <option value={nofilter}>{_get("lifestyles")}</option>
                      {_.sortBy(_.zip(_.uniq(_.flattenDeep(_.map(items.lifestyles,'lifestyles.id'))),_.uniq(_.flattenDeep(_.map(items.lifestyles,'lifestyles.lang.'+language))))).map((item,index) =>
                       <option key={index} value={item[0]}>{item[1]}</option>
                      )}
                  </select>
                </section>
              }

              {show.touristic_subjects && //!show.municipalities &&
                 <section className="col-sm-4 fsmr margin-top-10">
                   <select className="form-control" id="touristic_subjects" name="touristic_subjects" onChange={this.onChange} value={("touristic_subjects" in _filters) ? _filters["touristic_subjects"].value: nofilter}>
                       <option value={nofilter}>{_get("touristic_subjects")}</option>
                       {_.sortBy(_.zip(_.uniq(_.flattenDeep(_.map(items.touristic_subjects,'touristic_subjects.id'))),_.uniq(_.flattenDeep(_.map(items.touristic_subjects,'touristic_subjects.lang.'+language))))).map((item,index) =>
                        <option key={index} value={item[0]}>{item[1]}</option>
                       )}
                   </select>
                 </section>
              }
              </div>
            }


          </div>
          );

}


      }

    });



     module.exports = SearchFilters;
