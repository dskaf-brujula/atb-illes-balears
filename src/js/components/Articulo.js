var React = require('react');
var _ = require('lodash');

var ArticuloStore = require('../stores/ArticuloStore');
var ActionsCreator = require('../actions/appActions');
var dameTextoi18 = require('../webutils/utilidades.js').dameTextoi18;
var literales = require('../../resources/literalesArticulo.json');
var renderHtml = require('react-render-html');

var TagsButtons = require('./TagsButtons');

var longitud = 0;
var latitud = 0;
var map = null;
var itinerario = [];
var articulo = [];

// Obtiene los recursos para los cubos 'articulo'.
function cargarArticulo(theUrl){
  try{
	  	if(typeof $ != 'undefined'){
		  $.ajax({
		    url: theUrl,
		    dataType: 'json',
		    cache: false,
		    async: false,
		    success: function(data){
				articulo = data;
				console.log('[ARTICULO] ciclo vida 1 articulo. success '+articulo);
		    }.bind(this),
		    error: function(xhr, status, err) {
		      console.error(this.props.url, status, err.toString());
		    }.bind(this),
		    complete: function(xhr, status){
				console.log('[ARTICULO] ciclo vida 2 '+status+' '+articulo);
		    }.bind(this)
		  });
	   	}

	} catch(e){
		console.log('[ARTICULO] Cargar articulo ' + e.message);
	}
}

function getStateFromStore(){
	return {articulo: ArticuloStore.getAll()}
}


var Articulo = React.createClass({

	getInitialState: function() {

		/**
		* Hay que distinguir entre servidor y navegador para hacer el renderizado.
		*/
		// Client side
		if(this.props.sUrl){
			var theUrl = String(this.props.sUrl);
			console.log('[ARTICULO] URL '+ theUrl);
			cargarArticulo(theUrl);
		} else {
			// Server side
			articulo = this.props.componente;
		}
    var descarga_class = 'descargas_off';

		ActionsCreator.updateArticulo({data:articulo});
		console.warn('[articulo] getInitialState');
		return getStateFromStore();
	},
	componentDidMount: function(){
		console.warn('[articulo] componentDidMount');
		ArticuloStore.addLoadListener(this.onLoad);
	},
	ComponentWillUnMount: function(){
		ArticuloStore.removeLoadListener(this.onLoad);
	},
	onLoad: function() {
		this.setState(getStateFromStore());
	},
	onClick: function(event) {
		console.log('ARTICULO MANEJANDO CLICK EVENT ..');
	},
	render: function(){

		var idioma = String(this.props.sIdioma);
    var pagetype =  this.props.pagetype;



		var  _get  = function(txt){
			try{
			return literales[0][txt][idioma];
			} catch(e){
				console.log("[SearchFilters.js] render() _get(txt): "+txt+" Error: "+e.message);
				return txt;
			}
		}

		var cultura		= dameTextoi18(literales[0].cultura,idioma)
		var gastronomia	= dameTextoi18(literales[0].gastronomia,idioma)
		var deporte		= dameTextoi18(literales[0].deporte,idioma)
		var naturaleza	= dameTextoi18(literales[0].naturaleza,idioma)
		var nautica		= dameTextoi18(literales[0].nautica,idioma)
		var ocio		= dameTextoi18(literales[0].ocio,idioma)
		var familia		= dameTextoi18(literales[0].familia,idioma)
		var amics		= dameTextoi18(literales[0].amics,idioma)
    var plandeviaje		= dameTextoi18(literales[0].plandeviaje,idioma)
    var jornadas		= dameTextoi18(literales[0].jornadas,idioma)


    var islands = this.state.articulo.data.islands;

		var _isla = this.state.articulo.data.islands[0];
    var _clisla = this.state.articulo.data.islands.length>1? 'ni': _isla.substring(0,2);
		var _clname = _clisla + ' title-v2';

		var _title = dameTextoi18(this.state.articulo.data.title,idioma);
		var _subtitle = dameTextoi18(this.state.articulo.data.subtitle,idioma);
		var _opening_paragraph = dameTextoi18(this.state.articulo.data.opening_paragraphs,idioma);
		var _description = dameTextoi18(this.state.articulo.data.description,idioma);
    var _journeys_number =  (pagetype=='pla') ? this.state.articulo.data.journeys_number: '';

    var pattern = "./froala_upload/";
    var reg =  new RegExp(pattern, "g");
     _description = _description.replace(reg,'https://froala-editor.s3.amazonaws.com/');



    var strListTitle = (pagetype=='iti') ? literales[0].punts_itinerari[idioma] : literales[0].saber_mas[idioma];
    console.log('[Articulo.js] pagetype: '+pagetype);


    // Tags
    var oDatosTagsButtons = []
    try{

      console.log("[Articulo.js] oDatosTagsButtons...");
      oDatosTagsButtons.sIdioma = idioma;
      oDatosTagsButtons.sIsla = _isla;
      oDatosTagsButtons.oTouristicSubjects = this.state.articulo.data.touristic_subjects;
      oDatosTagsButtons.oLifeStyles = this.state.articulo.data.lifestyles;
      oDatosTagsButtons.oSeasons = this.state.articulo.data.seasons;
      oDatosTagsButtons.componentType = pagetype;


    }catch(e){
      console.log("[Articulo.js] Render (Error)..."+e.message);
    }


    // Downloads
		var hayDescargas = false;
    var descarga_class = 'descargas_off';
		var descargas = this.state.articulo.data.multimedia.map(function(descarga,index){

      console.log("[Articulo.js] descargas... descarga.type: "+descarga.type);


			if(descarga.type == 'document'){
				descarga_class = 'container';

        hayDescargas = true;

        var descarga_title = descarga.title[idioma]!='' ? descarga.title[idioma] : dameTextoi18(literales[0].descarga_texto,idioma);

      	return(
          <div key={index} className='col-sm-4 sm-margin-bottom-40'>
            <h3 className='heading-sm'>
              <a href={descarga.url} target="_blank">
                <i className='icon-custom rounded-x icon-sm icon-bg-u fa fa-file-pdf-o'>
              </i>
              <span>{descarga_title} </span>
              </a>
            </h3>
          </div>
  			)
      }
		});

  var haySaberMas = false;

  var t_resources = _.filter(this.state.articulo.data.t_resources,function(o) { return !(('rrtt_noshow' in o) && (o.rrtt_noshow==true)); });

  var saber_mas_resources = t_resources.map(function(resource,index){

      var _componentType = "rrtt";
      var _isla = ('rrtt_islands' in resource) ? resource.rrtt_islands[0] : '';
      var _title = ('rrtt_title' in resource) ? dameTextoi18(resource.rrtt_title,idioma): '';
      var _subtitle = ('rrtt_subtitle' in resource) ? dameTextoi18(resource.rrtt_subtitle,idioma) : '';
      // var _opening_paragraph = ('rrtt_opening_paragraph' in resource) ? dameTextoi18(resource.rrtt_opening_paragraph,idioma) : '';
      var _opening_paragraph = ('rrtt_opening_paragraphs' in resource) ? dameTextoi18(resource.rrtt_opening_paragraphs,idioma) : '';

      var _municipio = ('rrtt_municipalities' in resource) ? resource.rrtt_municipalities.name[0]: '';


      var _thumbnail = ('rrtt_multimedia' in resource) ?resource.rrtt_multimedia.url : '';

      // var multimedia = _.filter(resource.rrtt_multimedia,{"type":"image"});
      // var _thumbnail = multimedia[0].url;



      var url_target = "_self";

      var url_friendly = ('rrtt_id_friendly' in resource) ? dameTextoi18(resource.rrtt_id_friendly,idioma): '';
		  // var strUrl = 'javascript:recargaRecurso('+idioma+',url_friendly)';



      if (("rrtt_term" in resource) && ('club-nautico,escuela-nautica,estacion-nautica,marina,puerto-deportivo,spa,bodega').indexOf(resource.rrtt_term.id)>=0){
       url_friendly = resource.rrtt_web;
       url_target = "_blank";
      }


      if (("rrtt_tipology_id" in resource) && ('alojamiento').indexOf(resource.rrtt_tipology_id.toLowerCase())>=0){
        url_friendly = resource.rrtt_web;
        url_target = "_blank";

      }




      var oTagsButtonsResource = []

      try{

        console.log("[Articulo.js] oTagsButtonsResource...");
        oTagsButtonsResource.sIdioma = idioma;
        oTagsButtonsResource.sIsla = _isla;
        // oTagsButtonsResource.oLifeStyles = resource.lifestyles;
        // oTagsButtonsResource.oSeasons = resource.seasons;
        oTagsButtonsResource.componentType = _componentType;

        oTagsButtonsResource.oTouristicSubjects = ('rrtt_touristic_subjects' in resource) ? resource.rrtt_touristic_subjects : '';
        oTagsButtonsResource.sTerm = ('term' in resource) ? resource.rrtt_term.lang[idioma] : '' ;

      }catch(e){
        console.log("[Articulo.js] Render (Error)..."+e.message);
      }

      haySaberMas = true;

      console.log("[Articulo.js] t_resources... _title: "+_title);

      var count = index+1;
      var strCount = (count < 10) ?  ('0'+count.toString()): count.toString();
      var comma = (_isla!='') ? ',': '';
      var bracket_l = (_municipio!=''||_isla!='')? '(':'';
      var bracket_r = (_municipio!=''||_isla!='')? ')':'';

      return(
        <div key={count}>
          <div className='row'>
            <div className='col-sm-2 fsml'>
              <img alt='' className='img-responsive' src={_thumbnail}  />
            </div>
            <div className='col-sm-10'>
              <div className='news-v3'>
                <span className='numlist'>{strCount}</span>
                <h3 className='title-v2'>
                  <a href={url_friendly} target={url_target}>{_title} {bracket_l}{_municipio}{comma} {_get(_isla)}{bracket_r}</a>
                </h3>
                <p className='title-v2'>
                  <strong>{_subtitle}</strong>
                </p>
                <p className='margin-left-60'>
                {renderHtml(_opening_paragraph)}
                </p>
                <TagsButtons oDatosTagsButtons={oTagsButtonsResource} className="margin-left-60 margin-bottom-30 margin-top-10"/>
              </div>
            </div>
          </div>
          <div className='clearfix margin-bottom-20'>
            <hr/>
          </div>
        </div>
      );
  });



		return(
      <div role="main">
		  <section className='container content-sm txtalign'>
          {pagetype=='pla'&&
          <h1 className="title-vt">{_title}
          </h1>
          }
          <div className='margin-bottom-30'>

          {pagetype=='pla' &&
                    <h2 className="bl title-v3">{_subtitle}
                    {
                      islands.map((isla, index) => {
                          return (<span key={index} className={"b"+isla.substring(0,2)}></span>);
                      })
                    }
                    </h2>
          }

          {pagetype!='pla' &&

                    <h1 className="bl title">{_title}
                      {
                        islands.map((isla, index) => {
                            return (<span key={index} className={"b"+isla.substring(0,2)}></span>);
                        })
                      }
                    </h1>
          }

          <TagsButtons oDatosTagsButtons={oDatosTagsButtons} className="margin-bottom-30 margin-top-10"/>
          {pagetype!='pla' &&

            <h2 className="title-v2 font-italic">{_subtitle}</h2>

          }


          <p>{_opening_paragraph}</p>
					{renderHtml(_description)}
        </div>
		</section>

   {hayDescargas &&
    <div className='bg-grey margin-bottom-30'>
      <div className={descarga_class}>
      <h2 className='title-v5  padding-top-20'>{dameTextoi18(literales[0].descarga_titulo,idioma)}</h2>
      <div className='row content-boxes-v2 margin-bottom-20'>
          {descargas}
        </div>
      </div>
    </div>
    }


    <section className='container'>
     {haySaberMas &&
      <div className='shop-subscribe bg-color-green'>
        <div className='container'>
         <h2 className='margin-bottom-20'><strong> {strListTitle}</strong> </h2>
        </div>
      </div>
    }
      <div className='margin-top-20'> </div>

      <div className='container margin-top-40'>
        {saber_mas_resources}
      </div>
    </section>

</div>
		);
	}
});

module.exports = Articulo;
