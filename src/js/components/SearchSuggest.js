var React = require('react');
var PropTypes = require('prop-types'); // ES5 with npm
var Autosuggest = require('react-autosuggest');
var ReactDOM = require('react-dom');
var _ = require('lodash');


var searchSuggest = [];




    // https://developer.mozilla.org/en/docs/Web/JavaScript/Guide/Regular_Expressions#Using_Special_Characters
    function escapeRegexCharacters(str) {
      return str.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
    }


    // function searchByText(collection, text, exclude) {
    //   text = _.toLower(text);
    //   return _.filter(collection, function(object) {
    //     return _(object).omit(exclude).some(function(string) {
    //       return _(string).toLower().includes(text);
    //     });
    //   });
    // }


function getSearchString(item,language,searcherType){

      var string='';

      if (searcherType=="1"||searcherType=="5"){
            try{
              string += '|' + ((item.component_type == "rrtt") ? item.term.lang[language] : '');

              string += '|' + ((item.component_type == "rrtt") ? item.title : item.title[language]);

              string += '|' + item.subtitle[language];

            }catch(e){
              console.warn("[SearchSuggest.js] getSearchString() title (error): "+e.message);
            }

      }else{
            try{

              switch (item.component_type) {
                  case "eve":
                      string += '|' + item.title[language];
                      break;
                  case "pub":
                      string += '|' +  item.title[language];
                      break;
                  case "art":
                      string += '|' + item.title[language];
                      break;
                  case "iti":
                      string += '|' + item.title[language];
                      break;
                  case "rrtt":
                      string += '|' + item.title;
                      break;
                  default:
                      string += '|' + item.title;
                      break;
                }

            }catch(e){
              console.warn("[SearchSuggest.js] getSearchString() title (error): "+e.message);
            }
      }

      console.log("[SearchSuggest.js] getSearchString()  string: "+string);

      return string.replace('|',' ');
}


    function searchByText(collection, text, language, searcherType) {
        text = _.toLower(text);
        console.log('[SearchSuggest.js] searchByText() searcherType '+searcherType);

          return _.filter(collection, function(object) {
            var string = '';

            // if (searcherType=='1'){
            //   string =  object.term.lang[language]+'  '+object.title;
            // }
            // else{
            //   string =  object.title;
            // }



            var string = getSearchString(object,language,searcherType)
            console.log('[SearchSuggest.js] searchByText() '+string);
            return _(string).toLower().includes(text);
          });
    }


    function getSuggestions(value,items,idioma,searcherType) {
      console.log ('[SearchSuggest.js] getSuggestions()... value: '+JSON.stringify(value));
      console.log ('[SearchSuggest.js] getSuggestions()... searcherType: '+searcherType);


      return searchByText(items,value,idioma,searcherType);
    }



    var SearchSuggest = React.createClass({


    getInitialState: function() {

      console.log ('[SearchSuggest.js] getInitialState()...')

       var _value = location.hash ? location.hash.split('#')[1] : '';
       var searchontype = this.props.dosearch
       var searchonenter = !this.props.dosearch;
       var hashsearch = (_value.length>0);

       return {
         items : this.props.items,
         value: _value,
         dosearch: this.props.dosearch,
         searchontype:  searchontype,
         searchonenter: searchonenter,
         hashsearch : hashsearch,
         suggestions: []
      }
    },
      componentWillMount: function(){

        console.log("[SearchSuggest.js] componentWillMount()...");

        this.state=this.getInitialState();
      },
      componentDidMount: function(){

        console.log("[SearchSuggest.js] componentDidMount()  this.state.hashsearch "+this.state.hashsearch);

        if (this.state.hashsearch){
          this.onSuggestionsFetchRequested(this.state.value,true);

          window.addEventListener("hashchange", this.hashchange, false);


        }


      },
      hashchange: function(){
        console.log('[SearchSuggest.js] hashchange');
        var _value = location.hash ? location.hash.split('#')[1] : '';
        var hashsearch = (_value.length>0);

        this.setState({
          value:_value,
          hashsearch : hashsearch
        });

        if (this.state.hashsearch){
          this.onSuggestionsFetchRequested(this.state.value,true);
        }

      },
      onChange: function(event, { newValue, method }){
        console.log("[SearchSuggest.js] onChange()... newValue: "+JSON.stringify(newValue));
        console.log("[SearchSuggest.js] onChange()... method: "+method);

        // console.log("[SearchSuggest.js] onChange()... event.charCode: "+event.charCode);


          this.setState({
            value: newValue
          });

        console.log("[SearchSuggest.js] onChange()... this.state.value: "+this.state.value);

      },

      onKeyPress: function(event){
        console.log("[SearchSuggest.js] onKeyPress()... event.charCode: "+event.charCode);

        if (event.charCode==13&&!this.state.searchontype){
          location.hash = '';
          this.onSuggestionsFetchRequested(this.state.value,true);
          event.preventDefault();
        }
        else{
          console.log("[SearchSuggest.js] onKeyPress()... nothing to do...: ");
        }


      },
      onBlur: function(event, { highlightedSuggestion }){
        console.log("[SearchSuggest.js] onBlur()... highlightedSuggestion: "+JSON.stringify(highlightedSuggestion));

      },
      onClickSearch: function(event){
        console.log("[SearchSuggest.js] onBlur()... onClickSearch (event): ");

        this.onSuggestionsFetchRequested(this.state.value,true);
        event.preventDefault();

      },

      onSuggestionsFetchRequested : function({value},dosearch){
        console.log('[SearchSuggest.js] onSuggestionsFetchRequested()...');
        console.log("[SearchSuggest.js] onSuggestionsFetchRequested()... this.state.value: "+this.state.value);
        console.log("[SearchSuggest.js] onSuggestionsFetchRequested()... this.state.dosearch: "+this.state.dosearch);

        if (dosearch||this.state.searchontype){

          var _suggestions = getSuggestions(this.state.value,this.state.items,this.props.idioma,this.props.searcherType);
          this.setState({
            suggestions: _suggestions,
            hashsearch: false
          });
          // call change page function in parent component
          this.props.onAutoSuggest(_suggestions,this.state.value);

        } else{
          console.log("[SearchSuggest.js] onSuggestionsFetchRequested()... nothing to do...: ");
        }
        event.preventDefault();

      },
       shouldRenderSuggestions: function(newvalue) {
          console.log ('[SearchSuggest.js] shouldRenderSuggestions()... this.state.value: '+this.state.value+' newvalue: '+newvalue);
          // return value.trim().length > 2;
          return newvalue.trim().length > 0;

      },
      onSuggestionsClearRequested : function(){
        console.log ('[SearchSuggest.js] onSuggestionsClearRequested()... ');
        console.log ('[SearchSuggest.js] onSuggestionsClearRequested() this.state.suggestions.length '+this.state.suggestions.length+'  this.state.value: '+this.state.value);

        var _suggestions;
        var _value;

        console.log ('[SearchSuggest.js] onSuggestionsClearRequested() this.state.suggestions.length '+this.state.suggestions.length+'  this.state.value: '+this.state.value);

      },


      getSuggestionValue: function (suggestion) {
        console.log ('[SearchSuggest.js] getSuggestionValue() of ...'+suggestion.title);


        return suggestion.title;
      },

      renderSuggestion:  function (suggestion) {
        console.log ('[SearchSuggest.js] renderSuggestion() of ...'+suggestion.title);

        // CSC: ocultamos la capa
        // return (
        //   <span>{suggestion.title}</span>
        // );

      },
       renderSuggestionsContainer: function({ containerProps , children, query }) {
         console.log ('[SearchSuggest.js] renderSuggestionsContainer() of ... query: '+query);
        //  return (<div></div>);
        // return (
        //   <div {... containerProps}>
        //     {children}
        //     <div>
        //       Press Enter to search <strong>{query}</strong>
        //     </div>
        //   </div>
        // );
      },

      componentWillReceiveProps: function(nextProps) {
        console.log("[SearchSuggest.js] componentWillReceiveProps()...");
        console.log("[SearchSuggest.js] componentWillReceiveProps() this.props.reset..."+this.props.reset);

				if (this.props.reset){
				      this.setState({value:''});
				}



      },
      render() {

          console.log ('[SearchSuggest.js] render this.state.value: '+JSON.stringify(this.state.value));
          console.log ('[SearchSuggest.js] render this.state.suggestions : '+this.state.suggestions);
          console.log ('[SearchSuggest.js] render this.props.reset : '+this.props.reset);

          var reset = this.props.reset;


          const { value, suggestions } = this.state;
          const inputProps = {
            placeholder: "Buscar ...",
            name: 'inputsearch',
            value,
            onChange: this.onChange,
            onKeyPress: this.onKeyPress,
            onBlur: this.onBlur
          };

          const renderInputComponent = inputProps => (
          <div className="no-padding input-wrapper">
            <input {...inputProps} className="form-control inbd"/>
            <label className="fa fa-search input-icon margin-bottom-5" onClick={this.onClickSearch} >
            </label>
          </div>

          );



          return (
            <Autosuggest
              suggestions={suggestions}
              onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
              onSuggestionsClearRequested={this.onSuggestionsClearRequested}
              getSuggestionValue={this.getSuggestionValue}
              renderSuggestion={this.renderSuggestion}
              inputProps={inputProps}
              renderInputComponent={renderInputComponent}
              renderSuggestionsContainer={this.renderSuggestionsContainer}
              shouldRenderSuggestions={this.shouldRenderSuggestions}
              alwaysRenderSuggestions={true}/>
          );
      }
    }
  );

     module.exports = SearchSuggest;
