var React = require('react');
var DestacadosStore = require('../stores/DestacadosStore');
var ActionsCreator = require('../actions/appActions');
var dameTextoi18 = require('../webutils/utilidades.js').dameTextoi18;
var sortDestacados = require('../webutils/utilidades.js').sortDestacados;
var literales = require('../../resources/literalesDestacados.json');
var destacados = [];
var _ = require('lodash');


// Obtiene los recursos para los cubos 'Destacados'.
function cargarDestacados(theUrl){
	try{
	  	if(typeof $ != "undefined"){
			$.ajax({
				url: theUrl,
				dataType: 'json',
				cache: false,
				async: false,
				success: function(data){
					destacados = data;
					//console.log('ciclo vida 1 destacados. success '+destacados);
				}.bind(this),
					error: function(xhr, status, err) {
					//console.error(this.props.url, status, err.toString());
				}.bind(this),
					complete: function(xhr, status){
					//console.log('ciclo vida 1 '+status+' '+destacados);
				}.bind(this)
			});
		}
	} catch(e){
		console.log('Cargar destacados ' + e.message);
	}
}

function cargarFiltro(bFiltro){
  var rt="";
	try{

		if(bFiltro == 'true'){
			rt = <div id='filters-container' className='cbp-l-filters-text'>
			<div data-filter='*' className='cbp-filter-item-active cbp-filter-item dn'></div>
			<div data-filter='.mallorca' className='cbp-filter-item'>Mallorca</div> |
			<div data-filter='.menorca' className='cbp-filter-item'>Menorca </div> |
			<div data-filter='.ibiza' className='cbp-filter-item'>Ibiza </div> |
			<div data-filter='.formentera' className='cbp-filter-item'> Formentera </div>
			</div>
		}
		return rt

	} catch(e){
		console.log(e.message);
	}
}

function getStateFromStore(){
	return {destacados: DestacadosStore.getAll()}
}
/**
* Máximo de 9 items, presentados de forma aleatoria
*/
/*
function maxAleatorios(cubos){
	// Primero barajo, y luego selecciono.
	cubos.sort(function(a, b){return 0.5 - Math.random()});
	cubos.splice(9, cubos.length);
	if(cubos.length % 3 !== 0){
		cubos.splice(6, cubos.length);
		if(cubos.length % 3 !== 0){
			cubos.splice(3, cubos.length);
		}
	}
	return cubos;
}
*/
var Destacados = React.createClass({

	getInitialState: function() {

		console.log('[Destacados.js] getInitialState()');

		/**
		* Hay que distinguir entre servidor y navegador para hacer el renderizado.
		*/
		// Client side
		if(this.props.sUrl){
			var theUrl = String(this.props.sUrl);
			console.log('URL '+ theUrl);
			cargarDestacados(theUrl);
		} else {
			// Server side
			destacados = this.props.componente;
		}
		ActionsCreator.updateDestacados({data:destacados});
		return getStateFromStore();
	},
	componentDidMount: function(){
	 	console.info('[Destacados] componentDidMount');

		DestacadosStore.addLoadListener(this.onLoad);
	},
	ComponentWillUnMount: function(){
		DestacadosStore.removeLoadListener(this.onLoad);
	},
	onLoad: function() {
		this.setState(getStateFromStore());
	},

	render: function(){

		var idioma = String(this.props.sIdioma);
		var filtro = cargarFiltro(String(this.props.sFiltro));
		var LandingLevel = this.props.sLandingLevel;
		var section = this.props.section;


		//var icono = 'l'+String(this.props.sIsla).substring(0,2);
		/**
		* Literales del componente
		*/
		var tituloComponente = dameTextoi18(literales[0].titulo,idioma);
		var subtituloComponente = dameTextoi18(literales[0].subtitulo,idioma);
		var parrafoComponente = dameTextoi18(literales[0].parrafo,idioma);
		var sDiccComponentes = this.props.sDiccComponentes;


		console.log("[Destacados.js] LandingLevel: "+LandingLevel);

		var columns = (section=='publicaciones')?4:3;
		var maxitems =  (section=='publicaciones')?8:9;


		var  _get  = function(txt){
			try{
			return literales[0][txt][idioma];
			} catch(e){
				console.log("[SearchFilters.js] render() _get(txt): "+txt+" Error: "+e.message);
				return txt;
			}
		}


		var cubos = sortDestacados(this.state.destacados.data.items,columns,maxitems).map(function(destacado, index){

			try{
				var _componentType = destacado.component_type;

				console.log("[Destacados.js] Destacado número : "+index+" - - - - - - ");
				console.log("[Destacados.js]  _componentType: "+_componentType);
				var islands = [];
				destacado.islands.map(function(island, index){
					console.log("[Destacados.js]  island: "+island);
					islands.push(_get(island));
				});
				var _islas = (islands.length==4) ? "Illes Balears" : _.join(islands,'/');
				var _imagen = (_componentType!='pub')? destacado.multimedia[0].url : destacado.multimedia[1].url;

				var _titulo = "";
				if (_componentType!='pub'){
					try{
					_titulo += 	dameTextoi18(destacado.term.lang,idioma);
			  	}catch(e){
						console.log("[Destacados.js] _titulo (error): "+e.message);
	     		}
				}

				_titulo +=  ' ' + ((_componentType == "rrtt") ? destacado.title : dameTextoi18(destacado.title,idioma));
				console.log("[Destacados.js]  _titulo 2: "+_titulo);


				var _urlFriendly = (_componentType!='pub')? eval('destacado.data_sheet.'+ idioma +'.url_friendly'):destacado.multimedia[0].url;

				var _urlTarget = "_blank";


				if (_componentType!='pub'){

					// var _tipoClase = 'cbp-item '+ destacado.islands[0];
					var _tipoClase = 'cbp-item ';


					try{
						var componentTypeName = _.capitalize(dameTextoi18(sDiccComponentes[_componentType],idioma));
						console.log('[Destacados.js]  componentTypeName: '+componentTypeName);
					}catch(e){
						console.log('[Destacados.js] Recomendación '+index+', no tiene componentTypeName:\n' + e.message);
					}

					return(

								<div className={_tipoClase} key={index} >
									<a href={_urlFriendly} target={_urlTarget} className='cbp-caption' data-title={_titulo}>
										<div className='cbp-caption-defaultWrap'>
											<img alt={'Imagen '+_titulo} src={_imagen} />
										</div>
										<div className='cbp-caption-activeWrap'>
											<div className='cbp-l-caption-alignLeft'>
												<div className='cbp-l-caption-body'>
													<div className='cbp-l-caption-title text-left'>{_titulo}</div>
													<div className='cbp-l-caption-desc'>{_islas}</div>
												</div>
											</div>
										</div>
									</a>
								</div>

					)

				}else{

					var _tipoClase;

					console.log("[Destacados.js] index: "+index);


						if (0<=index && index<3){
							_tipoClase="cbp-item fil2r";
						}
						else if (index==3){
							_tipoClase="cbp-item";
						}
						else if ((index+1)%4==0){
							_tipoClase="cbp-item fil2t";
						}
						else{
							_tipoClase="cbp-item fil2";
						}
						console.log("[Destacados.js] _tipoClase: "+_tipoClase);

					return(

		 						<div className={_tipoClase} key={index}>
							 			<a href={_urlFriendly} className='cbp-caption' data-title={_titulo} target='_blank'>
							 				<div className='f20'>
							 					<img alt='' src={_imagen}  />
							 				</div>
							 				<div className='cbp-caption-activeWrap publi'>
							 					<div className='cbp-l-caption-alignCenter'>
							 						<div className='cbp-l-caption-body'>
							 							<div className='cbp-l-caption-title ts6 ps6'>{_titulo}</div>
							 						</div>
							 					</div>
							 				</div>
							 			</a>
							 		</div>
						)
				}


			} catch (e){
				console.warn("Se ha producido una incidencia con el destacado nº "+index+", y no se mostrará:\n" + e.message);
			}
		});

		// TODO: presentacion aleatoria
		// TODO mostrar máximo de 9 destacados o multiplo de 3.

//cbp cbp-caption-active cbp-caption-overlayBottomAlong cbp-ready

		var gridClass = 'cbp cbp-caption-active cbp-caption-overlayBottomAlong cbp-ready';
		var gridId = (section=='publicaciones')?'js-grid-masonry':'grid-container';

		return(
			<div role="complementary">
				<div className='text-justify margin-bottom-30' data-version='Destacados-v1.4'>
				  {filtro}
					<div id={gridId} className='cbp-l-grid-gallery'  style={{visibility:'hidden'}}>
					{cubos}
					</div>
				</div>
			</div>
		);
	}
});

module.exports = Destacados;
