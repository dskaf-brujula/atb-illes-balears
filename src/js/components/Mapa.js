var React = require('react');
var ActionsCreator = require('../actions/appActions');
var MapaStore = require('../stores/MapaStore');
var omnivore = require ('leaflet-omnivore');
var markerClusterGroup = require ('leaflet.markercluster');

var _ = require ('lodash');
// var markersItinerario = require('../webutils/Mapa.js').generaPOIs;

var lon = 0;
var lat = 0;
var map = null;
var googleLayer = null;
var itinerario = [];
var sIdioma = null ;
var sIsla = null;
var sTitle = null;
var latlon = null;

var oDataMap =[];
var mapType = null;


function getStateFromStore(){
	return {map: MapaStore.getAll()}
}

var Mapa = React.createClass({

	getInitialState: function(){
		// cargarDatosMapa();
    console.log("[Mapa.js] getInitialState() ");
		console.log("[Mapa.js] getInitialState()  this.props.maptype "+this.props.maptype);

		mapType = this.props.maptype;
		sIdioma 		=  this.props.sIdioma;
		sIsla		 		=  this.props.sIsla.toUpperCase();

		if (mapType=='itinerario'){
			console.log("[Mapa.js] getInitialState()  itinerario");

			sTitle 			= this.props.sTitle;
			oDataMap.itinerario = this.props.itinerario;
			console.log("[Mapa.js] getInitialState()  oDataMap.itinerario: "+JSON.stringify(oDataMap.itinerario));

			lat = oDataMap.itinerario[0].rrtt_latitude;
			lon = oDataMap.itinerario[0].rrtt_longitude;
			console.log("[Mapa.js] getInitialState()  lon "+lon+" lat "+lat);
			ActionsCreator.updateMapa({itinerario:oDataMap.itinerario}); // Object
		}else if (mapType=='latlon'){
			oDataMap.latlon 		=  this.props.latlon;
			lat = this.props.latlon.latitude;
			lon = this.props.latlon.longitude;
			ActionsCreator.updateMapa({latlon:oDataMap.latlon}); // Object
		}else if (mapType=='kml'){
			oDataMap.latlon 		=  this.props.latlon;
			oDataMap.kml 				= this.props.kml;
			lat = this.props.latlon.latitude;
			lon = this.props.latlon.longitude;
			console.log("[Mapa.js]  oDataMap.kml: "+oDataMap.kml);

			ActionsCreator.updateMapa({kml:oDataMap.kml,latlon:oDataMap.latlon}); // Object
		}else if (mapType=='cluster'){
			console.log("[Mapa.js] getInitialState()  cluster");
			oDataMap.cluster 		=  this.props.cluster;

			lat = oDataMap.cluster[0].latitude;
			lon = oDataMap.cluster[0].longitude;
			console.log("[Mapa.js] getInitialState()  lon "+lon+" lat "+lat);
			ActionsCreator.updateMapa({cluster:oDataMap.cluster}); // Object
		}else{
			console.warn("[Mapa.js] mapType no identificado mapa sin puntos");
		}


		// console.log("[Mapa.js] getInitialState()  this.oDataMap "+JSON.stringify(oDataMap));
		//
		// console.log("[Mapa.js] getInitialState()  oDataMap.latlon "+JSON.stringify(oDataMap.latlon));
		//
		// console.log("[Mapa.js] getInitialState()  this.props.latlon "+JSON.stringify(this.props.latlon));
		//
		// console.log("[Mapa.js] getInitialState()  oDataMap.kml "+JSON.stringify(oDataMap.kml));


		return getStateFromStore();
	},
	componentDidMount: function(){
		console.log("[Mapa.js] componentDidMount() lon "+lon+" lat:"+lat);

		this.pintarMapa(lon,lat);
		// console.log("[Mapa.js] componentDidMount() this.state.map "+JSON.stringify(this.state.map));

		// MapaStore.addLoadListener(this.onLoad);
	},
	componentDidUpdate: function(){
		console.log("[Mapa.js] componentDidUpdate() lon "+lon+" lat:"+lat);
		console.log("[Mapa.js] componentDidUpdate() this.props.cluster.length()..."+this.props.cluster.length);

		this.pintarMapa(lon,lat);
		// console.log("[Mapa.js] componentDidMount() this.state.map "+JSON.stringify(this.state.map));

		// MapaStore.addLoadListener(this.onLoad);
	},
	componentWillReceiveProps: function(nextProps) {
		console.log("[Mapa.js] componentWillReceiveProps()...");

		console.log("[Mapa.js] componentWillReceiveProps() this.props.cluster.length..."+this.props.cluster.length);

		oDataMap.cluster 		=  nextProps.cluster;


		console.log("[Mapa.js] componentWillReceiveProps() nextProps.cluster.length..."+nextProps.cluster.length);

		this.setState({cluster: oDataMap.cluster})

		// this.getInitialState();

		// if (nextProps.maptype=='cluster'){
		// 	console.log("[Mapa.js] componentWillReceiveProps()  cluster");
		// 	oDataMap.cluster 		=  nextProps.cluster;
		//
		// 	lat = oDataMap.cluster[0].latitude;
		// 	lon = oDataMap.cluster[0].longitude;
		// 	console.log("[Mapa.js] componentWillReceiveProps()  lon "+lon+" lat "+lat);
		// 	ActionsCreator.updateMapa({cluster:oDataMap.cluster}); // Object
		// }else{
		// 	console.warn("[Mapa.js] mapType no identificado mapa sin puntos");
		// }
		//


	},
	componentWillUpdate: function(nextProps) {
		console.log("[Mapa.js] componentWillUpdate()...");
		// this.getInitialState();
	},

	ComponentWillUnMount: function(){
		MapaStore.removeLoadListener(this.onLoad);
	},
	// onLoad: function() {
	// 	this.setState(getStateFromStore());
	// },
	pintarMapa: function(lon,lat){

		if(isNaN(lon)){
			console.error("Longitud is nan");
			return;
		}

		if(map!=null){
			map.eachLayer(function(layer){
    		map.removeLayer(layer);
			});
			map.remove();
			map = null
		}

		// var tiles = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
		// 		maxZoom: 18,
		// 		attribution: '&copy; <a href="/">IllesBalears.travel</a> 2017'
		// 	});


		console.log("[Mapa.js] pintarMapa() Start");

		if(map==null){

				map = new L.Map('map', {center: new L.LatLng(lat, lon), zoom: 10,scrollWheelZoom: false});
				// googleLayer = new L.Google('ROADMAP');

				map.addLayer(new L.Google('ROADMAP'));
				map.once('focus', function() { map.scrollWheelZoom.enable(); });
				map.on('click', function() {
  				if (map.scrollWheelZoom.enabled()) {
					    map.scrollWheelZoom.disable();
					    }
					    else {
					    map.scrollWheelZoom.enable();
					    }
  			});


		if (mapType=='itinerario'){
			this.markersitinerario(this.state.map.itinerario);
		}else if (mapType=='latlon'){
			this.markerslatlon(this.state.map.latlon);
		}else if (mapType=='kml'){
			this.markerskml(this.state.map.kml);
		}else if (mapType=='cluster'){
			this.markerscluster(oDataMap.cluster); //@TODO: repasar status cluster
		}else{
			console.warn("[Mapa.js] maptype no identificado mapa sin puntos");
		}


		console.log("[Mapa.js] pintarMapa() End ");
		}
	},
	markerscluster: function(cluster){
		console.info("[Mapa.js] markersCluster()  cluster.length: "+cluster.length);
		var aCapas = []; // Capas de POIs
		var aBounds = []



				var markerscluster = L.markerClusterGroup({showCoverageOnHover: false});



				function populate() {

								for (var i = 0; i < cluster.length; i++) {
									// var a = addressPoints[i];
									var lon = new Number((cluster[i].longitude).valueOf());
									var lat = new Number((cluster[i].latitude).valueOf());
									var title = cluster[i].term.lang[sIdioma]+": "+cluster[i].title;
									var marker =  L.marker(new L.LatLng(lat, lon),{ title: title, alt: title});//.addTo(map);
									marker.bindPopup("<strong>"+title+"</strong>").openPopup();
									// marker.setZIndexOffset(100000);
									markerscluster.addLayer(marker);
									// aBounds.push(marker);

								}

								return false;
				}

				populate();
				map.addLayer(markerscluster);

				// markerscluster.refreshClusters();

				console.info("[Mapa.js] markersCluster()  addLayer");
				map.fitBounds(markerscluster._currentShownBounds);
				console.info("[Mapa.js] markersCluster()  end.");
	},
	markersitinerario: function(itinerario){
		console.info("[Mapa.js] markersItinerario()  itinerario.length: "+itinerario.length);
		var aCapas = []; // Capas de POIs
		var aBounds = []
		for(i=0;i<itinerario.length;i++){
				var lon = new Number((itinerario[i].rrtt_longitude).valueOf());
				var lat = new Number((itinerario[i].rrtt_latitude).valueOf());
				console.info("[Mapa.js] markersItinerario() lon: "+lon+" lat: "+lat);
				var title = "    ";
			  var marker =  L.marker(new L.LatLng(lat, lon),{ title: title, alt: title}).addTo(map);
				marker.bindPopup("<strong>"+itinerario[i].rrtt_title[sIdioma]+"</strong>").openPopup();
				// marker.bindPopup("<strong><a href=''"+itinerario[i].rrtt_web+"'>"+itinerario[i].rrtt_title[sIdioma]+"</a></strong><br/>").openPopup();
				aBounds.push(marker);
	  }
		map.fitBounds(new L.featureGroup(aBounds),{padding: [60, 60]});
		console.info("[Mapa.js] markersItinerario()  end.");
	},
	markerslatlon: function(latlon){
		console.info("[Mapa.js] markerslatlon()  latlon: "+JSON.stringify(latlon));
		var lat = new Number((latlon.latitude).valueOf());
		var lon = new Number((latlon.longitude).valueOf());
		console.info("[Mapa.js] markerslatlon() lon: "+lon+" lat: "+lat);
		var title = "    ";
	  var marker =  L.marker(new L.LatLng(lat, lon),{ title: title, alt: title}).addTo(map);
				// marker.bindPopup("<strong><a href=''"+itinerario[i].rrtt_web+"'>"+itinerario[i].rrtt_title[sIdioma]+"</a></strong><br/>").openPopup();
		console.info("[Mapa.js] markerslatlon()  end.");
	},
	markerskml: function(kml){
		console.info("[Mapa.js] markerskml()  kml "+oDataMap.kml[0].url);
		// var lon = new Number((latlon.latitude).valueOf());
		// var lat = new Number((latlon.longitude).valueOf());
		// console.info("[Mapa.js] markerskml() lon: "+lon+" lat: "+lat);
		var kmlTest = "https://s3-eu-west-1.amazonaws.com/illes-balears-cms-media/kml-test-google.kml";
		var kmlurl = oDataMap.kml[0].url;

		var layer = omnivore.kml(kmlurl)
		    .on('ready', function(e) {
					var latlngbounds = e.target.getBounds();
					map.fitBounds(latlngbounds);
					var latlon = latlngbounds.getCenter();

					var lat = new Number((latlon.lat).valueOf());
					var lon = new Number((latlon.lng).valueOf());
					console.log("[Mapa.js] lat: "+lat+" lon: "+lon);
					window.oDatosMeteoUrl = window.oDatosMeteoUrl.replace("latitude/0","latitude/"+lat);
					window.oDatosMeteoUrl = window.oDatosMeteoUrl.replace("longitude/0","longitude/"+lon);

		        // when this is fired, the layer
		        // is done being initialized
		    })
		    .on('error', function(e) {
					console.info("[Mapa.js] markerskml()  error "+e.message);

						// fired if the layer can't be loaded over AJAX
		        // or can't be parsed
		    })
		    .addTo(map);


		// omnivore.kml(kmlTest).addTo(map);

				// marker.bindPopup("<strong><a href=''"+itinerario[i].rrtt_web+"'>"+itinerario[i].rrtt_title[sIdioma]+"</a></strong><br/>").openPopup();
		console.info("[Mapa.js] markerskml()  end.");
	},

	render: function(){

		console.log("[Mapa.js] render() ");

		// console.log("[Mapa.js] getInitialState()  this.state.itinerario.data "+JSON.stringify(this.state.oDataMap.itinerario.data));


		// longitud = this.state.itinerario.data[0].t_resources.rrtt_longitude;
		// latitud = this.state.itinerario.data[0].t_resources.rrtt_latitude;
		// style="z-index: 6; white-space: nowrap; font-size: 20px; line-height: 30px; font-weight: 300; color: rgba(255, 255, 255, 8.00);font-family:Roboto;background: #000000; opacity:0.8; padding:0 30px 0 30px; margin-top: 200px ">
// style="display:block; font-size: 14px"


// <div className="tp-caption tp-resizeme"
// style={{ zIndex: 6, fontSize: 20, lineHeight: '30px', fontWeight: [300], color: 'rgba(255, 255, 255, 8.00)', fontFamily: 'Roboto',backgroundColor: 'rgba(0, 0, 0, 0.8)' , borderColor: 'rgba(0, 0, 0, 0)' , padding:'0 30px 0 30px', marginTop:200}}>
// <span style={{fontSize: 14, display: 'none' }}> {sIsla}</span>
// {sTitle}
// </div>
		return (

		<div>


					<div className="tp-caption   tp-resizeme"

					style={{zIndex: 6,
					 fontFamily: 'Roboto',
					  marginTop: '150px', marginLeft: '40px'}}>
					{mapType=='itinerario'&&
					<span	style={{zIndex: 6, whiteSpace: 'no-wrap', fontSize: '30px', lineHeight: '50px', fontWeight: 'bold', color: 'rgba(255, 255, 255, 1.00)', fontFamily: 'Roboto',
												backgroundColor: 'rgba(0, 0, 0, 0.7)',padding:'15px 30px 15px 30px'}}>{sIsla}</span>
				  }
				  {mapType=='itinerario'&&
					<div	style={{zIndex: 6, whiteSpace: 'no-wrap', fontSize: '20px', lineHeight: '50px', fontWeight: '300', color: 'rgba(255, 255, 255, 1.00)',fontFamily: 'Roboto',
												backgroundColor:'rgba(0, 0, 0, 0.7)',padding:'5px 30px 5px 30px', marginTop:'20px'}}>{sTitle}</div>
					}
					</div>

					{mapType=='cluster'?

					<div id="map" className="map mapcluster">
					</div>
					:
					<div id="map" className="map ">
					</div>
					}


		</div>

	)
//
// 	<div class="tpCaption   tp-resizeme" style="z-index: 6; white-space: nowrap; font-size: 20px; line-height: 30px; font-weight: 300; color: rgba(255, 255, 255, 8.00);font-family:Roboto;background: #000000; opacity:0.8; padding:0 30px 0 30px; margin-top: 200px "><span style="display:block; font-size: 14px"> MALLORCA</span>
// 	 Ruta de la Pedra en Sec </div>
// <div class="tp-caption tp-resizeme" style="z-index: 6; font-size: 20px; line-height: 30; font-weight: 300; color: rgb(255, 255, 255); font-family: Roboto; background-color: rgba(0, 0, 0, 0.8); border-color: rgba(0, 0, 0, 0); padding: 0px 30px; margin-top: 200px;"><span style="font-size: 14px; display: block;"> MALLORCA</span><!-- react-text: 4 -->Ruta de la Pedra en Sec<!-- /react-text --></div>


	}
});

module.exports = Mapa;
