var React = require('react');

var idioma = '';
var _host = '';
var isla = '';
var estaticos = require('../../resources/estaticosCabecera.json');
var dameTextoi18 = require('../webutils/utilidades.js').dameTextoi18;
var dameUrlFriendly = require('../webutils/utilidades.js').dameUrlFriendly;



var MenuCabecera = React.createClass({


  // getInitialState: function() {
	// 	/**
	// 	* Hay que distinguir entre servidor y navegador para hacer el renderizado.
	// 	*/
	// 	// Client side
	// 	if(this.props.sUrl){
	// 		var theUrl = String(this.props.sUrl);
	// 		console.log('URL '+ theUrl);
	// 		cargarDestacados(theUrl);
	// 	} else {
	// 		// Server side
	// 		destacados = this.props.componente;
	// 	}
	// 	ActionsCreator.updateDestacados({data:destacados});
	// 	return getStateFromStore();
	// },
	// componentDidMount: function(){
	// 	//console.warn('[Destacados] componentDidMount');
	//
	// 	DestacadosStore.addLoadListener(this.onLoad);
	// },
	// ComponentWillUnMount: function(){
	// 	DestacadosStore.removeLoadListener(this.onLoad);
	// },
	// onLoad: function() {
	// 	this.setState(getStateFromStore());
	// },



	clickMenu: function(sUrl) {
		console.log("[MenuCabecera.js] onClick..."+sUrl);
		location.href=sUrl;
	},
	onSearch: function(event) {
		console.log('[MenuCabecera.js] onSearch()...');

		if (event.type=='submit'){
			location.href=event.target.action+'#'+(event.target.children[0].value).toLowerCase();
		}


		console.log("[MenuCabecera.js] onSearch()... event.target.value: "+event.target.value);

		if (event.charCode==13){
			location.href=event.target.form.action+'#'+(event.target.value).toLowerCase();
			event.preventDefault();
		}
		else{
			console.log("[MenuCabecera.js] onSearch()... nothing to do...: ");
		}


		// console.log("[MenuCabecera.js] onSearch()... event.charCode: "+event.charCode);
    //
		// if (event.charCode==13&&!this.state.searchontype){
		// 	this.onSuggestionsFetchRequested(this.state.value,true);
		// 	event.preventDefault();
		// }
    //
		// console.log('[MenuCabecera.js] onAutoSuggest() currentPage'+currentPage);
		// console.log('[MenuCabecera.js] onSearch() event'+event.target.value);
		// if (dataSuggest.length>0){
		// 		this.setState({searcher: dataSuggest, totalItems: dataSuggest.length, pageOfItems: [], currentPage:1, searchNotFound: false, resetFilters: true, resetText:false});
		// }else{
		// 		this.setState({searcher: searcher, totalItems: totalItems, pageOfItems: [], currentPage:1, searchNotFound: true,resetFilters: true, resetText:false});
		// }
		// console.log('[Searcher.js] onAutoSuggest() 			this.state.currentPage: '+this.state.currentPage);
	},


	render: function(){

		console.log("[MenuCabecera.js] render...");
		console.log("[MenuCabecera.js] this.props.oDatosHeader.sIdioma..."+this.props.oDatosHeader.sIdioma);
		console.log("[MenuCabecera.js] this.props.oDatosHeader.sIsla..."+this.props.oDatosHeader.sIsla);
		console.log("[MenuCabecera.js] this.props.oDatosHeader.aUrlsFriendly..."+JSON.stringify(this.props.oDatosHeader.aUrlsFriendly));

		// console.log("[MenuCabecera.js] this.props.oDatosHeader.sPageType..."+this.props.oDatosHeader.sPageType);
		// console.log("[MenuCabecera.js] this.props.oDatosHeader.sUrl..."+this.props.oDatosHeader.sUrl);

		var idioma = String(this.props.oDatosHeader.sIdioma);
		var isla = String(this.props.oDatosHeader.sIsla);
		var aUrlsFriendly =  this.props.oDatosHeader.aUrlsFriendly;

    if (isla ==''){isla ='baleares'};

		// var aUrlsFriendlyClient = JSON.stringify(this.props.oDatosHeader.aUrlsFriendly);

    var aUrlsFriendlyClient = JSON.stringify(this.props.oDatosHeader.aUrlsFriendly);
        aUrlsFriendlyClient = aUrlsFriendlyClient.replace(/"/g, "'");   // escape quotes


		console.log("[MenuCabecera.js] aUrlsFriendlyClient..."+aUrlsFriendlyClient);



		var style1 = {float:'left'};
		var style2 = {cursor:'pointer'};

		/***********************************************************************/
		/**********************	Literales		************************************/
		/***********************************************************************/
		// var strSetUrlCatalan	  = "javascript:estableceIdioma('ca','"+isla+"')";
		// var strSetUrlEspanol	  = "javascript:estableceIdioma('es','"+isla+"')";
		// var strSetUrlIngles 	  = "javascript:estableceIdioma('en','"+isla+"')";
		// var strSetUrlAleman 	  = "javascript:estableceIdioma('de','"+isla+"')";
		// var strSetUrlFrances	  = "javascript:estableceIdioma('fr','"+isla+"')";
		//
		//
		// var strUrlCatalan	  =
		// var strUrlEspanol	  = "javascript:estableceIdioma('es','"+isla+"','"+dameUrlFriendly(idFriendly,idioma,isla)+"')";
		// var strUrlIngles 	  = "javascript:estableceIdioma('en','"+isla+"','"+dameUrlFriendly(idFriendly,idioma,isla)+"')";
		// var strUrlAleman 	  = "javascript:estableceIdioma('de','"+isla+"','"+dameUrlFriendly(idFriendly,idioma,isla)+"')";
		// var strUrlFrances	  = "javascript:estableceIdioma('fr','"+isla+"','"+dameUrlFriendly(idFriendly,idioma,isla)+"')";
		//
		// console.log("[MenuCabecera.js]  strUrlCatalan: "+strUrlCatalan);

		// console.log("[MenuCabecera.js]  strUrlEspanol: "+strUrlEspanol);
		// console.log("[MenuCabecera.js]  strUrlIngles: "+strUrlIngles);
		// console.log("[MenuCabecera.js]  strUrlAleman: "+strUrlAleman);z
		// console.log("[MenuCabecera.js]  strUrlFrances: "+strUrlFrances);


		var strCaActivo 	  = (idioma.toUpperCase()=='CA')? "fa fa-check" : "";
		var strEsActivo		  = (idioma.toUpperCase()=='ES')? "fa fa-check" : "";
		var strEnActivo		  = (idioma.toUpperCase()=='EN')? "fa fa-check" : "";
		var strDeActivo		  = (idioma.toUpperCase()=='DE')? "fa fa-check" : "";
		var strFrActivo		  = (idioma.toUpperCase()=='FR')? "fa fa-check" : "";


    function _get(txt){
      return dameTextoi18(estaticos[0][txt],idioma);
    }

		console.log('className CA: ' + strCaActivo);
		console.log('className ES: ' + strEsActivo);

		// var sUrlHome  = '/' + idioma + '/baleares/';
		var sUrlHome  = '/';

		var sUrlMallorca	  = '/' + idioma + '/mallorca/';
		var sUrlMenorca	  	  = '/' + idioma + '/menorca/';
		var sUrlIbiza	 	  = '/' + idioma + '/ibiza/';
		var sUrlFormentera	  = '/' + idioma + '/formentera/';

		var sUrlMaryPlayas = '/' + idioma + '/' + isla + '/mar-playa/';
		var sUrlPlayas = '/' + idioma + '/' + isla + '/buscador-playas/';

		var strEspanol 			   		 = dameTextoi18(estaticos[0]['espanol'],idioma);
		var strCatalan 			   		 = dameTextoi18(estaticos[0]['catalan'],idioma);
		var strIngles 			   		 = dameTextoi18(estaticos[0]['ingles'],idioma);
		var strAleman 			   		 = dameTextoi18(estaticos[0]['aleman'],idioma);
		var strFrances 			   		 = dameTextoi18(estaticos[0]['frances'],idioma);
		var strIslas               = dameTextoi18(estaticos[0]['islas'],idioma);
		var strCuandoViajas        = dameTextoi18(estaticos[0]['cuandoViajas'],idioma);

		var strPrimavera           = dameTextoi18(estaticos[0]['primavera'],idioma);
		var strUrlPrimavera		   	 = dameUrlFriendly(estaticos[0]['url_primavera'],idioma,isla);
		var strVerano              = dameTextoi18(estaticos[0]['verano'],idioma);
		var strUrlVerano		   		 = dameUrlFriendly(estaticos[0]['url_verano'],idioma,isla);
		var strOtono               = dameTextoi18(estaticos[0]['otono'],idioma);
		var strUrlOtono		       	 = dameUrlFriendly(estaticos[0]['url_otono'],idioma,isla);
		var strInvierno            = dameTextoi18(estaticos[0]['invierno'],idioma);
		var strUrlInvierno		     = dameUrlFriendly(estaticos[0]['url_invierno'],idioma,isla);
		var strComoViajas          = dameTextoi18(estaticos[0]['comoViajas'],idioma);
		var strConFamilia          = dameTextoi18(estaticos[0]['conFamilia'],idioma);
		var strUrlFamilia          = dameUrlFriendly(estaticos[0]['url_familia'],idioma,isla);
		var strUrlAmigos           = dameUrlFriendly(estaticos[0]['url_amigos'],idioma,isla);
		var strConAmigos           = dameTextoi18(estaticos[0]['conAmigos'],idioma);
		var strConPareja           = dameTextoi18(estaticos[0]['conPareja'],idioma);
		var strUrlPareja           = dameUrlFriendly(estaticos[0]['url_pareja'],idioma,isla);
		var strPorTrabajo          = dameTextoi18(estaticos[0]['porTrabajo'],idioma);
		var strUrlTrabajo          = dameUrlFriendly(estaticos[0]['url_trabajo'],idioma,isla);
		var ofrecemos           = dameTextoi18(estaticos[0]['ofrecemos'],idioma);
		var strGuiaPractica        = dameTextoi18(estaticos[0]['guiaPractica'],idioma);
		var agenda              = dameTextoi18(estaticos[0]['agenda'],idioma);
		var agenda_url              = dameTextoi18(estaticos[0]['agenda_url'],idioma);
		var maryplayas          = dameTextoi18(estaticos[0]['maryplayas'],idioma);
		var playas 			   		 = dameTextoi18(estaticos[0]['playas'],idioma);
		var strBuscador            = dameTextoi18(estaticos[0]['buscador'],idioma);
		var strBuscador_help       = dameTextoi18(estaticos[0]['buscador_help'],idioma);
		var instalnauticas      = dameTextoi18(estaticos[0]['instalnauticas'],idioma);
		var activnauticas       = dameTextoi18(estaticos[0]['activnauticas'],idioma);
		var arteycultura        = dameTextoi18(estaticos[0]['arteycultura'],idioma);
		var patrimoniocultural  = dameTextoi18(estaticos[0]['patrimoniocultural'],idioma);
		var fiestasytradiciones = dameTextoi18(estaticos[0]['fiestasytradiciones'],idioma);
		var mice                = dameTextoi18(estaticos[0]['mice'],idioma);
    var deporteyturismoactivo        = dameTextoi18(estaticos[0]['deporteyturismoactivo'],idioma);
		var cicloturisme        = dameTextoi18(estaticos[0]['cicloturisme'],idioma);
		var golf                = dameTextoi18(estaticos[0]['golf'],idioma);
		var senderisme          = dameTextoi18(estaticos[0]['senderisme'],idioma);
    var turismoactivo       = dameTextoi18(estaticos[0]['turismoactivo'],idioma);

		var esdeveniments       = dameTextoi18(estaticos[0]['esdeveniments'],idioma);
		var paisaje             = dameTextoi18(estaticos[0]['paisaje'],idioma);
		var patrimonionatural    = dameTextoi18(estaticos[0]['patrimonionatural'],idioma);
		var birdwatching        = dameTextoi18(estaticos[0]['birdwatching'],idioma);
		var bienestar           = dameTextoi18(estaticos[0]['bienestar'],idioma);
		var turismosanitario    = dameTextoi18(estaticos[0]['turismosanitario'],idioma);
		var relax               = dameTextoi18(estaticos[0]['relax'],idioma);
		var gastronomia         = dameTextoi18(estaticos[0]['gastronomia'],idioma);
		var productostierra     = dameTextoi18(estaticos[0]['productostierra'],idioma);
		var enoturismo          = dameTextoi18(estaticos[0]['enoturismo'],idioma);
		var mercadosyferias     = dameTextoi18(estaticos[0]['mercadosyferias'],idioma);
    var gastronomiacomer    = dameTextoi18(estaticos[0]['gastronomiacomer'],idioma);
    var gastronomiacomer_url    = dameTextoi18(estaticos[0]['gastronomiacomer_url'],idioma);

		var gastronomiacomer            = dameTextoi18(estaticos[0]['gastronomiacomer'],idioma);
		var shopping            = dameTextoi18(estaticos[0]['shopping'],idioma);
		var strBuscador            = dameTextoi18(estaticos[0]['buscador'],idioma);
		var ocioyatracciones    = dameTextoi18(estaticos[0]['ocioyatracciones'],idioma);
		var rutasyplanes        = dameTextoi18(estaticos[0]['rutasyplanes'],idioma);
		var strUnesco              = dameTextoi18(estaticos[0]['unesco'],idioma);
		var strTramontana          = dameTextoi18(estaticos[0]['tramontana'],idioma);
		var strCulturaTalayot      = dameTextoi18(estaticos[0]['culturaTalayot'],idioma);
		var strReservaBiosfera     = dameTextoi18(estaticos[0]['reservaBiosfera'],idioma);
		// var strComoLlegar          = dameTextoi18(estaticos[0]['comoLlegar'],idioma);
		var strComoMoverse         = dameTextoi18(estaticos[0]['comoMoverse'],idioma);
		var strMapa                = dameTextoi18(estaticos[0]['mapa'],idioma);
		var strLocalizador         = dameTextoi18(estaticos[0]['localizador'],idioma);
		var strAppInteres          = dameTextoi18(estaticos[0]['appInteres'],idioma);
		var strPublicaciones       = dameTextoi18(estaticos[0]['publicaciones'],idioma);
		var strAccesible           = dameTextoi18(estaticos[0]['accesible'],idioma);
		var strDondeComer          = dameTextoi18(estaticos[0]['dondeComer'],idioma);
		var strAgenciasViajes      = dameTextoi18(estaticos[0]['agenciasViajes'],idioma);
		var strDondeDormir         = dameTextoi18(estaticos[0]['dondeDormir'],idioma);
		var strInformacionInteres  = dameTextoi18(estaticos[0]['informacionInteres'],idioma);
		var strPorAire             = dameTextoi18(estaticos[0]['porAire'],idioma);
		var strPorMar              = dameTextoi18(estaticos[0]['porMar'],idioma);
		var strPorTierra           = dameTextoi18(estaticos[0]['porTierra'],idioma);
		var strTurismoBaleares     = dameTextoi18(estaticos[0]['turismoBaleares'],idioma);
		var strInformacion         = dameTextoi18(estaticos[0]['informacion'],idioma);
		var strInfoTuristica       = dameTextoi18(estaticos[0]['infoTuristica'],idioma);
		var strSeguridad           = dameTextoi18(estaticos[0]['seguridad'],idioma);
		var strContacto          	 = dameTextoi18(estaticos[0]['contacto'],idioma);


    var comollegar          	 = dameTextoi18(estaticos[0]['comollegar'],idioma);
    var comomoverse          	 = dameTextoi18(estaticos[0]['comomoverse'],idioma);
    var gastronomiacomer       = dameTextoi18(estaticos[0]['gastronomiacomer'],idioma);
    var dondecomer          	 = dameTextoi18(estaticos[0]['dondecomer'],idioma);

    var dondedormir          	 = dameTextoi18(estaticos[0]['dondedormir'],idioma);
    var localizador          	 = dameTextoi18(estaticos[0]['localizador'],idioma);
    var publicaciones          = dameTextoi18(estaticos[0]['publicaciones'],idioma);
    var informacioninteres     = dameTextoi18(estaticos[0]['informacioninteres'],idioma);
    var accesible              = dameTextoi18(estaticos[0]['accesible'],idioma);

    var comollegar_url          	 = dameTextoi18(estaticos[0]['comollegar_url'],idioma);
    var comomoverse_url          	 = dameTextoi18(estaticos[0]['comomoverse_url'],idioma);
    var gastronomiacomer_url       = dameTextoi18(estaticos[0]['gastronomiacomer_url'],idioma);

    var dondecomer_url             = dameTextoi18(estaticos[0]['dondecomer_url'],idioma);
    var dondedormir_url          	 = dameTextoi18(estaticos[0]['dondedormir_url'],idioma);
    var localizador_url         	 = dameTextoi18(estaticos[0]['localizador_url'],idioma);
    var publicaciones_url          = dameTextoi18(estaticos[0]['publicaciones_url'],idioma);
    var informacioninteres_url     = dameTextoi18(estaticos[0]['informacioninteres_url'],idioma);
    var accesible_url              = dameTextoi18(estaticos[0]['accesible_url'],idioma);

    //
		// <form action={localizador_url} onSubmit={this.onSearch}>
		// 	<input type='submit' title={strBuscador_help}  className='form-control' placeholder={strBuscador_help} onChange={this.onSearch} onSubmit={this.onSearch} />
		// 	<div className='search-close'>
		// 	</div>
		// </form>

		return(
			<div role="navigation">
      <script id="aUrlsFriendlyClient" type="application/json">
          { aUrlsFriendlyClient }
      </script>

				<div className='blog-topbar' data-version="MenuCabecera.v.1.1">
					<div className='topbar-search-block'>

						<div className='container'>



							<form action={localizador_url} onSubmit={this.onSearch}>
							<input type="text" className="form-control" title={strBuscador_help} placeholder={strBuscador_help} onChange={this.onSearch} onKeyPress={this.onSearch}/>
							<div className="search-close"><i className="icon-close"></i></div>
							</form>


						</div>

					</div>
					<div className='container'>
						<div className='row'>
							<div className='col-sm-2 col-xs-4'>
								<ul className='topbar-list topbar-menu' >
									<li>
										<a href='javascript:void(0);'>
											Language <i className='fa  fa-chevron-down'></i>
										</a>
										<ul className='topbar-dropdown language'>
											<li><a lang='ca' href={aUrlsFriendly.ca}> <img alt={strCatalan} src='/assets/images/navigation/catala.png'   />{strCatalan} (CA)  <i className={strCaActivo}></i> </a></li>
											<li><a lang='es' href={aUrlsFriendly.es}> <img alt={strEspanol} src='/assets/images/navigation/spain.png'   />{strEspanol} (ES) <i className={strEsActivo}></i> </a></li>
											<li><a lang='en' href={aUrlsFriendly.en}> <img alt={strIngles}  src='/assets/images/navigation/english.png'  />{strIngles} (EN)   <i className={strEnActivo}></i> </a></li>
											<li><a lang='de' href={aUrlsFriendly.de}> <img alt={strAleman}  src='/assets/images/navigation/germany.png'   />{strAleman} (DE)  <i className={strDeActivo}></i> </a></li>
										</ul>
									</li>
								</ul>
							</div>
							<div className='col-sm-6 col-xs-4'>

								<div id='tempoWeather'>
									<div>
										<span className='tiempolabel'>Mallorca</span>
										<span id='mallorca-weather' style={style1}></span>
									</div>
									<div>
										<span className='tiempolabel'>Menorca</span>
										<span id='menorca-weather'></span>
									</div>
									<div>
										<span className='tiempolabel'>Ibiza</span>
										<span id='ibiza-weather'></span>
									</div>
									<div>
										<span className='tiempolabel'>Formentera</span>
										<span id='formentera-weather'></span>
									</div>

								</div>

							</div>
							<div className='col-sm-4 col-xs-4'> <i className='fa fa-search search-btn pull-right'></i>
							</div>
						</div>
					</div>
				</div>
				<div className='navbar mega-menu' role='navigation' data-version='MenuCabecera.v1.0'>
					<div className='container'>
						<div className='res-container'>
							<button type='button' className='navbar-toggle' data-toggle='collapse' data-target='.navbar-responsive-collapse'>
								<span className='sr-only'>Menu</span>
								<span className='icon-bar'></span>
								<span className='icon-bar'></span>
								<span className='icon-bar'></span>
							</button>
							<div className='navbar-brand'>
								<a href={sUrlHome}>
									<img alt='Logo Illesbalears.travel' id='logo-header' src='/assets/images/logo.png'/>
								</a>
							</div>
						</div>
						<div className='collapse navbar-collapse navbar-responsive-collapse'  data-version='MenuCabecera.v1.0'>
							<div className='res-container'>
								<ul className='nav navbar-nav'>
									<li className='dropdown mega-menu-fullwidth'>
										<a href='javascript:void(0);' className='dropdown-toggle' data-hover='dropdown' data-toggle='dropdown'> {strIslas} </a>
										<ul className='dropdown-menu'>
											<li>
												<div className='mega-menu-content'  data-version='MenuCabecera.v1.0'>
													<div className='container'>
														<div className='row'>
															<div className='col-md-3 col-xs-3' style={style2} >
																<div className='thumbnails thumbnail-style thumbnail-kenburn'>
																		<div className='thumbnail-img' onClick={() => this.clickMenu(sUrlMallorca)}>
																			<div className='overflow-hidden'>
																				<img alt='Menu Mallorca' className='img-responsive' src='/assets/images/navigation/mallorca-578x325.jpg' alt='' id='mallorca_1' />
																			</div>
																			<span className='btn-more' id='mallorca_2'><strong>MALLORCA</strong></span>
																		</div>

																</div>
															</div>

															<div className='col-md-3 col-xs-3' style={style2}>
																<div className='thumbnails thumbnail-style thumbnail-kenburn'>
																	<div className='thumbnail-img' onClick={() => this.clickMenu(sUrlMenorca)}>
																		<div className='overflow-hidden'>
																			<img alt='Menu Menorca' className='img-responsive' src='/assets/images/navigation/menorca-578x325.jpg' alt='' id='menorca_1' />
																		</div>
																		<span className='btn-more' id='menorca_2'><strong>MENORCA</strong></span>
																	</div>
																</div>
															</div>
															<div className='col-md-3 col-xs-3' style={style2} >
																<div className='thumbnails thumbnail-style thumbnail-kenburn'>
																	<div className='thumbnail-img' onClick={() => this.clickMenu(sUrlIbiza)}>
																		<div className='overflow-hidden'>
																			<img alt='Menu Ibiza' className='img-responsive' src='/assets/images/navigation/ibiza-578x325.jpg'  alt='' id='ibiza_1' />
																		</div>
																		<span className='btn-more' id='ibiza_2' ><strong>IBIZA</strong></span>
																	</div>
																</div>
															</div>
															<div className='col-md-3 col-xs-3' style={style2}>
																<div className='thumbnails thumbnail-style thumbnail-kenburn'>
																	<div className='thumbnail-img' onClick={() => this.clickMenu(sUrlFormentera)}>
																		<div className='overflow-hidden'  id='formentera_1'>
																			<img alt='Menu Formentera' className='img-responsive' src='/assets/images/navigation/formentera-578x325.jpg'  />
																		</div>
																		<span className='btn-more' id='formentera_2'><strong>FORMENTERA</strong></span>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</li>
										</ul>
									</li>
									<li className='dropdown mega-menu-fullwidth'>
										<a href='javascript:void(0);' className='dropdown-toggle' data-hover='dropdown' data-toggle='dropdown'>{strCuandoViajas}</a>
										<ul className='dropdown-menu'>
											<li>
												<div className='mega-menu-content'>
													<div className='container'>
														<div className='row'>
															<div className='col-md-3 col-xs-3' style={style2} >
																<div className='thumbnails thumbnail-style thumbnail-kenburn'>
																	<div className='thumbnail-img' onClick={() => this.clickMenu(strUrlPrimavera)}>
																		<div className='overflow-hidden'>
																			<img alt={'Menu '+strPrimavera} className='img-responsive' src='/assets/images/navigation/primavera-578x325.jpg' title={strUrlPrimavera}/>
																		</div>
																		<span className='btn-more' title={strPrimavera} ><strong>{strPrimavera}</strong></span>
																	</div>
																</div>
															</div>
															<div className='col-md-3 col-xs-3' style={style2} >
																<div className='thumbnails thumbnail-style thumbnail-kenburn'>
																	<div className='thumbnail-img' onClick={() => this.clickMenu(strUrlVerano)}>
																		<div className='overflow-hidden'>
																			<img alt={'Menu '+strVerano} className='img-responsive' src='/assets/images/navigation/verano-578x325.jpg' title={strUrlVerano} />
																		</div>
																		<span className='btn-more' title={strUrlVerano} ><strong>{strVerano}</strong></span>
																	</div>
																</div>
															</div>
															<div className='col-md-3 col-xs-3'  style={style2} >
																<div className='thumbnails thumbnail-style thumbnail-kenburn'>
																	<div className='thumbnail-img' onClick={() => this.clickMenu(strUrlOtono)}>
																		<div className='overflow-hidden'>
																			<img alt={'Menu '+strVerano} className='img-responsive' src='/assets/images/navigation/otono-578x325.jpg' title={strUrlOtono} />
																		</div>
																		<span className='btn-more' title={strUrlOtono} ><strong>{strOtono}</strong></span>
																	</div>
																</div>
															</div>
															<div className='col-md-3 col-xs-3' style={style2} >
																<div className='thumbnails thumbnail-style thumbnail-kenburn'>
																	<div className='thumbnail-img' onClick={() => this.clickMenu(strUrlInvierno)}>
																		<div className='overflow-hidden'>
																			<img alt={'Menu '+strInvierno} className='img-responsive' src='/assets/images/navigation/invierno-578x325.jpg' title={strUrlInvierno} />
																		</div>
																		<span className='btn-more' title={strUrlInvierno}><strong>{strInvierno}</strong></span>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</li>
										</ul>
									</li>
									<li className='dropdown mega-menu-fullwidth'>
										<a href='javascript:void(0);' className='dropdown-toggle' data-hover='dropdown' data-toggle='dropdown'>{strComoViajas}</a>
										<ul className='dropdown-menu'>
											<li>
												<div className='mega-menu-content'>
													<div className='container'>
														<div className='row'>
															<div className='col-md-3 col-xs-3' style={style2} >
																<div className='thumbnails thumbnail-style thumbnail-kenburn'>
																	<div className='thumbnail-img' onClick={() => this.clickMenu(strUrlFamilia)}>
																		<div className='overflow-hidden'>
																			<img alt={'Menu '+strConFamilia} className='img-responsive' src='/assets/images/navigation/familia-578x325.jpg' title={strUrlFamilia} />
																		</div>
																		<span className='btn-more' title={strUrlFamilia} ><strong>{strConFamilia}</strong></span>
																	</div>
																</div>
															</div>
															<div className='col-md-3 col-xs-3' style={style2} >
																<div className='thumbnails thumbnail-style thumbnail-kenburn'>
																	<div className='thumbnail-img' onClick={() => this.clickMenu(strUrlAmigos)}>
																		<div className='overflow-hidden'>
																			<img alt={'Menu '+strConAmigos} className='img-responsive' src='/assets/images/navigation/amigos-578x325.jpg' title={strUrlAmigos} />
																		</div>
																		<span className='btn-more' title={strUrlAmigos}><strong>{strConAmigos}</strong></span>
																	</div>
																</div>
															</div>
															<div className='col-md-3 col-xs-3' style={style2} >
																<div className='thumbnails thumbnail-style thumbnail-kenburn'>
																	<div className='thumbnail-img' onClick={() => this.clickMenu(strUrlPareja)}>
																		<div className='overflow-hidden'>
																			<img alt={'Menu '+strConPareja} className='img-responsive' src='/assets/images/navigation/pareja-578x325.jpg' title={strUrlPareja}/>
																		</div>
																		<span className='btn-more'  title={strUrlPareja}><strong>{strConPareja}</strong></span>
																	</div>
																</div>
															</div>
															<div className='col-md-3 col-xs-3'  style={style2} >
																<div className='thumbnails thumbnail-style thumbnail-kenburn'>
																	<div className='thumbnail-img' onClick={() => this.clickMenu(strUrlTrabajo)}>
																		<div className='overflow-hidden'>
																			<img alt={'Menu '+strPorTrabajo} className='img-responsive' src='/assets/images/navigation/negocios-578x325.jpg' title={strUrlTrabajo} />
																		</div>
																		<span className='btn-more'  title={strUrlTrabajo}><strong>{strPorTrabajo}</strong></span>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</li>
										</ul>
									</li>




                  									<li className='dropdown mega-menu-fullwidth'>
                  										<a href='javascript:void(0);' className='dropdown-toggle' data-hover='dropdown' data-toggle='dropdown'>{_get('ofrecemos')}</a>

                                      <ul className="dropdown-menu">
                                            <li>
                                              <div className="mega-menu-content">
                                                <div className="container">
                                                  <div className="row">
                                                    <div className="col-md-2 col-sm-6">
                                                      <span className="mega-menu-heading"><a href={_get('arteycultura_url')}>{_get('arteycultura')}</a></span>
                                                      <ul className="list-unstyled style-list">
                                                        <li><a href={_get('patrimoniocultural_url')}>{_get('patrimoniocultural')}</a></li>
                                                        <li><a href={_get('fiestasytradiciones_url')}>{_get('fiestasytradiciones')}</a></li>
																												<li><a href={_get('artesania_url')}>{_get('artesania')}</a></li>
																								      </ul>
                                                      <span className="mega-menu-heading"><a href={_get('paisaje_url')}>{_get('paisaje')}</a></span>
                                                      <ul className="list-unstyled style-list">
                                                        <li><a href={_get('patrimonionatural_url')}>{_get('patrimonionatural')}</a></li>
                                                        <li><a href={_get('birdwatching_url')}>{_get('birdwatching')}</a></li>
                                                      </ul>
                                                    </div>


                                                    <div className="col-md-2 col-sm-6">
                                                      <span className="mega-menu-heading"><a href={_get('deporteyturismoactivo_url')}>{_get('deporteyturismoactivo')}</a></span>
                                                      <ul className="list-unstyled style-list">
                                                        <li><a href={_get('cicloturisme_url')}>{_get('cicloturisme')}</a></li>
                                                        <li><a href={_get('golf_url')}>{_get('golf')} </a></li>
                                                        <li><a href={_get('senderisme_url')}>{_get('senderisme')} </a></li>
                                                        <li><a href={_get('turismoactivo_url')}>{_get('turismoactivo')} </a></li>
                                                        <li><a href={_get('eventosdeportivos_url')}>{_get('eventosdeportivos')}</a></li>
                                                      </ul>
                                                    </div>

                                                    <div className="col-md-2 col-sm-6">
                                                      <span className="mega-menu-heading"><a href={_get('maryplayas_url')}>{_get('maryplayas')}</a> </span>
                                                      <ul className="list-unstyled style-list">
                                                        <li><a href={_get('playas_url')}>{_get('playas')}</a></li>
                                                        <li><a href={_get('instalnauticas_url')} >{_get('instalnauticas')} </a></li>
                                                        <li><a href={_get('activnauticas_url')}>{_get('activnauticas')} </a></li>
                                                      </ul>
                                                      <span className="mega-menu-heading"><a href={_get('bienestar_url')}>{_get('bienestar')}</a></span>
                                                      <ul className="list-unstyled style-list">
                                                        <li><a href={_get('turismosanitario_url')}>{_get('turismosanitario')}</a></li>
                                                        <li><a href={_get('relax_url')}>{_get('relax')}</a></li>
                                                      </ul>
                                                    </div>
                                                    <div className="col-md-2 col-sm-6">
                                                      <span className="mega-menu-heading"><a href={_get('gastronomia_url')}>{_get('gastronomia')}</a></span>
                                                      <ul className="list-unstyled style-list">
                                                        <li><a href={_get('productostierra_url')}>{_get('productostierra')}</a></li>
                                                        <li><a href={_get('enoturismo_url')}>{_get('enoturismo')}</a></li>
                                                        <li><a href={_get('mercadosyferias_url')}>{_get('mercadosyferias')}</a></li>
                                                        <li><a href={_get('gastronomiacomer_url')}>{_get('gastronomiacomer')}</a></li>
                                                      </ul>
                                                    </div>
                                                    <div className="col-md-2 col-sm-6">
                                                      <span className="mega-menu-heading"><a href={_get('ocioyatracciones_url')}>{_get('ocioyatracciones')}</a></span>
                                                      <span className="mega-menu-heading"><a href={_get('shopping_url')}>{_get('shopping')}</a></span>
                                                      <span className="mega-menu-heading"><a href={_get('rutasyplanes_url')}>{_get('rutasyplanes')}</a></span>
                                                    </div>
                                                    <div className="col-md-2 col-sm-6">
                                                      <span className="mega-menu-heading"><a href={_get('mice_url')}>{_get('mice')}</a></span>
                                                      <span className="mega-menu-heading"><a href={_get('unesco_url')}>{_get('unesco')}</a></span>
                                                    </div>



                                                  </div>
                                                </div>
                                              </div>
                                            </li>
                                          </ul>

                                        </li>











                                    <li className='dropdown mega-menu-fullwidth'>
          										              <a href='javascript:void(0);' className='dropdown-toggle' data-hover='dropdown' data-toggle='dropdown'>{strGuiaPractica}</a>
                                            <ul className="dropdown-menu">
                                              <li>
                                                <div className="mega-menu-content">
                                                  <div className="container">
                                                    <div className="row">
                                                      <div className="col-md-3 col-sm-3">
                                                        <span className="mega-menu-heading"><a href={comollegar_url}>{comollegar}</a></span>
                                                        <span className="mega-menu-heading"><a href={comomoverse_url}>{comomoverse}</a></span></div>
                                                      <div className="col-md-3 col-sm-3">
                                                        <span className="mega-menu-heading"><a href={dondecomer_url}>{dondecomer}</a></span>
                                                        <span className="mega-menu-heading"><a href={dondedormir_url}>{dondedormir}</a></span>   </div>
                                                      <div className="col-md-3 col-sm-3">
                                                       <span className="mega-menu-heading"><a href={localizador_url}>{localizador}</a></span>
                                                       <span className="mega-menu-heading"><a href={publicaciones_url}>{publicaciones}</a></span>
                                                      </div>
                                                      <div className="col-md-3 col-sm-3">
                                                       <span className="mega-menu-heading"><a href={informacioninteres_url}>{informacioninteres}</a> </span>
                                                       <span className="mega-menu-heading"><a href={accesible_url}>{accesible}</a></span>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>
                                              </li>
                                            </ul>
          										         </li>


									<li><a href={agenda_url}>{agenda}</a></li>
								</ul>

							</div>
						</div>
					</div>
				</div>

			</div>
		);
	}
});

module.exports = MenuCabecera;
