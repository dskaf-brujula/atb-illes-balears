var React = require('react');
var _ = require('lodash');
var capitalizedString = require('../webutils/utilidades.js').capitalizedString;


var idioma = '';
var _host = '';
var isla = '';



var BreadCrumbs = React.createClass({

	handleClick: function(sUrl) {
		console.log("[BreadCrumbs.js] onClick..."+sUrl);
		location.href=sUrl;
	},
	render: function(){

		console.log("[BreadCrumbs.js] render...");

    console.log("[BreadCrumbs.js] this.props.pagetype..."+this.props.pagetype);
    console.log("[BreadCrumbs.js] this.props.sIdioma..."+this.props.oDatosBreadCrumbs.sIdioma);
    console.log("[BreadCrumbs.js] this.props.sTitle..."+this.props.oDatosBreadCrumbs.sTitle);


    var pagetype  = this.props.pagetype;
    var sIdioma = this.props.oDatosBreadCrumbs.sIdioma;
    var sIsla = this.props.oDatosBreadCrumbs.sIsla;
    var sTitle = this.props.oDatosBreadCrumbs.sTitle;
    var sUrl = this.props.oDatosBreadCrumbs.sUrl;

    var sUrlHome  = '/' + sIdioma + '/baleares/';
    // var sUrlBaleares  = '/' + sIdioma + '/baleares/';
    var sUrlIsla	  = '/' + sIdioma + '/'+sIsla+'/';


    var breadcrumbText = [];
    var breadcrumbUrls = [];
    var breadcrumbsNum = 0;

    if (pagetype!='Ficha'){

      console.log("[BreadCrumbs.js] this.props.sIsla..."+this.props.oDatosBreadCrumbs.sIsla);
      // console.log("[BreadCrumbs.js] this.props.sTerm..."+this.props.oDatosBreadCrumbs.sTitle);
      console.log("[BreadCrumbs.js] this.props.sUrl..."+this.props.oDatosBreadCrumbs.sUrl);
      console.log("[BreadCrumbs.js] this.props.breadcrumb..."+this.props.oDatosBreadCrumbs.breadcrumb);



      // var sTerm = this.props.oDatosBreadCrumbs.sTerm;


      // var sUrls =  this.props.oDatosBreadCrumbs.sUrl.split('/');

			// _.words('/ca/baleares/art-i-cultura/patrimoni-cultural',/[^\/ ]+/g);
			// (4) ["ca", "baleares", "art-i-cultura", "patrimoni-cultural"]
			//
			var sUrlPieces = _.words(sUrl,/[^\/ ]+/g);


      sUrlPieces.map(function(brcmb,index){
		        if (index==0){
								breadcrumbUrls[index]='/'+brcmb+'/'+'baleares'+'/';
						}
						else{
							breadcrumbUrls[index]='/'+_.join(_.slice(sUrlPieces,0,index+1),'/')+'/';
						}
						 console.log("[BreadCrumbs.js] breadcrumbUrls["+index+"]: "+  breadcrumbUrls[index]);
      });


			breadcrumbText = this.props.oDatosBreadCrumbs.breadcrumb.split('/');
			breadcrumbsNum = breadcrumbUrls.length-1;


    }else{

      // breadcrumbText = rastroLanding(this.props.oDatosBreadCrumbs.sUrl);
      breadcrumbText[0]="Home";
      breadcrumbText[1]=capitalizedString(sIsla);
      breadcrumbText[2]=sTitle;

      // breadcrumbUrls[0]="";
      breadcrumbUrls[0]=sUrlHome;
      breadcrumbUrls[1]=sUrlIsla;
      breadcrumbUrls[2]=sUrl;
			breadcrumbsNum = breadcrumbUrls.length-1;
			console.log("[BreadCrumbs.js] breadcrumbText: "+JSON.stringify(breadcrumbText));

    }

    // BreadCrumbs.js] breadcrumbText...["Home","Illes Balears","Formentera","Formentera sap a ..."]
    // [BreadCrumbs.js] breadcrumbUrls...["","/ca/","/ca/baleares/","/ca/formentera/","#"]

    // [BreadCrumbs.js] breadcrumbUrls...["","ca","formentera",""]


    console.log("[BreadCrumbs.js] breadcrumbText..."+JSON.stringify(breadcrumbText));
    console.log("[BreadCrumbs.js] breadcrumbUrls..."+JSON.stringify(breadcrumbUrls) );
    console.log("[BreadCrumbs.js] breadcrumbUrls.length..."+breadcrumbUrls.length );



		return(

      <div className="container" data-version="BreadCrumbs.v1.1">
        <ul className="pull-left breadcrumb">
        {breadcrumbText.map((bcrumb, index) =>
             (index==breadcrumbsNum)? (
              <li key={index} data-url={breadcrumbUrls[index]} className='active'>
              {bcrumb}
              </li>
            ) : (
              <li key={index} data-url={breadcrumbUrls[index]}>
              <a href={breadcrumbUrls[index]}>{bcrumb}</a>
              </li>
            )
        )}
        </ul>
      </div>
		);
	}
});

module.exports = BreadCrumbs;
