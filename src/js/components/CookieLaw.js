var React = require('react');



var CookieLaw = React.createClass({

	render: function(){

				console.log('[CookieLaw.js] render() version 1.1');
				var idioma = this.props.idioma;


				console.log('[CookieLaw.js] render() idioma: '+idioma);

				switch (idioma) {

					case "es":
						return(

							<div id="barraaceptacion">
								<div className="inner">
									Solicitamos su permiso para obtener datos estad&iacute;sticos de su navegaci&oacute;n en esta web, en cumplimiento del Real Decreto-ley 13/2012. Si contin&uacute;a navegando consideramos que acepta el uso de cookies.
									<a href="javascript:HideCookieLaw();" className="Aceptar" ><b>Aceptar</b></a>
								</div>
							</div>

						)

							break;
					case "ca":

						return(
							<div id="barraaceptacion">
								<div className="inner">
								Sol·licitem el seu permís per obtenir dades estatístics de la seva navegació en aquesta web, en compliment del Reial decret llei 13/2012. Si continuau navegant considerem que acceptau l&apos;us de cookies.
								<a href="javascript:HideCookieLaw();" className="Aceptar" >Aceptar</a>
								</div>
							</div>
						)

					break;
					case "en":

						return(

							<div id="barraaceptacion">
								<div className="inner">
								We request your permission to obtain statistical data of your navigation on this website, in compliance with Royal Decree-Law 13/2012. If you continue browsing, we consider that you accept the use of cookies.
								<a href="javascript:HideCookieLaw();" className="Aceptar" >OK</a>
								</div>
							</div>
						)


					break;

					case "de":

					return(

						<div id="barraaceptacion">
							<div className="inner">
							Wir bitten um Erlaubnis, statistische Daten Ihrer Navigation auf dieser Website unter Einhaltung des Königlichen Dekrets 13/2012 zu erhalten. Wenn Sie weiter surfen, sehen wir, dass Sie die Verwendung von Cookies akzeptieren.
							<a href="javascript:HideCookieLaw();" className="Aceptar" >OK</a>
							</div>
						</div>

					)
					break;
				}



		}

});


module.exports = CookieLaw;
