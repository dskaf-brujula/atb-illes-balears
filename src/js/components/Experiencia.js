var React = require('react');
var ExperienciaStore = require('../stores/ExperienciaStore');
var ActionsCreator = require('../actions/appActions');
var dameTextoi18 = require('../webutils/utilidades.js').dameTextoi18;
var TagsButtons = require('./TagsButtons');


var experiencia = [];

function cargarExperiencia(theUrl){
  try{
	  	if(typeof $ != "undefined"){
		  $.ajax({
		    url: theUrl,
		    dataType: 'json',
		    cache: false,
		    async: false,
		    success: function(data){
				experiencia = data;
				console.log('ciclo vida 1 experiencia. success '+experiencia);
		    }.bind(this),
		    error: function(xhr, status, err) {
		      console.error(this.props.url, status, err.toString());
		    }.bind(this),
		    complete: function(xhr, status){
				console.log('ciclo vida 2 '+status+' '+experiencia);
		    }.bind(this)
		  });
	   	}

	} catch(e){
		console.log('Cargar experiencia ' + e.message);
	}
}

function getStateFromStore(){
	return {experiencia: ExperienciaStore.getAll()}
}


var Experiencia = React.createClass({

	getInitialState: function() {

		/**
		* Hay que distinguir entre servidor y navegador para hacer el renderizado.
		*/
		// Client side
		if(this.props.sUrl){
			var theUrl = String(this.props.sUrl);
			console.log('URL '+ theUrl);
			cargarExperiencia(theUrl);
		} else {
			// Server side
			experiencia = this.props.componente;
		}

		console.log("RENDER experiencia: " + experiencia )
		ActionsCreator.updateExperiencia({data:experiencia});
		console.warn('[experiencia] getInitialState');
		return getStateFromStore();
	},
	componentDidMount: function(){
		console.warn('[experiencia] componentDidMount');
		ExperienciaStore.addLoadListener(this.onLoad);
	},
	ComponentWillUnMount: function(){
		ExperienciaStore.removeLoadListener(this.onLoad);
	},
	onLoad: function() {
		this.setState(getStateFromStore());
	},
	onClick: function(event) {
		console.log("experiencia MANEJANDO CLICK EVENT ..");
	},
	render: function(){

		console.log("RENDER ..");

		var idioma = String(this.props.sIdioma);
		console.log("RENDER idioma: "+ idioma);

    var  componentType =  "Ficha";
    var  pagetype =  "exp";

		var _isla = this.state.experiencia.data.islands[0];
		var _clname = _isla.substring(0,2) + ' title';
		var _title = dameTextoi18(this.state.experiencia.data.title,idioma);
		var _subtitle = dameTextoi18(this.state.experiencia.data.subtitle,idioma);
		var _entradilla = dameTextoi18(this.state.experiencia.data.opening_paragraphs,idioma);
		var _descripcion = dameTextoi18(this.state.experiencia.data.description,idioma);


    var pattern = "./froala_upload/";
    var reg =  new RegExp(pattern, "g");
     _descripcion = _descripcion.replace(reg,'http://froala-editor.s3.amazonaws.com/');




    // Tags
    var oDatosTagsButtons = []
    try{

        console.log("[RRTT.js] oDatosTagsButtons...");
        oDatosTagsButtons.sIdioma = idioma;
        oDatosTagsButtons.sIsla = _isla;
        oDatosTagsButtons.oTouristicSubjects = this.state.rrtt.data.touristic_subjects;
        oDatosTagsButtons.oLifeStyles = this.state.rrtt.data.lifestyles;
        oDatosTagsButtons.pageType = pagetype;
        oDatosTagsButtons.componentType = componentType;


    }catch(e){
        console.log("[RRTT.js] Render (Error)..."+e.message);
    }

		console.log("RENDER titulo " + _title);

    // <p className='margin-bottom-30'>
    //   <button className='btn-u btn-u-xs rounded-4x btn-u-yellow' onClick={this.onClick}>cultura</button>
    //   <button className='btn-u btn-u-xs rounded-4x btn-u-red' type='button'>gastronomía</button>
    //   <button className='btn-u btn-u-xs rounded-4x btn-u-orange' type='button'>deporte</button>
    //   <button className='btn-u btn-u-xs rounded-4x btn-u-green' type='button'>naturaleza</button>
    //   <button className='btn-u btn-u-xs rounded-4x btn-u-blue' type='button'>naútica</button>
    //   <button className='btn-u btn-u-xs rounded-4x btn-u-purple' type='button'>ocio</button>
    //   <button className='btn-u btn-u-xs rounded-4x btn-u-default' type='button'>familia</button>
    //   <button className='btn-u btn-u-xs rounded-4x btn-u-default' type='button'>amics</button>
    // </p>



		return(
			  <section role="main" className='container content-sm txtalign'>
			    <div className='margin-bottom-30'>
            <h1 className={_clname}>{_title}<h1>
            <TagsButtons oDatosTagsButtons={oDatosTagsButtons} className="margin-bottom-30 margin-top-10"/>
            <h2 classname="title-v2 font-italic"> {_subtitle} </h2>
			      <p>{_opening_paragraph}</p>
            {renderHtml(_descripcion)}
			    </div>

			  </section>
		);
	}
});

module.exports = Experiencia;
