var React = require('react');
var AgendaStore = require('../stores/AgendaStore');
var ActionsCreator = require('../actions/appActions');
var dameTextoi18 = require('../webutils/utilidades.js').dameTextoi18;
var sortEventos = require('../webutils/utilidades.js').sortEventos;
var literales = require('../../resources/literalesAgenda.json');
var eventos = [];
var _ = require('lodash');



// Formatea Date de forma personalizada. @TODO: pasar a SharedFactory
function formatDate(date,language){

//console.log ("function - formatDate: "+language);

	if (language){
        switch (language.toUpperCase()) {
            case "ES":
                var m_names = new Array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
                break;
            case "EN":
                var m_names = new Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
                break;
            case "CA":
                var m_names = new Array ("Gener", "Febrer", "Març", "Abril", "Maig", "Juny", "juliol", "agost", "Setembre", "Octubre", "Novembre", "Desembre") ;
                break;
            case "DE":
                var m_names = new Array ( "Januar", "Februar", "März", "April", "Mai", "June", "Juli", "August", "September", "October", "November", "December") ;
                break;
        }

		var d = new Date(date);
		var curr_date = d.getDate();
		var curr_month = d.getMonth();
		var curr_year = d.getFullYear();

//console.log ("function - formatDate (response): "+curr_date + "." + m_names[curr_month].toUpperCase().substr(0,3) + "." + curr_year);


		return curr_date + "." + m_names[curr_month].toUpperCase().substr(0,3) + "." + curr_year;
	}else{

		return date.toLocaleDateString();

	}

}


// Obtiene los recursos para los cubos 'Agenda'.
function fetchDataOnClient(theUrl){
	try{
	  	if(typeof $ != "undefined"){
			$.ajax({
				url: theUrl,
				dataType: 'json',
				cache: false,
				async: false,
				success: function(data){
					eventos = data;
					//console.log('ciclo vida 1 eventos. success '+eventos);
				}.bind(this),
					error: function(xhr, status, err) {
					//console.error(this.props.url, status, err.toString());
				}.bind(this),
					complete: function(xhr, status){
					//console.log('ciclo vida 1 '+status+' '+eventos);
				}.bind(this)
			});
		}
	} catch(e){
		console.log('Cargar eventos ' + e.message);
	}
}

function getStateFromStore(){
	console.log('[Agenda.js] getStateFromStore ...');

	return {eventos: AgendaStore.getAll()}
}

var Agenda = React.createClass({
	//
	// handleClick: function(sUrl) {
	// 	console.log("[Agenda.js]. handleClick..."+sUrl);
	// 	location.href=sUrl;
	// },
	handleClick: function() {
		console.log("[Agenda.js]. handleClick...");

	},
	getInitialState: function() {

			//Client side
			if (this.props.render=='client'){
				console.log('[Recomendaciones.js] is client');

				if (this.props.sUrl) {
						var theUrl = String(this.props.sUrl);
						fetchDataOnClient(theUrl);
						ActionsCreator.updateAgenda({ data: eventos });
				}
			}
			// Server side
			else if (this.props.render=='server') {
				console.log('[Recomendaciones.js] is server');

				recomendaciones = this.props.componente;
				ActionsCreator.updateAgenda({ data: eventos });
			}
			else if (this.props.render=='isomorphic') {
				console.log('[Recomendaciones.js] is isomorphic');
			}

			return getStateFromStore();
	},



	componentDidMount: function(){
		//console.warn('[eventos] componentDidMount');
		console.log('[Agenda.js] componentDidMount()');
		$(this.refs['masfechas']).tooltip();
		$('[data-toggle="tooltip"]').tooltip();

		AgendaStore.addLoadListener(this.onLoad);
	},
	ComponentWillUnMount: function(){
		AgendaStore.removeLoadListener(this.onLoad);
	},
	onLoad: function() {
		this.setState(getStateFromStore());
	},
	clickAgenda: function(sUrl,target) {
		console.log("[Agenda.js] clickAgenda..."+sUrl);
		if (target="_blank"){
			window.open(sUrl);
		}else{
			location.href=sUrl;
		}
	},
	componentDidUpdate : function(){
		console.log('[Agenda.js] componentDidUpdate()');
		$(this.refs['masfechas']).tooltip();
		$('[data-toggle="tooltip"]').tooltip();
	},
	render: function(){
		var idioma = String(this.props.sIdioma);
		var style0 = {cursor: 'pointer'};
		var _titulo = dameTextoi18(literales[0].titulo,idioma);
		var _subtitulo = dameTextoi18(literales[0].subtitulo,idioma);


		var  _get  = function(txt){
			try{
			return literales[0][txt][idioma];
			} catch(e){
				console.log("[SearchFilters.js] render() _get(txt): "+txt+" Error: "+e.message);
				return txt;
			}
		}

		// var cubos = this.state.eventos.data.items.map(function(evento, index){
			var cubos = this.state.eventos.data.items.map((evento, index) =>{



			try{

				console.log('[Agenda.js] Evento Carousel número: '+index+' - - - - - - - -' );

				try{
					var _imagen = evento.multimedia[0].url;
					console.log('[Agenda.js]  _imagen: '+_imagen);
				}catch(e){
					console.log('[Agenda.js] evento '+index+', no tiene _imagen:\n' + e.message);
				}

				try{
					var _sortingdate = formatDate(evento.sorting_date,idioma);
					console.log('[Agenda.js]  _sortingdate: '+_sortingdate);
				}catch(e){
					console.log('[Agenda.js] evento '+index+', no tiene _sortingdate:\n' + e.message);
				}


				if ("alternate_dates" in evento){

					try{
							var _startdate = formatDate(evento.sorting_date,idioma);
							// var _enddate = formatDate(_.last(evento.alternate_dates),idioma);
							var _enddate = _startdate;
							console.log('[Agenda.js]  _startdate: '+_startdate);
							console.log('[Agenda.js]  _enddate: '+_enddate);

							// var _alternateDates =  _.join(evento.alternate_dates, ' / ');

							var _alternateDates = _.join(_.map(evento.alternate_dates,function(value, key) {return formatDate(value,idioma)}), ' / ');;




							// <a href="#" class="masfechas"><i class="fa fa-calendar"></i> Consultar más fechas para este evento</a>

					}catch(e){
						console.log('[Agenda.js] evento '+index+', no tiene _startdate:\n' + e.message);
					}

				}else{


						try{
							var _startdate = formatDate(evento.start_date,idioma);
							console.log('[Agenda.js]  _startdate: '+_startdate);
						}catch(e){
							console.log('[Agenda.js] evento '+index+', no tiene _startdate:\n' + e.message);
						}

						try{
							var _enddate = formatDate(evento.end_date,idioma);
							console.log('[Agenda.js]  _enddate: '+_enddate);
						}catch(e){
							console.log('[Agenda.js] evento '+index+', no tiene _enddate:\n' + e.message);
						}

				}

				try{
					var _dates  = (_startdate != _enddate) ? (_startdate+' - '+_enddate) : _startdate;
					console.log('[Agenda.js]  _dates: '+_dates);
				}catch(e){
					console.log('[Agenda.js] evento '+index+', no tiene _dates:\n' + e.message);
				}

				try{
					var _isla = evento.islands[0];
					console.log('[Agenda.js]  _isla: '+_isla);
				}catch(e){
					console.log('[Agenda.js] evento '+index+', no tiene _isla:\n' + e.message);
				}

				try{
					var _municipio = evento.municipalities.names[0];
					console.log('[Agenda.js]  _municipio: '+_municipio);
				}catch(e){
					console.log('[Agenda.js] evento '+index+', no tiene _municipio:\n' + e.message);
				}

				try{
					var _titulo = dameTextoi18(evento.title,idioma);
					var show_titulo = (_titulo.length>0);
					console.log('[Agenda.js]  _titulo: '+_titulo);
				}catch(e){
					console.log('[Agenda.js] evento '+index+', no tiene _titulo:\n' + e.message);
				}

				try{
					var _urlEvento = dameTextoi18(evento.link_urls,idioma);
					var _urlTarget = _urlEvento.indexOf('illesbalears.travel')>=0 ? '_self':'_blank';
					console.log('[Agenda.js]  _urlEvento: '+_urlEvento);
				}catch(e){
					console.log('[Agenda.js] evento '+index+', no tiene _urlEvento:\n' + e.message);
				}

				try{
					// var _style_color = '#'+evento.color+"80";
					var _style_color = '#'+evento.color;
					var _styles_bgcolor = {background: _style_color};
					console.log('[Agenda.js]  _style_color: '+_style_color+ '_styles_bgcolor: '+_styles_bgcolor);
				}catch(e){
					console.log('[Agenda.js] evento '+index+', no tiene _style_color:\n' + e.message);
				}


				return(
						<div role="complementary" data-version="Agenda-v1.2" className='item news-v2' style={style0} key={index} data-url={_urlEvento} onClick={() => this.clickAgenda(_urlEvento,_urlTarget)}>
							<img alt='' className='img-responsive' src={_imagen}  />
							<div className='news-v2-desc' style={_styles_bgcolor}>

								<div className="txtdata">{_dates}</div>
								{_alternateDates &&
								<span ref="masfechas" data-toggle="tooltip"  className="masfechas" title={_alternateDates}>{_.truncate(_alternateDates, {'length': 84})}</span>
								}

								<span className='txtilles'>{_get(_isla)}</span>
								<span className='txtmunicipi'>{_municipio}</span>
								{show_titulo&& <h3 className='txttitle'>{_titulo}</h3>}
							</div>
						</div>
				)

			} catch (e){
				console.log('[Agenda.js] Se ha producido una incidencia con el evento nº '+index+', y no se mostrará:\n' + e.message);
			}
		});

		// TODO: presentacion aleatoria
		// TODO mostrar máximo de 9 eventos o multiplo de 3.
//		cubos = sortEventos(cubos);


		var _center = "['center','center','center','center']";
		var _middle = "['middle','middle','middle','middle']";
		var _left = "['left','left','left','left']";
		var _top = "['top','top','top','top']";
		var _cero = "['0','0','0','0']";

		var str1 = "['50','50','50','30']";
		var str2 = "['165','135','105','130']";
		var str3 = "['50','50','50','30']";
		var str4 = "['55','55','55','35']";
		var str5 = "['600','600','600','420']";
		var str6 = "['50','50','50','30']";
		var str7 = "['140','110','80','100']";
		var str8 = "['53','53','53','30']";
		var str9 = "['361','331','301','245']";

		var style1 = {
			zIndex: 5,
			backgroundColor: 'rgba(0, 0, 0, 0.35)',
			borderColor: 'rgba(0, 0, 0, 1.00)'
		}
		var style2 = {
			zIndex: 6,
			minWidth: '600px',
			maxWidth: '600px',
			whiteSpace: 'normal'
		};

		var style3 = {
			zIndex: 7,
			whiteSpace: 'nowrap'
		}

	 	var style4 = {
				zIndex: 8,
				whiteSpace: 'nowrap',
				outline:'none',
				boShadow:'none',
				boxSizing:'border-box',
				MozBoxSizing:'border-box',
				WebkitBoxSizing:'border-box'
	 	}

	 	var style5 = {
	 		height: '5px',
	 		backgroundColor: '#3096ff'
	 	}

		// var eventosDestacados = this.state.eventos.data.items.map(function(eventoDestacado, index){


			var eventosDestacados = _.filter(this.state.eventos.data.items,function(o) { return ("featured" in o && o.featured)}).map((eventoDestacado, index) => {

			// var eventosDestacados = this.state.eventos.data.items.map((eventoDestacado, index) => {


			try{

				console.log('[Agenda.js] Evento Destacado número: '+index+' - - - - - - - -' );

				try{
					var _imagen = eventoDestacado.multimedia[0].url;
					console.log('[Agenda.js]  _imagen: '+_imagen);
				}catch(e){
					console.log('[Agenda.js] evento '+index+', no tiene _imagen:\n' + e.message);
				}


				if ("alternate_dates" in eventoDestacado){

					try{
							var _startdate = formatDate(eventoDestacado.sorting_date,idioma);
							var _enddate = _startdate;
							console.log('[Agenda.js]  _startdate: '+_startdate);
							console.log('[Agenda.js]  _enddate: '+_enddate);
					}catch(e){
						console.log('[Agenda.js] evento '+index+', no tiene _startdate:\n' + e.message);
					}

				}else{


						try{
							var _startdate = formatDate(eventoDestacado.start_date,idioma);
							console.log('[Agenda.js]  _startdate: '+_startdate);
						}catch(e){
							console.log('[Agenda.js] evento '+index+', no tiene _startdate:\n' + e.message);
						}

						try{
							var _enddate = formatDate(eventoDestacado.end_date,idioma);
							console.log('[Agenda.js]  _enddate: '+_enddate);
						}catch(e){
							console.log('[Agenda.js] evento '+index+', no tiene _enddate:\n' + e.message);
						}

				}




				try{
					var _isla = eventoDestacado.islands[0].toUpperCase();
					console.log('[Agenda.js]  _isla: '+_isla);
				}catch(e){
					console.log('[Agenda.js] evento '+index+', no tiene _isla:\n' + e.message);
				}

				try{
					var _area = eventoDestacado.areas.names[0];
					console.log('[Agenda.js]  _area: '+_area);
				}catch(e){
					console.log('[Agenda.js] evento '+index+', no tiene _area:\n' + e.message);
				}

				try{
					var _descripcion = dameTextoi18(eventoDestacado.title,idioma);
					console.log('[Agenda.js]  _descripcion: '+_descripcion);
				}catch(e){
					console.log('[Agenda.js] evento '+index+', no tiene _descripcion:\n' + e.message);
				}

				try{
					var _dataTitle = _startdate + ' | ' + _isla ;
					console.log('[Agenda.js]  _dataTitle: '+_dataTitle);
				}catch(e){
					console.log('[Agenda.js] evento '+index+', no tiene _dataTitle:\n' + e.message);
				}

				try{
					var _tituloDestacado = dameTextoi18(eventoDestacado.title,idioma);
					console.log('[Agenda.js]  _tituloDestacado: '+_tituloDestacado);
				}catch(e){
					console.log('[Agenda.js] evento '+index+', no tiene _tituloDestacado:\n' + e.message);
				}

				try{
					var _subtituloDestacado = dameTextoi18(eventoDestacado.subtitle,idioma);
					console.log('[Agenda.js]  _subtituloDestacado: '+_subtituloDestacado);
				}catch(e){
					console.log('[Agenda.js] evento '+index+', no tiene _subtituloDestacado:\n' + e.message);
				}

				try{
					var _urlEventoDestacado = dameTextoi18(eventoDestacado.link_urls,idioma);
					// var _urlTarget = _urlEventoDestacado.indexOf('illesbalears.travel')>=0 ? '_self':'_blank';
					var _urlTarget = '_blank';
					console.log('[Agenda.js]  _urlEventoDestacado: '+_urlEventoDestacado);
				}catch(e){
					console.log('[Agenda.js] evento '+index+', no tiene _urlEventoDestacado:\n' + e.message);
				}


				var _indice = 'rs-'+index;

				return (
							<li key = {index}
								data-index={_indice}
								data-transition='parallaxvertical'
								data-slotamount='default'
								data-easein='default'
								data-easeout='default'
								data-masterspeed='default'

								data-thumb = {_imagen}

								data-rotate='0'
								data-fstransition='fade'
								data-fsmasterspeed='1500'
								data-fsslotamount='7'
								data-saveperformance='off'

								data-title = {_dataTitle}

								data-description = {_descripcion}>

								<img alt='' src = {_imagen} alt='' data-bgposition='center center' data-bgfit='cover' data-bgrepeat='no-repeat' data-bgparallax='10' className='rev-slidebg' data-no-retina />

								<div className='tp-caption tp-shape tp-shapewrapper  tp-resizeme rs-parallaxlevel-0'
									 id={'slide-129-layer-3-'+index}
									 data-x= {_center} data-hoffset= {_cero}
									 data-y= {_middle} data-voffset= {_cero}
									data-width='full'
									data-height='full'
									data-whitespace='normal'
									data-transform_idle='o:1;'

									 data-transform_in='opacity:0;s:1500;e:Power3.easeInOut;'
									 data-transform_out='opacity:0;s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;'
									data-start='1000'
									data-basealign='slide'
									data-responsive_offset='on'

									style = {style1}>

								</div>

								<div className='tp-caption Newspaper-Title   tp-resizeme rs-parallaxlevel-0'
									 id={'slide-129-layer-1-'+index}
									 data-x= {_left} data-hoffset = {str1}
									 data-y= {_top} data-voffset = {str2}
									data-fontsize = {str3}
									data-lineheight = {str4}
									data-width = {str5}
									data-height='none'
									data-whitespace='normal'
									data-transform_idle='o:1;'

									 data-transform_in='y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;'
									 data-transform_out='auto:auto;s:1000;e:Power3.easeInOut;'
									 data-mask_in='x:0px;y:0px;s:inherit;e:inherit;'
									 data-mask_out='x:0;y:0;s:inherit;e:inherit;'
									data-start='1000'
									data-splitin='none'
									data-splitout='none'
									data-responsive_offset='on'


									style = {style2}>

									{_tituloDestacado.toUpperCase()}: {_subtituloDestacado}

								</div>

								<div className='tp-caption Newspaper-Subtitle   tp-resizeme rs-parallaxlevel-0'
									 id={'slide-129-layer-2-'+index}
									 data-x= {_left} data-hoffset = {str6}
									 data-y= {_top} data-voffset = {str7}
									data-width='none'
									data-height='none'
									data-whitespace='nowrap'
									data-transform_idle='o:1;'

									 data-transform_in='y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;'
									 data-transform_out='auto:auto;s:1000;e:Power3.easeInOut;'
									 data-mask_in='x:0px;y:0px;s:inherit;e:inherit;'
									 data-mask_out='x:0;y:0;s:inherit;e:inherit;'
									data-start='1000'
									data-splitin='none'
									data-splitout='none'
									data-responsive_offset='on'


									style = {style3}>

									{_dataTitle}

								</div>

								<div className='tp-caption Newspaper-Button rev-btn  rs-parallaxlevel-0'
									 id={'slide-129-layer-5-'+index}
									 data-x= {_left} data-hoffset = {str8}
									 data-y= {_top} data-voffset = {str9}
												data-width='none'
									data-height='none'
									data-whitespace='nowrap'
									data-transform_idle='o:1;'
										data-transform_hover='o:1;rX:0;rY:0;rZ:0;z:0;s:300;e:Power1.easeInOut;'

										data-style_hover = 'c:rgba(255, 255, 255, 1.00);bc:rgba(255, 255, 255, 1.00);cursor:pointer;'

									 data-transform_in='y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;'
									 data-transform_out='auto:auto;s:1000;e:Power3.easeInOut;'
									 data-mask_in='x:0px;y:0px;'
									 data-mask_out='x:0;y:0;'
									data-start='1000'
									data-splitin='none'
									data-splitout='none'
									data-responsive_offset='on'
									data-responsive='off'

									style = {style4} data-url={_urlEventoDestacado} onClick={() => this.clickAgenda(_urlEventoDestacado,_urlTarget)}> {_get('vermas')}
								</div>
							</li>
						)
			} catch (e){
				console.log('[Agenda.js] Se ha producido una incidencia con el evento destacado nº '+index+', y no se mostrará:\n' + e.message);
			}
		});


		return(

			<div role="complementary">
				<div  id='rev_slider_34_1_wrapper' className='rev_slider_wrapper fullwidthbanner-container' data-alias='news-gallery34'>
					<div id='rev_slider_34_1' className='rev_slider fullwidthabanner'  data-version='5.0.7' style={{backgroundColor: '#e5e5e5'}}>
						<ul>
							{eventosDestacados}
						</ul>
						<div className='tp-bannertimer tp-bottom' style={style5}></div>
					</div>
				</div>
				<div className='margin-top-40'></div>
				<section className='container'>
					<div className='text-center margin-bottom-30'>
						<h2 className='title-v2 title-center'> {_titulo} </h2>
						<p className=''>{_subtitulo}</p>
						<div className='owl-carousel owl-theme margin-top-30'>
							{cubos}
						</div>
					</div>
				</section>

			</div>

		);
	}
});

module.exports = Agenda;
