var React = require('react');



var WelcomeModal = React.createClass({


	handleClick: function(event){
		console.log('[WelcomeModal.js] handleClick() ');

		window.HideWelcomeModal();

	},
	render: function(){

				console.log('[WelcomeModal.js] render() version 1.1');
				var idioma = this.props.idioma;


				console.log('[WelcomeModal.js] render() idioma: '+idioma);
				switch (idioma) {

					case "es":
						return(

								<div className="modal fade bs-example-modal-lg" tabindex="-1" role="dialog"  aria-hidden="true"  id="onload">
										<div className="modal-dialog">
											<div className="modal-content">
												<div className="modal-header">
													<button type="button" className="close" data-dismiss="modal">×</button>
													<h4 className="modal-title"><i className="fa fa-exclamation-circle"></i>Bienvenid@ a Illes Balears</h4>
												</div>
												<div className="modal-body">
												<p><img alt='' src="/assets/images/logo.png" alt="Logo Illes Balears"/></p>
												 <h4><strong> Renovamos el portal turístico www.illesbalears.travel</strong></h4>
												 <p> Seguimos trabajando para ofrecer la mejor experiencia de uso, accesibilidad, navegación, con contenidos audiovisuales y una gran cantidad de información de los recursos turísticos de las islas. Nos encontramos en fase Beta y próximamente será el lanzamiento oficial del portal turístico,&nbsp;<a href="#news" data-dismiss="modal">suscríbete a nuestra newsletter</a> para que podamos avisarte.</p>
												 <p>Mientras tanto estaremos encantados de recibir tus opiniones, escríbenos a <a href="mailto:info@illesbalears.travel">info@illesbalears.travel</a>, todas las sugerencias nos ayudarán a mejorar la calidad del portal.</p>
												<p>Muchas gracias por tu colaboración. <br/><i>Agència de Turisme de les Illes Balears</i> </p>
												</div>
												<div className="modal-footer">
													<button type="button" className="btn-u btn-u-default" data-dismiss="modal" onClick={this.handleClick}>Gracias!</button>
												</div>
											</div>
										</div>
								</div>

						)

							break;
					case "ca":

						return(

							<div className="modal fade bs-example-modal-lg" tabindex="-1" role="dialog"  aria-hidden="true"  id="onload">
										<div className="modal-dialog">
											<div className="modal-content">
												<div className="modal-header">
												<button type="button" className="close" data-dismiss="modal">×</button>
													<h4 className="modal-title"><i className="fa fa-exclamation-circle"></i>Benvingut a Illes Balears</h4>
												</div>
												<div className="modal-body">
											 <p> <img alt='' src="/assets/images/logo.png" alt="Logo Illes Balears"/></p>
												 <h4><strong>Renovem el portal turístic www.illesbalears.travel</strong></h4>
												<p>Seguim treballant per oferir la millor experiència d'ús accessibilitat, navegació, amb continguts audiovisuals i una gran quantitat d'informació dels recursos turístics de les illes. Ens trobem en fase Beta i pròximament serà el llançament oficial del portal turístic,&nbsp;
												 <a href="#news" data-dismiss="modal">subscriu-te a la nostra newsletter</a> perquè puguem avisar.</p>
												<p>Mentrestant estarem encantats de rebre les teves opinions, escriu-nos a <a href="mailto:info@illesbalears.travel">info@illesbalears.travel</a>, tots els suggeriments ens ajudaran a millorar la qualitat del portal.</p>
												<p>Moltes gràcies per la vostra col·laboració. <br/><i>Agència de Turisme de les Illes Balears</i> </p>
												</div>
												<div className="modal-footer">
													<button type="button" className="btn-u btn-u-default" data-dismiss="modal"  onClick={this.handleClick}>Gràcies!</button>
												</div>
											</div>
										</div>
								</div>
						)

					break;
					case "en":

						return(

							<div className="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true"  id="onload">
											<div className="modal-dialog">
												<div className="modal-content">
													<div className="modal-header">
													<button type="button" className="close" data-dismiss="modal">×</button>
														<h4 className="modal-title"><i className="fa fa-exclamation-circle"></i>Welcome to Illes Balears</h4>
													</div>
													<div className="modal-body">
												 <p> <img alt='' src="/assets/images/logo.png" alt="Logo Balearic Islands"/></p>
													 <h4><strong>Renew the tourist portal www.illesbalears.travel</strong></h4>
													<p>We continue to work to provide the best user experience, accessibility, navigation, audiovisual content and a large amount of information of tourism resources of the islands. We are in Beta phase and will soon be the official launch of the tourist portal,&nbsp; <a href="#news" data-dismiss="modal">subscribe to our newsletter</a> so that we can notify you .</p>
													<p>Meanwhile be pleased to receive your opinions, please write to <a href="mailto:info@illesbalears.travel">info@illesbalears.travel</a>, all suggestions will help us improve the quality of the portal.</p>
													<p>Thank you very much for your collaboration. <br/><i>Tourism Agency of the Balearic Islands</i> </p>
													</div>
													<div className="modal-footer">
														<button type="button" className="btn-u btn-u-default" data-dismiss="modal"  onClick={this.handleClick}>Thank you!</button>
													</div>
												</div>
											</div>
									</div>
						)


					break;

					case "de":

					return(

						<div className="modal fade bs-example-modal-lg" tabindex="-1" role="dialog"  aria-hidden="true"  id="onload">
											<div className="modal-dialog">
												<div className="modal-content">
													<div className="modal-header">
													<button type="button" className="close" data-dismiss="modal">×</button>
														<h4 className="modal-title"><i className="fa fa-exclamation-circle"></i>Willkommen in Illes Balears</h4>
													</div>
													<div className="modal-body">
												 <p> <img alt='' src="/assets/images/logo.png" alt="Logo Balearen"/></p>
													 <h4><strong>Erneuern Sie das touristische Portal www.illesbalears.travel</strong></h4>
													<p>Wir sind weiterhin arbeiten , um die beste Benutzererfahrung, Zugänglichkeit, Navigation, audiovisuelle Inhalte und eine große Menge an Informationen der touristischen Ressourcen der Inseln zu bieten. Wir sind in der Beta - Phase und wird bald der offizielle Start der touristischen Portal,&nbsp; <a href="#news" data-dismiss="modal">in unseren Newsletter</a> ein, damit wir mitteilen können Sie .</p>
													<p>Inzwischen freuen Ihre Meinung zu erhalten, schreiben Sie bitte an <a href="mailto:info@illesbalears.travel">info@illesbalears.travel</a>, alle Vorschläge werden uns helfen , die Qualität des Portals zu verbessern.</p>
													<p>Vielen Dank für Ihre Zusammenarbeit.<br/><i>Tourismus - Agentur der Balearen</i> </p>
													</div>
													<div className="modal-footer">
														<a href="javascript:HideModalOnLoad();">
														<button type="button" className="btn-u btn-u-default" data-dismiss="modal"  onClick={this.handleClick}>Dank!</button>
														</a>
													</div>
												</div>
											</div>
									</div>
					)
					break;
				}



		}

});


module.exports = WelcomeModal;
