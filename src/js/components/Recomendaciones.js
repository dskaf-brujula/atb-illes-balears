var React = require('react');
var RecomendacionesStore = require('../stores/RecomendacionesStore');
var ActionsCreator = require('../actions/appActions');
var dameTextoi18 = require('../webutils/utilidades.js').dameTextoi18;
var sortRecomendados = require('../webutils/utilidades.js').sortRecomendados;
var literales = require('../../resources/literalesRecomendados.json');
var results = [];
var recomendaciones = [];
var _ = require('lodash');


function getStateFromStore() {
    //console.warn('[Recomendaciones] getStateFromStore');
    return RecomendacionesStore.getAll();
}

// Obtiene los recursos para 'descubre'.
function fetchDataOnClient(theUrl) {
    try {
      console.log('[Recomendaciones.js] fetchDataOnClient() theUrl' + theUrl);

        if (typeof $ != "undefined") {
            $.ajax({
                url: theUrl,
                dataType: 'json',
                cache: false,
                async: false,
                success: function(data) {
                    // recomendaciones = data;
                    results = data;
                }.bind(this),
                error: function(xhr, status, err) {
                    console.error(this.props.url, status, err.toString());
                }.bind(this),
                complete: function(xhr, status) {
                    console.log('ciclo vida 1 '+status+' '+recomendaciones);
                }.bind(this)
            });
        }
    } catch (e) {
        console.log('[Recomendaciones.js] Cargar recomendados ' + e.message);
    }
}

function cargarFiltro(bFiltro) {
    try {
        var rt = <div></div>
        if (bFiltro == 'true') {
            rt = <div className = 'btn-filter-wrap margin-bottom-30 cbp-l-filters-text margin-top-20'>
                    <div className = 'btn-filter cbp-filter-item'  data-filter = '.mallorca'> MALLORCA </div>
                    <div className = 'btn-filter cbp-filter-item' data-filter = '.menorca'> MENORCA </div>
                    <div className = 'btn-filter cbp-filter-item' data-filter = '.ibiza'> IBIZA </div>
                    <div className = 'btn-filter cbp-filter-item' data-filter = '.formentera'> FORMENTERA</div>
                </div>
        }
        return rt
    } catch (e) {
        console.log(e.message);
    }
}

var Recomendaciones = React.createClass({

    onLoad: function() {
        this.setState(getStateFromStore());
    },
    getInitialState: function() {

        //Client side
        if (this.props.render=='client'){
          console.log('[Recomendaciones.js] is client');

          if (this.props.sUrl) {
              var theUrl = String(this.props.sUrl);
              fetchDataOnClient(theUrl);
              recomendaciones = results;

              theUrl = "/diccionario/componentes/"
              fetchDataOnClient(theUrl);
              ActionsCreator.updateRecomendaciones({recomendaciones: recomendaciones, sDiccComponentes: results });


          }


        }
        // Server side
        else if (this.props.render=='server') {
          console.log('[Recomendaciones.js] is server');

          recomendaciones = this.props.componente;

          console.log('[Recomendaciones.js]  recomendaciones: '+recomendaciones);
          console.log('[Recomendaciones.js]  sDiccComponentes: '+this.props.sDiccComponentes);

          ActionsCreator.updateRecomendaciones({recomendaciones: recomendaciones, sDiccComponentes: this.props.sDiccComponentes });

        }
        else if (this.props.render=='isomorphic') {
          console.log('[Recomendaciones.js] is isomorphic');
        }

        return getStateFromStore();
    },

    componentDidMount: function() {
        RecomendacionesStore.addLoadListener(this.onLoad);
    },
    ComponentWillUnMount: function() {
        RecomendacionesStore.removeLoadListener(this.onLoad);
    },
    clickRecomendacion: function(sUrl,sTarget) {
  		console.log("[Recomendaciones.js] clickRecomendacion... sUrl: "+sUrl+" sTarget: "+sTarget);
      if (sTarget=='_blank'){
        window.open(sUrl);
      } else{
        location.href=sUrl;
      }
    },
    render: function() {
            var idioma = String(this.props.sIdioma);
            var PageType = this.props.sPageType;
            console.log("[Recomendaciones.js] PageType "+PageType);
            var tituloRecomendado = ((PageType=='Landing') ? dameTextoi18(literales[0].titulo, idioma): dameTextoi18(literales[0].titulo2, idioma));
            var subtituloRecomendado = dameTextoi18(literales[0].subtitulo, idioma);
            var sDiccComponentes= this.state.sDiccComponentes;
            var recomendaciones = this.state.recomendaciones;


            console.log("[Recomendaciones.js] render ...");

        //@TODO Check if needed.
        // recomendaciones = sortRecomendados(recomendaciones);

        if (recomendaciones == null) {
            return (
                <div>
                <section className = 'container sm'>
                <div className = 'text-center margin-bottom-30'>
                </div>
                </section>
                </div>
            );
        } else {
            return (
                <div className='boxre'>
                    <section className = 'container sm' >
                        <div className = 'text-center margin-bottom-30'>
                            <h2 className = 'title-v2 title-center'> {tituloRecomendado } </h2>
                            <p>{ subtituloRecomendado }</p>
                            <div className = 'owl-carousel owl-theme margin-top-30'>
                                { this.state.recomendaciones.items.map((recomendacion, index) =>
                                      <Recomendacion recomendacion={recomendacion} clickRecomendacion={this.clickRecomendacion} index={index} idioma={idioma} sDiccComponentes={sDiccComponentes} key={index}/>
                                )}

                            </div>
                        </div>
                    </section>



                </div>
            );
        }
    }
});

// {recomendaciones}


var Recomendacion = React.createClass({


render: function () {


    var recomendacion = this.props.recomendacion;
    var index = this.props.index;
    var idioma = this.props.idioma;
    var sDiccComponentes = this.props.sDiccComponentes;

		var  _get  = function(txt){
			try{
			return literales[0][txt][idioma];
			} catch(e){
				console.log("[SearchFilters.js] render() _get(txt): "+txt+" Error: "+e.message);
				return txt;
			}
		}


          try {

            console.log('[Recomendaciones.js] Recomendación número: '+index+' - - - - - - - -' );



            try{
              var _componentType = recomendacion.component_type;
              console.log("[Recomendaciones.js]  _componentType: "+_componentType);
            }catch(e){
              console.log('[Recomendaciones.js] Recomendación '+index+', no tiene _componentType:\n' + e.message);
            }

            try{
              console.log('[Recomendaciones.js]  sDiccComponentes: \n'+JSON.stringify(sDiccComponentes));

              var componentTypeName = dameTextoi18(sDiccComponentes[_componentType],idioma);
              console.log('[Recomendaciones.js]  componentTypeName: '+componentTypeName);
            }catch(e){
              console.log('[Recomendaciones.js] Recomendación '+index+', no tiene componentTypeName:\n' + e.message);
            }

            try{
              // var _islas = recomendacion.islands[0];
              var islands = [];
              recomendacion.islands.map(function(island, index){
                console.log("[Destacados.js]  island: "+island);
                islands.push(_get(island));
              });
              var _islas = (islands.length==4) ? "Illes Balears" : _.join(islands,'/');


              console.log('[Recomendaciones.js]  _islas: '+_islas);
            }catch(e){
              console.log('[Recomendaciones.js] Recomendación '+index+', no tiene _islas:\n' + e.message);
            }

            try{

              var multimedia = _.filter(recomendacion.multimedia,{"type":"image"});

              var _imagen = multimedia[0].url;
              console.log('[Recomendaciones.js]  _imagen: '+_imagen);
            }catch(e){
              console.log('[Recomendaciones.js] Recomendación '+index+', no tiene _imagen:\n' + e.message);
            }


            try{
              var _titulo =  ' ' + ((_componentType == "rrtt") ? recomendacion.title : dameTextoi18(recomendacion.title,idioma));

              console.log('[Recomendaciones.js]  _titulo: '+_titulo);
            }catch(e){
              console.log('[Recomendaciones.js] Recomendación '+index+', no tiene _titulo:\n' + e.message);
            }

            try{
              var _municipio = (_componentType == "rrtt") ?  recomendacion.municipalities.names[0] : '';
              console.log('[Recomendaciones.js]  _municipio: '+_municipio);
            }catch(e){
              console.log('[Recomendaciones.js] Recomendación '+index+', no tiene _municipio:\n' + e.message);
            }

            try{
              var _termino = (_componentType == "rrtt") ? dameTextoi18(recomendacion.term.lang,idioma): '';
              var _termino_id = (_componentType == "rrtt") ? recomendacion.term.id: null;

              console.log('[Recomendaciones.js]  _termino: '+_termino);
              console.log('[Recomendaciones.js]  _termino_id: '+_termino_id);

            }catch(e){
              console.log('[Recomendaciones.js] Recomendación '+index+', no tiene _termino:\n' + e.message);
            }

            try{
              var _tipologia = (_componentType == "rrtt") ? dameTextoi18(recomendacion.tipology.lang,idioma): '';
              var _tipologia_id = (_componentType == "rrtt") ? recomendacion.tipology.id: '';

              console.log('[Recomendaciones.js]  _tipologia: '+_tipologia);
              console.log('[Recomendaciones.js]  _tipologia_id: '+_tipologia_id);

            }catch(e){
              console.log('[Recomendaciones.js] Recomendación '+index+', no tiene _tipologia:\n' + e.message);
            }

            var _urlTarget = '_blank';


            try{
              var _urlRecomendacion = dameTextoi18(recomendacion.data_sheet,idioma).url_friendly;

              if  ((_componentType == "rrtt") && ('club-nautico,escuela-nautica,estacion-nautica,marina,puerto-deportivo,spa,bodega').indexOf(_termino_id)>=0){
                // _urlTarget = '_blank';
                _urlRecomendacion = recomendacion.web;

              }

              if ((_componentType == "rrtt") && ('alojamiento').indexOf(_tipologia_id)>0){
                // _urlTarget = '_blank';
                _urlRecomendacion = recomendacion.web;
              }

              console.log('[Recomendaciones.js]  _urlRecomendacion: '+_urlRecomendacion);


            }catch(e){
              console.log('[Recomendaciones.js] evento '+index+', no tiene _urlRecomendacion:\n' + e.message);
            }



              var tituloCard = _titulo;
              if(_componentType=='rrtt'){
                  tituloCard = _termino + ' ' + _titulo;
              } else{
                  // _tipologia = componentTypeName; @IECI: commented by change requirement of ATB.
              }

              // var _urlFriendly = eval('recomendacion.data_sheet.' + idioma + '.url_friendly');
              var stylePointer = {cursor: 'pointer'}



              return (
                  <div data-version='Recomendaciones-v1.1' style={stylePointer} className = 'item news-v2' key={index}  data-url={_urlRecomendacion} onClick={() => this.props.clickRecomendacion(_urlRecomendacion,_urlTarget)}>

                          <img alt='' className = 'img-responsive' src = { _imagen }/>

                      <div className = 'news-v2-desc bg-color-normal' >
                          <h3 className = 'txttitle' >
                             <span className = 'txttermino'> {_tipologia } </span>
                             {tituloCard}
                          </h3>
                          <span className = 'txtilles' > { _islas } </span>
                          <span className = 'txtmunicipi' > { _municipio } </span>
                      </div>
                  </div>
              )
          } catch (e) {
              console.warn("Recomendaciones. Se ha producido una incidencia con el recomendado nº " + index + ", y no se mostrará:\n" + e.message);
          }
    }
});

module.exports = Recomendacion;


module.exports = Recomendaciones;
