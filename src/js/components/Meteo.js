var React = require('react');
var estaticos = require('../../resources/estaticosContent.json');
var literalesMeteo = require('../../resources/literalesMeteoclim.json');

var _ = require('lodash');
var moment = require('moment');


// var cargarComponente = require('../ServerHelper.js').cargarComponente;
// var  meteoData = [];
// var norender = true;


// Obtiene los recursos para los cubos 'articulo'.

// var url


var Meteo = React.createClass({


		getInitialState: function() {
			console.log('[Meteo.js] getInitialState');

			var url = this.props.url;


      // if (oDatosMeteoUrl.indexOf("0")>0){
			// 	norender=true;
			// }


			// var lat = this.props.oDatosMeteo.lat;
			// var lon = this.props.oDatosMeteo.lon;
			// var touristic_subject = this.props.oDatosMeteo.oTouristicSubjects.id[0];
			// var url = "http://54.77.91.226:5000/components/meteoclim/latitude/"+lat+"/longitude/"+lon+"/touristic_subjects/"+touristic_subject;

			console.log('[Meteo.js] getInitialState() url: '+url);

			this.setState({norender:true});

			// console.log('[Meteo.js] getInitialState() meteoData: '+JSON.stringify(meteoData));

			return ({norender:true, meteo: null});

			// ActionsCreator.updateRrtt({data:rrtt});
			// console.warn('[rrtt] getInitialState');
		},
		componentDidMount: function(){
			// this.setState({meteo:meteoData})
			console.log('[Meteo.js] componentDidMount');
      this.loadMeteo(this.props.url);

			// loadMeteo(url,function(comp){
			// 	this.setState({meteo:meteoData})
			// });

		},
    loadMeteo: function(theUrl){
      try{
    		console.log('[Meteo.js] loadMeteo (theUrl) ' + theUrl);
    	  	if(typeof $ != 'undefined'){
        		  $.ajax({
        		    url: theUrl,
        		    dataType: 'json',
        		    cache: false,
        		    async: true,
        		    success: function(data){
        					console.log('[Meteo.js] loadMeteo '+data);
        					// meteoData = data;
                  // norender = false;
                  this.setState({norender:false, meteo:data});
        		    }.bind(this),
        		    error: function(xhr, status, err) {
        		      console.error('[Meteo.js]  loadMeteo (error)'+this.props.url, status, err.toString());
        		    }.bind(this),
        		    complete: function(xhr, status){
        					console.log('[Meteo.js]  loadMeteo status'+status+'');
        		    }.bind(this)
        		  });
    			}

    	} catch(e){
    		console.log('[Meteo.js] loadMeteo (error) ' + e.message);
    	}
    },
	  render: function(){

        console.log('[Meteo.js] render() version 1.1');

				var meteotype = this.props.meteotype;
				var norender = this.state.norender;

				console.log("[Meteo.js] render() meteotype: "+meteotype);
				console.log("[Meteo.js] render() norender: "+norender);


        if (norender==true) {return(null)};


				// var url = String(this.props.oDatosMeteo.url);
				// console.log('[Meteo.js] render() oDatosMeteo.url: '+url);

			  var idioma = String(this.props.sIdioma);
				console.log('[Meteo.js] render() sIdioma: '+idioma);


				var isla = String(this.props.sIsla);
				console.log('[Meteo.js] render() sIsla: '+isla);

				var meteo = this.state.meteo;


        var nowtime = moment().get('hour');

        console.log('[Meteo.js] render() nowtime: '+nowtime);

				if (nowtime<8) {nowtime= 8;}
				if (nowtime>22) {nowtime= 22;}

        var meteotime = ((nowtime<10) ? ('0'+nowtime) : nowtime)+':00' ;

        console.log('[Meteo.js] render() meteotime: '+meteotime);


        function _get(txt){
          return estaticos[0][txt][idioma];
        }


		    function _txtMeteo(txt){
		      return literalesMeteo[0][txt][idioma];
		    }


				function _iconMeteo (url){
					var pattern = "http://widget.meteoclim.eu/";
					var reg =  new RegExp(pattern, "g");
					return url.replace(reg,'/assets/images/meteoclim/');
				}



        var meteodays = [];

				// var title = String(this.props.oDatosMeteo.sTitle);
				// console.log('[Meteo.js] render() title: '+title);

				// var searchtitle = dameTextoi18(estaticos[0]['searchtitle'],idioma);
				// console.log('[Meteo.js] render() searchtitle: '+searchtitle);

				_.forEach(meteo, function(value, key) {
					console.log("[Meteo.js] render() key "+key+ " value: "+JSON.stringify(value));
					meteodays.push(value);
					// console.log("[Meteo.js] render() weekday: "+meteoData[key].weekday[idioma]);
					console.log("[Meteo.js] render() weekday: "+meteodays[0].weekday[idioma]);

				});

				console.log("[Meteo.js] render() meteodays.length: "+meteodays.length);

				// meteodays.map((meteoday, index) => {
				// 	console.log("[Meteo.js] render() weekday meteoday: "+meteoday.weekday[idioma]);
				// 	console.log("[Meteo.js] render() comfort_index meteoday: "+meteoday.touristic_subjects[0].comfort_index);
        //
        //
				// });


        if (meteotype=='meteomini'){



              return (

                <div className="flatWeatherPlugin simple">
                    <div className="wiToday">
                      <div className="wiIconGroup">
                        <div className="wi">
                          <img alt='' src={_iconMeteo(meteodays[0].weather_icons[meteotime].url)} className="icontime2"/>
                        </div>
                        <p className="wiText">Max. {meteodays[0].max_temperature}º /  Min. {meteodays[0].min_temperature}º {_txtMeteo(meteodays[0].weather_icons[meteotime].code)}</p>
                      </div>
                      <p className="wiTemperature">{meteodays[0].current_temperature}º C</p>
                    </div>
                </div>


              );

        }
        else if (meteotype=='meteofull') {

              return(

                    <div className="meteo">
                        <h3 className="font-bold">{_get('eltiempo')}</h3>
                        <div id="accordion" >


                          {
                            meteodays.map((meteoday, index) => {


                                return (

                                  <div key={index} >

                                      <p className="panel-title panelph" data-toggle="collapse" data-parent="#accordion" href={"#collapse"+index} >
                                        <img alt='' src={_iconMeteo(meteoday.weather_icons[meteotime].url)} className="icontime2"/>
                                        <span className="hfecha">	{meteoday.weekday[idioma]}, {meteoday.date}</span>
                                        <span className="prevision">Max. {meteoday.max_temperature}º /  Mín. {meteoday.min_temperature}º</span>
                                        <span className="ttder">{_txtMeteo(meteoday.weather_icons[meteotime].code)}</span>
                                      </p>

                                      <div id={"collapse"+index} className="panel-collapse collapse panel-body2">
                                      <dl>
                                        {index==0 &&
                                        <dd className="clearfix">   <img alt='' src={_iconMeteo(meteoday.weather_icons[meteotime].url)} className="icontime"/>
                                        <span className="ftleft"> {meteoday.current_temperature}º</span>
                                        </dd>
                                        }
                                        <dd className="ttime"> {_get('temperaturamaxima')} {meteoday.max_temperature}º</dd>
                                        <dd className="ttime"> {_get('temperaturaminima')} {meteoday.min_temperature}º</dd>
                                        <dd className="ttime"> {_get('humedad')} {meteoday.avg_humidity}</dd>
                                        <dd className="ttime"> {_get('velocidadmaxima')} {meteoday.max_wind} km/h</dd>
                                        <dd className="ttime sep clearfix">{_get('indiceconfort')}
                                          <div className="ic">
                                          <span className={meteoday.touristic_subjects[0].comfort_index==1?"ic1activo":"ic1"}></span>
                                          <span className={meteoday.touristic_subjects[0].comfort_index==2?"ic2activo":"ic2"}></span>
                                          <span className={meteoday.touristic_subjects[0].comfort_index==3?"ic3activo":"ic3"}></span>
                                          <span className={meteoday.touristic_subjects[0].comfort_index==4?"ic4activo":"ic4"}></span>
                                          <span className={meteoday.touristic_subjects[0].comfort_index=='5'?"ic5activo":"ic5"}></span>
                                          <span className={meteoday.touristic_subjects[0].comfort_index==6?"ic6activo":"ic6"}></span>
                                          <span className={meteoday.touristic_subjects[0].comfort_index==7?"ic7activo":"ic7"}></span>
                                          </div>
                                          <span className="icl"> {_get('incomodo')}</span> <span className="icr"> {_get('ideal')}</span>
                                        </dd>
                                      </dl>
                                    </div>
                                </div>

                              );
                            })
                          }

                      </div>
                </div>

            );

        }




		}

});


module.exports = Meteo;
