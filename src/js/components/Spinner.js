var React = require('react');
var Loader = require('halogen/BounceLoader');

var sectionsColors = require('../../resources/sectionsColors.json');



var  _getColor  = function(section){
	try{
			return sectionsColors[0][section]['color'];

	} catch(e){
		console.log("[Spinner.js] render() _getColor(section): "+section+" Error: "+e.message);
		return '';
	}
}


var Spinner = React.createClass({
  render: function() {


		var section = this.props.section;
		var colour = _getColor(section);



		colour = (colour=='')? 'green' : colour;

		console.log('[Spinner.js] section: '+section);
		console.log('[Spinner.js] colour: '+colour);

    return (
      <Loader color={colour} size="32px" margin="4px"/>
    );
  }
});




module.exports = Spinner;
