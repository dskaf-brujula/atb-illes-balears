var React = require('react');
var sortSubsections = require('../webutils/utilidades.js').sortSubsections;
var dameTextoi18 = require('../webutils/utilidades.js').dameTextoi18;
var SubsectionsStore = require('../stores/SubsectionsStore');
var ActionsCreator = require('../actions/appActions');

// var jsonFile = require('../../resources/literalesDestacados.json');
var subsections = [];

// Obtiene JSON.
function cargarSubsections(theUrl){
	try{
	  	if(typeof $ != "undefined"){
			$.ajax({
				url: theUrl,
				dataType: 'json',
				cache: false,
				async: false,
				success: function(data){
					subsections = data;
					//console.log('ciclo vida 1 subsections. success '+subsections);
				}.bind(this),
					error: function(xhr, status, err) {
					//console.error(this.props.url, status, err.toString());
				}.bind(this),
					complete: function(xhr, status){
					//console.log('ciclo vida 1 '+status+' '+subsections);
				}.bind(this)
			});
		}
	} catch(e){
		console.log('Cargar subsections ' + e.message);
	}
}


function getStateFromStore(){
	return {subsections: SubsectionsStore.getAll()}
}

var Subsections = React.createClass({

	getInitialState: function() {

		console.log('[Subsections.js] Cargar getInitialState()');

		// Client side
		if(this.props.sUrl){
			var theUrl = String(this.props.sUrl);
			console.log('URL '+ theUrl);
			cargarSubsections(theUrl);
		} else {
			// Server side
			subsections = this.props.componente;
		}
		ActionsCreator.updateSubsections({data:subsections});
		return getStateFromStore();
	},
	componentDidMount: function(){
		SubsectionsStore.addLoadListener(this.onLoad);
	},
	ComponentWillUnMount: function(){
		SubsectionsStore.removeLoadListener(this.onLoad);
	},
	onLoad: function() {
		this.setState(getStateFromStore());
	},

	render: function(){
		console.log('[Subsections.js] render()');


		var idioma = String(this.props.sIdioma);

		// console.log('[Subsections.js] render() this.state.subsections:\n'+JSON.stringify(this.state.subsections));


		var numcubos = this.state.subsections.data.length;

		console.log('[Subsections.js] render()  numcubos: '+numcubos);
		numcubos = (numcubos==4)?2:numcubos;
		numcubos = (numcubos==6)?3:numcubos;

		// var numcubosStyle_1= (numcubos=2) ? {width: '570px',left: '0px', top: '0px'} : '';
		// var numcubosStyle_2 = (numcubos=2) ? {height: '321px'} : '';


		var cubos = this.state.subsections.data.map(function(subsection, index){
					try{
					  var title = dameTextoi18(subsection.title,idioma);
						var background_image = subsection.background_image;
						var url_friendly  = dameTextoi18(subsection.url_friendly,idioma);
						// var tipoClase =  subsection.tipology;

						var background_color = {backgroundColor: subsection.background_color +'!important', opacity: '0.8 !important'};

						console.log('[Subsections.js] render()  title: '+title);
						console.log('[Subsections.js] render()  background_image: '+background_image);
						console.log('[Subsections.js] render()  url_friendly: '+url_friendly);
						return(

									<div className='cbp-item'>
										<a href={url_friendly} className='cbp-caption' data-title={title}>
										<div className=''><img alt='' src={background_image}  /></div>
											<div className='cbp-caption-activeWrap nautica' style={background_color}>
												<div className='cbp-l-caption-alignCenter'>
													<div className='cbp-l-caption-body'>
														<div className='cbp-l-caption-title ts5'>{title}</div>
													</div>
												</div>
											</div>
										</a>
									</div>


						)
					}catch(e){
						console.log('[Subsections.js] render() (Error) en index num.'+index+': '+e.message);

					}
		});


		// // @TODO: presentacion aleatoria
		// // @TODO mostrar máximo de 9 subsections o multiplo de 3.
		// cubos = sortSubsections(cubos);
		//


		return(
		<div id='js-grid-masonry' className='cbp' data-version='Subsections-v1.3' data-columns={numcubos}>
			{cubos}
		</div>
		);


	}

	//
	// <!--=== Start Agrupaciones  ===-->
  //  <section className="container content-sm">
  //    <div className="text-center margin-bottom-30">
  //      <h2 className="title-v2 title-center"> ART I CULTURA </h2>
  //      <p>Muntanyes d'aventura, forats amb vistes al mar, competicions apassionants ... T'atreveixes?
  //        A les Illes Balears viuràs tota l'emoció de l'esport. Comptes amb prop de 30 camps de golf en els quals practicar el swing tot l'any. A l'hivern pots lliscar per la neu sota el sol. Amb els esports d'aventura sentiràs el que és l'adrenalina ... I això només és el principi.</p>
  //    </div>
	//
  //    <!-- -->
  //   //  <div id="js-grid-masonry" className="cbp">
	//
  //      <div className="cbp-item">
	// 		   <a href="#" className="cbp-caption">
  //        <div className=""> <img alt='' src="assets/images/segmentos/patrimonio_cultural.jpg" alt=""> </div>
  //        <div className="cbp-caption-activeWrap cultura">
  //          <div className="cbp-l-caption-alignCenter">
  //            <div className="cbp-l-caption-body">
  //              <div className="cbp-l-caption-title ts5">Patrimoni Cultural</div>
  //            </div>
  //          </div>
  //        </div>
  //        </a>
	// 		</div>
	//
  //      <div className="cbp-item"> <a href="#" className="cbp-caption">
  //        <div className=""> <img alt='' src="assets/images/segmentos/festes.jpg" alt=""> </div>
  //        <div className="cbp-caption-activeWrap cultura">
  //          <div className="cbp-l-caption-alignCenter">
  //            <div className="cbp-l-caption-body">
  //              <div className="cbp-l-caption-title ts5">Festes i tradicions</div>
  //            </div>
  //          </div>
  //        </div>
  //        </a> </div>
	//
  //    </div>
  //  </section>
	//
  //  <!--=== End Agrupaciones  ===-->


});


module.exports = Subsections;
