var React = require('react');
var ReactDOM = require('react-dom');
var _ = require('lodash');

var sortSearcher = require('../webutils/utilidades.js').sortSearcher;
var dameTextoi18 = require('../webutils/utilidades.js').dameTextoi18;
var capitalizedString = require('../webutils/utilidades.js').capitalizedString;
var literales = require('../../resources/literalesSearch.json');



var SearcherStore = require('../stores/SearcherStore');
var ActionsCreator = require('../actions/appActions');

var SearchSuggest = require('./SearchSuggest');
var SearchPagination = require('./SearchPagination');
var SearchFilters = require('./SearchFilters');

var Spinner = require('./Spinner');
var TagsButtons = require('./TagsButtons');
var Mapa = require('./Mapa');
var renderHtml = require('react-render-html');




// var jsonFile = require('../../resources/literalesDestacados.json');


var searcher = [];
var totalItems;




// Formatea Date de forma personalizada. @TODO: pasar a SharedFactory
function formatDate(date,language){

//console.log ("function - formatDate: "+language);

	if (language){
        switch (language.toUpperCase()) {
            case "ES":
                var m_names = new Array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
                break;
            case "EN":
                var m_names = new Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
                break;
            case "CA":
                var m_names = new Array ("Gener", "Febrer", "Març", "Abril", "Maig", "Juny", "juliol", "agost", "Setembre", "Octubre", "Novembre", "Desembre") ;
                break;
            case "DE":
                var m_names = new Array ( "Januar", "Februar", "März", "April", "Mai", "June", "Juli", "August", "September", "October", "November", "December") ;
                break;
        }

		var d = new Date(date);
		var curr_date = d.getDate();
		var curr_month = d.getMonth();
		var curr_year = d.getFullYear();

//console.log ("function - formatDate (response): "+curr_date + "." + m_names[curr_month].toUpperCase().substr(0,3) + "." + curr_year);


		return curr_date + "." + m_names[curr_month].toUpperCase().substr(0,3) + "." + curr_year;

	}else{

		return date.toLocaleDateString();

	}

}

var isFetching = true;


function getStateFromStore(){

	// console.log('[Searcher.js] getStateFromStore() ...\n'+JSON.stringify(SearcherStore.getAll()));
	console.log('[Searcher.js] getStateFromStore() ...');

	return SearcherStore.getAll();

	// return {searcher: SearcherStore.getAll(), totalItems:SearcherStore.totalItems,pageOfItems: SearcherStore.pageOfItems, currentPage: SearcherStore.currentPage}
}



var Searcher = React.createClass({


		getInitialState: function() {
			console.log('[Searcher.js] Cargar getInitialState()');
			// Client side
			// // if(this.props.sUrl){
			// 	var theUrl = String(this.props.sUrl);
			// 	console.log('[Searcher.js] doSearch() theUrl '+ theUrl);
			// 	doSearch(theUrl);
			// } else {
			// 	// Server side
			// 	searcher = this.props.componente;
			// }

			ActionsCreator.updateSearcher({isLoading:true, searcher:searcher, totalItems:0, pageOfItems:[], currentPage: 1,  searchNotFound: false});
			return getStateFromStore();
		},
		componentDidMount: function(){
			console.log('[Searcher.js] componentDidMount()');

			// Client side
			if(this.props.sUrl){
				var theUrl = String(this.props.sUrl);
				console.log('[Searcher.js] doSearch() theUrl '+ theUrl);

				var theHash = location.hash;

				console.log('[Searcher.js] doSearch() theHash '+ theHash);


				this.doSearch(theUrl);
				console.log('[Searcher.js] doSearch() theUrl '+ theUrl);



			} else {
				// Server side @Todo: delete
				searcher = this.props.componente;
			}
		},
		componentDidUpdate : function(){
			console.log('[Searcher.js] componentDidUpdate()');
			$(this.refs['masfechas']).tooltip();
			$('[data-toggle="tooltip"]').tooltip();
		},
		ComponentWillUnMount: function(){
			console.log('[Searcher.js] ComponentWillUnMount()');
			SearcherStore.removeLoadListener(this.onLoad);
		},
		onLoad: function() {
			console.log('[Searcher.js] onLoad()');
			this.setState(getStateFromStore());
			SearcherStore.addLoadListener(this.onLoad);
		},
		onChangePage: function(pageOfItems,currentPage) {
			console.log('[Searcher.js] onChangePage()...');
			console.log('[Searcher.js] onChangePage() currentPage'+currentPage);
			// update state with new page of items
			this.setState({pageOfItems: pageOfItems, currentPage:currentPage});
			console.log('[Searcher.js] onChangePage() this.state.currentPage: '+this.state.currentPage);
		},
		onAutoSuggest: function(dataSuggest,searchText) {
			console.log('[Searcher.js] onAutoSuggest()...');
			// console.log('[Searcher.js] onAutoSuggest() currentPage'+currentPage);
			console.log('[Searcher.js] onAutoSuggest() dataSuggest'+ dataSuggest+ ' searchText: '+searchText);
			if (dataSuggest.length>0){
					this.setState({searcher: dataSuggest, totalItems: dataSuggest.length, pageOfItems: [], currentPage:1, searchNotFound: false, resetFilters: true, resetText:false, searchText: searchText});
			}else{
					this.setState({searcher: searcher, totalItems: totalItems, pageOfItems: [], currentPage:1, searchNotFound: true,resetFilters: true, resetText:false, searchText: ''});
			}
			// console.log('[Searcher.js] onAutoSuggest() 			this.state.currentPage: '+this.state.currentPage);
		},
		onResetFilters: function() {
			console.log('[Searcher.js] onResetFilters()...');
			this.setState({searcher: searcher, totalItems: totalItems, pageOfItems: [], currentPage:1, searchNotFound: false, resetFilters: true, resetText:true, searchText: ''});
						// console.log('[Searcher.js] onAutoSuggest() 			this.state.currentPage: '+this.state.currentPage);
		},
		onSearchFilter: function(dataFilter) {
			console.log('[Searcher.js] onSearchFilter()...');
			console.log('[Searcher.js] onSearchFilter() dataFilter.length'+ dataFilter.length);
			if (dataFilter.length>0){
					this.setState({searcher: dataFilter, totalItems: dataFilter.length, pageOfItems: [], currentPage:1, searchNotFound: false, resetFilters: false, resetText:true, searchText: ''});
			}else{
					this.setState({searcher: searcher, totalItems: totalItems, pageOfItems: [], currentPage:1, searchNotFound: false, resetFilters: false, resetText:true, searchText: ''});
			}
			// console.log('[Searcher.js] onAutoSuggest() 			this.state.currentPage: '+this.state.currentPage);
		},
		// onClickSearch: function(txtsearch) {
		// 	console.log('[Searcher.js] onClickSearch()...');
		// 	console.log('[Searcher.js] onClickSearch() txtsearch'+txtsearch);
		// 	var theUrl
		// 	console.log('[Searcher.js] onClickSearch() theUrl: '+theUrl);
		// 	doSearch(theUrl);
		// 	// ActionsCreator.updateSearcher({data:searcher, pageOfItems:[]});
		// },
		doSearch: function(theUrl){
			try{
			  	if(typeof $ != "undefined"){
					$.ajax({
						url: theUrl,
						dataType: 'json',
						cache: false,
						async: true,
						success: function(data){
							// console.log('[Searcher.js] doSearch() success \n'+JSON.stringify(data));
							searcher = data.items;
							totalItems= data.total_items;
							console.log('[Searcher.js] doSearch() success totalItems: '+totalItems);
							this.setState({isLoading:false,searcher:searcher, totalItems:totalItems, pageOfItems:[], currentPage: 1,  searchNotFound: false,viewtype:"listview"});
							// isFetching = false;
						}.bind(this),
							error: function(xhr, status, err) {
							//console.error(this.props.url, status, err.toString());
						}.bind(this),
							complete: function(xhr, status){
							//console.log('ciclo vida 1 '+status+' '+searcher);
						}.bind(this)
					});
				}
			} catch(e){
				console.log('Cargar searcher ' + e.message);
			}
		},
		setViewType: function (event){
			console.log('[Searcher.js] setViewType(): '+event.target.name);
			if (this.state.viewtype!=event.target.name){
				this.setState({viewtype: event.target.name});
			}
		},
		render: function(){
					console.log('[Searcher.js] render() version 1.32');


					var isLoading = this.state.isLoading;

					var idioma = String(this.props.sIdioma);
					var numcubos = this.state.totalItems;
					var searcherType = this.props.searcherType;
					var section = this.props.section;
					var markText = this.state.searchText;



					var  _get  = function(txt){
						try{
						return literales[0][txt][idioma];
						} catch(e){
							console.log("[Searcher.js] render() _get(txt): "+txt+" Error: "+e.message);
							return txt;
						}
					}


					var  _mark  = function(txt){
						try{

						console.log("[Searcher.js] render() _mark() markText: "+markText+" txt: "+txt);

						return renderHtml(txt.replace(eval('/'+markText+'/gi'), function (x) {return '<mark>'+x+'</mark>';}));

							// <mark></mark>

						} catch(e){
							console.log("[Searcher.js] render() _mark(txt): "+txt+" Error: "+e.message);
							return txt;
						}
					}





					console.log('[Searcher.js] idioma: '+idioma);
					console.log('[Searcher.js] render()  numcubos: '+numcubos);
					console.log('[Searcher.js] searcherType: '+searcherType);
					console.log('[Searcher.js] section: '+section);




					var searcherType1 = (searcherType=="1")? true:false;
					var searcherType2 = (searcherType=="2")? true:false;
					var searcherType3 = (searcherType=="3")? true:false;
					var searcherType4 = (searcherType=="4")? true:false;
					var searcherType5 = (searcherType=="5")? true:false;


					var searchNotFound = !isLoading && (this.state.searchNotFound || this.state.searcher.length==0);

					function cubosPage(pageOfItems,cuboStart,cuboEnd,currentPage){

								console.log("[Searcher.js] render()  cubosPage...");

								console.log("[Searcher.js] render()		cubosPage... cuboStart: "+cuboStart+" cuboEnd: "+cuboEnd+" currentPage: "+currentPage);

								var cubos = pageOfItems.slice(cuboStart,cuboEnd).map(function(resultItem, index){

									//@TODO: CSC: dividirlo según componentType
									try{



												var count = 1+((currentPage-1)*10+(cuboStart+index));
												var strCount = (count < 10) ?  ('0'+count.toString()): count.toString();

												console.log("[Searcher.js] render()		cuboStart: "+cuboStart+" index: "+index+" cuboEnd: "+cuboEnd+" currentPage: "+currentPage+" count: "+count);


												var componentType = resultItem.component_type;
												console.log("[Searcher.js] render()  index num: "+index);

												console.log("[Searcher.js] render()  componentType: "+componentType);

												var title = '';

												if (searcherType1||searcherType5){
														try{
															title =  ' ' + ((componentType == "rrtt") ? dameTextoi18(resultItem.term.lang,idioma) : '');
														}catch(e){
															console.warn("[Searcher.js] render() title (error): "+e.message);
														}
															title +=  ' ' + ((componentType == "rrtt") ? resultItem.title : dameTextoi18(resultItem.title,idioma));
															console.log("[Searcher.js] render()  title: "+title);
												}else if (searcherType2){
														try{
														title = 	resultItem.title;
														}catch(e){
															console.warn("[Searcher.js] render() title (error): "+e.message);
														}
												}else if (searcherType3){
														try{
															title = 	resultItem.title[idioma];
															console.log("[Searcher.js] render()  title: "+title);
														}catch(e){
															console.warn("[Searcher.js] render() title (error): "+e.message);
														}
											}


												var subtitle ='';

												if (searcherType1||searcherType3||searcherType5){
													try{
														subtitle = dameTextoi18(resultItem.subtitle,idioma);
													}catch(e){
														console.warn("[Searcher.js] render() subtitle (error): "+e.message);
													}
												}


												var multimedia = _.filter(resultItem.multimedia,{"type":"image"});

												var background_image ="";
												var tipology,tipology_id ="";

												if (searcherType1||searcherType5){
														try{



															background_image = multimedia[0].url;
															console.log("[Searcher.js] render()  background_image: "+background_image);
														}catch(e){
															console.warn("[Searcher.js] render() multimedia (error): "+e.message);
														}

														try{
															tipology = (componentType == "rrtt") ? dameTextoi18(resultItem.tipology.lang,idioma): '';
															tipology_id = (componentType == "rrtt") ? resultItem.tipology.id:'';

															console.log('[Searcher.js] render()  tipology: '+tipology);
														}catch(e){
															console.warn("[Searcher.js] render() tipology (error): "+e.message);
														}
												} else
													if (searcherType3){
														background_image = resultItem.multimedia[1].url;
													}




												var isla = "";

												if (searcherType1||searcherType3||searcherType4||searcherType5){
														isla = resultItem.islands[0];
												}else{
														isla = resultItem.island;
												}

												isla = capitalizedString(_get(isla));

												console.log("[Searcher.js] render()  isla: "+isla);


												var islands = [];
												var txtIslands = ''

												if (!searcherType2){
													islands=resultItem.islands
													txtIslands= (islands.length==4)? "Illes Balears": _.join(_.map(islands, _get),'/');
												}


												console.log("[Searcher.js] render()  txtIslands: "+txtIslands);


												var municipality = "";
												var comma = "";

												if (searcherType1||searcherType3||searcherType5){
														municipality = (componentType == "rrtt") ? resultItem.municipalities.names[0] : '';
														console.log('[Searcher.js] render()  municipality: '+municipality);
														comma = (componentType == "rrtt") ? ", ": '';
												}else if (searcherType2){
														municipality = ("municipality" in resultItem) ? resultItem.municipality.name : '';
												}

												var activities = "";

												if (searcherType2){
														activities = ("activity_types" in resultItem) ? _.join(resultItem.activity_types.lang[idioma], ', ') : '';
												}


												var companyType = "";

												if (searcherType2){
														companyType =resultItem.company_type.id;
												}

												var touristic_subjects = "";

												if (searcherType3||searcherType5){
														touristic_subjects = ("touristic_subjects" in resultItem) ? resultItem.touristic_subjects.lang[idioma] : '';
														touristic_subjects_text = _.join(touristic_subjects,', ');
												}





												var styleh3 = {float: 'left', width: '70%'}


												if (componentType == "pub"){

													var languages = _.join(resultItem.languages,', ');

												}


												if (searcherType4){


																		try{
																			var _imagen = resultItem.multimedia[0].url;
																			console.log('[Searcher.js]  _imagen: '+_imagen);
																		}catch(e){
																			console.log('[Searcher.js] resultItem '+index+', no tiene _imagen:\n' + e.message);
																		}

																		try{
																			var _sortingdate = formatDate(resultItem.sorting_date,idioma);
																			console.log('[Searcher.js]  _sortingdate: '+_sortingdate);
																		}catch(e){
																			console.log('[Searcher.js] resultItem '+index+', no tiene _sortingdate:\n' + e.message);
																		}


																		if ("alternate_dates" in resultItem){

																			try{
																					var _startdate = formatDate(resultItem.sorting_date,idioma);
																					var _enddate = _startdate;
																					console.log('[Searcher.js]  _startdate: '+_startdate);
																					console.log('[Searcher.js]  _enddate: '+_enddate);

																					var _alternateDates = _.join(_.map(resultItem.alternate_dates,function(value, key) {return formatDate(value,idioma)}), ' / ');;
                                          //
																					// var _alternateDates = _.chunk(_.map(resultItem.alternate_dates,function(value, key) {return formatDate(value,idioma)}),3);
                                      		// var _alternateDates = _.map(_.chunk(_.map(resultItem.alternate_dates,function(item, key) {return formatDate(item,idioma)}),3),function(item,key){return _.join(item, ' / ')})
                                          //

																			}catch(e){
																				console.log('[Searcher.js] resultItem '+index+', no tiene _startdate:\n' + e.message);
																			}

																		}else{


																				try{
																					var _startdate = formatDate(resultItem.start_date,idioma);
																					console.log('[Searcher.js]  _startdate: '+_startdate);
																				}catch(e){
																					console.log('[Searcher.js] resultItem '+index+', no tiene _startdate:\n' + e.message);
																				}

																				try{
																					var _enddate = formatDate(resultItem.end_date,idioma);
																					console.log('[Searcher.js]  _enddate: '+_enddate);
																				}catch(e){
																					console.log('[Searcher.js] resultItem '+index+', no tiene _enddate:\n' + e.message);
																				}

																		}


																		try{
																			var _dates  = (_startdate != _enddate) ? (_startdate+' - '+_enddate) : _startdate;
																			console.log('[Searcher.js]  _dates: '+_dates);
																		}catch(e){
																			console.log('[Searcher.js] resultItem '+index+', no tiene _dates:\n' + e.message);
																		}

																		try{
																			var firstIsland = resultItem.islands[0];
																			console.log('[Searcher.js]  firstIsland: '+firstIsland);
																		}catch(e){
																			console.log('[Searcher.js] resultItem '+index+', no tiene firstIsland:\n' + e.message);
																		}

																		try{
																			var _municipio = resultItem.municipalities.names[0];
																			console.log('[Searcher.js]  _municipio: '+_municipio);
																		}catch(e){
																			console.log('[Searcher.js] resultItem '+index+', no tiene _municipio:\n' + e.message);
																		}

																		try{
																			var _titulo = dameTextoi18(resultItem.title,idioma);
																			console.log('[Searcher.js]  _titulo: '+_titulo);
																		}catch(e){
																			console.log('[Searcher.js] resultItem '+index+', no tiene _titulo:\n' + e.message);
																		}

																		try{
																			var _urlEvento = dameTextoi18(resultItem.link_urls,idioma);
																			var _urlTarget = _urlEvento.indexOf('illesbalears.travel')>=0 ? '_self':'_blank';
																			console.log('[Searcher.js]  _urlEvento: '+_urlEvento);
																		}catch(e){
																			console.log('[Searcher.js] resultItem '+index+', no tiene _urlEvento:\n' + e.message);
																		}

																		try{
																			var _style_color = '#'+resultItem.color;
																			var _styles_bgcolor = {background: _style_color};
																			console.log('[Searcher.js]  _style_color: '+_style_color);
																		}catch(e){
																			console.log('[Searcher.js] evento '+index+', no tiene _style_color:\n' + e.message);
																		}


												}


												if (searcherType5){

																	try{
																		var featured = resultItem.featured ? _get("featured") : "";
																		console.log('[Searcher.js]  featured: '+featured);
																	}catch(e){
																		console.log('[Searcher.js] evento '+index+', no tiene featured:\n' + e.message);
																	}

																	try{
																		var area = ("areas" in resultItem) ? _.join(resultItem.areas.names, ', ') : '';
																		console.log('[Searcher.js]  area: '+area);
																	}catch(e){
																		console.log('[Searcher.js] resultItem '+index+', no tiene area:\n' + e.message);
																	}


												}


												if (searcherType1||searcherType5){

													var oTagsButtonsItem = []

													try{

														console.log("[Searcher.js] oTagsButtonsItem...");
														oTagsButtonsItem.sIdioma = idioma;
														// oTagsButtonsItem.sIsla = firstIsland;
														oTagsButtonsItem.oLifeStyles = ('lifestyles' in resultItem) ? resultItem.lifestyles: '';
														oTagsButtonsItem.oSeasons =  ('seasons' in resultItem) ? resultItem.seasons: '';
														oTagsButtonsItem.componentType = componentType;
														// if (componentType=='rrtt'){
															oTagsButtonsItem.oTouristicSubjects = ('touristic_subjects' in resultItem) ? resultItem.touristic_subjects : '';
															oTagsButtonsItem.sTerm = ('term' in resultItem) ? resultItem.term.lang[idioma] : '' ;
														// }

													}catch(e){
														console.log("[Searcher.js] Render (Error)..."+e.message);
													}

												}


												var url_friendly = "";
												var phone = "";

												if (searcherType1||searcherType5){
														url_friendly = eval('resultItem.data_sheet.'+ idioma +'.url_friendly');

														//Exception
														console.log("[Searcher.js] render()  tipology_id: "+tipology_id);


														if ((componentType == "rrtt") && ('club-nautico,escuela-nautica,estacion-nautica,marina,puerto-deportivo,spa,bodega').indexOf(resultItem.term.id)>=0){
							               url_friendly = resultItem.web;

							              }


														if ((componentType == "rrtt") && ('alojamiento').indexOf(tipology_id.toLowerCase())>=0){
															url_friendly = resultItem.web;
														}

														// if (searcherType1 && tipology_id!='' && ('instalaciones-nauticas,relax,enoturismo-y-oleoturismo,donde-dormir,donde-comer').indexOf(section)>=0){
														// 	url_friendly = resultItem.web;
														// }



														console.log("[Searcher.js] render()  url_friendly: "+url_friendly);


														if (tipology_id=='alojamiento'){

														var phone = ('features' in resultItem) ? resultItem.phone1:'';
														var email = ('features' in resultItem) ? resultItem.email:'';
														var category = ('features' in resultItem) ? _.join(resultItem.features[idioma]):'';
														console.log("[Searcher.js] render()  category: "+category);


														}





												}else
												if (searcherType2){
														url_friendly = resultItem.web;
												}
												else
												if (searcherType3){
														url_friendly = resultItem.multimedia[0].url;
												}


												let show = [];

												show.thumbnail =   section == "donde-dormir" ? false: true;


												// var url_target = url_friendly.indexOf('http')>=0 ? '_blank':'_self';
												var url_target = "_blank";

												// Details:

												if (municipality==txtIslands) { municipality=''; comma='';};

												if (searcherType5){

																url_target="_blank";

																return (
																				<div key={count} className="row">

																							<div className="col-sm-2"> <img alt='' className="img-responsive" src={background_image} /> </div>

																							<div className="col-sm-10">
																								<div className="news-v3">
																									<h3 className="title-v2"> <a href={url_friendly} target={url_target}>{_mark(title)}</a> <i className={(featured!=''?"fa-star ":"")+"fa color-blue margin-left-10"}></i></h3>

																									<p className="tn0">{_mark(subtitle)}</p>
																									<div className="tn1"> {_get(componentType)} {tipology&& '('+tipology+')'} </div>
																									<div className="tn3">{txtIslands}{comma} <span>{area}{comma} {municipality} </span> <a href="#" className="tn4">
																									<i className="fa fa-map-marker" name="mapview" onClick={this.setViewType}></i></a>
																									</div>
																									<p className="pmn">
																										<TagsButtons oDatosTagsButtons={oTagsButtonsItem} className="margin-bottom-30 margin-top-10"/>
																									</p>
																								</div>
																							</div>
																						 {(index!=10)&&
																							<div className="clearfix margin-bottom-20 margin-left-10 margin-right-10">
																								 <hr/>
																							</div>
																							}
																			 </div>

																);


												}	else{

															return(

																<div key={count}>
																			{searcherType1 &&
																						<div className="row" data-title={title}>
																					{show.thumbnail &&
																							<div className="col-sm-2"> <img alt='' className="img-responsive" src={background_image} /> </div>
																					}
																						<div className="col-sm-10">
																						 <div className="news-v3"> <span className="numlist">{strCount}</span>
																							 <h3 className="title-v2"> <a href={url_friendly} target={url_target}>{title} ({municipality}{comma}{txtIslands})</a></h3>
																							 <p className="margin-left-60">{subtitle} </p>
																							 { (tipology_id=='alojamiento') &&
																							 	<div className="margin-left-60 tn2 bold">
																								{category&& _get('category')+' '+category}
																								{phone&& ' | '+_get('phone')+' '+phone}
																								{email&& ' | '+_get('email')+' '}{email.toLowerCase()&& <a href={"mailto:"+email} className="text-lowercase">{email}</a>}
																								</div>
																							 }
																							 <TagsButtons oDatosTagsButtons={oTagsButtonsItem} className="margin-left-60 margin-bottom-30 margin-top-10"/>
																						 </div>
																						</div>
																						<div className="clearfix margin-bottom-20">
																							 <hr/>
																						</div>
																					</div>
																			}

																		{searcherType2 &&
																					<div className="row boxbg" data-title={title}  key={count}>
																						<div className="col-sm-12">
																							<div className="news-v3"> <span className="numlist">{strCount}</span>
					 																			<h3 className="tlist sp"> <a href={url_friendly} target={url_target}>{title}</a></h3>
																								<div className="margin-left-60 no-padding-bottom"><span className="txtactividades">{activities}</span></div>
																								<p className="margin-left-60"> <span className="txtilles">{isla}</span><span className="txtmunicipi">{municipality}</span></p>
																							</div>
																						</div>
																					</div>
																		}

																		{searcherType3 &&
																			<div className="row boxbg" data-title={title}  key={count}>
																							<div className="col-sm-4">
																							<img alt='' className="img-responsive" src={background_image} /> </div>
																							<div className="col-sm-8">
																								<div className="news-v3">
																									<h3 className="title-v2">
																									<a href={url_friendly} target={url_target}>{title}</a></h3>
																									<p> <span className="idlangs">{languages}</span></p>
																									<p className="vfiles">
																											 {islands.map((item, index) => {
																													 switch (item) {
																													       case "mallorca":   return (<i className="lma" key={index}></i>);
																													          case "menorca": return (<i className="lme" key={index}></i>);
																													          case "ibiza":  return (<i className="lib" key={index}></i>);
																																		case "formentera":  return (<i className="lfo" key={index}></i>);
																													          default:      return "#FFFFFF";
																													        }
																												  }
																											 )}
																									</p>
																								</div>
																							</div>
																						</div>
																		}


																	{searcherType4 &&
																		<div className="item col-md-4 margin-top-30 news-v2" style={{paddingLeft: '0'}} key={count}>
																					<a href={_urlEvento} target={_urlTarget}  data-url={_urlEvento}>
																					<img alt='' className="img-responsive" src={_imagen} />
																					</a>
																		     <div className="news-v2-desc" style={_styles_bgcolor} >
																		        <div className="txtdata">{_dates}</div>
																						{_alternateDates &&
																								<span ref="masfechas" data-toggle="tooltip" className="masfechas" title={_alternateDates}>{_.truncate(_alternateDates, {'length': 84})}</span>
																						}
																		        <span className="txtilles">{_get(firstIsland)}</span>
																						<span className="txtmunicipi">{_municipio}</span>
																		     		<a href={_urlEvento} target={_urlTarget} className="txttitle">{_titulo}</a>
																						<span className="addtocalendar atc-style-button-icon atc-style-menu-wb addiconcal" style={{position: 'absolute'}}>
																		        <a className="atcb-link fa fa-calendar-plus-o">
																		        </a>
																		        <var className="atc_event">
																		            <var className="atc_date_start">{_startdate}</var>
																		            <var className="atc_date_end">{_enddate}</var>
																		            <var className="atc_timezone">Europe/Madrid</var>
																		            <var className="atc_title">{_titulo}</var>
																		            <var className="atc_description">{_titulo}</var>
																		            <var className="atc_location">{firstIsland}</var>
																		            <var className="atc_organizer">Govern Illes Balears</var>
																		            <var className="atc_organizer_email">info@illesbalears.es</var>
																		        </var>
																		    	</span>

																					</div>
																		    </div>
																			}





																</div>

															);
													}

										}catch(e){
											console.log('[Searcher.js] render() (Error) en index num.'+index+': '+e.message);
										}

								});


					return cubos;
					}



	console.log('[Searcher.js] searcherType: '+searcherType);


	console.log('[Searcher.js] render() isLoading: '+isLoading);
	console.log('[Searcher.js] render() searchNotFound: '+searchNotFound);

	// <SearchFilters reset={this.state.resetFilters} items={this.state.searcher} onSearchFilter={this.onSearchFilter} language={idioma} searcherType={this.props.searcherType}/>

	var viewtype = this.state.viewtype;

	console.log('[Searcher.js] render() viewtype: '+viewtype);

	var listviewclass = (viewtype=='listview')? "btn-u-white":"btn-u-grey";
	var mapviewclass = (viewtype=='mapview')? "btn-u-white":"btn-u-grey";

	if (searcherType5){

					return(

					<div role="search">

						{!isLoading &&

						 <div className="filtros">
						   <div className="btn-group">
						        <button type="button" name="listview" className={"btn-u  rounded-3x "+listviewclass} onClick={this.setViewType}>
										<i className="fa fa-th-list" name="listview" onClick={this.setViewType}></i> {_get("lista")}
										</button>
						    </div>
						    <div className="btn-group">
						          <button type="button" name="mapview" className={"btn-u  rounded-3x "+mapviewclass} onClick={this.setViewType}>
											<i className="fa fa-map-marker" name="mapview" onClick={this.setViewType}></i> {_get("mapa")}
											</button>
						    </div>
						 </div>
					 }


					  <div id="row-main">

							{isLoading &&
						  <Spinner loading={isLoading} section={section}/>
						  }

							{!isLoading &&

					    <div className="col-md-3 padding-top-0 filterbar" id="sidebar">
					        <div className="col-md-12 filterslist">
										<SearchSuggest dosearch={false} reset={this.state.resetText} items={this.state.searcher} onAutoSuggest={this.onAutoSuggest} idioma={idioma} searcherType={this.props.searcherType}/>
									</div>
					        <div className="clearfix margin-bottom-20">
					        </div>
									<SearchFilters reset={this.state.resetFilters} items={this.state.searcher} onSearchFilter={this.onSearchFilter} onResetFilters={this.onResetFilters} language={idioma} searcherType={this.props.searcherType} section={section}/>
					    </div>
						  }





							{!isLoading && viewtype=='listview' &&

							<div className="col-md-9 noborder" id="content">
									{searchNotFound &&
									<div className="cg">
									<div className="alert alert-warning fade in">
										<strong>Lo sentimos!</strong> no hemos encontrado resultados en tu búsqueda, te mostramos todos los recursos disponibles de está categoría.
									</div>
									</div>
									}

					        {cubosPage(this.state.pageOfItems,0,10,this.state.currentPage)}
					    </div>
							}


							{!isLoading && viewtype=='mapview' &&
								<div className="col-md-9 fsmr"  >
		  						<div style={{height: '800px'}}>
									<Mapa render='client' maptype='cluster' sIdioma={idioma} cluster ={_.filter(this.state.searcher,{"component_type":"rrtt"})} sIsla="" sTitle=""/>
									</div>
								</div>
							}

					 </div>


					 <div className="text-center" id="searchpagination">
					   {!isLoading && viewtype=='listview' &&
							<SearchPagination items={this.state.searcher} onChangePage={this.onChangePage} />
						  }
					 </div>


					</div>

				);






	}else {
						return(

							<div role="search">
									{!isLoading&&
										<form action="#" id="sky-form" className="sky-form">
											<SearchSuggest dosearch={true} reset={this.state.resetText}  items={this.state.searcher} onAutoSuggest={this.onAutoSuggest} idioma={idioma} searcherType={this.props.searcherType}/>
											<SearchFilters reset={this.state.resetFilters} items={this.state.searcher} onSearchFilter={this.onSearchFilter} onResetFilters={this.onResetFilters} language={idioma} searcherType={this.props.searcherType} section={section}/>
										</form>
									}


											 {searchNotFound &&
											 <div className="cg">
												<div className="alert alert-warning fade in">
													<strong>Lo sentimos!</strong> no hemos encontrado resultados en tu búsqueda, te mostramos todos los recursos disponibles de está categoría.
												</div>
											 </div>
											 }


											 {isLoading && searcherType1 &&
											 <Spinner loading={isLoading} section={section}/>
										 	 }

											 {!isLoading && searcherType1 &&
												cubosPage(this.state.pageOfItems,0,10,this.state.currentPage)
											 }



											 {isLoading && (searcherType2 || searcherType3) &&
												 <div className="col-sm-12 fsmr">
												 <Spinner loading={isLoading} section={section}/>
												 </div>
											 }


											 {!isLoading && (searcherType2 || searcherType3)&&
												<div className="col-sm-6 col2sm fsml">
												{cubosPage(this.state.pageOfItems,0,5,this.state.currentPage)}
												</div>
											 }

											 {!isLoading && (searcherType2 || searcherType3)&&
												<div className="col-sm-6 fsmr">
												{cubosPage(this.state.pageOfItems,5,10,this.state.currentPage)}
												</div>
											 }



											 {searcherType4 &&
												 <div className="container margin-bottom-40" style={{paddingLeft: '0'}}>
												 {isLoading?
												 <Spinner loading={isLoading} section={section}/>
												 :
												 cubosPage(this.state.pageOfItems,0,9,this.state.currentPage)}
												</div>
											 }



											 <div className="text-center" id="searchpagination">
											 		{!isLoading &&
													<SearchPagination items={this.state.searcher} onChangePage={this.onChangePage} />
												  }
											 </div>


						</div>

					);

		}




		}

});


module.exports = Searcher;
