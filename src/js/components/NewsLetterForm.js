var React = require('react');
var estaticos_pie = require('../../resources/estaticosFooter.json');
var $ = require('jquery');

// var FormControl = require('react-bootstrap').FormControl;
// var FormGroup = require('react-bootstrap').FormGroup;




var NewsLetterForm = React.createClass({

  onClick: function (event){
		console.log('[NewsLetterForm.js] onClick() ...');


    console.log('[NewsLetterForm.js] onClick() ...'+event.currentTarget.form.checkValidity());


    if (event.currentTarget.form.checkValidity()){
        console.log('[NewsLetterForm.js] onClick()  OK ...');
	      // $('#form-newsletter-modal').modal('show');
    }
    else{
      console.log('[NewsLetterForm.js] onClick()  KO ...');

      event.preventDefault();
      event.stopPropagation();
    }
    //
    // //
    //
    // if ($(this.refs['form-newsletter']).attr('data-validated') == "true") {
    //   event.preventDefault();
  	// 	event.stopPropagation();
  	// 	$(this.refs['form-newsletter']).attr('data-validated',"true");
  	// 	$('#modal-newsletter').modal('show');
    //
		// }
    //





		// if ($(this.refs['form-newsletter']).attr('data-validated') == "false") {
		// } else{
		// }

  },
  onSubmit: function (event){
    console.log('[NewsLetterForm.js] onSubmit() ...');
    console.log('[NewsLetterForm.js] onSubmit() checkValidity() ...'+event.target.checkValidity());

    event.preventDefault();
    event.stopPropagation();
  },
  componentDidMount: function (){

        // $('#form2 input[type=email]').on('change invalid', function() {


          //
          $(this.refs['form-newsletter-input']).on('change invalid', function() {



            var textfield = $(this).get(0);
            var textvalue = $(this).attr('data-invalid');
            // 'setCustomValidity not only sets the message, but also marks
            // the field as invalid. In order to see whether the field really is
            // invalid, we have to remove the message first
            textfield.setCustomValidity('');


            if (!textfield.validity.valid) {
              console.log('[NewsLetterForm.js] validateEmail()  change invalid KO...');
              $(this).attr('data-validated',"false");
                // $(this.refs['form-newsletter-input']).popover('show')

              textfield.setCustomValidity(textvalue);

            }

          });

          $(this.refs['form-newsletter-input']).on('change valid', function() {


            var textfield = $(this).get(0);

            if (textfield.validity.valid) {
              console.log('[NewsLetterForm.js] validateEmail()  change valid OK ...');

              $(this).attr('data-validated',"true");
              $(this).popover('hide')

              // textfield.setCustomValidity('Gracias');
            }




          });



  },

	validateEmail: function (value) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(value);
  },
	render: function(){

				console.log('[NewsLetterForm.js] render() version 1.1');


			  var idioma = String(this.props.idioma);
				console.log('[NewsLetterForm.js] render() idioma: '+idioma);



				function _get(txt){
					try {
				  	return estaticos_pie[0][txt][idioma];
					} catch (e){
						console.log("_get(txt) (error): "+e.message);
						return txt;
					}
				}


				return(

					<div>
						<form action="#"  onSubmit={this.onSubmit} id="needs-validation"
            data-validated="false" ref="form-newsletter" className="input-group form-newsletter" novalidate>

						<input type="email"
						className="form-control"
            ref="form-newsletter-input"
            id="form-newsletter-input"
            name="newsletterform"
            aria-label={_get('tuemail')}
            title={_get('tuemail')}
						placeholder={_get('tuemail')}
            data-invalid={_get('emailvalido')}
            data-content={_get('emailvalido')}
            required/>


						<span className="input-group-btn">
						<button className="btn" type="submit" data-toggle="modal" data-target="#form-newsletter-modal" onClick={this.onClick}  aria-label={_get('tuemail')}>
						<i className="fa fa-envelope-o"></i></button>
						</span>

						</form>
					</div>

				);





		}

});


module.exports = NewsLetterForm;
