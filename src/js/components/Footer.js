var React = require('react');

var idioma = '';
var isla = '';
var estaticos_pie = require('../../resources/estaticosFooter.json');
// var estaticos_cab = require('../../resources/estaticosCabecera.json');

var dameTextoi18 = require('../webutils/utilidades.js').dameTextoi18;
var dameUrlFriendly = require('../webutils/utilidades.js').dameUrlFriendly;
var CookieLaw = require('./CookieLaw');
var NewsLetterForm = require('./NewsLetterForm');
var NewsLetterModal = require('./NewsLetterModal');





function _get(txt){
  return dameTextoi18(estaticos_pie[0][txt],idioma);
}

// function _get2(txt){
//   return dameTextoi18(estaticos_pie[0][txt],idioma);
// }


var Footer = React.createClass({

  render: function(){
try{
    idioma = String(this.props.sIdioma)
    if(typeof this.props.sIsla != 'undefined'){
      isla = String(this.props.sIsla);
    } else {
      isla = location.pathname.split('/')[2];
    }



    /***********************************************************************/
    /**************** LIterales   ************************************/
    /***********************************************************************/
    var strIslas                =   dameTextoi18(estaticos_pie[0]['fislas'],idioma);
    var strComoViajas           =   dameTextoi18(estaticos_pie[0]['comoViajas'],idioma);
    var strConFamilia           =   dameTextoi18(estaticos_pie[0]['conFamilia'],idioma);
    var strConAmigos            =   dameTextoi18(estaticos_pie[0]['conAmigos'],idioma);
    var strConPareja            =   dameTextoi18(estaticos_pie[0]['conPareja'],idioma);
    var strPortrabajo           =   dameTextoi18(estaticos_pie[0]['portrabajo'],idioma);
    var strOfrecemos            =   dameTextoi18(estaticos_pie[0]['ofrecemos'],idioma);
    var strAgenda               =   dameTextoi18(estaticos_pie[0]['agenda'],idioma);
    var strAgenciasdeViaje      =   dameTextoi18(estaticos_pie[0]['agenciasViajes'],idioma);
    var strGuiaPractica         =   dameTextoi18(estaticos_pie[0]['guiaPractica'],idioma);
    var strContacto             =   dameTextoi18(estaticos_pie[0]['contacto'],idioma);


    var strComeryBeber          =   dameTextoi18(estaticos_pie[0]['comeryBeber'],idioma);
    var strArteyCultura         =   dameTextoi18(estaticos_pie[0]['arteyCultura'],idioma);
    var strCultura              =   dameTextoi18(estaticos_pie[0]['cultura'],idioma);
    var strInstalNauticas       =   dameTextoi18(estaticos_pie[0]['instalNauticas'],idioma);
    var strActivNauticas        =   dameTextoi18(estaticos_pie[0]['activNauticas'],idioma);
    var strMaryPlayas           =   dameTextoi18(estaticos_pie[0]['maryPlayas'],idioma);
    var strDeporteNatural       =   dameTextoi18(estaticos_pie[0]['deporteNatural'],idioma);
    var strDeporte              =   dameTextoi18(estaticos_pie[0]['deporte'],idioma);
    var strNaturaleza           =   dameTextoi18(estaticos_pie[0]['naturaleza'],idioma);


    var strRutasyPlanes         =   dameTextoi18(estaticos_pie[0]['rutasyPlanes'],idioma);
    var strPatrimonioMundial    =   dameTextoi18(estaticos_pie[0]['patrimonioMundial'],idioma);
    var strCuandoViajas         =   dameTextoi18(estaticos_pie[0]['cuandoViajas'],idioma);
    var strAutentico            =   dameTextoi18(estaticos_pie[0]['autentico'],idioma);
    var strPalacioCongresos     =   dameTextoi18(estaticos_pie[0]['palacioCongresos'],idioma);
    var strGuiaPractica         =   dameTextoi18(estaticos_pie[0]['guiaPractica'],idioma);
    var strPublicaciones        =   dameTextoi18(estaticos_pie[0]['publicaciones'],idioma);
    var strOficinasTurismo      =   dameTextoi18(estaticos_pie[0]['oficinasTurismo'],idioma);
    var strBalearesMultimedia   =   dameTextoi18(estaticos_pie[0]['balearesMultimedia'],idioma);
    var strNoticias             =   dameTextoi18(estaticos_pie[0]['Noticias'],idioma);
    var strAgenciaTurismo       =   dameTextoi18(estaticos_pie[0]['agenciaTurismo'],idioma);
    var strCopyright            =   dameTextoi18(estaticos_pie[0]['copyright'],idioma);
    var strMapaWeb              =   dameTextoi18(estaticos_pie[0]['mapaWeb'],idioma);
    var strAccesible            =   dameTextoi18(estaticos_pie[0]['accesible'],idioma);
    var strAvisoLegal           =   dameTextoi18(estaticos_pie[0]['avisoLegal'],idioma);

    var strGastronomia         = dameTextoi18(estaticos_pie[0]['gastronomia'],idioma);
    var strOcioyAtracciones    = dameTextoi18(estaticos_pie[0]['ocioyAtracciones'],idioma);
    var strTurismoActivo       = dameTextoi18(estaticos_pie[0]['turismoActivo'],idioma);
    var strPaisaje             = dameTextoi18(estaticos_pie[0]['paisaje'],idioma);

    var strUrlFamilia           =   dameUrlFriendly(estaticos_pie[0]['url_familia'],idioma,isla);
    var strUrlAmigos            =   dameUrlFriendly(estaticos_pie[0]['url_amigos'],idioma,isla);
    var strUrlPareja            =   dameUrlFriendly(estaticos_pie[0]['url_pareja'],idioma,isla);
    var strUrlTrabajo           =   dameUrlFriendly(estaticos_pie[0]['url_trabajo'],idioma,isla);
    var strPrimavera           = dameTextoi18(estaticos_pie[0]['primavera'],idioma);
    var strUrlPrimavera        = dameUrlFriendly(estaticos_pie[0]['url_primavera'],idioma,isla);
    var strVerano              = dameTextoi18(estaticos_pie[0]['verano'],idioma);
    var strUrlVerano       = dameUrlFriendly(estaticos_pie[0]['url_verano'],idioma,isla);
    var strOtono               = dameTextoi18(estaticos_pie[0]['otono'],idioma);
    var strUrlOtono          = dameUrlFriendly(estaticos_pie[0]['url_otono'],idioma,isla);
    var strInvierno            = dameTextoi18(estaticos_pie[0]['invierno'],idioma);
    var strUrlInvierno       = dameUrlFriendly(estaticos_pie[0]['url_invierno'],idioma,isla);

    var consells       = dameTextoi18(estaticos_pie[0]['consells'],idioma,isla);
    var consellmallorca       = dameTextoi18(estaticos_pie[0]['consellmallorca'],idioma,isla);
    // var consellmallorca_url      = dameUrlFriendly(estaticos_pie[0]['consellmallorca_url'],idioma,isla);
    var consellmenorca       = dameTextoi18(estaticos_pie[0]['consellmenorca'],idioma,isla);
    // var consellmenorca_url      = dameUrlFriendly(estaticos_pie[0]['consellmenorca_url'],idioma,isla);
    var consellibiza       = dameTextoi18(estaticos_pie[0]['consellibiza'],idioma,isla);
    // var consellibiza_url      = dameUrlFriendly(estaticos_pie[0]['consellibiza_url'],idioma,isla);
    var consellformentera       = dameTextoi18(estaticos_pie[0]['consellformentera'],idioma,isla);
    // var consellformentera_url      = dameUrlFriendly(estaticos_pie[0]['consellformentera_url'],idioma,isla);

    var dondeir      = dameTextoi18(estaticos_pie[0]['dondeir'],idioma,isla);
    var dondeir_url     = dameTextoi18(estaticos_pie[0]['dondeir_url'],idioma,isla);
    var dondecomer      = dameTextoi18(estaticos_pie[0]['dondecomer'],idioma,isla);
    var dondecomer_url     = dameTextoi18(estaticos_pie[0]['dondecomer_url'],idioma,isla);
    var comollegar      = dameTextoi18(estaticos_pie[0]['comollegar'],idioma,isla);
    var comollegar_url     = dameTextoi18(estaticos_pie[0]['comollegar_url'],idioma,isla);
    var dondedormir      = dameTextoi18(estaticos_pie[0]['dondedormir'],idioma,isla);
    var dondedormir_url     = dameTextoi18(estaticos_pie[0]['dondedormir_url'],idioma,isla);


    var strUrlMallorca    = '/' + idioma + '/mallorca/';
    var strUrlMenorca       = '/' + idioma + '/menorca/';
    var strUrlIbiza     = '/' + idioma + '/ibiza/';
    var strUrlFormentera    = '/' + idioma + '/formentera/';


} catch(e) {
  console.log('FOOTER ERR: ' + e.message);
}




    return(
      <div role="contentinfo">


          <div className="margin-top-20"> </div>
          <section className="accesos margin-top-20">
                   <div className="container margin-top-20">
                        <div className="row text-center margin-bottom-20">
                  				        <a href={dondedormir_url}>
                                  <div className="col-md-3 col-xs-3 flat-service">
                                      <i className="fa  fa-bed   icon-custom icon-lg rounded-x icon-bg-u"></i>
                      						    <h3 className="title-v3-md margin-bottom-10">{dondedormir}</h3>
                  					      </div>
                                  </a>
                                  <a href={dondeir_url}>
                                  <div className="col-md-3 col-xs-3 flat-service">
                                      <i className="fa  fa-map-marker  icon-custom icon-lg rounded-x icon-bg-u"></i>
                      						    <h3 className="title-v3-md margin-bottom-10">{dondeir}</h3>
                        					</div>
                                  </a>
                                  <a href={comollegar_url}>
                                  <div className="col-md-3 col-xs-3 flat-service">
                                       <i className="fa  fa-plane   icon-custom icon-lg rounded-x icon-bg-u"></i>
                      						     <h3 className="title-v3-md margin-bottom-10">{comollegar}</h3>
                            			</div>
                                  </a>
                                  <a href={dondecomer_url}>
                                  <div className="col-md-3 col-xs-3 flat-service">
                                      <i className="fa  fa-cutlery  icon-custom icon-lg rounded-x icon-bg-u"></i>
                  						        <h3 className="title-v3-md margin-bottom-10">{dondecomer}</h3>
                          				</div>
                                  </a>
                        </div>
                   </div>
            </section>

            <div className="shop-subscribe bg-color-green" id="newsletterModule">
              <div className="container">
                <div className="row">
                    <div className="col-md-8 md-margin-bottom-20">
                      <h2>{_get('subscribete')} <strong>{_get('newsletter')} </strong></h2>
                    </div>
                    <div className="col-md-4" id="newsletterform">
                      <NewsLetterForm idioma={idioma}/>
                    </div>
                </div>
              </div>
            </div>
            <div id="newslettermodal">
            <NewsLetterModal idioma={idioma}/>
            </div>







        <div className='footer' data-version="Footer-v1.1">
          <div className='container'>
            <div className='row'>

              <div className='col-sm-2 md-margin-bottom-40'>
                <a href='#'>
                  <img alt='' id='logo-footer' src='/assets/images/logo_aetib.png' alt='Agència Turisme Illes Balears' />
                </a>
              </div>
              <div className='col-sm-2 padding-top-20'>
                  <h3  className='th3'>{strIslas}</h3>
                  <ul className='footer-lists'>
                      <li><a href={strUrlMallorca} className='rounded-4x rn4 text-capitalize' target="_blank">Mallorca</a></li>
                      <li><a href={strUrlMenorca} className='rounded-4x rn4 text-capitalize' target="_blank">Menorca</a></li>
                      <li><a href={strUrlIbiza} className='rounded-4x rn4 text-capitalize' target="_blank">Ibiza</a></li>
                      <li><a href={strUrlFormentera} className='rounded-4x rn4 text-capitalize' target="_blank">Formentera</a></li>
                    </ul>
              </div>
              <div className='col-sm-2 padding-top-20'>
                  <h3  className='th3'>{strCuandoViajas}</h3>
                  <ul className='footer-lists'>
                      <li><a href={strUrlPrimavera} className='rounded-4x rn4 text-lowercase' target="_blank">{strPrimavera}</a></li>
                      <li><a href={strUrlVerano} className='rounded-4x rn4 text-lowercase' target="_blank">{strVerano}</a></li>
                      <li><a href={strUrlOtono} className='rounded-4x rn4 text-lowercase' target="_blank">{strOtono}</a></li>
                      <li><a href={strUrlInvierno} className='rounded-4x rn4 text-lowercase' target="_blank">{strInvierno}</a></li>
                    </ul>
              </div>
              <div className='col-sm-2 padding-top-20'>
                  <h3  className='th3'>{strComoViajas}</h3>
                  <ul className='footer-lists'>
                      <li><a href={strUrlFamilia} className='rounded-4x rn4 text-lowercase' target="_blank">{strConFamilia}</a></li>
                      <li><a href={strUrlAmigos} className='rounded-4x rn4 text-lowercase' target="_blank">{strConAmigos}</a></li>
                      <li><a href={strUrlPareja} className='rounded-4x rn4 text-lowercase' target="_blank">{strConPareja}</a></li>
                      <li><a href={strUrlTrabajo} className='rounded-4x rn4 text-lowercase' target="_blank">{strPortrabajo}</a></li>
                    </ul>
              </div>
              <div className="col-sm-4 padding-top-20">
                  <h3 className="th3">{consells}</h3>
                  <ul className="footer-lists">
                    <li className="col-md-6 col-xs-6 fma"><a href={_get('consellmallorca_url')} target="_blank">{consellmallorca}</a></li>
                    <li className="col-md-6 col-xs-6 fme"><a href={_get('consellmenorca_url')} target="_blank">{consellmenorca}</a></li>
                    <li className="col-md-6 col-xs-6 fib"><a href={_get('consellibiza_url')} target="_blank">{consellibiza}</a></li>
                    <li className="col-md-6 col-xs-6 ffo"><a href={_get('consellformentera_url')} target="_blank">{consellformentera}</a></li>
                  </ul>
              </div>

            </div>




            <div className="row margin-bottom-20">

                <div className='col-sm-2 md-margin-bottom-40'>
                  <a href={_get('betterinwinter_url')} target="_blank">
                    <img alt='' src='/assets/images/BetterinWinter_IBFooter_LOGO.png' alt='Better in Winter IllesBalears' />
                  </a>
                </div>

                 <div className="col-sm-2">
                   <h3 className="th3"> {_get('ofrecemos')}</h3>
                   <ul className="footer-lists">
                     <li><a href={_get('arteycultura_url')} target="_blank"> {_get('arteycultura')}</a></li>
                     <li><a href={_get('paisaje_url')} target="_blank"> {_get('paisaje')} </a></li>
                     <li><a href={_get('turismoactivo_url')} target="_blank">{_get('turismoactivo')}  </a></li>
                     <li><a href={_get('maryplayas_url')} target="_blank">{_get('maryplayas')} </a></li>
                   </ul>
                 </div>


                 <div className="col-sm-2 padding-top-40">
                   <ul className="footer-lists">
                    <li><a href={_get('bienestar_url')} target="_blank"> {_get('bienestar')}</a></li>
                    <li><a href={_get('gastronomia_url')} target="_blank">{_get('gastronomia')} </a></li>
                    <li><a href={_get('ocioyatracciones_url')} target="_blank">{_get('ocioyatracciones')} </a></li>
                    <li><a href={_get('shopping_url')} target="_blank">{_get('shopping')} </a></li>
                  </ul>
                 </div>

                 <div className="col-sm-2 padding-top-40">
                 <ul className="footer-lists">
                    <li><a href={_get('rutasyplanes_url')} target="_blank"> {_get('rutasyplanes')}</a></li>
                    <li><a href={_get('mice_url')} target="_blank">{_get('mice')} </a></li>
                    <li><a href={_get('unesco_url')} target="_blank">{_get('unesco')} </a></li>
                  </ul>
                 </div>

                 <div className="col-sm-2">
                  <h3 className="th3">{_get('guiaPractica')}</h3>
                  <ul className="footer-lists">
                    <li><a href={_get('localizador_url')} target="_blank">{_get('localizador')} </a></li>
                    <li><a href={_get('publicaciones_url')} target="_blank">{_get('publicaciones')} </a></li>
                    <li><a href={_get('informacioninteres_url')} target="_blank">{_get('informacioninteres')} </a></li>
                  </ul>
                 </div>

                 <div className="col-sm-2">
                  <h3 className="th3">{_get('agenda')}</h3>
                    <ul className="footer-lists">
                    <li><a href={_get('agendamallorca_url')} target="_blank"> {_get('agendamallorca')} </a></li>
                    <li><a href={_get('agendamenorca_url')} target="_blank"> {_get('agendamenorca')} </a></li>
                    <li><a href={_get('agendaibiza_url')} target="_blank"> {_get('agendaibiza')} </a></li>
                    <li><a href={_get('agendaformentera_url')} target="_blank"> {_get('agendaformentera')} </a></li>
                    </ul>
                 </div>


            </div>



          </div>
             <div className="copyright">
                 <div className="container">
                   <div className="row">
                     <div className="col-md-9">
                       <p> {strCopyright}&nbsp;&nbsp; <a href="mailto:info@illesbalears.travel"> info@illesbalears.travel </a>&nbsp;|&nbsp;<a href={_get('politicaprivacidad_url')} target='_blank'>{_get('politicaprivacidad')}</a> &nbsp;|&nbsp;<a href={_get('cookies_url')} target='_blank'>{_get('cookies')}</a>  &nbsp;|&nbsp; <a href={_get('avisolegal_url')} target='_blank'>{_get('avisolegal')}</a>  </p>
                     </div>
                                <div className="col-md-3">
                                    <a href="http://www.w3.org/WAI/WCAG2AA-Conformance" target="_blank" title="Explanation of WCAG 2.0 Level Double-A Conformance" >
                                    <img height="31" width="72" src="/assets/images/1921277123-wcag2AA_ibtravel.png" alt="Level Double-A conformance, W3C WAI Web Content Accessibility Guidelines 2.0" style={{marginTop: '10px'}}/></a>

                                     <ul className="social-icons pull-right">
                                       <li><a href="https://www.facebook.com/turismeillesbalears" aria-label="Facebook" target="_blank" data-original-title="Facebook" className="rounded-x social_facebook"></a></li>
                                       <li><a href="https://twitter.com/turismebalears" aria-label="Twitter" target="_blank"  data-original-title="Twitter" className="rounded-x social_twitter"></a></li>
                                       <li><a href="https://instagram.com/turismoislasbaleares/"  aria-label="Instagram" target="_blank" data-original-title="Instagram" className="rounded-x social_instagram"></a></li>
                                       <li><a href="https://www.youtube.com/user/turismeillesbalears" aria-label="Youtube" target="_blank" data-original-title="Youtube" className="rounded-x social_youtube"></a></li>
                                     </ul>
                                   </div>
                       </div>
                     </div>
                   </div>
              </div>
              <CookieLaw idioma={idioma}/>
            </div>
          );
        }
      });




module.exports = Footer;
