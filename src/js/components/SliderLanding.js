var React = require('react');
var ActionsCreator = require('../actions/appActions');
var SliderStore = require('../stores/SliderStore');
var dameTextoi18 = require('../webutils/utilidades.js').dameTextoi18;
var rastroLanding = require('../webutils/utilidades.js').rastroLanding;

var slides = [];
var barTitle = "";
var barSubtitle = "";

var videoPoster = "/assets/images/ico_play.jpg";

function cargarSlides(_url){
	try{
	  	if(typeof $ != "undefined"){
		  $.ajax({
		    //url: "https://private-248ed3-atbsliderlanding.apiary-mock.com/slider_land",
		    url: _url,
		    dataType: 'json',
		    cache: false,
		    async: false,
		    success: function(data){
				slides = data;
				console.error('[Slider] ciclo vida 1. success '+slides);
		    }.bind(this),
		    error: function(xhr, status, err) {
		      console.error(this.props.url, status, err.toString());
		    }.bind(this),
		    complete: function(xhr, status){
		    }.bind(this)
		  });
		}
	} catch(e){
		console.log('[SliderLanding]  Cargar Slider ' + e.message);
	}
}

function getStateFromStore(){
	return {slides: SliderStore.getAll()}
}

var SliderLanding = React.createClass({

	// Dispara la carga de los datos del slider
	getInitialState: function(){
		//Client side
		if(this.props.sUrl){
			var _url = String(this.props.sUrl);
			cargarSlides(_url);
		} else {
			slides = this.props.componente;
		}
		ActionsCreator.updateSlides({data:slides});
		return getStateFromStore();
	},
	componentDidMount: function(){
		SliderStore.addLoadListener(this.onLoad);
	},
	ComponentWillUnMount: function(){
		SliderStore.removeLoadListener(this.onLoad);
	},
	onLoad: function() {
		//console.warn('[Recomendaciones] ciclo vida 6');
		this.setState(getStateFromStore());
	},
	render: function(){

		var idioma = String(this.props.sIdioma);
		var idSlider = String(this.props.sId);
		var displayNone = {display: 'none'};

		// var tituloComponente = dameTextoi18(this.state.slides.data.items[0].landing_title,idioma);
		// var subtituloComponente = dameTextoi18(this.state.slides.data.items[0].landing_text,idioma);

		var _rastro = rastroLanding(this.props.sRastro);

		// Estilos in-line
		var _style1 = {
			zIndex: '5',
			whiteSpace: 'nowrap',
			fontSize: 30,
			lineHeight: 70,
			fontWeight: [700],
			color: 'rgba(255, 255, 255, 1.00)',
			fontFamily:'Roboto',
			backgroundColor:'rgba(0, 0, 0, 1.00)',
			padding:'0 30px 0 30px'
		}
		var _style2 = {
			zIndex: '6',
			whiteSpace: 'nowrap',
			fontSize: 30,
			lineHeight: 50,
			fontWeight: [300],
			color: 'rgba(255, 255, 255, 1.00)',
			fontFamily:'Roboto',
			backgroundColor:'rgba(0, 0, 0, 1.0)',
			padding:'0 30px 0 30px'
		}
		var _style3 = {
			visibility: 'hidden !important'
		};

		var styleLayer1 = {
			zIndex: '5',
			backgroundColor:'rgba(0, 0, 0, 0.50)',
			borderColor:'rgba(0, 0, 0, 0)',
			background: 'linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(0,0,0,0.45) 100%)'
		};
		var styleLayer2 = {
			zIndex: '5',
			whiteSpace: 'nowrap',
			fontSize: 50,
			lineHeight: 70,
			fontWeight: [700],
			color: 'rgba(255, 255, 255, 1.00)',
			fontFamily:'Roboto',
			backgroundColor: 'rgba(0, 0, 0, 1.0)',
			padding: '0 30px 0 30px'
		}
		var styleLayer3 = {
			zIndex: '6',
			whiteSpace: 'nowrap',
			fontSize: 30,
			lineHeight: 50,
			fontWeight: [300],
			color: 'rgba(255, 255, 255, 1.00)',
			fontFamily:'Roboto',
			backgroundColor:'rgba(0, 0, 0, 1.0)',
			padding: '0 30px 0 30px'
		}

		var styleLayer4 = {
			zIndex: '8',
			whiteSpace: 'nowrap',
			borderColor:'rgba(255, 255, 255, 0.25)',
			outline:'none',
			boxShadow:'none',
			boxSizing:'border-box',
			MozBoxSizing:'border-box',
			WebkitBoxSizing:'border-box'
		}
		var styleLayer5 = {
			zIndex: '9',
			whiteSpace: 'nowrap',
			padding: '15px 20px 15px 20px',
			borderColor:'rgba(255, 255, 255, 0.25)',
			outline:'none',
			boxShadow:'none',
			boxSizing:'border-box',
			MozBoxSizing:'border-box',
			WebkitBoxSizing:'border-box'
		}

		var _left = "['left','left','left','left']";
		var _bottom="['bottom','bottom','bottom','bottom']";
		var _center="['center','center','center','center']";
		var _str0="['0','0','0','0']";
		var _str1="['30','30','30','20']";
		var _str2="['80','80','80','50']";
		var _str3="['20','20','20','10']";
		var _str4="['50','50','40','30']";
		var _str5="['140','140','120','85']";
		var _str6="['30','20','20','20']";
		var _str7="['70','60','50','40']";
		var _str8="['400','400','400','550']";
		var _str9="['30','20','20','20']";
		var _str10="['600','600','30','20']";
		var _str11="['80','80','30','20']";
		var _str12="['790','790','226','216']";

		var isla = dameTextoi18(this.state.slides.data.items[0].title,idioma);
		var sTitle = dameTextoi18(this.state.slides.data.items[0].subtitle,idioma);


		// SLIDES
		var slides = this.state.slides.data.items[0].multimedia_slider.map(function(slide, index){


			var dataIndex = 'rs-' + index;
			var layer1 = 'slide-'+index+'-layer-1';
			var layer2 = 'slide-'+index+'-layer-2';
			var transicion = 'slideremovehorizontal';


			var mediaUrl = slide.url;
			var thumbUrl = slide.type=="video"? videoPoster : slide.url;
			var slideImg = slide.type=="video"? videoPoster : slide.url;

			var show_video = (slide.type=="video");

			console.log('[SliderFicha] slide num - ' + dataIndex+' index: '+index);
			console.log('[SliderFicha] slide.url ' + slide.url);
			console.log('[SliderFicha] slide.type ' + slide.type);
			console.log('[SliderFicha] slide.title ' + JSON.stringify(slide.title));


			console.log('[SliderFicha] mediaUrl: '+mediaUrl);
			console.log('[SliderFicha] thumbUrl: '+thumbUrl);
			console.log('[SliderFicha] slideImg: '+slideImg);


			try {

				//@TODO: Según Component Type.

				sTitle = slide.subtitle[idioma];

				var imageTitle = (sTitle=='') ? this.props.oSlides.sTitle : sTitle;

			} catch(e){
				console.log('[SliderLanding] error ' + e.message);
			}


			// BACKGROUND VIDEO LAYER
			var videoLayer = <div className='rs-background-video-layer'
				data-forcerewind='on'
				data-volume='mute'
				data-videowidth='100%'
				data-videoheight='100%'
				data-videomp4={mediaUrl}
				data-videopreload='preload'
				data-videostartat='00:00'
				data-videoloop='loop'
				data-forceCover='1'
				data-aspectratio='16:9'
				data-autoplay='true'
				data-autoplayonlyfirsttime='false'
				data-nextslideatend='true'>
			</div>

			// LAYER 1
			var _layer1 = <div
				className='tp-caption tp-shape tp-shapewrapper rs-parallaxlevel-0'
				id={'slide-312-layer-11-'+index}
				data-x= {_center}
				data-hoffset={_str0}
				data-y={_bottom}
				data-voffset={_str0}
				data-width='full'
				data-height={_str8}
				data-whitespace='nowrap'
				data-transform_idle='o:1;'
				data-style_hover='cursor:default;'
				data-transform_in='opacity:0;s:1500;e:Power2.easeInOut;'
				data-transform_out='opacity:0;s:1000;s:1000;'
				data-start='0'
				data-basealign='slide'
				data-responsive_offset='off'
				data-responsive='off'
				style={styleLayer1}>
			</div>

			// LAYER 2
			var _layer2 = <div className='tp-caption tp-resizeme'
				id={'slide-1953-layer-1-'+index}
				data-x={_left}
				data-hoffset={_str1}
				data-y={_bottom}
				data-voffset={_str5}
				data-fontsize={_str9}
				data-lineheight={_str7}
				data-width='none'
				data-height='none'
				data-whitespace='nowrap'
				data-transform_idle='o:1'

				data-transform_in='x:right(R);s:800;e:Power1.easeInOut;'
				data-transform_out='x:left(R);s:800;e:Power1.easeInOut;'
				data-start='0'
				data-splitin='none'
				data-splitout='none'
				data-basealign='slide'
				data-responsive_offset='on'
				style={styleLayer2}>

				{isla}

				</div>

			var _layer3 = <div className='tp-caption tp-resizeme'
						id={'slide-1953-layer-2-'+index}
						data-x={_left}
						data-hoffset={_str1}
						data-y={_bottom}
						data-voffset={_str2}
						data-fontsize={_str3}
						data-lineheight={_str4}
						data-width='none'
						data-height='none'
						data-whitespace='nowrap'
						data-transform_idle='o:1;'
						data-transform_in='x:right(R);s:800;e:Power1.easeInOut;'
						data-transform_out='x:left(R);s:800;e:Power1.easeInOut;'
						data-start='0'
						data-splitin='none'
						data-splitout='none'
						data-basealign='slide'
						data-responsive_offset='on'

						style={styleLayer3} >

						{sTitle}

						</div>


				var _layer4 = <div className='tp-caption BigBold-Button rev-btn rs-parallaxlevel-0'
					id={'slide-313-layer-7-'+index}
					data-x={_left}
					data-hoffset={_str10}
					data-y={_bottom}
					data-voffset={_str11}
					data-width='none'
					data-height='none'
					data-whitespace='nowrap'
					data-transform_idle='o:1;'
					data-transform_hover='o:1;rX:0;rY:0;rZ:0;z:0;s:300;e:Power1.easeInOut;'
					data-style_hover='c:rgba(255, 255, 255, 1.00);bc:rgba(255, 255, 255, 1.00);cursor:pointer;'
					data-transform_in='y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeInOut;'
					data-transform_out='y:50px;opacity:0;s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;'
					data-start='650'
					data-splitin='none'
					data-splitout='none'
					data-actions='[{"event":"click","action":"scrollbelow","offset":"px"}]'
					data-basealign='slide'
					data-responsive_offset='off'
					data-responsive='off'

					style={styleLayer4}>
					</div>

					var _layer5 = <div className='tp-caption BigBold-Button rev-btn rs-parallaxlevel-0'
						id={'slide-313-layer-12-'+index}
						data-x={_left}
						data-hoffset={_str12}
						data-y={_bottom}
						data-voffset={_str11}
						data-width='none'
						data-height='none'
						data-whitespace='nowrap'
						data-transform_idle='o:1;'
						data-transform_hover='o:1;rX:0;rY:0;rZ:0;z:0;s:300;e:Power1.easeInOut;'
						data-style_hover='c:rgba(255, 255, 255, 1.00);bc:rgba(255, 255, 255, 1.00);cursor:pointer;'
						data-transform_in='y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeInOut;'
						data-transform_out='y:50px;opacity:0;s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;'
						data-start='650'
						data-splitin='none'
						data-splitout='none'
						data-actions='[{"event":"click","action":"jumptoslide","slide":"next","delay":""}]'
						data-basealign='slide'
						data-responsive_offset='off'
						data-responsive='off'

				style={styleLayer5}>

				<i className='fa-icon-chevron-right'></i>

				</div>





			return(

				<li key={index}
			        data-index={dataIndex}
			        data-transition={transicion}
			        data-slotamount='default'
						        data-easein='Power1.easeInOut'
						        data-easeout='Power1.easeInOut'
						        data-masterspeed='1000'
										data-thumb= {thumbUrl}
						        data-rotate='0'
						        data-saveperformance='off'
						        data-title={isla}
						        data-description=''>

								<img alt='' src={slideImg}  data-bgposition='center center' data-bgfit='cover' data-bgrepeat='no-repeat' data-bgparallax='10' className='rev-slidebg' data-no-retina />

								{show_video && videoLayer}
								{_layer1}
								{_layer2}
								{_layer3}

		        </li>
			)
		});







	 	return(
			<div role="banner">
						<div id="rev_slider_104_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="scroll-effect76"
					  styles={{margin: '0px auto', backgroundColor: 'transparent' , padding: '0px', marginTop: '0px', marginBottom: '0px'}}>
							<div>
							  <div id={idSlider} className='rev_slider_wrapper fullwidthbanner-container' style={displayNone} data-version='5.0.7'>
							    <ul>
							      {slides}
							    </ul>
							    <div className='tp-bannertimer tp-bottom' style={_style3}></div>
							  </div>
							</div>
						</div>
    </div>

	 	);




	 }
});

module.exports = SliderLanding;
