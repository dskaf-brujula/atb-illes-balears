var React = require('react');

var Servidor = React.createClass({

  render: function(){
    var nombre = this.props.nombre;
    var hoy = this.props.hoy;
		return(
      <div>
        <div>Hola, {nombre}</div>
        <div>Esta página se ha ejecutado en el servidor a las {hoy}</div>
      </div>
    );
  }
});

module.exports = Servidor;
