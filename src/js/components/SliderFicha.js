var React = require('react');
var ActionsCreator = require('../actions/appActions');
var SliderStore = require('../stores/SliderStore');
var dameTextoi18 = require('../webutils/utilidades.js').dameTextoi18;

var slides = [];

var videoPoster = "/assets/images/ico_play.jpg";

var SliderFicha = React.createClass({

	render: function(){
		try{

		var idioma = String(this.props.sIdioma);
		var sTerm = this.props.oSlides.sTerm;
		var sTitle = this.props.oSlides.sTitle;

		var _left = "['left','left','left','left']";
		var _bottom="['bottom','bottom','bottom','bottom']";
		var _center="['center','center','center','center']";
		var _str0="['0','0','0','0']";
		var _str1="['30','30','30','20']";
		var _str2="['80','80','80','50']";
		var _str3="['20','20','20','10']";
		var _str4="['50','50','40','30']";
		var _str5="['140','140','120','85']";
		var _str6="['30','20','20','20']";
		var _str7="['70','60','50','40']";
		var _str8="['400','400','400','550']";
		var _str9="['30','20','20','20']";
		var _str10="['600','600','30','20']";
		var _str11="['80','80','30','20']";
		var _str12="['790','790','226','216']";
		var _voffset1="['130','130','130','130']";
		var _voffset2="['200','200','180','169']";
		var _voffset3="['80','80','80','50']";
		var _hoffset1 = "['30','30','30','20']";
		var _fontsize1="['30','20','20','20']";
		var _fontsize2="['20','20','20','10']";
		var _lineheight1="['70','60','50','40']";
		var _lineheight2="['50','50','40','30']";


		console.log('[SliderFicha] sTerm ' + sTerm);
		console.log('[SliderFicha] sTitle ' + sTitle);


		var slides = this.props.oSlides.multimedia.map(function(slide, index){

			var dataIndex = 'rs-'+index;


			var mediaUrl = slide.url;
			var thumbUrl = slide.type=="video"? videoPoster : slide.url;
			var slideImg = slide.type=="video"? videoPoster : slide.url;


			console.log('[SliderFicha] slide num - ' + dataIndex+' index: '+index);
			console.log('[SliderFicha] slide.url ' + slide.url);
			console.log('[SliderFicha] slide.type ' + slide.type);
			console.log('[SliderFicha] slide.title ' + JSON.stringify(slide.title));


			console.log('[SliderFicha] mediaUrl: '+mediaUrl);
			console.log('[SliderFicha] thumbUrl: '+thumbUrl);
			console.log('[SliderFicha] slideImg: '+slideImg);


			try {

				var thumbTitle  = slide.title[idioma];
				// sTitle = (sTitle=='') ? this.props.oSlides.sTitle : sTitle;

			} catch(e){
				console.log('[SliderFicha] error ' + e.message);
				thumbTitle = '';
			}

			thumbTitle = (thumbTitle=='') ? sTitle : thumbTitle;

			console.log('[SliderFicha] thumbTitle ' + thumbTitle);


			if(slide.type == 'image'){


				var dataTransform ="";

				if (index==0){
			 			dataTransform = {
							dataTransform_idle: 'o:1;',
							dataTransform_in:'x:right(R);s:800;e:Power1.easeInOut;',
							dataTransform_out:'x:left(R);s:800;e:Power1.easeInOut;'
						}
				}


				return (
				<li key={index}
					data-index = {dataIndex}
					data-transition='zoomout'
					data-slotamount='default'
					data-easein='Power4.easeInOut'
					data-easeout='Power4.easeInOut'
					data-masterspeed='2000'
					data-thumb={thumbUrl}
					data-rotate='0'
					data-fstransition='fade'
					data-fsmasterspeed='1500'
					data-fsslotamount='7'
					data-saveperformance='off'
					data-title={thumbTitle}
					data-description=''>

					{(index==0) &&
					<img alt='' src={slideImg}
					data-bgposition='center center'
					data-bgfit='cover'
					data-bgrepeat='no-repeat'
					data-bgparallax='10'
					className='rev-slidebg'
					data-no-retina />
					}

					{(index>0) &&
					<img alt='' src={slideImg}
					data-bgposition="center center"
					data-kenburns="on"
					data-duration="30000"
					data-ease="Linear.easeNone"
					data-scalestart="100"
					data-scaleend="120"
					data-rotatestart="0"
					data-rotateend="0"
					data-offsetstart="0 0"
					data-offsetend="0 0"
					data-bgparallax="10"
					className='rev-slidebg'
					data-no-retina/>
					}



					<div
						className='tp-caption tp-resizeme'
						id={'slide-'+index+'-layer-1'}
						data-x={_left}
						data-y={_bottom}
						data-hoffset={_hoffset1}
						data-voffset={_voffset1}
						data-fontsize={_fontsize1}
						data-lineheight={_lineheight1}
						data-width="none"
						data-height="none"
						data-whitespace='nowrap'
						data-style_hover='cursor:default;'
						{...dataTransform}
						data-start='0'
						data-basealign='slide'
						data-responsive_offset='on'
						data-responsive='on'
						style={{ zIndex: 5, fontSize: '50px', lineHeight: '70px', fontWeight: [700], color: 'rgba(255, 255, 255, 1.00)', fontFamily: 'Roboto',backgroundColor: 'rgba(0, 0, 0, 1.0)' , borderColor: 'rgba(0, 0, 0, 0)' , padding:'0 30px 0 30px' }}>
						{sTitle}
					</div>

					<div
						className='tp-caption tp-resizeme'
						id={'slide-'+index+'-layer-2'}
						data-x={_left}
						data-y={_bottom}
						data-hoffset={_hoffset1}
						data-voffset={_voffset2}
						data-fontsize={_fontsize2}
						data-lineheight={_lineheight2}
						data-width='none'
						data-height='none'
						data-whitespace='nowrap'
						{...dataTransform}
						data-start='0'
						data-splitin='none'
						data-splitout='none'
						data-basealign='slide'
						data-responsive_offset='on'
						style={{zIndex: 6, whiteSpace: 'nowrap', fontSize: '30px', lineHeight: '50px', fontWeight: [300], color: 'rgba(255, 255, 255, 1.00)', fontFamily: 'Roboto', backgroundColor: 'rgba(0, 0, 0, 1.0)', padding:'0 30px 0 30px', visibility: 'inherit' }}>
						{sTerm}
					</div>
					</li>
				)


			}
			if(slide.type == 'image' && index > 20){

				return (

					<li key={index}
						data-index={dataIndex} data-transition='zoomout' data-slotamount='default'  data-easein='Power4.easeInOut' data-easeout='Power4.easeInOut' data-masterspeed='2000'
						data-thumb={thumbUrl}
						data-rotate='0'  data-saveperformance='off'  data-title='Mallorca' data-description='' data-bgfit='contain'>

						<img alt='' src={slideImg}  data-bgposition='center center' data-kenburns='on' data-duration='30000' data-ease='Linear.easeNone' data-scalestart='100' data-scaleend='120' data-rotatestart='0' data-rotateend='0' data-offsetstart='0 0' data-offsetend='0 0' data-bgparallax='10' className='xº' data-no-retina/>

						<div className='tp-caption   tp-resizeme'
						id={'slide-'+index+'-layer-1'}
						data-x={_left}
						data-hoffset={_str1}
						data-y={_bottom}
						data-voffset={_voffset1}
						data-fontsize={_str1}
						data-lineheight={_str7}
						data-width='none'
						data-height='none'
						data-whitespace='nowrap'
						data-start='0'
						data-splitin='none'
						data-splitout='none'
						data-basealign='slide'
						data-responsive_offset='on'
						style={{zIndex: 5,  fontSize: 50, lineHeight: 70, fontWeight: [700], color: 'rgba(255, 255, 255, 1.00)',fontFamily:'Roboto',backgroundColor:'rgba(0, 0, 0, 1.0)',padding:'0px 30px'}}>   {sTitle}   </div>

						<div className='tp-caption   tp-resizeme'
						id={'slide-'+index+'-layer-2'}
						data-x={_left}
						data-hoffset={_str1}
						data-y={_bottom}
						data-voffset={_voffset3}
						data-fontsize={_str3}
						data-lineheight={_str4}
						data-width='none'
						data-height='none'
						data-whitespace='nowrap'
						data-start='0'
						data-splitin='none'
						data-splitout='none'
						data-basealign='slide'
						data-responsive_offset='on'
						style={{zIndex: 6, whiteSpace: 'nowrap', fontSize: 30, lineHeight: 50, fontWeight: [300], color: 'rgba(255, 255, 255, 1.00)',fontFamily:'Roboto',backgroundColor:'rgba(0, 0, 0, 1.0)',padding:'0px 30px'}}>   {sTerm}  </div>
					</li>
				)
			}

			if(slide.type == 'video'){


				// BACKGROUND VIDEO LAYER
				var videoLayer = <div className='rs-background-video-layer'
					data-forcerewind='on'
					data-volume='mute'
					data-videowidth='100%'
					data-videoheight='100%'
					data-videomp4={mediaUrl}
					data-videopreload='preload'
					data-videostartat='00:04'
					data-videoloop='loop'
					data-forceCover='1'
					data-aspectratio='16:9'
					data-autoplay='true'
					data-autoplayonlyfirsttime='false'
					data-nextslideatend='true'>
				</div>

				return (

					<li key={index}
						data-index={dataIndex} data-transition='zoomout' data-slotamount='default'  data-easein='Power4.easeInOut' data-easeout='Power4.easeInOut' data-masterspeed='2000'
						data-thumb={thumbUrl}
						data-rotate='0'  data-saveperformance='off'  data-title='Mallorca' data-description='' data-bgfit='contain'>

						<img alt='' src={slideImg}  data-bgposition='center center' data-kenburns='on' data-duration='30000' data-ease='Linear.easeNone' data-scalestart='100' data-scaleend='120' data-rotatestart='0' data-rotateend='0' data-offsetstart='0 0' data-offsetend='0 0' data-bgparallax='10' className='xº' data-no-retina/>

						{videoLayer}


						<div className='tp-caption   tp-resizeme'
						id={'slide-'+index+'-layer-1'}
						data-x={_left}
						data-hoffset={_str1}
						data-y={_bottom}
						data-voffset={_voffset1}
						data-fontsize={_str1}
						data-lineheight={_str7}
						data-width='none'
						data-height='none'
						data-whitespace='nowrap'
						data-start='0'
						data-splitin='none'
						data-splitout='none'
						data-basealign='slide'
						data-responsive_offset='on'
						style={{zIndex: 5,  fontSize: 50, lineHeight: 70, fontWeight: [700], color: 'rgba(255, 255, 255, 1.00)',fontFamily:'Roboto',backgroundColor:'rgba(0, 0, 0, 1.0)',padding:'0px 30px'}}>   {sTitle}   </div>

						<div className='tp-caption   tp-resizeme'
						id={'slide-'+index+'-layer-2'}
						data-x={_left}
						data-hoffset={_str1}
						data-y={_bottom}
						data-voffset={_voffset3}
						data-fontsize={_str3}
						data-lineheight={_str4}
						data-width='none'
						data-height='none'
						data-whitespace='nowrap'
						data-start='0'
						data-splitin='none'
						data-splitout='none'
						data-basealign='slide'
						data-responsive_offset='on'
						style={{zIndex: 6, whiteSpace: 'nowrap', fontSize: 30, lineHeight: 50, fontWeight: [300], color: 'rgba(255, 255, 255, 1.00)',fontFamily:'Roboto',backgroundColor:'rgba(0, 0, 0, 1.0)',padding:'0px 30px'}}>   {sTerm}  </div>
					</li>
				)
			}

			// [SliderFicha] slide.url http://illes-balears-cms-media.s3.amazonaws.com/rrtt-mallorca-finca-publica-raixa-video10.mp4
			// [SliderFicha] slide.type video
			// [SliderFicha] sTerm Finca Pública
			// [SliderFicha] sTitle Raixa
			// [SliderFicha.js] (Error) _imagen is not defined




		});

	} catch(e){
		console.log('[SliderFicha.js] (Error) ' + e.message);
	}

	 	return(

		  <div role="banner" id='rev_slider_2_1' className='rev_slider fullwidthabanner' style={{display:'none'}} data-version='5.0.8'>
			  <ul>
					{slides}
			  </ul>
				<div className='tp-static-layers'></div>
				<div className='tp-bannertimer' style={{height: '7px',backgroundColor: 'rgba(255, 255, 255, 0.25)'}}></div>
			</div>
	 	);
	 }
});


module.exports = SliderFicha;
