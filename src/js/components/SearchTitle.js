var React = require('react');

var dameTextoi18 = require('../webutils/utilidades.js').dameTextoi18;
var estaticos = require('../../resources/estaticosContent.json');
var sectionsColors = require('../../resources/sectionsColors.json');

var  _getColor  = function(section){
	try{
			return sectionsColors[0][section]['color'];

	} catch(e){
		console.log("[SearchTitle.js] render() _getColor(section): "+section+" Error: "+e.message);
		return '';
	}
}


var SearchTitle = React.createClass({

	render: function(){

				console.log('[SearchTitle.js] render() version 1.28');


			  var idioma = String(this.props.oDatosSearchTitle.sIdioma);
				console.log('[SearchTitle.js] render() idioma: '+idioma);

				var isla = String(this.props.oDatosSearchTitle.sIsla);
				console.log('[SearchTitle.js] render() isla: '+isla);

				var title = String(this.props.oDatosSearchTitle.sTitle);
				console.log('[SearchTitle.js] render() title: '+title);

				var searchtitle = dameTextoi18(estaticos[0]['searchtitle'],idioma);
				console.log('[SearchTitle.js] render() searchtitle: '+searchtitle);

				var section = String(this.props.oDatosSearchTitle.section);
				console.log('[SearchTitle.js] render() section: '+section);

				var bgcolour;


				switch (section) {

					case "publicaciones":
							bgcolour = 'bg-color-petroleo';
							break;
					case "agenda":
							bgcolour = 'bg-color-grey';
							break;
					case "buscador":
							bgcolour = 'bg-color-grey';
							title = dameTextoi18(estaticos[0]['titlebuscador'],idioma);//@TODO: pasar a json con idioma.
							break;
					case "ocio-y-atracciones":
							bgcolour = 'bg-color-purple';
							break;
					case "shopping":
							bgcolour = 'bg-color-purple';
							break;
					case "mice":
							bgcolour = 'bg-color-purple';
							break;
					case "rutas-y-planes":
							bgcolour = 'bg-color-green';
							break;

					default:
							bgcolour = 'bg-color-green';
							break;

				}

				var bgcolourstyle = _getColor(section)+' !important';

					// <section className="container margin-top-40">

			// style={{backgroundColor: bgcolourstyle}}
			console.log("[SearchTitle.js] render() bgcolourstyle: "+bgcolourstyle);


				return(
					<div className={"shop-subscribe "+bgcolour+" margin-bottom-20"} style={{backgroundColor: bgcolourstyle}}>
						<div className="container">
							<h2 className="margin-bottom-20">{title}</h2>
						</div>

					</div>
			);
			// </section>





		}

});


module.exports = SearchTitle;
