var AppDispatcher = require('../dispatchers/appDispatcher');

var EventEmitter = require('events').EventEmitter;
var assign = require('object-assign');

var articulo = [];

var ArticuloStore = assign({}, EventEmitter.prototype, {
	emitLoad: function(){
		this.emit('load');
	},
	addLoadListener: function(callback){
		this.on('load',callback);
	},
	removeLoadListener: function(callback){
		this.removeListener('load',callback);
	},
	getAll: function(){
		return articulo;
	}
});

AppDispatcher.register(function(action){
	switch(action.actionType){
		case "UPDATE_ARTICULO":
			//console.warn('[ArticuloStore] ciclo vida 4. url' + action.articulo.data.multimedia[0].url);
			articulo = (action.articulo) ;
			ArticuloStore.emitLoad();
			break;
		default:
	}
});

module.exports = ArticuloStore;