var AppDispatcher = require('../dispatchers/appDispatcher');

var EventEmitter = require('events').EventEmitter;
var assign = require('object-assign');

var isla = [];

var IslaStore = assign({}, EventEmitter.prototype, {
	emitLoad: function(){
		this.emit('load');
	},
	addLoadListener: function(callback){
		this.on('load',callback);
	},
	removeLoadListener: function(callback){
		this.removeListener('load',callback);
	},
	getAll: function(){
		return isla;
	}
});

AppDispatcher.register(function(action){
	switch(action.actionType){
		case "UPDATE_ISLA":
			console.warn('[IslaStore] ciclo vida 4. url' + action.isla.data.multimedia[0].url);
			isla = (action.isla) ;
			IslaStore.emitLoad();
			break;
		default:
	}
});

module.exports = IslaStore;