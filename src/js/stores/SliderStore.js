var AppDispatcher = require('../dispatchers/appDispatcher');

var EventEmitter = require('events').EventEmitter;
var assign = require('object-assign');

var slides = [];

var SliderStore = assign({}, EventEmitter.prototype,{

	emitLoad: function(){
		this.emit('load');
	},
	addLoadListener: function(callback){
		this.on('load',callback);
	},
	removeLoadListener: function(callback){
		this.removeListener('load',callback);
	},
	getAll: function(){
		return slides;
	}
});

AppDispatcher.register(function(action){
	switch(action.actionType){
		case "UPDATE_SLIDER":
//			console.warn('[SliderStore] register url: ' + action.slides.data[0].multimedia.url);
			slides = action.slides;
			SliderStore.emitLoad();
			break;
		default:
	}
});

module.exports = SliderStore;