var AppDispatcher = require('../dispatchers/appDispatcher');

var EventEmitter = require('events').EventEmitter;
var assign = require('object-assign');

var searcher = [];

var SearchPaginationStore = assign({}, EventEmitter.prototype, {
	emitLoad: function(){
		this.emit('load');
	},
	addLoadListener: function(callback){
		this.on('load',callback);
	},
	removeLoadListener: function(callback){
		this.removeListener('load',callback);
	},
	getAll: function(){
		return searcher;
	}
});

AppDispatcher.register(function(action){
	switch(action.actionType){
		case "UPDATE_SEARCHPAGINATION":
			//console.warn('[SearchPaginationStore] ciclo vida 4' + action.searcher);
			searcher = (action.searcher) ;
			SearchPaginationStore.emitLoad();
			break;
		default:
	}
});

module.exports = SearchPaginationStore;
