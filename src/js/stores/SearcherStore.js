var AppDispatcher = require('../dispatchers/appDispatcher');

var EventEmitter = require('events').EventEmitter;
var assign = require('object-assign');

var searcher = [];

var SearcherStore = assign({}, EventEmitter.prototype, {
	emitLoad: function(){
		this.emit('load');
	},
	addLoadListener: function(callback){
		this.on('load',callback);
	},
	removeLoadListener: function(callback){
		this.removeListener('load',callback);
	},
	getAll: function(){
		return {isLoading: isLoading, searcher: searcher ,totalItems: totalItems, pageOfItems: pageOfItems, currentPage: currentPage, searchNotFound: searchNotFound};
	}
});

AppDispatcher.register(function(action){
	switch(action.actionType){
		case "UPDATE_SEARCHER":
		  console.log('[SearcherStore] UPDATE_SEARCHER action.totalItems: ' + action.totalItems+ ' action.isLoading: ' + action.isLoading);
			isLoading =  (action.isLoading);
			searcher =  (action.searcher);
			totalItems = (action.totalItems);
			pageOfItems = (action.pageOfItems);
			currentPage = (action.currentPage);
			searchNotFound = (action.searchNotFound);
			SearcherStore.emitLoad();
			break;
		default:
	}
});

module.exports = SearcherStore;
