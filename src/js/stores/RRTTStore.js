var AppDispatcher = require('../dispatchers/appDispatcher');

var EventEmitter = require('events').EventEmitter;
var assign = require('object-assign');

var recurso = [];

var RRTTStore = assign({}, EventEmitter.prototype, {
	emitLoad: function(){
		this.emit('load');
	},
	addLoadListener: function(callback){
		this.on('load',callback);
	},
	removeLoadListener: function(callback){
		this.removeListener('load',callback);
	},
	getAll: function(){
		return recurso;
	}
});

AppDispatcher.register(function(action){
	switch(action.actionType){
		case "UPDATE_RRTT":
			console.warn('[recursoStore] ciclo vida 4. url' + action.recurso.data.multimedia[0].url);
			recurso = (action.recurso) ;
			RRTTStore.emitLoad();
			break;
		default:
	}
});

module.exports = RRTTStore;