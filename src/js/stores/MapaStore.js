var AppDispatcher = require('../dispatchers/appDispatcher');

var EventEmitter = require('events').EventEmitter;
var assign = require('object-assign');

var itinerario = [];

var MapaStore = assign({}, EventEmitter.prototype, {
	emitLoad: function(){
		this.emit('load');
	},
	addLoadListener: function(callback){
		this.on('load',callback);
	},
	removeLoadListener: function(callback){
		this.removeListener('load',callback);
	},
	getAll: function(){
		return map;
	}
});

AppDispatcher.register(function(action){
	switch(action.actionType){
		case "UPDATE_MAPA":
			console.warn('[MapaStore] ciclo vida 4' + action.map);
			map = (action.map) ;
			MapaStore.emitLoad();
			break;
		default:
	}
});

module.exports = MapaStore;
