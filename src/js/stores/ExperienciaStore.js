var AppDispatcher = require('../dispatchers/appDispatcher');

var EventEmitter = require('events').EventEmitter;
var assign = require('object-assign');

var experiencia = [];

var ExperienciaStore = assign({}, EventEmitter.prototype, {
	emitLoad: function(){
		this.emit('load');
	},
	addLoadListener: function(callback){
		this.on('load',callback);
	},
	removeLoadListener: function(callback){
		this.removeListener('load',callback);
	},
	getAll: function(){
		return experiencia;
	}
});

AppDispatcher.register(function(action){
	switch(action.actionType){
		case "UPDATE_EXPERIENCIA":
			console.warn('[ExperienciaStore] ciclo vida 4. url' + action.experiencia.data.multimedia[0].url);
			experiencia = (action.experiencia) ;
			ExperienciaStore.emitLoad();
			break;
		default:
	}
});

module.exports = ExperienciaStore;