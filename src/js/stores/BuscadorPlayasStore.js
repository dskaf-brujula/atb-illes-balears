var AppDispatcher = require('../dispatchers/appDispatcher');

var EventEmitter = require('events').EventEmitter;
var assign = require('object-assign');

var playas = [];

var BuscadorPlayasStore = assign({}, EventEmitter.prototype,{

	emitLoad: function(){
		this.emit('load');
	},
	addLoadListener: function(callback){
		this.on('load',callback);
	},
	removeLoadListener: function(callback){
		this.removeListener('load',callback);
	},
	getAll: function(){
		return playas;
	}
});

AppDispatcher.register(function(action){
	switch(action.actionType){
		case "UPDATE_PLAYAS":

console.log('BuscadorPlayasStore update playas');

			playas = action.playas;

			BuscadorPlayasStore.emitLoad();
			break;
		default:
	}
});

module.exports = BuscadorPlayasStore;