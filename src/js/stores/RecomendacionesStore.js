var AppDispatcher = require('../dispatchers/appDispatcher');


// 
// if(process.env.NODE_ENV == "production")
// {
//   console.log("[Server.js] process.env.NODE_ENV: "+process.env.NODE_ENV+ " Deshabilitando logs...")
// 	console.log = function(){};
// }


var EventEmitter = require('events').EventEmitter;
var assign = require('object-assign');

var recomendaciones = [];

var RecomendacionesStore = assign({}, EventEmitter.prototype, {
	emitLoad: function(){
console.log('ciclo vida 5. Aviso a navegantes load');
		this.emit('load');
	},
	addLoadListener: function(callback){
console.log('addLoadListener');
		this.on('load',callback);
	},
	removeLoadListener: function(callback){
		this.removeListener('load',callback);
	},
	getAll: function(){

		console.log('[RecomendacionesStore.js] getAll...');
		return {recomendaciones: recomendaciones ,sDiccComponentes: sDiccComponentes};

	}
});

AppDispatcher.register(function(action){

console.log('[RecomendacionesStore.js] register...');

try{
	switch(action.actionType){
		case "UPDATE_RECOMENDACIONES":
			console.warn('[RecomendacionesStore] action.recomendaciones' + JSON.stringify(action.recomendaciones));
			recomendaciones = (action.recomendaciones) ;
			sDiccComponentes = (action.sDiccComponentes) ;
			RecomendacionesStore.emitLoad();
			break;
		default:
	}

	} catch(e){

	console.log('RecomendacionesStore UPDATE_RECOMENDACIONES ' + e);

	}
});

module.exports = RecomendacionesStore;
