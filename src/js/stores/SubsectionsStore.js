var AppDispatcher = require('../dispatchers/appDispatcher');

var EventEmitter = require('events').EventEmitter;
var assign = require('object-assign');

var subsections = [];

var SubsectionsStore = assign({}, EventEmitter.prototype, {
	emitLoad: function(){
		this.emit('load');
	},
	addLoadListener: function(callback){
		this.on('load',callback);
	},
	removeLoadListener: function(callback){
		this.removeListener('load',callback);
	},
	getAll: function(){
		return subsections;
	}
});

AppDispatcher.register(function(action){
	switch(action.actionType){
		case "UPDATE_SUBSECTIONS":
			//console.warn('[DestacadosStore] ciclo vida 4' + action.destacados);
			subsections = (action.subsections) ;
			SubsectionsStore.emitLoad();
			break;
		default:
	}
});

module.exports = SubsectionsStore;
