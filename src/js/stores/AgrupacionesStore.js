var AppDispatcher = require('../dispatchers/appDispatcher');

var EventEmitter = require('events').EventEmitter;
var assign = require('object-assign');

var agrupaciones = [];

var AgrupacionesStore = assign({}, EventEmitter.prototype, {
	emitLoad: function(){
		this.emit('load');
	},
	addLoadListener: function(callback){
		this.on('load',callback);
	},
	removeLoadListener: function(callback){
		this.removeListener('load',callback);
	},
	getAll: function(){
		return agrupaciones;
	}
});

AppDispatcher.register(function(action){
	switch(action.actionType){
		case "UPDATE_AGRUPACIONES":
			//console.warn('[DestacadosStore] ciclo vida 4' + action.destacados);
			agrupaciones = (action.agrupaciones) ;
			AgrupacionesStore.emitLoad();
			break;
		default:
	}
});

module.exports = AgrupacionesStore;
