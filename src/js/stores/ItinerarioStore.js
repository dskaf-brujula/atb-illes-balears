var AppDispatcher = require('../dispatchers/appDispatcher');

var EventEmitter = require('events').EventEmitter;
var assign = require('object-assign');

var itinerario = [];

var ItinerarioStore = assign({}, EventEmitter.prototype, {
	emitLoad: function(){
		this.emit('load');
	},
	addLoadListener: function(callback){
		this.on('load',callback);
	},
	removeLoadListener: function(callback){
		this.removeListener('load',callback);
	},
	getAll: function(){
		return itinerario;
	}
});

AppDispatcher.register(function(action){
	switch(action.actionType){
		case "UPDATE_ITINERARIO":
			itinerario = (action.itinerario) ;
			ItinerarioStore.emitLoad();
			break;
		default:
	}
});

module.exports = ItinerarioStore;
