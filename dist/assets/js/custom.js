/* Marquee Tiempo - TopNav */
	// $(document).ready(function() {
	// 		//mallorca
	// 		var mallorcaW = $("#mallorca-weather").flatWeatherPlugin({
	// 			location: "Palma, ES",
	// 			country: "Spain",
	// 			api: "yahoo",
	// 			view : "simple"
	// 		});
 // 	//menorca
	// 		var menorcaW = $("#menorca-weather").flatWeatherPlugin({
	// 			location: "Mahon, ES",
	// 			country: "Spain",
	// 			api: "yahoo",
	// 			view : "simple"
	// 		});
	//
	//
 // 	//ibiza
	// 		var ibizaW = $("#ibiza-weather").flatWeatherPlugin({
	// 			location: "Ibiza, ES",
	// 			country: "Spain",
	// 			api: "yahoo",
	// 			view : "simple"
	// 		});
	//
  // //formentera
	// 		var formenteraW = $("#formentera-weather").flatWeatherPlugin({
	// 			location: "La Savina, ES",
	// 			country: "Spain",
	// 			api: "yahoo",
	// 			view : "simple"
	// 		});
	// });

$("#tempoWeather > div:gt(0)").hide();

setInterval(function() {
  $('#tempoWeather > div:first')
    .fadeOut(1000)
    .next()
    .fadeIn(1000)
    .end()
    .appendTo('#tempoWeather');
},  3000);

/* Carrusel recomendaciones */
$(function() {
		var owl = $('.owl-carousel').owlCarousel({
		    loop	:true,
		    margin	:20,
			navText: ["<span class='bxtp-leftarrow bxtparrows'></span>","<span class='bxtp-rightarrow bxtparrows'></span>"],
				navContainerClass: 'owl-buttons',
		    nav		:true,
		    responsive:{
		        0:{
		            items:1
		        },
		        600:{
		            items:2
		        },
		        1000:{
		            items:4
		        }
		    }
		})

		/* animate filter */
		var owlAnimateFilter = function(even) {
			$(this)
			.addClass('__loading')
			.delay(70 * $(this).parent().index())
			.queue(function() {
				$(this).dequeue().removeClass('__loading')
			})
		}

		$('.btn-filter-wrap').on('click', '.btn-filter', function(e) {
			var filter_data = $(this).data('filter');

			/* return if current */
			if($(this).hasClass('btn-active')) return;

			/* active current */
			$(this).addClass('btn-active').siblings().removeClass('btn-active');

			/* Filter */
			owl.owlFilter(filter_data, function(_owl) {
				$(_owl).find('.item').each(owlAnimateFilter);
			});
		})
	})
