
var Datepicker = function () {

    return {

        //Datepickers
        initDatepicker: function () {
	        // Regular datepicker



          ( function( factory ) {
          	if ( typeof define === "function" && define.amd ) {

          		// AMD. Register as an anonymous module.
          		define( [ "../widgets/datepicker" ], factory );
          	} else {

          		// Browser globals
          		factory( jQuery.datepicker );
          	}
          }( function( datepicker ) {

          datepicker.regional.de = {
          	closeText: "Schließen",
          	prevText: "&#x3C;Zurück",
          	nextText: "Vor&#x3E;",
          	currentText: "Heute",
          	monthNames: [ "Januar","Februar","März","April","Mai","Juni",
          	"Juli","August","September","Oktober","November","Dezember" ],
          	monthNamesShort: [ "Jan","Feb","Mär","Apr","Mai","Jun",
          	"Jul","Aug","Sep","Okt","Nov","Dez" ],
          	dayNames: [ "Sonntag","Montag","Dienstag","Mittwoch","Donnerstag","Freitag","Samstag" ],
          	dayNamesShort: [ "So","Mo","Di","Mi","Do","Fr","Sa" ],
          	dayNamesMin: [ "So","Mo","Di","Mi","Do","Fr","Sa" ],
          	weekHeader: "KW",
          	dateFormat: "dd.mm.yy",
          	firstDay: 1,
          	isRTL: false,
          	showMonthAfterYear: false,
          	yearSuffix: "" };

          return datepicker.regional.de;

          } ) );
          ( function( factory ) {
          	if ( typeof define === "function" && define.amd ) {

          		// AMD. Register as an anonymous module.
          		define( [ "../widgets/datepicker" ], factory );
          	} else {

          		// Browser globals
          		factory( jQuery.datepicker );
          	}
          }( function( datepicker ) {

            datepicker.regional.es = {
              closeText: "Cerrar",
              prevText: "&#x3C;Ant",
              nextText: "Sig&#x3E;",
              currentText: "Hoy",
              monthNames: [ "enero","febrero","marzo","abril","mayo","junio",
              "julio","agosto","septiembre","octubre","noviembre","diciembre" ],
              monthNamesShort: [ "ene","feb","mar","abr","may","jun",
              "jul","ago","sep","oct","nov","dic" ],
              dayNames: [ "domingo","lunes","martes","miércoles","jueves","viernes","sábado" ],
              dayNamesShort: [ "dom","lun","mar","mié","jue","vie","sáb" ],
              dayNamesMin: [ "D","L","M","X","J","V","S" ],
              weekHeader: "Sm",
              dateFormat: "dd/mm/yy",
              firstDay: 1,
              isRTL: false,
              showMonthAfterYear: false,
              yearSuffix: "" };

          return datepicker.regional.de;

          } ) );
          ( function( factory ) {
          	if ( typeof define === "function" && define.amd ) {

          		// AMD. Register as an anonymous module.
          		define( [ "../widgets/datepicker" ], factory );
          	} else {

          		// Browser globals
          		factory( jQuery.datepicker );
          	}
          }( function( datepicker ) {

            datepicker.regional.ca = {
              closeText: "Tanca",
              prevText: "Anterior",
              nextText: "Següent",
              currentText: "Avui",
              monthNames: [ "gener","febrer","març","abril","maig","juny",
              "juliol","agost","setembre","octubre","novembre","desembre" ],
              monthNamesShort: [ "gen","feb","març","abr","maig","juny",
              "jul","ag","set","oct","nov","des" ],
              dayNames: [ "diumenge","dilluns","dimarts","dimecres","dijous","divendres","dissabte" ],
              dayNamesShort: [ "dg","dl","dt","dc","dj","dv","ds" ],
              dayNamesMin: [ "dg","dl","dt","dc","dj","dv","ds" ],
              weekHeader: "Set",
              dateFormat: "dd/mm/yy",
              firstDay: 1,
              isRTL: false,
              showMonthAfterYear: false,
              yearSuffix: "" };


          return datepicker.regional.de;

          } ) );





            var  lang = location.pathname.split('/')[1];

            $.datepicker.setDefaults($.datepicker.regional[lang]);

	        // Date range
	        $('#date-filter-from').datepicker({
	            dateFormat: 'dd.mm.yy',
	            prevText: '<i class="fa fa-angle-left"></i>',
	            nextText: '<i class="fa fa-angle-right"></i>',
	            onSelect: function( selectedDate )
	            {

                try{
                  //  $.datepicker.parseDate( 'dd.mm.yyyy', selectedDate );
                   $('#date-filter-to').datepicker('option', 'minDate', selectedDate);
                    var event = new Event('input', { bubbles: true });
                    this.dispatchEvent(event);
                 } catch(e){
                   console.log("[datepicker.js] parseDate invalid: "+e.message);
                 }

	            }
	        });


	        $('#date-filter-to').datepicker({
	            dateFormat: 'dd.mm.yy',
	            prevText: '<i class="fa fa-angle-left"></i>',
	            nextText: '<i class="fa fa-angle-right"></i>',
	            onSelect: function( selectedDate )
	            {

                try{
                  //  $.datepicker.parseDate( 'dd.mm.yyyy', selectedDate );
                   $('#date-filter-from').datepicker('option', 'maxDate', selectedDate);
                    var event = new Event('input', { bubbles: true });
                    this.dispatchEvent(event);
                 } catch(e){
                   console.log("[datepicker.js] parseDate invalid: "+e.message);
                 }
              }
	        });

        }

    };
}();
