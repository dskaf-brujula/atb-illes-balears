var fs = require('fs'),
    http = require('http'),
    url = require('url'),
    log4js = require('log4js');


var compression = require('compression');


// Enrutador de las peticiones http.
var express = require('express')
var accepts = require('accepts');
var app = express();



var ReactDOMServer = require('react-dom/server');
var React = require('react');


// Make sure to include the JSX transpiler
require('node-jsx').install();

var _ = require('lodash');


var dameTextoi18 = require('./src/js/webutils/utilidades.js').dameTextoi18;

//Funcion auxiliar. Recupera el contenido de un componente invocando al servicio REST y deserializa el resultado.
var cargarComponente = require('./src/js/ServerHelper.js').cargarComponente;
// Obtiene los datos de los componentes que forman la plantila landig(tematica), para renderizarlo en servidor.
var componentesLanding = require('./src/js/ServerHelper.js').componentesLanding;
var componentesFicha = require('./src/js/ServerHelper.js').componentesFicha;
var obtenerComponenteFinal = require('./src/js/ServerHelper.js').obtenerComponenteFinal;
var esIsla = require('./src/js/ServerHelper.js').esIsla;
var esBuscador = require('./src/js/ServerHelper.js').esBuscador;
var esSearch = require('./src/js/ServerHelper.js').esSearch;
var esSearchCompanies = require('./src/js/ServerHelper.js').esSearchCompanies;
var esREST= require('./src/js/ServerHelper.js').esREST;
var esDiccionario= require('./src/js/ServerHelper.js').esDiccionario;
var restURL= require('./src/js/ServerHelper.js').restURL;
var apiSearch = require('./src/js/ServerHelper.js').apiSearch;
var apiREST = require('./src/js/ServerHelper.js').apiREST;
var apiREST_POST = require('./src/js/ServerHelper.js').apiREST_POST;
var apiMCHIMP_POST = require('./src/js/ServerHelper.js').apiMCHIMP_POST;

var esNewsletter = require('./src/js/ServerHelper.js').esNewsletter;
var esNewsletterMailChimp = require('./src/js/ServerHelper.js').esNewsletterMailChimp;







var esBaleares = require('./src/js/ServerHelper.js').esBaleares;
var recuperarDiccionario = require('./src/js/ServerHelper.js').recuperarDiccionario;


/**********************************************************/
/***********    Fichero de propiedades    ****************/
/**********************************************************/
var PropertiesReader = require('properties-reader');
var properties = PropertiesReader('./src/resources/config.properties');
var _host = properties.get('Host');
var _port = properties.get('Port');
var proxyHost = properties.get('proxyHost');
var proxyPort = properties.get('proxyPort');
var proxyUser = properties.get('proxyUser');
var proxyPass = properties.get('proxyPass');
var urlComponenTypes = properties.get('componentTypes');
var urlComponenTypesTrans = properties.get('componentTypesTrans');
var urlIdTerms = properties.get('ids_terms');

var mchimpAPIKey = properties.get('mchimpAPIKey');
var mchimpRESTmembers = properties.get('mchimpRESTmembers');

/***************************************************************/
/***********    Proxy-Authorization       **********************/
/***************************************************************/
var auth = 'Basic '+new Buffer(proxyUser+':'+proxyPass).toString('base64');
var options = {
  host: proxyHost,
  port: proxyPort,
  path: "http://"+_host+":"+_port+"/dictionary_of_webpages/",
  headers: {
    Host: _host,
    "Proxy-Authorization" : auth
  }
};
var oHost = {
  host: _host,
  port: _port
};



var dicc_paginas,dicc_jsonComponentesTrans,dicc_idTerms;

recuperarDiccionario(options, null, function(dicc){
  dicc_paginas = dicc;
  // console.log("[Server.js] dicc_paginas:\n"+JSON.stringify(dicc_paginas));
});

// Diccionario de datos: tipos de componentes traducidos
recuperarDiccionario(options, urlComponenTypesTrans, function(componentes_trans){
  dicc_jsonComponentesTrans = componentes_trans;
});
// Diccionario de datos: lista de términos.
recuperarDiccionario(options,urlIdTerms,function(terms){
  dicc_idTerms = terms;
});

//
// // Logger.
// //log4js.configure('./src/resources/log4js_configuracion.json', {reloadSecs: 900});
//
// //var logger = log4js.getLogger('server-logger');
//
// /**********************************************************/
// /***********    Fichero de propiedades    ****************/
// /**********************************************************/
// var PropertiesReader = require('properties-reader');
// var properties = PropertiesReader('./src/resources/config.properties');
// var _host = properties.get('Host');
// var _port = properties.get('Port');
// var proxyHost = properties.get('proxyHost');
// var proxyPort = properties.get('proxyPort');
// var proxyUser = properties.get('proxyUser');
// var proxyPass = properties.get('proxyPass');
// var urlComponenTypes = properties.get('componentTypes');
// var urlComponenTypesTrans = properties.get('componentTypesTrans');
// var urlIdTerms = properties.get('ids_terms');
// /***************************************************************/
// /***********    Proxy-Authorization       **********************/
// /***************************************************************/
// var auth = 'Basic '+new Buffer(proxyUser+':'+proxyPass).toString('base64');
// var options = {
//   host: proxyHost,
//   port: proxyPort,
//   path: "http://"+_host+":"+_port+"/dictionary_of_webpages/",
//   headers: {
//     Host: _host,
//     "Proxy-Authorization" : auth
//   }
// };
// var oHost = {
//   host: _host,
//   port: _port
// };

/********************************************************************/
/******** Definion de directorios de recursos estaticos ************/
/********************************************************************/
// Mapeo del directorio de estaticos a la url virtual '/'
try{
  app.use(compression()); // Gzip compression
  app.use('/js', express.static(__dirname + '/dist/js'));
  app.use('/assets', express.static(__dirname + '/dist/assets'));
  app.use('/a6sg92alniggkzxwfg7s8od3fxtjm2.html', express.static(__dirname + '/dist/assets/facebook/a6sg92alniggkzxwfg7s8od3fxtjm2.html'));

} catch(e){
  console.log("Recursos estaticos - " + e.message);
}

/********************************************************************/

var async = require('async');


process.env.NODE_ENV == "test";

require("log-suppress").init(console, 'test');


// if(process.env.NODE_ENV == "production")
// {
//   console.log("[Server.js] process.env.NODE_ENV: "+process.env.NODE_ENV+ " Deshabilitando logs...")
// 	console.log = function(){};
// }


// Interceptor de las requests, actua de router, redirigiendo según la solicitud (landing, componente...)

// app.get('/', function(urlRequest)uest, response) {
// }

app.get('*', function (req, res) {

  // var filePath = '.' + urlRequest;
  // if (filePath == './')
  //   filePath = './index.html';



  // req.acceptsLanguages( 'es', 'uk', 'ru-mo', 'be' );
  // Accept-Language:en,es;q=0.8,en-US;q=0.6

  getPage(req,res,
    function(plantilla){
        console.log("[Server.js] app.get recuperarPlantilla()"+JSON.stringify(res));
    });

});

app.listen(4000,function(err){
  if (err) throw err;
  console.log('Escuchando en el puerto 4000...')
  console.log('working directory...' + process.cwd());
  console.log('dirname...' + __dirname);
  //logger.debug('Escuchando en el puerto 8080...');
});


/**
* Se analiza la request y en base al formato se decide el tipo de plantilla que se debe renderizar.
*/
function getPage(req,res,callback){

  // Plantilla HTML que incluye el contenido del/los componentes React obtenidos a partir del servicio REST.
  var rt = null;
  var encontrado = false;
  var urlRequest = req.url;
  console.log("[Server.js] ------------------------------------------------------------------------------------");
  console.log("[Server.js] -------------------------------   S T A R T ----------------------------------------");
  console.log("[Server.js] ------------------------------------------------------------------------------------");

  console.log("[Server.js] getPage() urlRequest: "+urlRequest);


  var pathUrl = url.parse(urlRequest, true).pathname;


  console.log("[Server.js] getPage() pathUrl: "+pathUrl);

  if (pathUrl=="/"){

      console.log("[Server.js] getPage() : "+accepts(req).languages());
      console.log("[Server.js] getPage() req.headers: "+JSON.stringify(req.headers));
      var ua_langs = _.split(accepts(req).languages(),',');

      console.log("[Server.js] getPage()  ua_langs[0]: "+(ua_langs[0]).substr(0,2));

      switch((ua_langs[0]).substr(0,2)){
        case 'es':
          urlRequest = '/es/baleares/';
        break;

        case 'ca':
          urlRequest = '/ca/baleares/';
        break;

        case 'en':
          urlRequest = '/en/baleares/';
        break;

        case 'de':
          urlRequest = '/de/baleares/';
        break;

        case 'fr':
          urlRequest = '/fr/baleares/';
        break;

        default:
          urlRequest = '/en/baleares/';
        break;
      }

      console.log("[Server.js] getPage() redirect to: "+urlRequest);

      res.redirect (urlRequest);

    }


  // Peticion Search
  else if(esSearch(pathUrl)){
    //logger.debug(urlRequest + ' ES ISLA');

    console.log("[Server.js] getPage() if esSearch()= true..."+urlRequest);
    encontrado = true;

    apiSearch(urlRequest,res,function(rt){
      // console.log("[Server.js] apiSearch() result..."+JSON.stringify(rt));
      res.send(rt);
      callback(rt);

    });

  }
  // Peticion Search Companies
  else if(esSearchCompanies(pathUrl)){
    console.log("[Server.js] getPage() if esSearchCompanies()= true..."+urlRequest);
    encontrado = true;
    apiSearch(urlRequest,res,function(rt){
      // console.log("[Server.js] apiSearch() result..."+JSON.stringify(rt));
      res.send(rt);
      callback(rt);
    });
  }
  // Peticion API REST
  else if(esREST(pathUrl)){

    console.log("[Server.js] getPage() if esREST()= true..."+urlRequest);
    encontrado = true;

    apiREST(urlRequest,res,function(rt){
      console.log("[Server.js] apiREST() result..."+JSON.stringify(rt));
      res.send(rt);
      callback(rt);

    });

  }

  // Peticion API REST
  else if(esNewsletter(pathUrl)){

    console.log("[Server.js] getPage() if esNewsletter()= true..."+urlRequest);
    encontrado = true;

    var name = "";
    var email = req.query.email; // $_GET["id"]
    var lang = req.query.lang; // $_GET["id"]

    var country = "ES";
    var age = "";
    var interests = "";

    var JSONPost ={
      "user":{"name" : name,"email" : email,"language" : lang}
    };
    //
    // { "user":{"name" : name,"email" : email,"country" : country,"language" : idioma,
    //     "age" : age,
    //     "interests" : [interests]
    //   }
    // };



    console.log("[Server.js] esNewsletter() JSONPost..."+JSON.stringify(JSONPost));

    apiREST_POST(urlRequest,JSONPost,res,function(rt){
      console.log("[Server.js] apiREST_POST() result..."+JSON.stringify(rt));
      res.send(rt);
      callback(rt);

    });

  }
  else if(esNewsletterMailChimp(pathUrl)){

    console.log("[Server.js] getPage() if esNewsletterMailChimp()= true..."+urlRequest);
    encontrado = true;

    var name = "";
    var email = req.query.email; // $_GET["id"]
    var list = req.query.list; // $_GET["id"]

    var urlSubscribe = properties.get('mchimpRESTmembers').replace(':list',list);
    var suscriptionStatus = properties.get('mchimpRESTmembersStatus');


    var JSONPost =
      {
          "email_address": email,
          "status": suscriptionStatus
      };


    console.log("[Server.js] esNewsletterMailChimp() JSONPost..."+JSON.stringify(JSONPost));


    apiMCHIMP_POST(urlSubscribe,JSONPost,res,function(rt){
      console.log("[Server.js] apiREST_POST() result..."+JSON.stringify(rt));
      res.send(rt);
      callback(rt);

    });
  }


  // Mailchimp API KEY: cff28574217cfb32da8052dd10887619-us17
  // illesbalears
  // @Illesbalears2017


  // Peticion API Diccionario
  else if(esDiccionario(pathUrl)){

    console.log("[Server.js] getPage() if esDiccionario()= true..."+urlRequest);
    encontrado = true;


    // var urlComponenTypes = properties.get('componentTypes');
    // var urlIdTerms = properties.get('ids_terms');

    var urlRequest = properties.get('componentTypesTrans');

    apiREST(urlRequest,res,function(rt){
      console.log("[Server.js] apiREST() result..."+JSON.stringify(rt));
      res.send(rt);
      callback(rt);

    });
  }

  // Peticion Pagina Ficha (Componente Final)
  else if( !encontrado && esComponente(pathUrl,dicc_jsonComponentesTrans)){

      console.log("[Server.js] getPage() esComponente (Ficha)...");
      encontrado = true;
      var oTipo = getTemplate(urlRequest);
      console.log('[Server.js] getPage() Ficha Tipo:  ' +oTipo.tipo);
      obtenerFicha(urlRequest,res, oTipo, function(htmlFinal){
        rt = htmlFinal;
        res.send(rt);
        callback(rt);
      });
  }

  else if (!encontrado) {

    console.log("[Server.js] getPage() Url no categorizada buscamos en diccionario de landing...");

    var oComponentes = obtenerNodoUrlFriendly(dicc_paginas,pathUrl);


    if (oComponentes!=null){

          console.log("[Server.js] getPage() es Landing con componentes: "+JSON.stringify(oComponentes));


          //@TODO Mejorar sistema templates que vaya en el diccionario con PXX.

          //Página de landing
          encontrado = true;

          var template = ''

          if (oComponentes.rest_requests.subsections_module!=undefined){
             template = './src/templates/landing-secciones.html';
          }
          else
          if (oComponentes.rest_requests.searcher_module!=undefined){
            template = './src/templates/landing-buscador.html';
          }
          else
          if (oComponentes.rest_requests.searcher_companies!=undefined){
            template = './src/templates/landing-buscador.html';
          }
          else
          if (oComponentes.section=="agenda"){
            template = './src/templates/landing-buscador.html';
          }

          else if (esBaleares(urlRequest) ){
            template = './index.html';
          }
          else{
            template = './src/templates/landing-secciones.html';

          }

          console.log('[Server.js] obtenerLanding(). Template asignada : ' + template);

          var htmlIni = fs.readFileSync(template,'utf8');

          obtenerLanding(pathUrl,res,htmlIni,function(htmlRT){
            //logger.debug("RECUPERAR PLANTILLA, obtener landing " + urlRequest);
            rt = htmlRT;
            callback(rt);
          });


    }
    else {
      console.log("[Server.js] getPage() 404 PAGE");


      var ua_langs = _.split(accepts(req).languages(),',');

      console.log("[Server.js] getPage()  ua_langs[0]: "+(ua_langs[0]).substr(0,2));

      switch((ua_langs[0]).substr(0,2)){
        case 'es':
          urlRequest = '/articulo/es/illesbalears/404-error';
        break;

        case 'ca':
          urlRequest = '/article/ca/illesbalears/404-error';
        break;

        case 'en':
          urlRequest = '/article/en/illesbalears/404-error';
        break;

        case 'de':
          urlRequest = '/artikel/de/illesbalears/404-error';
        break;

        default:
          urlRequest = '/article/en/illesbalears/404-error';
        break;
      }


      req.url=urlRequest;

      var oTipo = getTemplate(urlRequest);
      obtenerFicha(urlRequest,res, oTipo, function(htmlFinal){
        rt = htmlFinal;
        // res.status(404).send("not found");
        res.status(404).send(rt);
        // callback(rt);
      });


    }



  }

}

/**
 * Tipo de ComponenteFinal
 */
function getTemplate(urlRequest){
      var patron = urlRequest.substring(1).split('/')[0];
      var idioma = urlRequest.substring(1).split('/')[1];
      var tipoComponente;
      var oTemplate = [];
      for(componente in dicc_jsonComponentesTrans){
        if(dicc_jsonComponentesTrans[componente][idioma] == patron){
          tipoComponente = componente;
          break;
        }
      }
      oTemplate.tipo = tipoComponente;
      // Se recupera el código HTML de la plantilla tipo (modelo de plantilla).
      switch(tipoComponente){
        case "art":
          htmlTemplate = fs.readFileSync('./src/templates/articulo.html','utf8');
          break;
        case "rrtt":
          htmlTemplate = fs.readFileSync('./src/templates/rrtt.html','utf8');
          break;
        case "exp":
          htmlTemplate = fs.readFileSync('./src/templates/experiencia.html','utf8');
          break;
        case "pla":
          htmlTemplate = fs.readFileSync('./src/templates/plan.html','utf8');
          break;
        case "iti":
          htmlTemplate = fs.readFileSync('./src/templates/itinerario.html','utf8');
          break;

      }
      oTemplate.htmlTemplate = htmlTemplate;
      return oTemplate;
}
  /**
  * Recupera la plantilla de las landing, e incluye las llamadas a los componentes.
  */
  function obtenerLanding(urlRequest,res, htmlIni, callback){
    try {
      let _url = urlRequest;

      console.log('[Server.js] obtenerLanding(). URL solicitada: ' + _url);



      var oComponentes = obtenerNodoUrlFriendly(dicc_paginas,_url);


      console.log("[Server.js] obtenerLanding(). Componentes: "+JSON.stringify(oComponentes));



      var _destacado = oComponentes.rest_requests.featured_module;
      var _recomendado = oComponentes.rest_requests.recommended_module;
      var _landing_slider = oComponentes.rest_requests.landing_sliders_module;
      var _agenda = oComponentes.rest_requests.agenda_module;
      var _subsections= oComponentes.rest_requests.subsections_module;
      var _searcher= oComponentes.rest_requests.searcher_module;
      var _searcher_companies = oComponentes.rest_requests.searcher_companies;
      var _searcher_agenda  = oComponentes.rest_requests.agenda_module;

      var _section = oComponentes.section;
      var _breadcrumb = oComponentes.breadcrumb;

      // Objeto que contiene las urls de los componentes, obtenidas de los diccionarios de datos.
      var oUrls = [];
      oUrls.destacado = _destacado;
      // oUrls.recomendado = _recomendado;
      oUrls.recomendado = (_recomendado!=undefined)? _recomendado.replace('illesbalears','--all--'): _recomendado;
      oUrls.slider = _landing_slider;
      oUrls.subsections = _subsections;
      oUrls.searcher = _searcher;
      oUrls.searcher_companies =_searcher_companies;
      oUrls.section = _section;
      oUrls.breadcrumb = _breadcrumb;
      if (_section=="agenda"){
        console.log("[Server.js] _section: "+_section);
        oUrls.searcher_agenda = _searcher_agenda;
        _agenda = null;
      }else{
        oUrls.agenda = _agenda;
      }



//      console.log('[Server.js] obtenerLanding(), recomendado url: ' + oUrls.recomendado);
      var _stComponents = "";

        // Se prepara el rencomponentes que forman la landig del componente en servidor y en navegador.
      componentesLanding(options,urlRequest,htmlIni,oUrls, function(componentes){

        // Cabecera
        var oPath = urlRequest.substring(1).split('/');
        var idioma = oPath[0];
        var isla = oPath[1];
        var MenuCabecera = require('./src/js/components/MenuCabecera');
        var cabecera = React.createFactory(MenuCabecera);
        var sUrl = 'http://'+_host+':'+_port+'/';

        // var strCabecera =  ReactDOMServer.renderToStaticMarkup(cabecera({sIdioma: idioma, sIsla: isla, sHost: sUrl}));
        // //servidor
        // htmlIni = htmlIni.replace('headerServerComponent',strCabecera);

        // HTML Lang
        htmlIni = htmlIni.replace(/langServerComponent/g,idioma);

        // HTML Title
        htmlIni = htmlIni.replace('seotitleServerComponent',componentes.seotitle);


        // HTML META Description
        // htmlIni = htmlIni.replace('seodescriptionServerComponent',componentes.seodescription);


        // HTML Header
        htmlIni = htmlIni.replace('headerServerComponent',componentes.menuheader);


        // HTML breadCrumbs
        htmlIni = htmlIni.replace('breadcrumbsServerComponent',componentes.breadcrumbs);


        // HTML
        htmlIni = htmlIni.replace('titleServerComponent',componentes.landingtitle);


        // Sustituye el valor de la semilla en la libreria principal de React.
        if(_destacado!=undefined){
          _stComponents += "var oDestacados = '"+restURL(_destacado)+"';\n";

          // server side
          if(typeof componentes.destacado == "undefined"){
            htmlIni = htmlIni.replace('destacadoServerComponent','');
          } else {
           htmlIni = htmlIni.replace('destacadoServerComponent',componentes.destacado);
          }
        }else{
          htmlIni = htmlIni.replace('destacadoServerComponent','');
        }




        /** SUBSECTIONS */
        if(_subsections!=undefined){
          _stComponents += "var oSubsections = '"+restURL(_subsections)+"';\n";

          // console.log('[Server.js] obtenerLanding() componentes.subsection: \n' +  componentes.subsections);
          if(typeof componentes.subsections == "undefined"){
            htmlIni = htmlIni.replace('subsectionsServerComponent','');
          } else {
            htmlIni = htmlIni.replace('subsectionsServerComponent',componentes.subsections);
            // _stComponents += "var oLandingSlider = '"+_landig_slider+"';\n";
          }
        }else{
          htmlIni = htmlIni.replace('subsectionsServerComponent','');
        }


        /** AGENDA **/
        if(_agenda!=undefined){
          _stComponents += "var oAgenda = '"+restURL(_agenda)+"';\n";

          if(typeof componentes.agenda == "undefined"){
           htmlIni = htmlIni.replace('agendaServerComponent','');
          } else {
           htmlIni = htmlIni.replace('agendaServerComponent',componentes.agenda);
            //  _stComponents += "var oAgenda = '"+_agenda+"';\n";
          }
        }else{
          htmlIni = htmlIni.replace('agendaServerComponent','');
        }




        /** RECOMENDADOS */
        if(_recomendado!=undefined){
          _stComponents += "var oRecomendados = '"+restURL(_recomendado)+"';\n";

          if(typeof componentes.recomendado == "undefined"){
            htmlIni = htmlIni.replace('recomendadoServerComponent','');
          } else {
            htmlIni = htmlIni.replace('recomendadoServerComponent',componentes.recomendado);
            // _stComponents += "var oRecomendados = '"+oRecomendados+"';\n";
          }
        }else{
          htmlIni = htmlIni.replace('recomendadoServerComponent','');
        }



        /** SLIDER */
        if(_landing_slider!=undefined){
          _stComponents += "var oLandingSlider = '"+restURL(_landing_slider)+"';\n";

          if(typeof componentes.slider == "undefined"){
            htmlIni = htmlIni.replace('sliderServerComponent','');
          } else {
            htmlIni = htmlIni.replace('sliderServerComponent',componentes.slider);
            // _stComponents += "var oLandingSlider = '"+_landig_slider+"';\n";
          }
        }else{
          htmlIni = htmlIni.replace('sliderServerComponent','');
        }


        console.log('[Server.js] obtenerLanding(), componentes.searchtitle: ' + componentes.searchtitle);


        /** SEARCH TITLE */
        if(_searcher!=undefined||_searcher_companies!=undefined||_searcher_agenda!=undefined ){
          if(typeof componentes.searchtitle != "undefined"){
            htmlIni = htmlIni.replace('searchTitleServerRender',componentes.searchtitle);
          }
        }


        console.log('[Server.js] obtenerLanding(), _searcher: ' + _searcher);

        /** SEARCHER  CLIENT RENDERING */
        if(_searcher!=undefined){
          _stComponents += "var oSearcher = '"+restURL(_searcher)+"';\n";
        }

        console.log('[Server.js] obtenerLanding(), _searcher_companies: ' + _searcher_companies);

        /** SEARCHER  CLIENT RENDERING */
        if(_searcher_companies!=undefined){
          _stComponents += "var oSearcherCompanies = '"+restURL(_searcher_companies)+"';\n";
        }

        console.log('[Server.js] obtenerLanding(), _searcher_agenda: ' + _searcher_agenda);

        /** SEARCHER  CLIENT RENDERING */
        if(_searcher_agenda!=undefined){
          _stComponents += "var oSearcherAgenda = '"+restURL(_searcher_agenda)+"';\n";
        }


        /** SEARCHER  CLIENT RENDERING */
        if(_section!=undefined){
          _stComponents += "var oSection = '"+_section+"';\n";
        }



        console.log('[Server.js] obtenerLanding() _stComponents: \n' +  _stComponents);

        htmlIni = htmlIni.replace('var serverSheed;',_stComponents);

        var Footer = require('./src/js/components/Footer');
        var footer = React.createFactory(Footer);
        var strFooter =  ReactDOMServer.renderToStaticMarkup(footer({sIdioma: idioma, sIsla: isla}));
        //servidor
        htmlIni = htmlIni.replace('footerServerComponent',strFooter);

        // console.log('[Server.js] obtenerLanding() res.send htmlIni: \n');

       res.send(htmlIni);

       callback(htmlIni);
      });

      callback(htmlIni);
    } catch (e) {
      //logger.debug("ERR componentesLanding: " + e.message);
    }
}



  /**
  * Recupera la plantilla de las fichas.
  */
  function obtenerFicha(urlRequest,res, oTipo, callback){
    try {

      // console.log('[Server.js] obtenerFicha()  obtener ficha options ' + options);
      console.log('[Server.js] obtenerFicha()  obtener ficha req ' + urlRequest);

      var htmlIni = oTipo.htmlTemplate;

      // URLs de los componentes generales de la página
      var oPath = urlRequest.substring(1).split('/');
      var idioma = oPath[1];
      var idFriendly = oPath[3];
      var restRequest = "http://"+oHost.host+":"+oHost.port+"/components/lang/"+idioma+"/id_friendly/"+idFriendly;
      var islands = _.split(oPath[2], '-');



      // Objeto que contiene las urls de los componentes, obtenidas de los diccionarios de datos.
      var oUrls = [];
      oUrls.ficha = restRequest;
      var islandrecomendado = islands.length>1?'--all--':islands[0].replace('illesbalears','--all--');

      oUrls.recomendado = "http://"+oHost.host+":"+oHost.port+"/components/recommended/island/" +islandrecomendado ;
      // oUrls.recomendado = oUrls.recomendado.replace('illesbalears','--all--');


      console.log('[Server.js] obtenerFicha()  oUrls.ficha: ' + oUrls.ficha);
      console.log('[Server.js] obtenerFicha()  oUrls.recomendado: ' + oUrls.recomendado);
      // console.log('[Server.js] obtenerFicha()  oTipo.htmlTemplate: ' + oTipo.htmlTemplate);




      // Imagenes para el slider.
      var oSliderImages = [];


      // Se prepara el rencomponentes que forman la landig del componente en servidor y en navegador.
      componentesFicha(options,urlRequest,oTipo,oUrls, function(componentes){

        var _stComponents = "";
        var htmlIni = oTipo.htmlTemplate;

        // Cabecera
        var oPath = urlRequest.substring(1).split('/');
        var tipoFicha = oPath[0]; // [rrtt, art, exp, pla]
        var idioma = oPath[1];
        var isla = oPath[2];


        // var MenuCabecera = require('./src/js/components/MenuCabecera');
        // var cabecera = React.createFactory(MenuCabecera);
        var sUrl = 'http://'+_host+':'+_port+'/';

        console.log('[Server.js] obtenerFicha () componentes, tipoFicha ' + tipoFicha);
        console.log('[Server.js] obtenerFicha () componentes, oTipo.tipo ' + oTipo.tipo);
        console.log('[Server.js] obtenerFicha () componentes, idioma ' + idioma);
        console.log('[Server.js] obtenerFicha () componentes, isla ' + isla);

        console.log('[Server.js] obtenerFicha () componentes, seotitle ' + componentes.seotitle);

        // var strCabecera =  ReactDOMServer.renderToStaticMarkup(cabecera({sIdioma: idioma, sIsla: isla, sHost: sUrl}));

        // HTML Lang
        htmlIni = htmlIni.replace(/langServerComponent/g,idioma);

        // HTML Title
        htmlIni = htmlIni.replace('seotitleServerComponent',componentes.seotitle);


        // HTML META Description
        htmlIni = htmlIni.replace('seodescriptionServerComponent',componentes.seodescription);



        // HTML Header
        htmlIni = htmlIni.replace('headerServerComponent',componentes.menuheader);

        // HTML breadCrumbs
        htmlIni = htmlIni.replace('breadcrumbsServerComponent',componentes.breadcrumbs);

        // HTML Tags
        htmlIni = htmlIni.replace('tagsbuttonsServerComponent',componentes.tagsbuttons);

        // TODO: sacar a un switch statement
        if(oTipo.tipo == 'rrtt'){
          // Contenido de la ficha
          htmlIni = htmlIni.replace('rrttServerComponent',componentes.ficha);
          htmlIni = htmlIni.replace('sliderServerComponent',componentes.slider);
        }
        if(oTipo.tipo == 'art'){
          htmlIni = htmlIni.replace('articuloServerComponent',componentes.ficha);
          htmlIni = htmlIni.replace('sliderServerComponent',componentes.slider);
        }
        if(oTipo.tipo == 'exp'){
          htmlIni = htmlIni.replace('experienciaServerComponent',componentes.ficha);
          htmlIni = htmlIni.replace('sliderServerComponent',componentes.slider);
        }
        if(oTipo.tipo == 'pla'){
          htmlIni = htmlIni.replace('planServerComponent',componentes.ficha);
          htmlIni = htmlIni.replace('sliderServerComponent',componentes.slider);
        }
        if(oTipo.tipo == 'iti'){
         htmlIni = htmlIni.replace('itinerarioServerComponent',componentes.ficha);
         console.log('[Server.js] componentes.mapa:'+JSON.stringify(componentes.mapa));
        }

        /** RECOMENDADOS */
        htmlIni = htmlIni.replace('recomendadoServerComponent',componentes.recomendado);

        var Footer = require('./src/js/components/Footer');
        var footer = React.createFactory(Footer);
        var strFooter =  ReactDOMServer.renderToStaticMarkup(footer({sIdioma: idioma, sIsla: isla}));
        //servidor
        htmlIni = htmlIni.replace('footerServerComponent',strFooter);


        // CLIENT RENDERING


        // if(_destacado!=undefined){
        // _stComponents += "var oDestacados = '"+_destacado+"';\n";
        // }
        // if(_subsections!=undefined){
        // _stComponents += "var oSubsections = '"+_subsections+"';\n";
        // }
        // if(_agenda!=undefined){
        // _stComponents += "var oAgenda = '"+_agenda+"';\n";
        // }
        // if(_landing_slider!=undefined){
        // _stComponents += "var oLandingSlider = '"+_landing_slider+"';\n";
        // }


        if(componentes.recomendado!=undefined){
          _stComponents += "var oRecomendados = '"+restURL(oUrls.recomendado)+"';\n";
        }

        if(componentes.mapa!=undefined){
          _stComponents += "var oDatosMap = "+JSON.stringify(componentes.mapa.itinerario)+";\n";
          _stComponents += "var oDatosMapTitle = "+JSON.stringify(componentes.mapa.sTitle)+";\n";
          _stComponents += "var oDatosMapIsla= '"+isla+"';\n";
          _stComponents += "var oDatosMapLatLon = "+JSON.stringify(componentes.mapa.latlon)+";\n";
          _stComponents += "var oDatosMapkml = "+JSON.stringify(componentes.mapa.kml)+";\n";
        }


        if(componentes.meteo!=undefined){
           var urlMeteo = "http://54.77.91.226:5000/components/meteoclim/latitude/"+componentes.meteo.lat+"/longitude/"+componentes.meteo.lon+"/touristic_subjects/"+componentes.meteo.touristic_subject;
          _stComponents += "var oDatosMeteoUrl = '"+restURL(urlMeteo)+"';\n";
        }




        console.log('[Server.js] obtenerLanding() _stComponents: \n' +  _stComponents);
        htmlIni = htmlIni.replace('var serverSheed;',_stComponents);

        // callback(htmlIni);

        // res.send(htmlIni);
        callback(htmlIni);

      });



    } catch (e) {
      console.log("[Server.js] ERR obtenerFicha: " + e.message);
    }
}


  // function MenuCabecera(urlRequest, htmlIni, callback){
  //
  //   var oPath = urlRequest.substring(1).split('/');
  //   var idioma = oPath[0];
  //   var isla = oPath[1];
  //   var sUrl = 'http://'+_host+':'+_port+'/';
  //
  //   // Slider
  //   // var SliderBuscador = require('./src/js/components/SliderBuscador');
  //   // var sliderBuscador = React.createFactory(SliderBuscador);
  //   var sliderUrl = sUrl + 'components/landing-slider/island/' + isla + '/touristic_subject/playas';
  //
  //   // Buscador playas
  //   var BuscadorPlayas = require('./src/js/components/BuscadorPlayas');
  //   var buscadorPlayas = React.createFactory(BuscadorPlayas);
  //   // Por defecto, no se filtra se piden todas las playas.
  //   var buscadorPlayasUrl = sUrl + 'components/searcher/component_type/--all--/island/--all--/area/--all--/municipality/--all--/tipology/playas/term/--all--/title/--all--';
  //
  //
  //   cargarComponente(buscadorPlayasUrl,function(comp){
  //     // Cabecera
  //     var MenuCabecera = require('./src/js/components/MenuCabecera');
  //     var cabecera = React.createFactory(MenuCabecera);
  //     var strCabecera =  ReactDOMServer.renderToStaticMarkup(cabecera({sIdioma: idioma, sIsla: isla, sHost: sUrl}));
  //     htmlIni = htmlIni.replace('headerServerComponent',strCabecera);
  //
  //     var playas = '';
  //     playas = ReactDOMServer.renderToStaticMarkup(buscadorPlayas({componente: comp, sIdioma: idioma}));
  //     htmlIni = htmlIni.replace('buscadorServerComponent',playas);
  //
  //     // Footer
  //     var Footer = require('./src/js/components/Footer');
  //     var footer = React.createFactory(Footer);
  //     var strFooter =  ReactDOMServer.renderToStaticMarkup(footer({sIdioma: idioma, sIsla: isla}));
  //     htmlIni = htmlIni.replace('footerServerComponent',strFooter);
  //
  //     var _stComponents = "var oSlider = '"+sliderUrl+"';\n";
  //     _stComponents += "var oBuscadorPlayas = ";
  //     _stComponents += "'"+buscadorPlayasUrl+"'";
  //
  //     htmlIni = htmlIni.replace('var serverSheed;',_stComponents);
  //
  //     callback(htmlIni);
  //   });
  //
  // }



// Comprueba si la petición interceptada es de tipo 'Componente'
//  req request interceptada.
//  dicc_jsonComponentesTrans diccionario de datos de traducción de componentes
var esComponente = function (urlRequest,dicc_jsonComponentesTrans){
  var patron = urlRequest.substring(1).split('/')[0];

  patron = patron.toLowerCase();

  var idioma = urlRequest.substring(1).split('/')[1];
  for(item in dicc_jsonComponentesTrans){
    if(dicc_jsonComponentesTrans[item][idioma] == patron){
//logger.debug("Es componente " + patron)
      return true;
    }
  }
  return false;
};


/**
* Obtiene el nodo que contiene las urls de los componentes que construyen la plantilla
* @param item. JSON del diccionario de datos.
* @param mask url de la que queremos obtener los componentes. Hay que tener en cuenta que la url puede venir con una barra al final, O EN MAYUSCULAS
* @return nodo con los componentes ecesarios para montar la plantilla.
*/
var obtenerNodoUrlFriendly = function(json,mask){
  var oNodo = null;
  var _mask = mask.toLowerCase();

  for(var i=0;i<json.length;i++){

    // Delete all / to compare
    if((json[i].url_friendly).replace(/\//g, '') === _mask.replace(/\//g, '')){
      // console.log('[Server.js] obtenerNodoUrlFriendly() json[i].url_friendly  ' + json[i].url_friendly);
      // console.log('[Server.js] obtenerNodoUrlFriendly() _mask ' + _mask);
      oNodo = json[i];
      break;
    }
  }

  return oNodo;
};
