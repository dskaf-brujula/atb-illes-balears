var gulp = require('gulp');
var dirstream = require('mkdirp-stream');
var browserify = require('browserify');

var reactify = require('reactify');
var source = require('vinyl-source-stream');

var zip = require('gulp-zip');
var dateFns = require('date-fns');

var gulp_remove_logging = require("gulp-remove-logging");


// Crea la estructura de directorios del proyecto.
gulp.task('esqueleto',	function(){
	dirstream(['./src/js/actions',
				'./src/js/components',
				'./src/js/dispatchers',
				'./src/js/stores',
				'./src/assets/css',
				'./src/assets/images']);
});

// Genera el bundle para la ejecución.
gulp.task('browserify',
	function(){
		try{
				browserify('./src/js/main.js')
				.transform('reactify')
				.bundle()
				.pipe(source('main.js'))
				.pipe(gulp.dest('dist/js'));

				browserify('./src/js/compIsla.js')
				.transform('reactify')
				.bundle()
				.pipe(source('compIsla.js'))
				.pipe(gulp.dest('dist/js'));

        browserify('./src/js/compLanding.js')
        .transform('reactify')
        .bundle()
        .pipe(source('compLanding.js'))
        .pipe(gulp.dest('dist/js'));

				browserify('./src/js/compArticulo.js')
				.transform('reactify')
				.bundle()
				.pipe(source('compArticulo.js'))
				.pipe(gulp.dest('dist/js'));

				browserify('./src/js/webutils/utilidades.js')
				.transform('reactify')
				.bundle()
				.pipe(source('main.js'))
				.pipe(gulp.dest('dist/js'));

				browserify('./src/js/compExperiencia.js')
				.transform('reactify')
				.bundle()
				.pipe(source('compExperiencia.js'))
				.pipe(gulp.dest('dist/js'));

				browserify('./src/js/compItinerario.js')
				.transform('reactify')
				.bundle()
				.pipe(source('compItinerario.js'))
				.pipe(gulp.dest('dist/js'));


				browserify('./src/js/compRRTT.js')
				.transform('reactify')
				.bundle()
				.pipe(source('compRRTT.js'))
				.pipe(gulp.dest('dist/js'));



		} catch (e){
			console.error("ERROR GENERANDO PAQUETE DE DISTRIBUCION.");
			console.error(e.message);
		}
	}
);


// npm run cleanlogs
gulp.task('cleanlogs', function() {


   gulp.src("./src/js/components/*.js")
    .pipe(
      gulp_remove_logging({})
    )
		.pipe(gulp.dest('./src/js/components/js'));

		gulp.src("./src/js/*.js")
		 .pipe(
			 gulp_remove_logging({})
		 )
		 .pipe(gulp.dest('./src/js'));



});

// Copia los ficheros fuentes al directorio de distribucion.
// Para ejecutarlo (A MANO): npm run copy
gulp.task('copy', function(){

	gulp.src('src/*.html')
	.pipe(gulp.dest('dist'));

	gulp.src('src/assets/**')
	.pipe(gulp.dest('dist/assets'));

	gulp.src('src/templates/**')
	.pipe(gulp.dest('dist/assets/templates'));

});

// Esta tarea crea la estructura de directorios del proyecto.
//gulp.task('esqueleto',['esqueleto']);
gulp.task('default',['browserify'], function(){
	return gulp.watch('src/**/*.*', {interval: 4000}, ['browserify'])
});
//gulp.task('default',['browserify']);


// Esta tarea hace una copia de seguridad del proyecto.
var paths = {
	fuentes: ['./src/**/*.*','./index.html','./server.js','./gulpfile.js']
}

gulp.task('respaldo', function(){
	var hoy = dateFns.format(new Date(),'DDMMYYYY-HHmm');
	return gulp.src(paths.fuentes)
					.pipe(zip('ATB_componentes-'+hoy+'.zip'))
					.pipe(gulp.dest('../backups/'));
});


/*gulp.task('serve', ['sass'], () => {
	browserSync.init({
			server: './src'
	});
	gulp.watch([
			'webapp/js/*.js',
			'webapp/js/actions/*.js',
			'webapp/js/api/*.js',
			'webapp/js/components/*.js',
			'webapp/js/dispatchers/*.js',
			'webapp/js/lib/*.js',
			'webapp/js/stores/*.js',
			'webapp/js/webutils/*.js'
	], ['sass']);
	gulp.watch('webapp/*.html').on('change', browserSync.reload);
	gulp.watch('webapp/app/layout/*.html').on('change', browserSync.reload);
});

gulp.task('default', ['serve'])*/