# Repositorio Web IllesBalears.travel.

Tecnología: basado en REACT FrontEnd + JSON REST sobre MongoDB de BackEnd.

Portal de Turismo ATB.


Instalación:

	· npm install gulp react react-addons react-dom reactify flux node-jsx browserify

	· npm install fs httpdispatcher gulp-zip express mkdir mkdirp-stream  date-fns  properties-reader read-file vinyl-source-stream log4js


Ejecución:

	· En entorno productivo
			npm start &

	· En entorno de desarrollo
			npm run copy
			npm install (en un terminal)
			nodemon ./server.js localhost 8080 ((o npm start), en otro terminal)

			Para poder ejecutar nodemon, ejecutar previamente npm install -g nodemon
